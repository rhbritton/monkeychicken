module.exports.monkey_chicken = {
	'GrCh': { 
		deck_type: 'monkey_chicken',
		id: 1,
		order: 10,
		name: 'Greedy Chicken',
		count: 4,
		player: { draw: 2 },
		img: 'monkey_chicken/greedy_chicken.png',
		tutorial: "The Greedy Chicken draws an extra card at the end of your turn."
	}, 
	'PoBa': { 
		deck_type: 'monkey_chicken',
		id: 2,
		order: 20,
		name: 'Poisonous Baboon',
		count: 4,
		opponent: { poison: 3 },
		img: 'monkey_chicken/poisonous_baboon.png', 
		tutorial: "The Poisonous Baboon gives your opponent 3 poison turns. Each time your opponent plays a card to end their turn they will take 5 damage."
	},
	'HeHe': { 
		deck_type: 'monkey_chicken',
		id: 3,
		order: 30,
		name: 'Helpful Hen',
		count: 4,
		player: { health: 15 },
		img: 'monkey_chicken/helpful_hen.png'
	},
	'KaMo': { 
		deck_type: 'monkey_chicken',
		id: 4,
		order: 40,
		name: 'Kamikaze Monkey',
		count: 1,
		player: { health: -50 },
		opponent: { health: -50 },
		img: 'monkey_chicken/kamikaze_monkey.png'
	}, 
	'InCh': { 
		deck_type: 'monkey_chicken',
		id: 5,
		order: 50,
		name: 'Interrupting Chicken',
		count: 2,
		bonus: true,
		player: { 
			unpoison: true, 
			undamage: true 
		},
		img: 'monkey_chicken/interrupting_chicken.png'
	},
	'MC3': {
		deck_type: 'monkey_chicken',
		id: 6,
		order: 11,
		name: '3 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -3 },
		img: 'monkey_chicken/3.png'
	},
	'MC4': {
		deck_type: 'monkey_chicken',
		id: 7,
		order: 12,
		name: '4 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -4 },
		img: 'monkey_chicken/4.png'
	},
	'MC5': {
		deck_type: 'monkey_chicken',
		id: 8,
		order: 13,
		name: '5 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -5 },
		img: 'monkey_chicken/5.png'
	},
	'MC6': {
		deck_type: 'monkey_chicken',
		id: 9,
		order: 14,
		name: '6 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -6 },
		img: 'monkey_chicken/6.png'
	},
	'MC7': {
		deck_type: 'monkey_chicken',
		id: 10,
		order: 15,
		name: '7 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -7 },
		img: 'monkey_chicken/7.png'
	},
	'MC8': {
		deck_type: 'monkey_chicken',
		id: 11,
		order: 16,
		name: '8 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -8 },
		img: 'monkey_chicken/8.png'
	},
	'MC9': {
		deck_type: 'monkey_chicken',
		id: 12,
		order: 17,
		name: '9 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -9 },
		img: 'monkey_chicken/9.png'
	},
	'MC10': {
		deck_type: 'monkey_chicken',
		id: 13,
		order: 18,
		name: '10 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -10 },
		img: 'monkey_chicken/10.png'
	},
	'MoCh': {
		deck_type: 'monkey_chicken',
		id: 100,
		order: 100,
		name: 'The Legendary MonkeyChicken',
		count: 1,
		unrestrictable: true,
		opponent: { health: -40 },
		chance: 1000,
		img: 'monkey_chicken/10.png'
	},
	'Back': {
		deck_type: 'monkey_chicken',
		id: 41,
		count: 0,
		name: 'Card Back',
		img: 'monkey_chicken/back.png'
	}
}

module.exports.camel_toad = {
	'CaCr': { 
		deck_type: 'camel_toad',
		id: 14,
		order: 10,
		name: 'Camel Crook',
		count: 4,
		opponent: { steal_cards: 1 },
		img: 'camel_toad/camel_crook.png'
	},
	'TrTo': { 
		deck_type: 'camel_toad',
		id: 15,
		order: 20,
		name: 'Treacherous Toad',
		count: 4,
		stackable: true,
		player: { health: -7 },
		opponent: { health: -15 },
		img: 'camel_toad/treacherous_toad.png'
	},
	'FrFr': { 
		deck_type: 'camel_toad',
		id: 16,
		order: 30,
		name: 'Friendly Frog',
		count: 4,
		player: {
			health: 5,
			energize: 3
		},
		img: 'camel_toad/friendly_frog.png'
	}, 
	'FraFr': {
		deck_type: 'camel_toad',
		id: 17,
		order: 40,
		name: 'Fracking Frog',
		count: 1,
		player: {
			poison: 3
		},
		opponent: {
			poison: 5,
			health: -5
		},
		img: 'camel_toad/fracking_frog.png'
	},
	'CoCa': { 
		deck_type: 'camel_toad',
		id: 18,
		order: 50,
		name: 'Copy Camel',
		count: 0,
		opponent: { copy: { turns_ago: 1 } },
		img: 'camel_toad/copy_camel.png'
	},
	'CT3': {
		deck_type: 'camel_toad',
		id: 6,
		order: 11,
		name: '3 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -3 },
		img: 'camel_toad/3.png'
	},
	'CT4': {
		deck_type: 'camel_toad',
		id: 7,
		order: 12,
		name: '4 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -4 },
		img: 'camel_toad/4.png'
	},
	'CT5': {
		deck_type: 'camel_toad',
		id: 8,
		order: 13,
		name: '5 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -5 },
		img: 'camel_toad/5.png'
	},
	'CT6': {
		deck_type: 'camel_toad',
		id: 9,
		order: 14,
		name: '6 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -6 },
		img: 'camel_toad/6.png'
	},
	'CT7': {
		deck_type: 'camel_toad',
		id: 10,
		order: 15,
		name: '7 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -7 },
		img: 'camel_toad/7.png'
	},
	'CT8': {
		deck_type: 'camel_toad',
		id: 11,
		order: 16,
		name: '8 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -8 },
		img: 'camel_toad/8.png'
	},
	'CT9': {
		deck_type: 'camel_toad',
		id: 12,
		order: 17,
		name: '9 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -9 },
		img: 'camel_toad/9.png'
	},
	'CT10': {
		deck_type: 'camel_toad',
		id: 13,
		order: 18,
		name: '10 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -10 },
		img: 'camel_toad/10.png'
	},
	'CaTo': {
		deck_type: 'camel_toad',
		id: 100,
		order: 100,
		name: 'The Legendary CamelToad',
		count: 1,
		unrestrictable: true,
		opponent: { health: -40 },
		chance: 1000000,
		img: 'monkey_chicken/10.png'
	},
	'Back': {
		id: 42,
		count: 0,
		name: 'Card Back',
		img: 'camel_toad/back.png'
	}
}

module.exports.snail_whale = {
	'SneSn': {
		deck_type: 'snail_whale',
		id: 27,
		order: 10,
		name: 'Sneaky Snail',
		count: 4,
		player: { draw: 1 },
		opponent: { poison: 1 },
		img: 'snail_whale/sneaky_snail.png'
	}, 
	'WhoWo': { 
		deck_type: 'snail_whale',
		id: 28,
		order: 20,
		name: 'Whale of Wonder',
		count: 4,
		player: { 
			health: -10,
			energize: 3
		},
		opponent: { poison: 3 },
		img: 'snail_whale/whale_of_wonder.png'
	},
	'WaWh': { 
		deck_type: 'snail_whale',
		id: 29,
		order: 30,
		name: 'Wayward Whale',
		count: 4,
		player: { discard: 2 },
		opponent: { health: -25 },
		img: 'snail_whale/wayward_whale.png'
	},
	'SnSnr': {
		deck_type: 'snail_whale',
		id: 30,
		order: 40,
		name: 'Snail Snare',
		count: 1,
		opponent: { restrict: 2 },
		img: 'snail_whale/snail_snare.png'
	},
	'FlSn': { 
		deck_type: 'snail_whale',
		id: 31,
		order: 50,
		name: 'Flailing Snail',
		count: 2,
		bonus: true,
		opponent: { health: -10 },
		img: 'snail_whale/flailing_snail.png'
	},
	'SW3': {
		deck_type: 'snail_whale',
		id: 6,
		order: 11,
		name: '3 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -3 },
		img: 'snail_whale/3.png'
	},
	'SW4': {
		deck_type: 'snail_whale',
		id: 7,
		order: 12,
		name: '4 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -4 },
		img: 'snail_whale/4.png'
	},
	'SW5': {
		deck_type: 'snail_whale',
		id: 8,
		order: 13,
		name: '5 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -5 },
		img: 'snail_whale/5.png'
	},
	'SW6': {
		deck_type: 'snail_whale',
		id: 9,
		order: 14,
		name: '6 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -6 },
		img: 'snail_whale/6.png'
	},
	'SW7': {
		deck_type: 'snail_whale',
		id: 10,
		order: 15,
		name: '7 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -7 },
		img: 'snail_whale/7.png'
	},
	'SW8': {
		deck_type: 'snail_whale',
		id: 11,
		order: 16,
		name: '8 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -8 },
		img: 'snail_whale/8.png'
	},
	'SW9': {
		deck_type: 'snail_whale',
		id: 12,
		order: 17,
		name: '9 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -9 },
		img: 'snail_whale/9.png'
	},
	'SW10': {
		deck_type: 'snail_whale',
		id: 13,
		order: 18,
		name: '10 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -10 },
		img: 'snail_whale/10.png'
	},
	'CaTo': {
		deck_type: 'snail_whale',
		id: 300,
		order: 100,
		name: 'The Legendary SnailWhale',
		count: 1,
		unrestrictable: true,
		opponent: { health: -40 },
		chance: 1000000,
		img: 'monkey_chicken/10.png'
	},
	'Back': {
		deck_type: 'snail_whale',
		id: 42,
		count: 0,
		name: 'Card Back',
		img: 'snail_whale/back.png'
	}
}