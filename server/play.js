var OneOnOne = require('one-on-one')
  , Game = OneOnOne.Game
  , User = OneOnOne.User
  , Decks = require('./static/decks')

Game.namespaces.game = [ '_id', 'turn', 'result', 'last_move' ]
Game.namespaces.player = [ '_id', 'health', 'poison', 'energize', 'restrict', 'hand', 'coins', 'wins', 'deck_type', 'unreadMessages' ]
Game.namespaces.opponent = [ '_id', 'health', 'poison', 'energize', 'restrict', 'hand.length', 'coins', 'wins', 'deck_type', 'unreadMessages' ]
User.namespaces.shared = [ '_id', 'username', 'coins', 'wins', 'decks', 'default_deck', 'tutorial' ]

function defaultPlayerData(player) {
	player.health = 100
	player.poison = 0
	player.energize = 0
	player.restrict = 0
	player.hand = []
	player.deck = []
	player.deck_type = ''
}

User.on('register', function(user, event, done) {
	user.decks = ['monkey_chicken']
	user.default_deck = 'monkey_chicken'
	user.coins = 10
	user.wins = 0

	done()
})

User.on('win', function(user, event, done) {
	if(user.coins) user.coins += 3
	else user.coins = 3

	if(user.wins) user.wins += 1
	else user.wins = 1

	done()
})

User.on('lose', function(user, event, done) {
	if(user.coins) user.coins += 1
	else user.coins = 1

	if(user.losses) user.losses += 1
	else user.losses = 1

	done()
})

User.on('tie', function(user, event, done) {
	if(user.coins) user.coins += 2
	else user.coins = 2

	if(user.ties) user.ties += 1
	else user.ties = 1

	done()
})

Game.on('start', function(game, event, done) {
	defaultPlayerData(game.player)
	defaultPlayerData(game.opponent)

	done()
})

Game.on('select-deck', function(game, event, done) {
	User.find(game.player, function(err, user) {
		if(err)
			return done(err)

		if(!user.decks)
			user.decks = ['monkey_chicken']

		if(!Decks[event.data.deck] || (Decks[event.data.deck] && user.decks.indexOf(event.data.deck.toString()) < 0))
			return done(new Game.Error('You Do Not Own That Deck'))

		if(!game.player.deck_type) {
			game.player.deck_type = event.data.deck.toString()
			if(!game.turn) game.turn = user._id
			drawCards(shuffleDeck(buildDeck(game.player.deck, Decks[game.player.deck_type], game.player.deck_type)), 7, game.player.hand)
			if(game.last_move && game.last_move.timestamp) drawCards(game.player.deck, 1, game.player.hand)
		}

		done()
	})
})

Game.on('pass', function(game, event, done) {
	if(game.turn == game.opponent._id.toString())
		return done(new Game.Error('Not Your Turn'))

	storeCardPlayed(game, {})
	Game.emit(game, game.player, 'change-turn')

	done()
})

Game.on('play-card', function(game, event, done) {
	var card = getCard(game, event)
	  , old_state = Game.getPreviousState(game, 'change-turn', 1) || Game.getPreviousState(game, 'start', 0)

	if(game.result) 
		return done(new Game.Error('Game Is Over'))

	if(!game.turn) 
		return done(new Game.Error('Not Your Turn'))
		
	User.find(game.player, function(err, user) {
		if(err)
			return done(err)
		User.find(game.opponent, function(err, opponent) {
			if(err)
				return done(err)

			if(game.turn.toString() == opponent._id.toString()) 
				return done(new Game.Error('Not Your Turn'))	
			
			if(!isLegalMove(game, card, old_state)) 
				return done(new Game.Error('Illegal Move'))
			
			removeCardsFromHand(game.player, card)
			playCards(game, card, old_state)
			storeCardPlayed(game, card)
			completeTurn(game, user, card)
			done()
		})
	})
})

Game.on('poison', function(game, event, done) {
	if(game.player.poison > 0) {
		game.player.health -= 5
		game.player.poison--
		done()
	} else {
		done(new Game.Error('Not Poisoned'))
	}
})

Game.on('energize', function(game, event, done) {
	if(game.player.energize > 0) {
		game.player.health += 5
		game.player.energize--
		done()
	} else {
		done(new Game.Error('Not Energized'))
	}
})

Game.on('restrict', function(game, event, done) {
	if(game.player.restrict > 0) {
		game.player.restrict--
		done()
	} else {
		done(new Game.Error('Not Restricted'))
	}
})

Game.on('change-turn', function(game, event, done) {
	User.find(game.player, function(err, user) {
		if(err)
			return done(err)
		User.find(game.opponent, function(err, opponent) {
			if(err)
				return done(err)
			if(game.turn == opponent._id) 
				return done(new Game.Error('Not Your Turn'))
			
			drawCards(game.opponent.deck, 1, game.opponent.hand)
			game.turn = opponent._id

			if(winConditions(game, user, opponent)) done()
			else done()
		})
	})
})

function getCard(game, event) {
	var card_key = event.data.card.card_key
      , card = copyObject(Decks[event.data.card.deck][card_key])

    card.key = card_key
    card.num = getNumberOfCards(game, card)
    return card
}

function completeTurn(game, player, card) {
	if(!card.bonus) {
		if(game.player.energize) Game.emit(game, player, 'energize')
		if(game.player.poison) Game.emit(game, player, 'poison')
		if(game.player.restrict) Game.emit(game, player, 'restrict')
		Game.emit(game, player, 'change-turn')
	}
}

function buildDeck(deck, stored_deck, deck_type) {
	Object.keys(stored_deck).forEach(function(card_key) {
		for (var i=0; i<stored_deck[card_key].count; i++) {
			if(stored_deck[card_key].stackable) var stackable = true
			else var stackable = false
			deck.push({
				  deck_type: deck_type
				, card_key: card_key
				, img: stored_deck[card_key].img
				, order: stored_deck[card_key].order
				, stackable: stackable
				, chance: stored_deck[card_key].chance
			})
		}
	})

	return deck
}

function shuffleDeck(deck) {
	for(var j, x, i = deck.length; i; j = Math.floor(Math.random() * i), x = deck[--i], deck[i] = deck[j], deck[j] = x);
    return deck
}

function stealCards(card_num, opp_hand, hand) {
	for(var i=0; i<card_num; i++) {
		if(opp_hand.length > 0) {
			var rand_index = Math.floor(Math.random()*opp_hand.length)
			hand.push(opp_hand[rand_index])
			opp_hand.splice(rand_index, 1)
		}
	}

	hand = organizeHand(hand)

	return hand
}

function discardCards(card_num, hand) {
	for(var i=0; i<card_num; i++) {
		var rand_index = Math.floor(Math.random()*hand.length)
		hand.splice(rand_index, 1)
	}

	return hand
}

function drawCards(deck, card_num, hand) {
	var cards = deck.splice(0, card_num)
	for(var i=0; i<cards.length; i++) {
		if(cards[i].chance) {
			i = checkChance(deck, card_num, hand, cards, i)
		}
	}
	hand.push.apply(hand, cards)
	hand = organizeHand(hand)

	return hand
}

function checkChance(deck, card_num, hand, cards, i) {
	var rand_chance = Math.floor(Math.random()*cards[i].chance+1)
	if(cards[i].chance != rand_chance) {
		cards.splice(i, 1)
		drawCards(deck, 1, cards)

		return i-1
	}

	return i
}

function organizeHand(cards) {
	function compare(a,b) {
		if (Decks[a.deck_type][a.card_key].order < Decks[b.deck_type][b.card_key].order)
			return -1;
		if (Decks[a.deck_type][a.card_key].order > Decks[b.deck_type][b.card_key].order)
			return 1;
		return 0;
	}

	cards.sort(compare)

	return cards
}

function copyObject(source, destination) {
    if (source === null || typeof source !== 'object') return source;
    var destination = destination || {}
    for (var key in source) {
    	destination[key] = source[key]
    }

    return destination
}

function cardExists(game, card) {
	for (var i=0; i<game.player.hand.length; i++) {
		if(game.player.hand[i].card_key == card.key) return true
	}
	return false
}

function playCards(game, card, old_state) {
	if(card.player) modifyPlayer(game, card.player, game.player, game.opponent, card.num, 'player', old_state)
	if(card.opponent) modifyPlayer(game, card.opponent, game.opponent, game.player, card.num, 'opponent', old_state)
}

function modifyPlayer(game, card, player, opponent, num_cards, which_player, old_state) {
	if(card.undamage && player.health < old_state.player.health) player.health = old_state.player.health
	if(card.health) player.health += card.health*num_cards
	if(card.poison) player.poison += card.poison*num_cards
	if(card.energize) player.energize += card.energize*num_cards
	if(card.restrict) player.restrict += card.restrict*num_cards
	if(card.unpoison) player.poison = 0
	if(card.unenergize) player.energize = 0
	if(card.unrestrict) player.restrict = 0
	if(card.draw) drawCards(player.deck, card.draw*num_cards, player.hand)
	if(card.steal_cards) stealCards(card.steal_cards*num_cards, player.hand, opponent.hand)
	if(card.discard) discardCards(card.discard*num_cards, player.hand)
}

function getNumberOfCards(game, card) {
	if(!card.stackable) return 1

	var num = 0
	game.player.hand.forEach(function(hand_card) {
		hand_card = Decks[hand_card.deck_type][hand_card.card_key]
		if(hand_card.id == card.id) return num++
		if(num > 0) return false
	})
	return num
}

function removeCardsFromHand(player, card) {
	player.hand.every(function(hand_card, i) {
		hand_card = Decks[hand_card.deck_type][hand_card.card_key]
		if(hand_card.id == card.id) {
			player.hand.splice(i, card.num)
			return false
		}
		return true
	})
	return player.hand
}

function storeCardPlayed(game, card) {
	if(!game.last_move) {
		game.last_move = {}
	} else {
		if(game.last_move.card || card.bonus) game.last_move = {}
	}

	if(card.bonus) game.last_move.bonus = card
	else game.last_move.card = card

	game.last_move.timestamp = new Date()
	game.last_move.initiator = game.player._id
}

function isLegalMove(game, card, old_state) {
	var has_card_in_hand = cardExists(game, card)
	  , card_restricted = !card.unrestrictable && game.player.restrict
	  , card_damages_self = card.player && card.player.health < 0
	  , poison_damage = game.player.poison > 0 ? 5 : 0
	  , energize_health = game.player.energize > 0 ? 5 : 0
	  , would_kill_self = card_damages_self && (game.player.health + card.player.health*card.num + energize_health - poison_damage <= 0)
	  , has_effect = hasEffect(game, card, old_state)
	  , bonus_card_already_played = card.bonus && game.last_move && game.last_move.bonus && !game.last_move.card
	  , has_enough_discardable_cards = !card.player || !card.player.discard || (card.player.discard && game.player.hand.length > parseInt(card.player.discard)*card.num ? true : false)

	return has_card_in_hand && !card_restricted && !would_kill_self && has_effect && has_enough_discardable_cards && !bonus_card_already_played
}

function hasEffect(game, card, old_state) {
	var players = ['player', 'opponent']
	for(var i = 0; i < players.length; i++) {
		var player = card[players[i]]
		if(player) {
			if(player.health || player.poison || player.energize || player.restrict || player.draw) {
				return true
			}
			if(player.unpoison && game[players[i]].poison) {
				return true
			}
			if(player.unenergize && game[players[i]].energize) {
				return true
			}
			if(player.unrestrict && game[players[i]].restrict) {
				return true
			}
			if(player.undamage && old_state && old_state[players[i]].health > game[players[i]].health) {
				return true
			}
			if(player.steal_cards && game[players[i]].hand.length > 0) {
				return true
			}
		}
	}
}

function winConditions(game, player, opponent) {
	if(game.player.health <= 0 && game.opponent.health <= 0) {
		game.result = 'tie'

		game.opponent.coins = 2
		game.player.coins = 2
		User.emit(game.opponent, 'tie', { game_id: game._id })
		User.emit(game.player, 'tie', { game_id: game._id })
	}
	else if(game.player.health <= 0) {
		game.result = game.opponent._id

		game.opponent.coins = 3
		game.player.coins = 1
		User.emit(game.opponent, 'win', { game_id: game._id })
		User.emit(game.player, 'lose', { game_id: game._id })
	}
	else if(game.opponent.health <= 0) {
		game.result = game.player._id

		game.player.coins = 3
		game.opponent.coins = 1
		User.emit(game.player, 'win', { game_id: game._id })
		User.emit(game.opponent, 'lose', { game_id: game._id })
	}

	return game.result
}

exports.winConditions = winConditions
exports.buildDeck = buildDeck
exports.shuffleDeck = shuffleDeck
exports.playCards = playCards