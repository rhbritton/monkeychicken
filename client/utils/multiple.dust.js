dust.helpers.multiple = function(chunk, ctx, bodies, params) {
	var number = dust.helpers.tap(params.number, chunk, ctx)
	return chunk.write(getMultiple(number))
}

dust.helpers.times = function(chunk, ctx, bodies, params) {
	var number = parseInt(dust.helpers.tap(params.number, chunk, ctx))
    return chunk.map(function(chunk) {
		if(number) for(var i = 0; i < number; i++) chunk.render(bodies.block, ctx)
		else if(bodies['else']) chunk.render(bodies['else'], ctx)
		chunk.end()
    })
}

function getMultiple(number) {
	if(number <= 1) return ''
	if(number == 2) return 'Double'
	if(number == 3) return 'Triple'
	if(number == 4) return 'Quadruple'
	if(number == 5) return 'Quintuple'
	if(number == 6) return 'Hextuple'
	if(number == 7) return 'Septuple'
	if(number == 8) return 'Octuple'
}