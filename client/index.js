var OneOnOne = require('one-on-one/client')
  , Decks = require('../server/static/decks.js')
  , Shop = require('../server/static/shop.js')
  , Play = window.Play = OneOnOne.PlayHandler
  , User = OneOnOne.UserModel
  , ramjet = require('ramjet')

require('./utils/multiple.dust.js')

WebFontConfig = {
	google: { families: [ 'McLaren::latin' ] }
}

;(function() {
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
	'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})()

OneOnOne.SetUpShop(Shop)
OneOnOne.App()

Play.on('load', render)
Play.on('select-deck', render)
Play.on('play-card', animate)
Play.on('poison', animate)
Play.on('change-turn', animate)
Play.on('pass', animate)
Play.on('resize', resize)

Play.on('player:update', updatePlayer)
Play.on('opponent:update', updateOpponent)

function render(game, player, opponent, event, done) {
	if(!game.player.deck_type) Play.emit('select-deck', { deck:'monkey_chicken' })

	var opp_super_health = game.opponent.health - 100
	if(opp_super_health <= 0) opp_super_health = 0
	var player_super_health = game.player.health - 100
	if(player_super_health <= 0) player_super_health = 0

	Play.render('play', { game:game, player:player, opponent:opponent, winner:game.result == player._id ? player : opponent, turn:game.turn != game.opponent._id, opp_super_health:opp_super_health, player_super_health:player_super_health  }, function(err, html) {
		Play.container.innerHTML = html
		Play.delegate('.player .hand', '.card', 'click', playCard)
		Play.delegate('.player .hand', '.card', 'tap', playCard)
		Play.delegate('.player .hand', '.card', 'mouseover', cardHover)
		Play.delegate('.player .hand', '.card', 'mouseout', cardStopHover)
		Play.listen('.pass', 'click', pass)

		setTimeout(resize, 100)

		done()
	})
}

function selectDeck() {
	var decks = Play.findAll('[name="select_deck"]')
	for(var i=0; i<decks.length; i++) {
		if(decks[i].checked) {
			Play.emit('select-deck', { deck:decks[i].value })
			break;
		}
	}
}

function pass() {
	Play.emit('pass', {})
}

function getCardWidth(cards) {
	for(var i=1; i<cards.length; i++) {
		if(cards[i].clientWidth) return cards[i].clientWidth
	}
	return 0
}

function resize() {
	var hand = Play.find('.player .hand')
	if(hand && hand.childNodes[0]) {
		var cards = hand.childNodes
		  , size = cards.length
		  , space = Play.container.clientWidth - getCardWidth(cards)*size
		  , margin = space/(size-1)-1

		if(margin>0) margin = 0

		for(var i=1; i<size; i++) {
			cards[i].style.marginLeft = margin+'px'
		}

		cards[0].style.marginLeft = 0
	}
}

function playCard(e) {
	e.stopPropagation()
	e.preventDefault()
	if((Play.ready && Play.currentGame.turn != Play.currentGame.opponent._id)) {
		// if(e.target.className.indexOf('stackable') != -1) {
		// 	var card_elements = Play.findAll('[data-cardkey="'+e.target.getAttribute('data-cardkey')+'"]')
		// 	for(var i=0; i<card_elements.length; i++) card_elements[i].remove()
		// } else e.target.remove()
		Play.emit('play-card', { card: { deck:e.target.getAttribute('data-deck'), card_key:e.target.getAttribute('data-cardkey') } })	
	}
	resize()
}

function cardHover(e) {
	if(e.target.classList.contains('stackable')) {
		var cards = Play.findAll('.player .hand .card[data-cardkey="'+e.target.getAttribute('data-cardkey')+'"]')
		for(var i=0; i<cards.length; i++) {
			cards[i].classList.add('hover')
		}
	} else {
		e.target.classList.add('hover')
	}
	e.stopPropagation()
	e.preventDefault()
}

function cardStopHover(e) {
	if(e.target.classList.contains('stackable')) {
		var cards = Play.findAll('.player .hand .card[data-cardkey="'+e.target.getAttribute('data-cardkey')+'"]')
		for(var i=0; i<cards.length; i++) {
			cards[i].classList.remove('hover')
		}
	} else {
		e.target.classList.remove('hover')
	}
}

function updatePlayer(user, event, done) {
	
	Play.find('.player .coins .amount').innerHTML = user.coins
	Play.find('.player .wins .amount').innerHTML = user.wins
	
	done()
}

function updateOpponent(user, event, done) {

	Play.find('.opponent .coins .amount').innerHTML = user.coins
	Play.find('.opponent .wins .amount').innerHTML = user.wins
	
	done()
}

function animateCard(game, player, event, done) {
	if(event.type != 'play-card') return done()

	var old_result = Play.find('.game_result')
	  , new_result = old_result.cloneNode(true)

	old_result.style.opacity = 0

	new_result.innerHTML = ''

	setTimeout(function() {
		old_result.remove()
	}, 1000)
	
	if(game.last_move) {
		if(game.last_move.bonus) {
			for(var i=0; i<game.last_move.bonus.num; i++) {
				Play.render('card', { img:game.last_move.bonus.img, card_key:game.last_move.bonus.key, deck_type:'monkey_chicken' }, function(err, html) {
					new_result.innerHTML += html
				})
			}
		}
	
		if(game.last_move.card) {
			for(var i=0; i<game.last_move.card.num; i++) {
				Play.render('card', { img:game.last_move.card.img, card_key:game.last_move.card.key, deck_type:'monkey_chicken' }, function(err, html) {
					new_result.innerHTML += html
				})
			}
		}
	}

	

	if(player._id == game.last_move.initiator) {
		old_result.parentNode.insertBefore(new_result, old_result)

		var card = game.last_move.card ? game.last_move.card : game.last_move.bonus
		  , cards_in_hand = Play.findAll('.player .hand [data-cardkey="'+card.key+'"]')
		  , result_cards = new_result.querySelectorAll('[data-cardkey="'+card.key+'"]')
		  , completed = 0
		
		for(var i = 0; i < card.num; i++) (function(i) {
			ramjet.transform(cards_in_hand[i], result_cards[i], { done:function() {
				result_cards[i].style.display = ''
				if(++completed == card.num) done()
			} })
			cards_in_hand[i].remove()
			result_cards[i].style.display = 'none'
		})(i)

	} else {
		new_result.style.top = '-10em'
		old_result.parentNode.insertBefore(new_result, old_result)
		setTimeout(function() {
			new_result.style.top = ''
			done()
		}, 10)
	}
}

function animate(game, player, opponent, event, done) {
	animateCard(game, player, event, function() {
		animateHealth(Play.find('.player .health'), 'player', game.player.health, function() {
			animateHealth(Play.find('.opponent .health'), 'opponent', game.opponent.health, function() {
				makeLikeOrderedArray(game.player.hand, Play.findAll('.player .hand .card'))
				animatePoison(Play.find('.player .statuses .poison'), game.player.poison, function() {
					animatePoison(Play.find('.opponent .statuses .poison'), game.opponent.poison, function() {
						if(game.opponent.hand.length > 0) {
							animateOpponentHand(Play.find('.opponent .hand .number'), game.opponent.hand.length, function() {
								Play.timeout(500, function() {
									var in_game = Play.find('.in_game')
									if(game.turn == player._id) in_game.setAttribute('data-your_turn', true)
									else in_game.removeAttribute('data-your_turn')
									
									resize()
									done()
								})
							})
						} else Play.timeout(500, function() {
							var in_game = Play.find('.in_game')
							if(game.turn == player._id) in_game.setAttribute('data-your_turn', true)
							else in_game.removeAttribute('data-your_turn')

							resize()
							done()
						})

						if(game.result) {
							Play.render('end_screen', { opponent_id: opponent._id, opponent_name: opponent.username, is_winner:game.result == player._id, who_won:(game.result == player._id ? player : opponent).username, coins: game.player.coins }, function(err, html) {
								Play.delegate('.end_game_screen', '.play_again', 'click', Play.challengeById.bind(this, opponent._id))

								Play.find('.end_game_screen').setAttribute('data-gameover', true)
								Play.find('.end_game_screen').innerHTML = html
							})
						}
					})
				})
			})
		})
	})
}

function animateOpponentHand(amount, num_cards, done) {
	var start_amount = parseInt(amount.textContent)
	  , total_change = Math.abs(num_cards-start_amount)

	if(!total_change) return done()

	Play.countAnimation(amount, num_cards, total_change*300, done)
}

function animatePoison(element, new_poison, done) {
	element.innerHTML = ''

	for(var i=0; i<new_poison; i++) {
		element.innerHTML += '<img src="images/icons/poison.png">'
	}

	done()
}

function animateHealth(element, player, newHealth, done) {
	var amount = element.querySelector('.amount')
	  , change_amount = element.querySelector('.change_amount')
	  , start_amount = parseInt(amount.textContent)
	  , total_change = Math.abs(newHealth-start_amount)
	  , total_duration = 400 + total_change*15
	  , negative_bar = getBarData('.negative', newHealth < 0 ? 100 + newHealth : 100)
	  , normal_bar = getBarData('.normal', newHealth > 100 ? 100 : (newHealth < 0 ? 0 : newHealth))
	  , super_bar = getBarData('.super', newHealth > 100 ? newHealth - 100 : 0)
	
	if(!total_change) return done()

	function animateChange() {
		var third_duration = Math.floor(total_duration/3)
		  , gameWindow = Play.container
		  , windowSize = Math.min(gameWindow.offsetWidth, gameWindow.offsetHeight)
		  , edge = player == 'player' ? 'bottom' : 'top'
		  , animation = {}
		  , scale = Math.floor(Math.pow(windowSize*total_change, 0.8)/20)
		  , duration = 500 + total_duration/2.5

		change_amount.textContent = (newHealth > start_amount ? '+' : '-') + total_change
		change_amount.style.color = newHealth > start_amount ? '#08c' : '#c00'
		change_amount.style[edge] = Math.floor(scale/2.5*-1)+'px'
		change_amount.style.fontSize = '1px'
		change_amount.style.opacity = 0
		change_amount.style.display = 'block'

		animation['font-size'] = scale+'px'
		animation[edge] = Math.floor(scale/2.5)+'px'
		animation.opacity = 1

		Play.animate(change_amount, animation, duration, 'linear', function() {
			animation['font-size'] = Math.floor(scale*1.2)+'px'
			animation[edge] = Math.floor((scale/6.25)+(scale/2.5))+'px'
			animation.opacity = 0
			Play.animate(change_amount, animation, Math.floor(duration/5), 'linear', function() {
				change_amount.style.display = 'none'
			})
		})
	}

	function getBarData(cls, newValue) {
		var el = element.querySelector(cls)
		return {
			  element: el
			, width: newValue
			, ease: 'linear'
			, duration: Math.floor(
				Math.abs(parseInt(el.style.width)-newValue)/total_change*total_duration
			)
		}
	}

	function animateBar(bar, fn) {
		Play.animate(bar.element, { width:bar.width+'%' }, bar.duration, bar.ease, fn)
	}

	Play.countAnimation(amount, newHealth, total_duration)

	if(newHealth > start_amount) {
		animateBar(negative_bar, function() {
			animateBar(normal_bar, function() {
				animateBar(super_bar, done)
			})
		})
	} else {
		animateBar(super_bar, function() {
			animateBar(normal_bar, function() {
				animateBar(negative_bar, done)
			})
		})
	}

	animateChange()
}

function makeLikeOrderedArray(array1, array2) {
	var clientCards = getClientCardArray(array2)
	for(var i=0; i<array1.length || i<array2.length; i++) {
		if(!array2[i]) {
			insertElementAt(array1[i], array2, i)
		} else if(!array1[i]) {
			removeElementAt(array2, i--)
		} else {
			var result = compare(array1[i], array2[i])
			if(result < 0) {
				insertElementAt(array1[i], array2, i)
			} else if (result > 0) {
				removeElementAt(array2, i--)
			}
		}
		var array2 = Play.findAll('.player .hand .card')
	}
	if(array1.length != array2.length) {
		
	} else {
		for(var i=0; i<array1.length; i++) {
			if(compare(array1[i], array2[i]) != 0) {
				break
			}
		}
	}
	resize()
}

function getServerCardArray(arr) {
	var a = []
	arr.forEach(function(e) {
		a.push(e.card_key)
	})
	return a
}

function getClientCardArray(arr) {
	var a = []
	;[].forEach.call(arr, function(e) {
		a.push(e.getAttribute('data-cardkey'))
	})
	return a
}

function compare(a,b) {
	return a.order-parseInt(b.dataset.order)
}

function insertElementAt(element, array, index) {
	var hand = Play.find('.player .hand')
	Play.renderNode('card', element, function(err, card) {
		hand.classList.remove('size_'+array.length)
		hand.classList.add('size_'+(array.length+1))
		card.classList.add('draw')
		hand.insertBefore(card, array[index])
		resize()
		Play.timeout(100, function() {
			card.classList.remove('draw')
		})
	})
}

function removeElementAt(array, index) {
	var hand = Play.find('.player .hand')
	hand.classList.remove('size_'+array.length)
	hand.classList.add('size_'+(array.length-1))
	array[index].remove()
}