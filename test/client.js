var assert = require("assert")

function makeLikeOrderedArray(array1, array2) {

	for(var i=0; i<array1.length || i<array2.length; i++) {
		if(!array2[i]) {
			insertElementAt(array1[i], array2, i)
		} else if(!array1[i]) {
			removeElementAt(array2, i--)
		} else {
			var result = compare(array1[i], array2[i])
			if(result < 0) {
				insertElementAt(array1[i], array2, i)
			} else if (result > 0) {
				removeElementAt(array2, i--)
			}
		}
	}

}

function compare(a,b) {
	return a-b
}

function insertElementAt(element, array, index) {
	array.splice(index, 0, element)
}

function removeElementAt(array, index) {
	array.splice(index, 1)
}

describe('Play', function(){
	describe('makeLikeOrderedArray', function(){
		it('should make 2nd array like the first array', function(){
			var array1 = [1, 3, 4]
			var array2 = [1, 2, 4]


			makeLikeOrderedArray(array1, array2)

			assert.equal(array1.length, array2.length)
			for(var i=0; i<array1.length || i<array2.length; i++) {
				assert.equal(array1[i], array2[i])
			}
		})

		it('should make 2nd array like the first array', function(){
			var array1 = [1, 3, 4, 5]
			var array2 = [1, 2, 4]


			makeLikeOrderedArray(array1, array2)

			assert.equal(array1.length, array2.length)
			for(var i=0; i<array1.length || i<array2.length; i++) {
				assert.equal(array1[i], array2[i])
			}
		})

		it('should make 2nd array like the first array', function(){
			var array1 = [1, 3, 4]
			var array2 = [1, 2, 4, 5]


			makeLikeOrderedArray(array1, array2)

			assert.equal(array1.length, array2.length)
			for(var i=0; i<array1.length || i<array2.length; i++) {
				assert.equal(array1[i], array2[i])
			}
		})

		it('should make 2nd array like the first array', function(){
			var array1 = [0, 1, 3, 3, 4, 10, 10, 12]
			var array2 = [-1, 1, 1, 2, 4, 5, 10, 10, 11]


			makeLikeOrderedArray(array1, array2)

			assert.equal(array1.length, array2.length)
			for(var i=0; i<array1.length || i<array2.length; i++) {
				assert.equal(array1[i], array2[i])
			}
		})
	})
})