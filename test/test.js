var assert = require("assert")
  , Play = require("../server/play")
  , Decks = require('../server/static/decks')

describe('Play', function(){
	describe('makeLikeOrderedArray', function(){
		it('should make 2nd array like the first array', function(){
			var array1 = [1, 2, 3, 4, 5, 6, 8]
			var array2 = [1, 2, 3, 3, 5, 6, 8]
			assert.equal(array1, array2)
		})
	})

	describe('winConditions', function(){
		it('should not set a result if no one has won or tied', function(){
			var game = Play.winConditions({ player: { health: 100 }, opponent: { health:100 } })
			assert.equal(undefined, game.result)
		})
		it('should set the opponent to the winner if the player has less than 1 health', function(){
			var game = Play.winConditions({ player: { health: 0 }, opponent: { _id:'opponentID', health:100 } })
			assert.equal('opponentID', game.result)
		})
		it('should set the player to the winner if the opponent has less than 1 health', function(){
			var game = Play.winConditions({ player: { _id:'playerID', health:100 }, opponent: { health:0 } })
			assert.equal('playerID', game.result)
		})
		it('should the result to tie if both players are at 0 or lower health', function(){
			var game = Play.winConditions({ player: { _id:'playerID', health:0 }, opponent: { health:0 } })
			assert.equal('tie', game.result)
		})
	})

	describe('applyPoisonDamage', function(){
		it('should deal poison damage of 5 health when poision is greater than 0 and lower poison by 1 turn', function(){
			var game = Play.applyPoisonDamage({ player: { poison: 1, health: 100 } }, 1)
			assert.equal(0, game.player.poison)
			assert.equal(95, game.player.health)
		})
		it('should not deal poison damage of 5 health when poision is less than or equal to 0', function(){
			var game = Play.applyPoisonDamage({ player: { poison: 0, health: 100 } }, 0)
			assert.equal(0, game.player.poison)
			assert.equal(100, game.player.health)
		})
	})

	describe('buildDeck', function(){
		it('build deck of monkeychicken', function(){
			var deck = Play.buildDeck(Decks.monkey_chicken, 'monkey_chicken')
			// console.log(deck)
		})
	})

	describe('shuffleDeck', function(){
		it('shuffle deck of monkeychicken', function(){
			var deck = Play.buildDeck(Decks.monkey_chicken, 'monkey_chicken')
			deck = Play.shuffleDeck(deck)
			// console.log(deck)
		})
	})

	describe('drawCardsNew', function(){
		it('draw cards from monkeychicken deck', function(){
			var deck = Play.buildDeck(Decks.monkey_chicken, 'monkey_chicken')
			deck = Play.shuffleDeck(deck)
			var cards = Play.drawCardsNew(deck, 7, [])
			console.log('cardsTest', cards)
		})
	})

	// describe('organizeHand', function(){
	// 	it('organizes your hand in order of the order variables', function(){
	// 		var deck = Play.buildDeck(Decks.monkey_chicken, 'monkey_chicken')
	// 		deck = Play.shuffleDeck(deck)
	// 		var cards = Play.drawCards(deck, 7)
	// 		cards.drawn_cards = Play.organizeHand(cards.drawn_cards, 'monkey_chicken')
	// 		// console.log(cards.drawn_cards)
	// 	})
	// })

	describe('playCards', function(){
		it('plays a 3 damage card when there is only 1 in your hand and does 3 damage to opponent', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'MC3' }]
			// game = { player: { _id:'playerID', health:100, cards:cards }, opponent: { _id:'opponentID', health:100 } }
			// game = Play.playCards(game, cards[0])
			// assert.equal(97, game.opponent.health)
			// assert.equal(0, game.player.cards.length)
		})

		it('plays a 10 damage card when there is 3 in your hand and does 30 damage to opponent', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'MC10' }, { deck:'monkey_chicken', card_key:'MC10' }, { deck:'monkey_chicken', card_key:'MC10' }]
			// game = { player: { _id:'playerID', health:100, cards:cards, deck:'monkey_chicken' }, opponent: { _id:'opponentID', health:100 } }
			// game = Play.playCards(game, cards[0])
			// assert.equal(70, game.opponent.health)
			// assert.equal(0, game.player.cards.length)
		})

		it('plays a poisonous babboon and gives the opponent 3 turns of poison', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'PoBa' }, { deck:'monkey_chicken', card_key:'MC10' }, { deck:'monkey_chicken', card_key:'MC10' }]
			// game = { player: { _id:'playerID', health:100, cards:cards, deck:'monkey_chicken', poison:0 }, opponent: { _id:'opponentID', health:100, poison:0 } }
			// game = Play.playCards(game, cards[0])
			// console.log(game)
			// assert.equal(3, game.opponent.poison)
			// assert.equal(2, game.player.cards.length)
		})

		it('plays a helpful hen and heals the player 15 health', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'HeHe' }, { deck:'monkey_chicken', card_key:'HeHe' }, { deck:'monkey_chicken', card_key:'MC10' }]
			// game = { player: { _id:'playerID', health:100, cards:cards, deck:'monkey_chicken', poison:0 }, opponent: { _id:'opponentID', health:100, poison:0 } }
			// game = Play.playCards(game, cards[0])
			// console.log(game)
			// assert.equal(115, game.player.health)
			// assert.equal(2, game.player.cards.length)
		})

		it('plays a kamikaze monkey and deals 50 damage to each player', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'KaMo' }, { deck:'monkey_chicken', card_key:'HeHe' }, { deck:'monkey_chicken', card_key:'MC10' }]
			// game = { player: { _id:'playerID', health:100, cards:cards, deck:'monkey_chicken', poison:0 }, opponent: { _id:'opponentID', health:100, poison:0 } }
			// game = Play.playCards(game, cards[0])
			// console.log(game)
			// assert.equal(50, game.player.health)
			// assert.equal(50, game.opponent.health)
			// assert.equal(2, game.player.cards.length)
		})

		it('plays a kamikaze monkey but fails because it would be suicide', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'KaMo' }, { deck:'monkey_chicken', card_key:'HeHe' }, { deck:'monkey_chicken', card_key:'MC10' }]
			// game = { player: { _id:'playerID', health:50, cards:cards, deck:'monkey_chicken', poison:0 }, opponent: { _id:'opponentID', health:100, poison:0 } }
			// game = Play.playCards(game, cards[0])
			// console.log(game)
			// assert.equal(50, game.player.health)
			// assert.equal(100, game.opponent.health)
			// assert.equal(3, game.player.cards.length)
		})

		it('plays a kamikaze monkey but fails because it would be suicide with the poison damage', function(){
			// cards = [{ deck:'monkey_chicken', card_key:'KaMo' }, { deck:'monkey_chicken', card_key:'HeHe' }, { deck:'monkey_chicken', card_key:'MC10' }]
			// game = { player: { _id:'playerID', health:55, cards:cards, deck:'monkey_chicken', poison:1 }, opponent: { _id:'opponentID', health:100, poison:0 } }
			// game = Play.playCards(game, cards[0])
			// console.log(game)
			// assert.equal(55, game.player.health)
			// assert.equal(100, game.opponent.health)
			// assert.equal(3, game.player.cards.length)
		})

		it('plays a 10 damage card but poison damage kills you', function(){
			cards = [{ deck:'monkey_chicken', card_key:'KaMo' }, { deck:'monkey_chicken', card_key:'HeHe' }, { deck:'monkey_chicken', card_key:'MC10' }]
			game = { player: { _id:'playerID', health:5, cards:cards, deck:'monkey_chicken', poison:1 }, opponent: { _id:'opponentID', health:100, poison:0 } }
			game = Play.playCards(game, cards[2])
			console.log(game)
			assert.equal(0, game.player.health)
			assert.equal(90, game.opponent.health)
			assert.equal(2, game.player.cards.length)
		})
	})

	
})