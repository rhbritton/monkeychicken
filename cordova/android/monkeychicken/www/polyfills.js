/*
 * Minimal classList shim for IE 9
 * By Devon Govett
 * MIT LICENSE
 */

;(function() {
    if (!("classList" in document.documentElement) && Object.defineProperty && typeof HTMLElement !== 'undefined') {
        Object.defineProperty(HTMLElement.prototype, 'classList', {
            get: function() {
                var self = this;
                function update(fn) {
                    return function(value) {
                        var classes = self.className.split(/\s+/),
                            index = classes.indexOf(value);

                        fn(classes, index, value);
                        self.className = classes.join(" ");
                    }
                }

                var ret = {                    
                    add: update(function(classes, index, value) {
                        ~index || classes.push(value);
                    }),

                    remove: update(function(classes, index) {
                        ~index && classes.splice(index, 1);
                    }),

                    toggle: update(function(classes, index, value) {
                        ~index ? classes.splice(index, 1) : classes.push(value);
                    }),

                    contains: function(value) {
                        return !!~self.className.split(/\s+/).indexOf(value);
                    },

                    item: function(i) {
                        return self.className.split(/\s+/)[i] || null;
                    }
                };
                
                Object.defineProperty(ret, 'length', {
                    get: function() {
                        return self.className.split(/\s+/).length;
                    }
                });

                return ret;
            }
        });
    }  
})()
;(function() {
	if (!Element.prototype.matches) {
		Element.prototype.matches = Element.prototype.matchesSelector ||
			Element.prototype.mozMatchesSelector ||
			Element.prototype.msMatchesSelector ||
			Element.prototype.oMatchesSelector ||
			Element.prototype.webkitMatchesSelector ||
		function (selector) {
			var node = this, nodes = (node.parentNode || node.document).querySelectorAll(selector), i = -1
			while (nodes[++i] && nodes[i] != node);
			return !!nodes[i]
		}
	}
})();(function() {
	var minutes = 60
	  , hours = 3600
	  , days = 86400
	  , months = 2628000
	  , years = 31536000
	  , timeouts = {}

	function getText(seconds) {
		if(seconds <= 15) return 'a few seconds'
		if(seconds < minutes) return 'less than a minute' 
		
		if(seconds < 100) return 'a minute'
		if(seconds < hours) return Math.floor((seconds+40)/minutes) + ' minutes'
		
		if(seconds < 100*minutes) return 'an hour'
		if(seconds < days) return Math.floor((seconds+40*minutes)/hours) + ' hours'
		
		if(seconds < 36*hours) return 'a day'
		if(seconds < 25*days) return Math.floor((seconds+12*hours)/days) + ' days'

		if(seconds < 46*hours) return 'a month'
		if(seconds < years) return Math.floor((seconds+16*days)/months) + ' months'

		if(seconds < 18*months) return 'a year'
		return Math.floor((seconds+6*months)/years) + ' years'
	}

	function getTimeToUpdate(seconds) {
		if(seconds < 15) return 15-seconds
		if(seconds < minutes) return 60-seconds
		if(seconds < hours) return 60 - (seconds-40) % minutes
		if(seconds < days) return 60*minutes - (seconds-40*minutes) % hours
		if(seconds < 25*days) return 24*hours - (seconds-12*hours) % days
		if(seconds < years) return 30*days - (seconds-16*days) % months
		return 12*months - (seconds-6*months) % years
	}

	function relativeTime(seconds) {
		if(seconds <= 0) return 'just now'

		return getText(Math.abs(Math.floor(seconds))) + ( seconds < 0 ? ' from now' : ' ago')
	}

	function getMonth(number) {
		switch(number) {
			case 0:  return 'January'
			case 1:  return 'February'
			case 2:  return 'March'
			case 3:  return 'April'
			case 4:  return 'May'
			case 5:  return 'June'
			case 6:  return 'July'
			case 7:  return 'August'
			case 8:  return 'September'
			case 9:  return 'October'
			case 10: return 'November'
			case 11: return 'December'
		}
	}

	function fullTime(date) {
		var then = new Date(date)
		  , year = then.getFullYear()
		  , month = getMonth(then.getMonth())
		  , day = then.getDate()
		  , hours = then.getHours()
		  , minutes = then.getMinutes()
		  , am_pm = hours < 12 ? 'am' : 'pm'
		
		hours = hours % 12
		hours = hours == 0 ? 12 : hours
		minutes = (minutes < 10 ? '0' : '') + minutes

		return month + ' ' + day +', ' + year +' at ' + hours + ':' + minutes + am_pm
	}

	function setRelativeTime(time) {
		var date = time.getAttribute('datetime')
		  , then = new Date(date)
		  , now = new Date()
		  , seconds = (now-then)/1000
		  , update = Math.ceil(getTimeToUpdate(Math.abs(seconds))*1000)
		  , timeout_key = Math.random()

		time.setAttribute('data-relative', timeout_key)
		if(document.contains(time) && !isNaN(then.getTime())) {
			time.textContent = relativeTime(seconds)
			time.setAttribute('title', fullTime(date))
			timeouts[timeout_key] = setTimeout(function() {
				setRelativeTime(time)
			}, update)
		}
	}

	function handleTimeElement(time) {
		if(time.hasAttribute('data-relative')) {
			var timeout = timeouts[time.getAttribute('data-relative')]
			if(timeout) clearTimeout(timeout)
			setRelativeTime(time)
		}
	}

	//on page load, find all time elements
	document.addEventListener('DOMContentLoaded', function() {
		[].forEach.call(document.querySelectorAll('time'), handleTimeElement)
	}, false)

	//watch the DOM for inserted nodes
	var observer = new MutationObserver(function(mutations) {
		mutations.forEach(function(mutation) {
			if(mutation.target.nodeName == 'TIME' && mutation.attributeName == 'datetime') {
				handleTimeElement(mutation.target)
			}
			;[].forEach.call(mutation.addedNodes, function(added) {
				if(added.nodeName == 'TIME' && added.attributes['data-relative']) {
					handleTimeElement(added)
				} else if(added.childNodes.length) {
					;[].forEach.call(added.querySelectorAll('time[data-relative]'), handleTimeElement)
				}
			})
		})
	})

	observer.observe(document, { attributes:true, childList:true, subtree:true })
})()