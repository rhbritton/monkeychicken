;(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var OneOnOne = require('one-on-one/client')
  , Decks = require('../server/static/decks.js')
  , Shop = require('../server/static/shop.js')
  , Play = window.Play = OneOnOne.PlayHandler
  , User = OneOnOne.UserModel
  , ramjet = require('ramjet')

require('./utils/multiple.dust.js')

WebFontConfig = {
	google: { families: [ 'McLaren::latin' ] }
}

;(function() {
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
	'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})()

OneOnOne.SetUpShop(Shop)
OneOnOne.App()

Play.on('load', render)
Play.on('select-deck', render)
Play.on('play-card', animate)
Play.on('poison', animate)
Play.on('change-turn', animate)
Play.on('pass', animate)
Play.on('resize', resize)

Play.on('player:update', updatePlayer)
Play.on('opponent:update', updateOpponent)

function render(game, player, opponent, event, done) {
	if(!game.player.deck_type) Play.emit('select-deck', { deck:'monkey_chicken' })

	var opp_super_health = game.opponent.health - 100
	if(opp_super_health <= 0) opp_super_health = 0
	var player_super_health = game.player.health - 100
	if(player_super_health <= 0) player_super_health = 0

	Play.render('play', { game:game, player:player, opponent:opponent, winner:game.result == player._id ? player : opponent, turn:game.turn != game.opponent._id, opp_super_health:opp_super_health, player_super_health:player_super_health  }, function(err, html) {
		Play.container.innerHTML = html
		Play.delegate('.player .hand', '.card', 'click', playCard)
		Play.delegate('.player .hand', '.card', 'tap', playCard)
		Play.delegate('.player .hand', '.card', 'mouseover', cardHover)
		Play.delegate('.player .hand', '.card', 'mouseout', cardStopHover)
		Play.listen('.pass', 'click', pass)

		setTimeout(resize, 100)

		done()
	})
}

function selectDeck() {
	var decks = Play.findAll('[name="select_deck"]')
	for(var i=0; i<decks.length; i++) {
		if(decks[i].checked) {
			Play.emit('select-deck', { deck:decks[i].value })
			break;
		}
	}
}

function pass() {
	Play.emit('pass', {})
}

function getCardWidth(cards) {
	for(var i=1; i<cards.length; i++) {
		if(cards[i].clientWidth) return cards[i].clientWidth
	}
	return 0
}

function resize() {
	var hand = Play.find('.player .hand')
	if(hand && hand.childNodes[0]) {
		var cards = hand.childNodes
		  , size = cards.length
		  , space = Play.container.clientWidth - getCardWidth(cards)*size
		  , margin = space/(size-1)-1

		if(margin>0) margin = 0

		for(var i=1; i<size; i++) {
			cards[i].style.marginLeft = margin+'px'
		}

		cards[0].style.marginLeft = 0
	}
}

function playCard(e) {
	e.stopPropagation()
	e.preventDefault()
	if((Play.ready && Play.currentGame.turn != Play.currentGame.opponent._id)) {
		// if(e.target.className.indexOf('stackable') != -1) {
		// 	var card_elements = Play.findAll('[data-cardkey="'+e.target.getAttribute('data-cardkey')+'"]')
		// 	for(var i=0; i<card_elements.length; i++) card_elements[i].remove()
		// } else e.target.remove()
		Play.emit('play-card', { card: { deck:e.target.getAttribute('data-deck'), card_key:e.target.getAttribute('data-cardkey') } })	
	}
	resize()
}

function cardHover(e) {
	if(e.target.classList.contains('stackable')) {
		var cards = Play.findAll('.player .hand .card[data-cardkey="'+e.target.getAttribute('data-cardkey')+'"]')
		for(var i=0; i<cards.length; i++) {
			cards[i].classList.add('hover')
		}
	} else {
		e.target.classList.add('hover')
	}
	e.stopPropagation()
	e.preventDefault()
}

function cardStopHover(e) {
	if(e.target.classList.contains('stackable')) {
		var cards = Play.findAll('.player .hand .card[data-cardkey="'+e.target.getAttribute('data-cardkey')+'"]')
		for(var i=0; i<cards.length; i++) {
			cards[i].classList.remove('hover')
		}
	} else {
		e.target.classList.remove('hover')
	}
}

function updatePlayer(user, event, done) {
	
	Play.find('.player .coins .amount').innerHTML = user.coins
	Play.find('.player .wins .amount').innerHTML = user.wins
	
	done()
}

function updateOpponent(user, event, done) {

	Play.find('.opponent .coins .amount').innerHTML = user.coins
	Play.find('.opponent .wins .amount').innerHTML = user.wins
	
	done()
}

function animateCard(game, player, event, done) {
	if(event.type != 'play-card') return done()

	var old_result = Play.find('.game_result')
	  , new_result = old_result.cloneNode(true)

	old_result.style.opacity = 0

	new_result.innerHTML = ''

	setTimeout(function() {
		old_result.remove()
	}, 1000)
	
	if(game.last_move) {
		if(game.last_move.bonus) {
			for(var i=0; i<game.last_move.bonus.num; i++) {
				Play.render('card', { img:game.last_move.bonus.img, card_key:game.last_move.bonus.key, deck_type:'monkey_chicken' }, function(err, html) {
					new_result.innerHTML += html
				})
			}
		}
	
		if(game.last_move.card) {
			for(var i=0; i<game.last_move.card.num; i++) {
				Play.render('card', { img:game.last_move.card.img, card_key:game.last_move.card.key, deck_type:'monkey_chicken' }, function(err, html) {
					new_result.innerHTML += html
				})
			}
		}
	}

	

	if(player._id == game.last_move.initiator) {
		old_result.parentNode.insertBefore(new_result, old_result)

		var card = game.last_move.card ? game.last_move.card : game.last_move.bonus
		  , cards_in_hand = Play.findAll('.player .hand [data-cardkey="'+card.key+'"]')
		  , result_cards = new_result.querySelectorAll('[data-cardkey="'+card.key+'"]')
		  , completed = 0
		
		for(var i = 0; i < card.num; i++) (function(i) {
			ramjet.transform(cards_in_hand[i], result_cards[i], { done:function() {
				result_cards[i].style.display = ''
				if(++completed == card.num) done()
			} })
			cards_in_hand[i].remove()
			result_cards[i].style.display = 'none'
		})(i)

	} else {
		new_result.style.top = '-10em'
		old_result.parentNode.insertBefore(new_result, old_result)
		setTimeout(function() {
			new_result.style.top = ''
			done()
		}, 10)
	}
}

function animate(game, player, opponent, event, done) {
	animateCard(game, player, event, function() {
		animateHealth(Play.find('.player .health'), 'player', game.player.health, function() {
			animateHealth(Play.find('.opponent .health'), 'opponent', game.opponent.health, function() {
				makeLikeOrderedArray(game.player.hand, Play.findAll('.player .hand .card'))
				animatePoison(Play.find('.player .statuses .poison'), game.player.poison, function() {
					animatePoison(Play.find('.opponent .statuses .poison'), game.opponent.poison, function() {
						if(game.opponent.hand.length > 0) {
							animateOpponentHand(Play.find('.opponent .hand .number'), game.opponent.hand.length, function() {
								Play.timeout(500, function() {
									var in_game = Play.find('.in_game')
									if(game.turn == player._id) in_game.setAttribute('data-your_turn', true)
									else in_game.removeAttribute('data-your_turn')
									
									resize()
									done()
								})
							})
						} else Play.timeout(500, function() {
							var in_game = Play.find('.in_game')
							if(game.turn == player._id) in_game.setAttribute('data-your_turn', true)
							else in_game.removeAttribute('data-your_turn')

							resize()
							done()
						})

						if(game.result) {
							Play.render('end_screen', { opponent_id: opponent._id, opponent_name: opponent.username, is_winner:game.result == player._id, who_won:(game.result == player._id ? player : opponent).username, coins: game.player.coins }, function(err, html) {
								Play.delegate('.end_game_screen', '.play_again', 'click', Play.challengeById.bind(this, opponent._id))

								Play.find('.end_game_screen').setAttribute('data-gameover', true)
								Play.find('.end_game_screen').innerHTML = html
							})
						}
					})
				})
			})
		})
	})
}

function animateOpponentHand(amount, num_cards, done) {
	var start_amount = parseInt(amount.textContent)
	  , total_change = Math.abs(num_cards-start_amount)

	if(!total_change) return done()

	Play.countAnimation(amount, num_cards, total_change*300, done)
}

function animatePoison(element, new_poison, done) {
	element.innerHTML = ''

	for(var i=0; i<new_poison; i++) {
		element.innerHTML += '<img src="images/icons/poison.png">'
	}

	done()
}

function animateHealth(element, player, newHealth, done) {
	var amount = element.querySelector('.amount')
	  , change_amount = element.querySelector('.change_amount')
	  , start_amount = parseInt(amount.textContent)
	  , total_change = Math.abs(newHealth-start_amount)
	  , total_duration = 400 + total_change*15
	  , negative_bar = getBarData('.negative', newHealth < 0 ? 100 + newHealth : 100)
	  , normal_bar = getBarData('.normal', newHealth > 100 ? 100 : (newHealth < 0 ? 0 : newHealth))
	  , super_bar = getBarData('.super', newHealth > 100 ? newHealth - 100 : 0)
	
	if(!total_change) return done()

	function animateChange() {
		var third_duration = Math.floor(total_duration/3)
		  , gameWindow = Play.container
		  , windowSize = Math.min(gameWindow.offsetWidth, gameWindow.offsetHeight)
		  , edge = player == 'player' ? 'bottom' : 'top'
		  , animation = {}
		  , scale = Math.floor(Math.pow(windowSize*total_change, 0.8)/20)
		  , duration = 500 + total_duration/2.5

		change_amount.textContent = (newHealth > start_amount ? '+' : '-') + total_change
		change_amount.style.color = newHealth > start_amount ? '#08c' : '#c00'
		change_amount.style[edge] = Math.floor(scale/2.5*-1)+'px'
		change_amount.style.fontSize = '1px'
		change_amount.style.opacity = 0
		change_amount.style.display = 'block'

		animation['font-size'] = scale+'px'
		animation[edge] = Math.floor(scale/2.5)+'px'
		animation.opacity = 1

		Play.animate(change_amount, animation, duration, 'linear', function() {
			animation['font-size'] = Math.floor(scale*1.2)+'px'
			animation[edge] = Math.floor((scale/6.25)+(scale/2.5))+'px'
			animation.opacity = 0
			Play.animate(change_amount, animation, Math.floor(duration/5), 'linear', function() {
				change_amount.style.display = 'none'
			})
		})
	}

	function getBarData(cls, newValue) {
		var el = element.querySelector(cls)
		return {
			  element: el
			, width: newValue
			, ease: 'linear'
			, duration: Math.floor(
				Math.abs(parseInt(el.style.width)-newValue)/total_change*total_duration
			)
		}
	}

	function animateBar(bar, fn) {
		Play.animate(bar.element, { width:bar.width+'%' }, bar.duration, bar.ease, fn)
	}

	Play.countAnimation(amount, newHealth, total_duration)

	if(newHealth > start_amount) {
		animateBar(negative_bar, function() {
			animateBar(normal_bar, function() {
				animateBar(super_bar, done)
			})
		})
	} else {
		animateBar(super_bar, function() {
			animateBar(normal_bar, function() {
				animateBar(negative_bar, done)
			})
		})
	}

	animateChange()
}

function makeLikeOrderedArray(array1, array2) {
	var clientCards = getClientCardArray(array2)
	for(var i=0; i<array1.length || i<array2.length; i++) {
		if(!array2[i]) {
			insertElementAt(array1[i], array2, i)
		} else if(!array1[i]) {
			removeElementAt(array2, i--)
		} else {
			var result = compare(array1[i], array2[i])
			if(result < 0) {
				insertElementAt(array1[i], array2, i)
			} else if (result > 0) {
				removeElementAt(array2, i--)
			}
		}
		var array2 = Play.findAll('.player .hand .card')
	}
	if(array1.length != array2.length) {
		
	} else {
		for(var i=0; i<array1.length; i++) {
			if(compare(array1[i], array2[i]) != 0) {
				break
			}
		}
	}
	resize()
}

function getServerCardArray(arr) {
	var a = []
	arr.forEach(function(e) {
		a.push(e.card_key)
	})
	return a
}

function getClientCardArray(arr) {
	var a = []
	;[].forEach.call(arr, function(e) {
		a.push(e.getAttribute('data-cardkey'))
	})
	return a
}

function compare(a,b) {
	return a.order-parseInt(b.dataset.order)
}

function insertElementAt(element, array, index) {
	var hand = Play.find('.player .hand')
	Play.renderNode('card', element, function(err, card) {
		hand.classList.remove('size_'+array.length)
		hand.classList.add('size_'+(array.length+1))
		card.classList.add('draw')
		hand.insertBefore(card, array[index])
		resize()
		Play.timeout(100, function() {
			card.classList.remove('draw')
		})
	})
}

function removeElementAt(array, index) {
	var hand = Play.find('.player .hand')
	hand.classList.remove('size_'+array.length)
	hand.classList.add('size_'+(array.length-1))
	array[index].remove()
}
},{"../server/static/decks.js":25,"../server/static/shop.js":26,"./utils/multiple.dust.js":2,"one-on-one/client":3,"ramjet":24}],2:[function(require,module,exports){
dust.helpers.multiple = function(chunk, ctx, bodies, params) {
	var number = dust.helpers.tap(params.number, chunk, ctx)
	return chunk.write(getMultiple(number))
}

dust.helpers.times = function(chunk, ctx, bodies, params) {
	var number = parseInt(dust.helpers.tap(params.number, chunk, ctx))
    return chunk.map(function(chunk) {
		if(number) for(var i = 0; i < number; i++) chunk.render(bodies.block, ctx)
		else if(bodies['else']) chunk.render(bodies['else'], ctx)
		chunk.end()
    })
}

function getMultiple(number) {
	if(number <= 1) return ''
	if(number == 2) return 'Double'
	if(number == 3) return 'Triple'
	if(number == 4) return 'Quadruple'
	if(number == 5) return 'Quintuple'
	if(number == 6) return 'Hextuple'
	if(number == 7) return 'Septuple'
	if(number == 8) return 'Octuple'
}
},{}],3:[function(require,module,exports){
var OneOnOne = module.exports

OneOnOne.App = require('./frontend/scripts/app')
OneOnOne.PlayHandler = require('./frontend/scripts/presenters/play')
OneOnOne.Languages = require('./frontend/scripts/lang').languages
OneOnOne.SetUpShop = require('./frontend/scripts/presenters/shop').loadItems

},{"./frontend/scripts/app":5,"./frontend/scripts/lang":7,"./frontend/scripts/presenters/play":18,"./frontend/scripts/presenters/shop":19}],4:[function(require,module,exports){
var env = 'prod' // dev, prod
  , settings

settings = {
  'dev': {
    hostname: '192.168.1.12', // 192.168.1.12
    port: 3001
  },
  'prod': {
    hostname: 'www.playmonkeychicken.com',
    port: 3001
  }
}

module.exports = {
  hostname: settings[env].hostname,
  port: settings[env].port
}
},{}],5:[function(require,module,exports){
window.useFacebook = false

var server = require('./socket.io')
  , Main = require('./presenters/main')
  , Login = require('./presenters/login')
  , facebookReady = false
  , started = false

require('./utils/dust-helpers')

function start() {
	window.startCalled = Date.now()
	if(useFacebook) {
		FB.init({
			  appId: '150062265126033'
			, status: true
			, channelUrl: window.location.protocol + '//' + window.location.host +'/channel.html'
		})
		FB.getLoginStatus(function(response) {
			window.facebookStatusReceived = Date.now()
			if(response.status == 'connected') {
				server.emit('auth/facebook', response.authResponse.accessToken)
				server.once('auth/facebook/response', function(data) {
					if(!data.err) {
						Login.complete(data)
					} else {
						alert(data.err)
						Login.init()
					}
				})
			} else standardLogin()
		})
	} else {
		standardLogin()
	}

	setTimeout(scrollTo,2000,0,1)
}

function standardLogin() {
	if(window.localStorage.token) {
		server.emit('auth/token', window.localStorage.token)
		server.once('auth/token/response', function(data, token) {
			if(!data.err) {
				Login.complete(data, token)
			} else {
				alert(data.err)
				Login.init()
			}
		})
	} else {
		Login.init()
	}
}

if(window.navigator.standalone) {
	document.body.className += ' standalone'
}

module.exports = start
},{"./presenters/login":15,"./presenters/main":16,"./socket.io":20,"./utils/dust-helpers":23}],6:[function(require,module,exports){
var English = module.exports = {
	// Language Name
	language_name: 'English' 

	// Authentication
	, login_button: 'Login'
	, login_link: 'Already have an account? Login!'
	, register_button: 'Register'
	, register_link: 'New here? Register an Account!'
	, username_label: 'Username'
	, password_label: 'Password'
	, logout_button: 'Logout'

	// Game List
	, challenge_button: 'Challenge'
	, challenge_random_button: 'Random Challenge'
	, no_games: 'You have no games, why not start one?'
	, new_game: 'Start a New Game'
	, game_list: 'Back to Game List'
	, your_turn: 'Your Turn'
	, their_turn: 'Their Turn'
	, game_over: 'Game Over'
}

},{}],7:[function(require,module,exports){
var Lang = module.exports = {}

Lang.set = function(newLang) {
	Lang.current = Lang.languages[newLang]
	window.localStorage.lang = newLang
	Lang.all.forEach(function(lang) { 
		lang.selected = lang.key == newLang 
	})

}

Lang.languages = {
	  english: require('./english')
	, weird: require('./weird')
}

Lang.all = []
Object.keys(Lang.languages).forEach(function(lang) {
	Lang.all.push({ key:lang, name:Lang.languages[lang].language_name, selected:window.localStorage.lang==lang })
})

Lang.current = Lang.languages[window.localStorage.lang] || Lang.languages.english

},{"./english":6,"./weird":8}],8:[function(require,module,exports){
var Weird = module.exports = {
	// Language Name
	language_name: 'English (Weird =P)' 

	// Authentication
	, login_button: 'Login :)'
	, login_link: 'So you think you have an account, eh?'
	, register_button: 'Register :)'
	, register_link: 'U new? Come register with me!'
	, username_label: 'ze Username'
	, password_label: 'ze Password'
	, logout_button: 'Logout!!!!!!'

	// Game List
	, challenge_button: 'This is Sparta!'
	, no_games: 'Why you no play games?'
	, new_game: 'Start ze New Game'
	, game_list: 'Back to ze Game List'
	
}

},{}],9:[function(require,module,exports){
var Event = require('events')
  , server = require('../socket.io')
  , BaseModel

BaseModel = module.exports = function(name) {
	this._loaded = false
	this._loading = false
	this._name = name
}

BaseModel.prototype = Object.create(Event.EventEmitter.prototype)

BaseModel.prototype._load = function() {
	window[this._name+'LoadStart'] = Date.now()
	var self = this
	self._loading = true
	server.emit(self._name+'/list')
	server.once(self._name+'/list/response', function(data) {
		if(data.err) return alert(data.err)
		self._data = self._data || {}
		data.forEach(function(item) {
			self._data[item._id] = item
		})
		self._loaded = true
		self._loading = false
		if(self._init) self._init()
		window[self._name+'LoadEnd'] = Date.now()
		self.emit('loaded', self._data)
	})
	server.on(self._name+'/new', function(data) {
		self._data[data._id] = data
		self._new(data)
	})
}

BaseModel.prototype._new = function(data) {
	this.emit('new', data)
}

BaseModel.prototype.remove = function(_id) {
	var data = this._data[_id]
	delete this._data[_id]
	this.emit('remove', data)
}

BaseModel.prototype.clear = function() {
	this._loaded = false
	this.removeAllListeners()
	delete this._data
}

BaseModel.prototype.all = function(fn) {
	if(this._loaded) return fn(this._data)
	if(!this._loading) this._load()
	this.once('loaded', fn)
}

BaseModel.prototype.getById = function(_id, fn) {
	var self = this
	this.all(function(data) {
		if(data[_id]) {
			fn(data[_id])
		} else {
			self.once('new', newListener)
		}
	})

	function newListener(data) {
		if(data._id == _id) {
			fn(data)
		} else {
			self.once('new', newListener)
		}
	}
}
},{"../socket.io":20,"events":27}],10:[function(require,module,exports){
var BaseModel = require('./base')
  , Games = window.Games = module.exports = new BaseModel('games')
  , Users = require('./users')
  , server = require('../socket.io')

Games.byList = function(fn) {
	if(this._loaded) return fn(this.lists)
	if(!this._loading) this._load()
	this.once('listed', fn)
}

Games.getByOpponentId = function(opponent_id, fn) {
	if(this._loaded) return fn(this.opponents[opponent_id])
	if(!this._loading) this._load()
	this.once('opponentized', function(opponents) {
		fn(opponents[opponent_id])
	})
}

Games.getById = function(game_id, fn) {
	var self = this

	BaseModel.prototype.getById.call(Games, game_id, function(game) {
		if(!game._incomplete) return fn(game)
		self.once('completed', completedListener)
	})

	function completedListener(game) {
		if(game._id == game_id) {
			fn(game)
		} else {
			self.once('completed', completedListener)
		}
	}
}

Games._init = function() {
	this.once('loaded', listGames)
	this.once('loaded', opponentize)
	this.once('listed', loadFullGames)
	Users.on('activity', function(user_id) {
		var games = Games.opponents[user_id]
		if(games) {
			games.forEach(function(game) {
				var list = getList(game)
				  , old_index = Games.lists[list].indexOf(game)
				  , new_index

				Games.lists[list].splice(old_index, 1)
				
				new_index = getIndex(game, list)

				Games.lists[list].splice(new_index, 0, game)

				Games.emit('opponent-activity', game, list, new_index)
			})
		}
	})
	server.on('games/event', function(game_id, event) {
		if(event.type == 'resign' && event.initiator == Users.self._id) {
			Games.remove(game_id)
		} else {
			var game = event.state
			  , list = getList(game)

			Games.getById(game_id, function(old_game) {
				var old_list = getList(old_game)
				  , old_index = Games.lists[old_list].indexOf(old_game)
				  ,	new_index

				Games.lists[old_list].splice(old_index, 1)
				
				game._id = game_id
				game._ = { history: old_game._.history }
				game._.history.push(event)
				Games._data[game_id] = game

				new_index = getIndex(game, list)
				Games.lists[list].splice(new_index, 0, game)

				Games.emit('event', game, event, list, old_list, new_index)
				if(game.result) Games.emit('end', game)
			})
		}
	})
}

Games.remove = function(_id) {
	var game, list, listIndex
	if(game = this._data[_id]) {
		list = getList(game)
		listIndex = Games.lists[list].indexOf(game)
		delete this._data[_id]
		Games.lists[list].splice(listIndex, 1)
		Games.lists.count--
		this.emit('remove', game, list, Games.lists.count)
		if(!game.result) Games.emit('end', game)
	}
}

Games._new = function(game) {
	var list = getList(game)
	var index = getIndex(game, list)
	Games.lists[list].splice(index, 0, game)
	Games.lists.count++
	this.emit('new', game, list, index)
}

function loadFullGames(lists) {
	var self = this
	  , games = this._data
	;['your_turn', 'their_turn', 'game_over'].forEach(function(list_name) {
		lists[list_name].forEach(function(game) {
			game._incomplete = true
			server.emit('games/by_id', game._id)
		})
	})
	server.on('games/by_id/response', function(game) {
		games[game._id] = extend(games[game._id], game)
		delete games[game._id]._incomplete
		self.emit('completed', games[game._id])
	})
}

function opponentize(games) {
	var game_ids = Object.keys(games)
	Games.opponents = {}
	
	game_ids.forEach(function(_id) {
		var game = games[_id]
		Games.opponents[game.opponent._id] = Games.opponents[game.opponent._id] || []
		Games.opponents[game.opponent._id].push(game)
	})
	Games.emit('opponentized', Games.opponents)
}

function listGames(games) {
	var Games = this
	  , game_ids = Object.keys(games)
	Games.lists = { your_turn:[], their_turn:[], game_over:[], count:game_ids.length }
	
	game_ids.forEach(function(_id) {
		var game = games[_id]
		  , list = getList(game)

		Games.lists[list].push(game)
	})
	Users.all(function() {
		Games.lists.your_turn.sort(compare)
		Games.lists.their_turn.sort(compare)
		Games.lists.game_over.sort(compare)
		Games.emit('listed', Games.lists)
	})
}

function getIndex(game, list) {
	var items = Games.lists[list]

	if(items.length > 0) {
		var start_index = 0
		  , stop_index = items.length - 1
		  , middle = Math.floor((stop_index + start_index)/2)
		  , inserted = false
		  , index = 0
		while(!inserted) {
			var item = items[middle]
			  , comparison = compare(game, item)

			if(comparison < 0) {
				stop_index = middle - 1
				if(middle == start_index) {
					inserted = true
					index = middle
				}
			} else if(comparison > 0) {
				start_index = middle + 1
				if(middle == stop_index) {
					inserted = true
					index = middle + 1
				}
			} else {
				inserted = true
				index = middle + 1
			}
			middle = Math.floor((stop_index + start_index)/2)
		}
		return index
	} else {
		return 0
	}
}

function compare(a, b) {
	var result = 0
	return result
	Users.all(function(users) {
		console.log(users)
		console.log(b.opponent._id)
		console.log(a.opponent)
		if(users[a.opponent._id].online != users[b.opponent._id].online) result = users[a.opponent._id].online ? -1 : 1
		else if(users[a.opponent._id].active != users[b.opponent._id].active) result = users[a.opponent._id].active ? -1 : 1
	})

	if(!result) Games.all(function(games) {
		var a_history = games[a._id].last_move
		var b_history = games[b._id].last_move
		if(a_history && b_history) 
			result = new Date(b_history.timestamp) - new Date(a_history.timestamp)
	})

	return result
}

function getList(game) {
	return game.result ? 'game_over' : game.turn == game.opponent._id ? 'their_turn' : 'your_turn'
}

function extend(extending, data) {
	Object.keys(data).forEach(function(key) {
		extending[key] = data[key]
	})
	return extending
}
},{"../socket.io":20,"./base":9,"./users":11}],11:[function(require,module,exports){
var BaseModel = require('./base')
  , Users = window.Users = module.exports = new BaseModel('users')
  , server = require('../socket.io')

Users._init = function() {
	server.on('users/activity', function(user_id, online, active) {
		Users.getById(user_id, function(user) {
			user.online = online
			user.active = active
			Users.emit('activity', user_id, online, active)
		})
	})
	server.on('users/event', function(user_id, user, event) {
		Object.keys(user).forEach(function(key) {
			Users._data[user_id][key] = user[key]
		})
		Users.emit('update', Users._data[user_id], event)
	})
}

Users.setSelf = function(user) {
	this._data = this._data || {}
	this._data[user._id] = user
	this.self = user
}

},{"../socket.io":20,"./base":9}],12:[function(require,module,exports){
var BasePresenter = require('./base')
  , Account = module.exports = new BasePresenter('#account')
  , Main = require('./main')
  , Login = require('./login')
  , server = require('../socket.io')
  , Users = require('../models/users')
  , Games = require('../models/games')
  , lang = require('../lang')

Account._init = function(container) {
	Account.render('account', { languages:lang.all }, function(err, html) {
		Account.container.innerHTML = html
		Account.listen('button.close', 'click', Account.hide)
		Account.listen('.account_info select.languages', 'change', changeLanguage)
		Account.listen('form.account_info', 'submit', saveAccount)
		Account.listen('button.logout', 'click', logout)
		Account.listen('TransitionEnd', displayNoneWhenDone)
	})
}

Account.show = function() {
	Account.container.style.display = 'block'
	Account.container.setAttribute('data-visible', true)
}

Account.hide = function() {
	Account.container.removeAttribute('data-visible')
}

function displayNoneWhenDone() {
	if(!parseInt(window.getComputedStyle(Account.container).opacity)) {
		Account.container.style.display = 'none'
	}
}

function saveAccount(e) {
	e.preventDefault()
	data = {}
	data.old_password = Account.find('[name="old_password"]').value
	data.password = Account.find('[name="password"]').value
	server.emit('account/savePassword', { data:data })
	server.once('account/savePassword/response', function(err, token) {
		if(!err) {
			window.localStorage.token = token
			Users.setSelf(user)
		}
	})
}

function changeLanguage(e) {
	lang.set(e.target.value)
	Main.init(true)
	Account.show()
}

function logout() {
	server.emit('auth/logout', localStorage.token)
	server.removeAllListeners()
	Games.clear()
	Users.clear()
	window.localStorage.token = ''
	Login.init()
}
},{"../lang":7,"../models/games":10,"../models/users":11,"../socket.io":20,"./base":13,"./login":15,"./main":16}],13:[function(require,module,exports){
var BasePresenter
  , Event = require('events')
  , lang = require('../lang')

BasePresenter = module.exports = function(selector) {
	this.containerSelector = selector || '#app'
}

BasePresenter.prototype = Object.create(Event.EventEmitter.prototype)

BasePresenter.prototype.init = function() {
	this.container = document.querySelector(this.containerSelector)
	return this._init.apply(this, arguments)
}

BasePresenter.prototype.find = function(selector) {
	return this.container.querySelector(selector)
}

BasePresenter.prototype.findAll = function(selector) {
	return this.container.querySelectorAll(selector) 
}

BasePresenter.prototype.listen = function(selector, event, fn) {
	var element
	if(arguments.length == 2) {
		element = this.container
		fn = event
		event = selector
	} else {
		element = typeof selector != 'string' ? selector : this.container.querySelector(selector)
	}
	addEventListener(element, event, fn, true)
}

BasePresenter.prototype.listenAll = function(selector, event, fn) {
	var view = this
	Array.prototype.forEach.call(view.container.querySelectorAll(selector), function(element) {
		addEventListener(element, event, fn, true)
	})
}

BasePresenter.prototype.delegate = function(parentSelector, childSelector, event, fn) {
	var element, delegations = this.delegations = this.delegations || {}

	if(arguments.length == 3) {
		element = this.container
		fn = event
		event = childSelector
		childSelector = parentSelector
	} else {
		element = typeof parentSelector != 'string' ? parentSelector : this.container.querySelector(parentSelector)
	}

	if(!element.hasAttribute('data-delegated-'+event)) {
		var id = event + '-' + Math.random()
		element.setAttribute('data-delegated-'+event, id)
		addEventListener(element, event, function eventHandler(e, proxied) {
			var parent = e.target.parentNode
			
			if(!proxied) e = eventProxy(e)

			for(var i = 0; i < delegations[id].length; i++) {
				if(e.target.matches(delegations[id][i].selector)) {
					delegations[id][i].handler(e)
				}
			}

			if(!e.cancelBubble && !e.propStopped && e.target != element && parent) {
				e.target = parent
				eventHandler(e, true)
			}
		})
		this.delegations[element.getAttribute('data-delegated-'+event)] = []
		
	}
	this.delegations[element.getAttribute('data-delegated-'+event)].push({
		  selector: childSelector
		, handler: fn
	})
}

BasePresenter.prototype.render = function(template, data, fn) {
	data.lang = lang.current
	dust.render(template, data, fn)
}

BasePresenter.prototype.renderNode = function(template, data, fn) {
	this.renderNodes(template, data, function(err, nodes) {
		if(err) return fn(err)
		fn(err, nodes[0])
	})
}

BasePresenter.prototype.renderNodes = function(template, data, fn) {
	this.render(template, data, function(err, html) {
		if(err) return fn(err)
		var wrapper = document.createElement('div')
		wrapper.innerHTML = html
		fn(err, wrapper.children)
	})
}

function eventProxy(event) {
	var key, proxy = { originalEvent: event }
	for(key in event)
    	if(!/^([A-Z]|layer[XY]$)/.test(key) && event[key] !== undefined) 
    		proxy[key] = event[key]
	proxy.stopPropagation = function() {
		proxy.propStopped = true
		event.stopPropagation.call(event, arguments)
	}
	proxy.preventDefault = function() {
		event.preventDefault.call(event, arguments)
	}
	return proxy
}

function addEventListener(element, event, listener, notDelegating) {
	var x, y, prefixes = ['Webkit', 'Moz', 'MS']
	if(notDelegating && (event == 'click' || event == 'tap') && 'ontouchstart' in window) {
		element.addEventListener('touchstart', function(e) {
			x = e.touches[0].clientX
			y = e.touches[0].clientY
			document.addEventListener('touchmove', reset, false)
			element.addEventListener('touchend', done, false)
		}, false)

		function done(e) {
			listener.apply(this, arguments)
			preventGhostClicks(x, y)
		}
		function reset() {
			document.removeEventListener('touchmove', reset, false)
			element.removeEventListener('touchend', done, false)
		}
	}
	if(!('on'+event.toLowerCase() in window)) {
		prefixes.some(function(prefix) {
			if(('on'+prefix+event).toLowerCase() in window) {
				return event = prefix + event
			}
		})
	} else {
		event = event.toLowerCase()
	}
	element.addEventListener(event, listener, false)
}

var taps = []
function preventGhostClicks(x, y) {
	taps.push(x, y)
	setTimeout(function() {
		taps.splice(0, 2)
	}, 2500)
}

document.addEventListener('click', function(e) {
	for (var i = 0; i < taps.length; i += 2) {
		var x = taps[i]
		var y = taps[i + 1]
		if (Math.abs(e.clientX - x) < 25 && Math.abs(e.clientY - y) < 25) {
			e.stopPropagation()
			e.preventDefault()
		}
	}
}, true)
},{"../lang":7,"events":27}],14:[function(require,module,exports){
var BasePresenter = require('./base')
  , History = module.exports = new BasePresenter('#history')
  , server = require('../socket.io')
  , Play = require('./play')
  , Users = require('../models/users')
  , Games = require('../models/games')

History._init = function(container, toggle, reinit) {

}

History.loadGame = function(game, history_index) {
	History.currentGameId = game._id
	History.render('history/main', { game:game }, function(err, html) {
		History.container.innerHTML = html
		History.listen('.toggle', 'click', openHistory)
		History.listen('button.close', 'click', closeHistory)
		History.listen('form', 'submit', History.sendMessage)
		History.scroller = new IScroll(History.find('.list'), { 
			  scrollbars: 'custom'
			, mouseWheel: true
			, interactiveScrollbars: true
		})
		History.scroller.on('scrollStart', function() {
			History.container.setAttribute('data-scrolling', true)
		})
		History.scroller.on('scrollEnd', function() {
			History.container.removeAttribute('data-scrolling')
		})
		for(var i = 0; i <= history_index; i++) {
			History.addHistoryItem(game, game._.history[i])
		}
		History.scrollToEndOfChat()
	})
}

History.addHistoryItem = function(game, event) {
	var was_scrolled_to_bottom = History.scrollIsAtEndOfChat()

	var history_list = History.find('.list > div')
	  , last_event = history_list.lastChild
	  , time = last_event && last_event.querySelector('time')
	  , chat_by_same_sender = event.type == 'chat' && last_event && last_event.getAttribute('data-chat-sender') == event.data.sender
	  , was_recent = time && (new Date(event.timestamp) - new Date(time.getAttribute('datetime')) < 60*1000)
	if(chat_by_same_sender && was_recent) {
		var text = document.createElement('div')
		text.className = 'text'
		text.textContent = event.data.message
		time.setAttribute('datetime', event.timestamp)
		time.parentNode.insertBefore(text, time)
		History.scroller.refresh()

		History.scrollToEndOfChat()
	} else { 
		Users.all(function(users) {
			History.render('history/'+event.type, { game:game, event:event, users:users }, function(err, html) {
				if(err) return
				history_list.innerHTML += html
				history_list.scrollTop = history_list.scrollHeight
				History.scroller.refresh()

				if(was_scrolled_to_bottom)
					History.scrollToEndOfChat()
			})
		})
	}
}

History.scrollToEndOfChat = function() {
	var last_chat = History.find('.list > div:first-child > div:last-child')
	if(last_chat)
		History.scroller.scrollToElement(last_chat, 0, 0, last_chat.offsetHeight)
}

History.scrollIsAtEndOfChat = function() {
	return Math.abs(History.scroller.maxScrollY) - Math.abs(History.scroller.y) < 10
}

History.sendMessage = function(e) {
	e.preventDefault()
	var message = History.find('[name="message"]').value
	if(message.length) {
		server.emit('games/message', { game_id: History.currentGameId, message:message })
		server.once('games/message/response', function(data) {
			if(data.err) console.log(data.err)
		})
		History.find('[name="message"]').value = ''
	}
}

History.updateUnreadMessages = function(unreadCount) {
	History.find('.unreadMessages').innerHTML = unreadCount
}

History.focusChat = function(text) {
	var input = History.find('input[name=message]')
	setTimeout(function(){
		if(input){
			input.focus()
			if(text) {
				if(History.container.hasAttribute('data-open')){
					input.value += text
				} else {
					input.value = text
				}
			}
			openHistory()
		} 
	}, 0)
}

History.focusChatInput = function(text) {
	var input = History.find('input[name=message]')
	input.focus()
	setTimeout(function() {
		History.scrollToEndOfChat()
	}, 700)
}

History.unfocusChatInput = function(text) {
	var input = History.find('input[name=message]')
	input.blur()
}

function openHistory() {

	server.emit('games/resetUnreadMessages', { game_id: History.currentGameId })
	server.once('games/resetUnreadMessages/response', function(data) {
		if(data.err) console.log(data.err)
	})
	History.updateUnreadMessages('')

	History.container.setAttribute('data-open', true)
	History.scrollToEndOfChat()
	History.emit('open-start')
	setTimeout(function() { History.emit('open-end') }, 300)
}

function closeHistory(e) {
	e.stopPropagation()

	server.emit('games/resetUnreadMessages', { game_id: History.currentGameId })
	server.once('games/resetUnreadMessages/response', function(data) {
		if(data.err) console.log(data.err)
	})
	History.updateUnreadMessages('')

	History.container.removeAttribute('data-open')
	History.emit('close-start')
	setTimeout(function() { History.emit('close-end') }, 300)
}
},{"../models/games":10,"../models/users":11,"../socket.io":20,"./base":13,"./play":18}],15:[function(require,module,exports){
var BasePresenter = require('./base')
  , Login = module.exports = new BasePresenter()
  , server = require('../socket.io')
  , lang = require('../lang')
  , Main = require('./main')
  , Users = require('../models/users')

Login._init = function(reinit) {
	window.loginInitStart = Date.now()
	Login.render('login', { languages:lang.all }, function(err, html) {
		Login.container.innerHTML = html
		Login.listen('button.facebook', 'click', loginWithFacebook)
		Login.listen('.select button.login', 'click', showLoginForm)
		Login.listen('.select button.register', 'click', showRegisterForm)
		Login.listen('#auth form button.cancel', 'click', cancelForm)
		Login.listen('form.languages select', 'change', changeLanguage)
		Login.listen('#auth form', 'submit', submitForm)
		if(!useFacebook) {
			Login.find('.facebook').remove()
			Login.find('.or').remove()
		}
		window.loginInitEnd = Date.now()
	})
}

function changeLanguage(e) {
	lang.set(e.target.value)
	Login.init(true)
}

function cancelForm(e) {
	Login.find('.select').classList.remove('hidden')
	Login.find('#auth form').removeAttribute('data-type')
	Login.find('#auth form .error').classList.add('hidden')
	e.preventDefault()
}

function showError(err) {
	var error = Login.find('#auth form .error')
	error.classList.remove('hidden')
	error.textContent = err
}

function showRegisterForm() {
	Login.find('.select').classList.add('hidden')
	Login.find('#auth form').setAttribute('data-type', 'register')
	Login.find('#auth form .login').setAttribute('type', 'button')
	Login.find('#auth form .register').setAttribute('type', 'submit')
	Login.find('[name="username"]').focus()
}

function showLoginForm() {
	Login.find('.select').classList.add('hidden')
	Login.find('#auth form').setAttribute('data-type', 'login')
	Login.find('#auth form .register').setAttribute('type', 'button')
	Login.find('#auth form .login').setAttribute('type', 'submit')
	Login.find('[name="username"]').focus()
}

function submitForm(e) {
	var method = Login.find('#auth form').getAttribute('data-type')
	server.emit('auth/'+method, { 
		  username: Login.find('#auth form [name="username"]').value
		, password: Login.find('#auth form [name="password"]').value
		, language: Login.find('.languages [name="lang"]').value
	})
	server.once('auth/'+method+'/response', function(user, token) {
		if(user.err) return showError(user.err)
		Login.complete(user, token)
	})
	e.preventDefault()
}

function loginWithFacebook() {
	FB.login(function(response) {
		if(response.status == 'connected') {
			server.emit('auth/facebook', response.authResponse.accessToken)
			server.once('auth/facebook/response', function(data) {
				if(data.err) return showError(data.err)
				Login.complete(data)
			})
		}
	})
}

Login.complete = function(user, token) {
	if(token) window.localStorage.token = token
	Users.setSelf(user)
	Main.init()
}
},{"../lang":7,"../models/users":11,"../socket.io":20,"./base":13,"./main":16}],16:[function(require,module,exports){
var BasePresenter = require('./base')
  , Main = module.exports = new BasePresenter()
  , server = require('../socket.io')
  , Play = require('./play')
  , History = require('./history')
  , Login = require('./login')
  , Account = require('./account')
  , Shop = require('./shop')
  , Menu = require('./menu')
  , Users = require('../models/users')

Main._init = function(reinit) {
	window.mainInitStart = Date.now()
	Main.active = true
	Main.lastActivity = Date.now()
	Main.activityTimeout = 5000
	Main.render('main', { user: Users.self }, function(err, html) {
		Main.container.innerHTML = html

		Menu.init(Main.find('#menu'), Main.find('#menu_toggle'), reinit)
		Play.init(Main.find('#game'), reinit)
		History.init(Main.find('#history'), Main.find('#history_toggle'), reinit)
		Account.init(Main.find('#account'), reinit)
		// Shop.init(Main.find('#shop'), reinit)

		Main.listen('body :not(input)', 'click', refocus)
		Main.listen(document.body, 'mousedown', makeActive)
		Main.listen(document.body, 'mousemove', makeActive)
		Main.listen(document.body, 'touchstart', makeActive)
		Main.listen(document.body, 'keypress', makeActive)
		Main.listen(document.body, 'scroll', makeActive)

		Main.listen(document.body, 'keypress', chatFocus)
		window.mainInitEnd = Date.now()
	})
	setTimeout(checkActive, Main.activityTimeout)
	if(!reinit) {
		Menu.on('open-end', function() {
			console.log('the game-list opened')
		})
	}
}

Main.setActive = function(active) {
	Main.container.setAttribute('data-active', active)
	Main.container.classList.add('force_repaint')
	Main.container.classList.remove('force_repaint')
}

Main.goBack = function() {
	var active = Main.container.getAttribute('data-active')
	if(active == 'chat') Main.setActive('play')
	else Main.setActive('games')
}

function chatFocus(e) {
	var charCode = e.charCode || e.which
	if(charCode !== 0 && document.activeElement.tagName.toUpperCase() != 'INPUT') {
		History.focusChat(String.fromCharCode(charCode))
	}
}

function refocus(e) {
	document.activeElement.blur()
}

function makeActive() {
	if(!Main.active) {
		Main.active = true
		server.emit('auth/activity', true)
		setTimeout(checkActive, Main.activityTimeout)
	}
	Main.lastActivity = Date.now()
}

function checkActive() {
	var timeSinceLastActivity = Date.now() - Main.lastActivity
	if(timeSinceLastActivity >= Main.activityTimeout) {
		Main.active = false
		server.emit('auth/activity', false)
	} else {
		setTimeout(checkActive, Main.activityTimeout-timeSinceLastActivity)
	}
}
},{"../models/users":11,"../socket.io":20,"./account":12,"./base":13,"./history":14,"./login":15,"./menu":17,"./play":18,"./shop":19}],17:[function(require,module,exports){
var BasePresenter = require('./base')
  , Menu = module.exports = new BasePresenter('#menu')
  , server = require('../socket.io')
  , Main = require('./main')
  , Play = require('./play')
  , Account = require('./account')
  , Shop = require('./shop')
  , Users = require('../models/users')
  , Games = require('../models/games')
  , DOM = require('../utils/dom')
  , unfriending = false

Menu._init = function(container, toggle, reinit) {
	window.menuInitStart = Date.now()
	Games.byList(function(games) {
		window.gamesReady = Date.now()
		var active = games.your_turn[0]||games.their_turn[0]||games.game_over[0]
		Users.all(function(users) {
			window.usersReady = Date.now()
			var challenge_array = []
			var in_game = games.their_turn.concat(games.your_turn)
			for(var user in users) {
				var match = false
				for(var i=0; i<in_game.length && !match; i++) {
					if(in_game[i].opponent._id == user) {
						match = true
					}
				}
				if(!match && user != Users.self._id) challenge_array.push(users[user])
			}

			server.emit('games/checkInQueue', {})
			server.once('games/checkInQueue/response', function(res) {
				Menu.render('menu/main', { games:games, users:users, usersArray:challenge_array, isInQueue: res.isInQueue }, function(err, html) {
					Menu.toggle = toggle
					Menu.container.innerHTML = html

					Menu.listen('touchmove', function(e){ e.preventDefault() })

					Menu.listen('.toggle', 'click', openMenu)

					Menu.listen('button.close', 'click', closeMenu)

					Menu.listen('form.play_random_opponent button', 'click', random_challenge)
					Menu.listen('form.play_opponent button', 'click', challenge)
					Menu.listen('form.play_opponent input', 'keypress', challengeEnter)

					Menu.listen('.games .to_friends', 'click', Menu.displayFriends)
					Menu.listen('.friends .to_games', 'click', Menu.displayGames)

					Menu.listen('.account', 'click', Account.show)
					// Menu.listen('.shop', 'click', Shop.show)

					Menu.delegate('.play_friend', '.friend_item', 'click', friend_challenge)
					Menu.delegate('.play_friend', '.unfriend', 'click', unfriend)
					
					Menu.delegate('.games', '.game_item', 'click', startGame)
					Menu.delegate('.games', '.resign', 'click', resignGame)
			
					if(active) Games.getById(active._id, function(active) {
						Menu.loadGame(active, true)
					})

					Menu.scroller = new IScroll(Menu.find('.scroll'), { 
						  scrollbars: 'custom'
						, mouseWheel: true
						, interactiveScrollbars: true
						, click: true
					})
					Menu.scroller.on('scrollStart', function() {
						Menu.container.setAttribute('data-scrolling', true)
					})
					Menu.scroller.on('scrollEnd', function() {
						Menu.container.removeAttribute('data-scrolling')
					})

					if(!games.count) {
						Menu.container.setAttribute('data-no-games', true)
						Menu.displayFriends()
					} else {
						Menu.displayGames()
					}
					window.menuInitEnd = Date.now()
				})
			})
		})
	})
	if(!reinit){
		Games.on('new', insertGameItem)
		Games.on('event', updateGameItem)
		Games.on('opponent-activity', moveGameItem)
		Games.on('end', endGameItem)
		Games.on('remove', removeGameItem)
		Users.on('activity', updateActivityStatus)
		server.on('games/new', newRandomGame)
		Users.on('update', updateUserGameItems)
	}
}

Menu.refreshScroller = function() {
	Menu.scroller.disable()
	Menu.scroller.refresh()
	setTimeout(function() {
		Menu.scroller.enable()
	})
}

function openMenu() {
	Menu.container.setAttribute('data-open', true)
	Menu.container.setAttribute('data-focused', true)
	Menu.emit('open-start')
	setTimeout(function() { Menu.emit('open-end') }, 300)
}

function closeMenu(e) {
	e.stopPropagation()
	Menu.container.removeAttribute('data-open')
	Menu.container.removeAttribute('data-focused')
	Menu.emit('close-start')
	setTimeout(function() { Menu.emit('close-end') }, 300)
}

function moveGameItem(game, list, index) {
	var item_container = Menu.find('.games .turn_list.'+list+' ul.content')
	var game_item = item_container.querySelector('.game_item[data-id="'+game._id+'"]')
	if(game_item) {
		if(index >= item_container.children.length) {
			item_container.appendChild(game_item)
		} else {
			item_container.insertBefore(game_item, item_container.children[index])
		}
	}
}

function insertGameItem(game, list, index, selected) {
	Users.all(function(users) {
		Menu.renderNode('menu/game-item', { game:game, users:users, opponent_user:users[game.opponent._id] }, function(err, game_item) {
			var turn_list = Menu.find('.games .turn_list.'+list)
			var item_container = turn_list.querySelector('ul.content')
			Menu.container.removeAttribute('data-no-games')
			turn_list.removeAttribute('data-empty')
			if(index >= item_container.children.length) {
				item_container.appendChild(game_item)
			} else {
				item_container.insertBefore(game_item, item_container.children[index])
			}
			if(selected) game_item.classList.add('selected')
			var friends = Menu.find('.play_friend .content [data-id="'+game.opponent._id+'"]')
			if(friends) {
				friends.remove()
				updateFriendCount(-1)
			}
			Menu.refreshScroller()
		})
	})
}

function updateFriendCount(diff) {
	var play_friend_container = Menu.find('.play_friend')
	var friend_count = parseInt(play_friend_container.getAttribute('data-friendcount'))
	friend_count = friend_count ? friend_count : 0
	friend_count = friend_count+diff
	if(friend_count < 0)
		friend_count = 0

	play_friend_container.setAttribute('data-friendcount', friend_count)	
}

function newRandomGame(game, is_random_game) {
	if(is_random_game) {
		Menu.find('.play_random_opponent button').setAttribute('data-queued', false)
		Menu.find('.waitingQueue').setAttribute('data-queued', false)
	}
}

function updateGameItem(game, event, list, old_list, index) {
	var old_list = Menu.find('.turn_list.'+old_list)
	  , new_list = Menu.find('.turn_list.'+list)
	  , old_game_item = old_list.querySelector('.game_item[data-id="'+game._id+'"]')
	  , selected = old_game_item.classList.contains('selected')
	old_game_item.remove()
	if(!old_list.querySelector('ul.content').childNodes.length) old_list.setAttribute('data-empty', true)
	insertGameItem(game, list, index, selected)
}

function endGameItem(game) {
	Users.all(function(users) {
		Menu.render('menu/friend-item', { _id: game.opponent._id, username: users[game.opponent._id].username }, function(err, html) {
			Menu.find('.play_friend .content').appendChild(DOM.htmlToNode(html))
			updateFriendCount(1)
		})
	})
}

function removeGameItem(game, list, remaining) {
	var list = Menu.find('.turn_list.'+list)
	  , game_item = list.querySelector('.game_item[data-id="'+game._id+'"]')

	game_item.remove()
	if(!list.querySelector('ul.content').childNodes.length) {
		list.setAttribute('data-empty', true)
	}
	if(!remaining) {
		Menu.container.setAttribute('data-no-games', true)
		Menu.displayFriends()
	}
	Menu.refreshScroller()
}

Menu.displayFriends = function(e) {
	Menu.container.setAttribute('data-active','friends')
	Menu.refreshScroller()
}

Menu.displayGames = function(e) {
	Menu.container.setAttribute('data-active','games')
	Menu.refreshScroller()
}

function resignGame(e) {
	e.preventDefault()
	var _id = e.target.getAttribute('data-id')
	Games.remove(_id)
	server.emit('games/resign', _id)
	server.once('games/resign/response', function(data) {
		if(data.err) return alert(data.err, 'error')
		Games.byList(function(games) {
			if(games.your_turn.length > 0) {
				Menu.loadGame(games.your_turn[0])
			} else if(games.their_turn.length > 0) {
				Menu.loadGame(games.their_turn[0])
			} else {
				Play.find('.in_game').innerHTML = ''
			}
		})
	})
}

function challenge(e) {
	e.stopPropagation()
	e.preventDefault()
	var searchField = Menu.find('.play_opponent [name="search_text"]')
	server.emit('games/challenge', { opponent:searchField.value })
	server.once('games/challenge/response', function(game_id) {
		if(game_id.err) return alert(game_id.err, 'error')
		searchField.value = ''
		Menu.displayGames()
		Games.getById(game_id, function(game) {
			Menu.loadGame(game)
		})
	})
}

function challengeEnter(e) {
	if(e.keyCode == 13)
		challenge(e)
}

function random_challenge(e) {
	e.stopPropagation()
	e.preventDefault()
	Menu.find('.random_challenge .waitingQueue').setAttribute('data-queued', true)
	Menu.find('.random_challenge .play_random_opponent button').setAttribute('data-queued', true)
	server.emit('games/random_challenge')
	server.once('games/random_challenge/response', function(game_id) {
		if(game_id.err) return alert(game_id.err, 'error')
		
		if(game_id) {
			Menu.displayGames()
			Games.getById(game_id, function(game) {
				Menu.loadGame(game)
			})
		}
	})
}

function friend_challenge(e) {
	if(!unfriending) {
		e.preventDefault()
		var opp_id = e.target.getAttribute('data-id')
		Play.challengeById(opp_id)
	} else {
		unfriending = false
	}
}

function unfriend(e) {
	e.preventDefault()
	e.stopPropagation()
	var unfriend_confirm = confirm('Are you sure you want to unfriend ' + e.target.parentNode.firstChild.textContent)
	
	if(!unfriend_confirm) return

	unfriending = true
	var friend_id = e.target.getAttribute('data-id')
	server.emit('users/unfriend', { friend_id:friend_id })
	server.once('users/unfriend/response', function() {
		Games.byList(function(games) {
			for(var i=0; i<games.game_over.length; i++) {
				if(games.game_over[i].opponent._id == friend_id) {
					var game = Menu.find('.game_item[data-id="'+games.game_over[i]._id+'"]')
					if(game) game.remove()
					games.game_over.splice(i, 1)
					i--
				}
			}
			if(games.game_over.length == 0) Menu.find('.turn_list.game_over').setAttribute('data-empty', true)

			Menu.find('.friend_item[data-id="'+friend_id+'"]').remove()
		})
	})
}

function startGame(e) {
	e.preventDefault()
	e.stopPropagation()
	Games.getById(e.target.getAttribute('data-id'), function(game) {
		Menu.loadGame(game)
	})
}

Menu.loadGame = function(game, init) {
	var selected = Menu.find('.selected')
	if(selected) selected.classList.remove('selected')
	Menu.find('[data-id="'+game._id+'"]').classList.add("selected")
	if(!init) Menu.container.removeAttribute('data-focused')
	Play.loadGame(game, init)
}

function updateActivityStatus(user_id, online, active) {
	var users = Menu.findAll('.game_item[data-opponent-id="'+user_id+'"]')
	if(online && active) {
		var activity = 'online'
	} else if(online && !active) {
		var activity = 'away'
	} else {
		var activity = 'offline'
	}

	for(var i=0; i<users.length; i++) {
		users[i].setAttribute('data-user-status', activity)
	}
}

function updateUserGameItems(user) {
	var game_items = Menu.findAll('.game_item[data-opponent-id="'+user._id+'"]')

	Users.all(function(users) {
		;[].forEach.call(game_items, function(item) {
			Games.getById(item.getAttribute('data-id'), function(game) {
				Menu.renderNode('menu/game-item', { game:game, users:users, opponent_user:user }, function(err, game_item) {
					item.parentNode.replaceChild(game_item, item)
				})
			})
		})
	})

}
},{"../models/games":10,"../models/users":11,"../socket.io":20,"../utils/dom":22,"./account":12,"./base":13,"./main":16,"./play":18,"./shop":19}],18:[function(require,module,exports){
var BasePresenter = require('./base')
  , Play = module.exports = new BasePresenter('#game')
  , server = require('../socket.io')
  , Main = require('./main')
  , Menu = require('./menu')
  , History = require('./history')
  , Games = require('../models/games')
  , Users = require('../models/users')
  , pop = new Audio('/sounds/pop.mp3')

Play.handlers = {}

Play._init = function(container, reinit) {
	Play.calcSize()
	if(!reinit) {
		Games.on('event', function(game, event) {
			if(game._id == Play.currentGame._id) {
				Play.currentGame = game
				Play.startTriggering()
			}

			if(event.initiator != Users.self._id && event.type == 'chat')
			{
				History.updateUnreadMessages(event.state.player.unreadMessages)
				pop.play()
			}
		})
		Users.on('update', function(user, event) {
			if(user == Users.self) {
				Play.triggerPlayerEvent('player:'+event.type, user, event, function() {})
				Play.triggerPlayerEvent('players:'+event.type, user, event, function() {})
				Play.triggerPlayerEvent('player:update', user, event, function() {})
				Play.triggerPlayerEvent('players:update', user, event, function() {})
			} else if(user._id == Play.currentGame.opponent._id) {
				Play.triggerPlayerEvent('opponent:'+event.type, user, event, function() {})
				Play.triggerPlayerEvent('players:'+event.type, user, event, function() {})
				Play.triggerPlayerEvent('opponent:update', user, event, function() {})
				Play.triggerPlayerEvent('players:update', user, event, function() {})
			}
		})
	}
}

window.addEventListener('resize', Play.calcSize, false)

Menu.on('open-end', function() {
	Play.container.setAttribute('data-menu', true)
	Play.calcSize()
})

Menu.on('close-start', function() {
	Play.container.removeAttribute('data-menu')
	Play.calcSize()
})

History.on('open-end', function() {
	Play.container.setAttribute('data-history', true)
	History.focusChatInput()
	Play.calcSize()
})

History.on('close-start', function() {
	Play.container.removeAttribute('data-history')
	History.unfocusChatInput()
	Play.calcSize()
})

Play.ready = true

Play.loadGame = function(game, init) {
	if(!init) Main.setActive('play')
	if(!Play.currentGame || Play.currentGame._id != game._id) {
		Play.currentGame = game
		Play.currentIndex = getHistoryIndex(game)
		Play.ready = true
		History.loadGame(Play.currentGame, Play.currentIndex)
		Users.getById(game.opponent._id, function(opponent) {
			Play.trigger('load', game._.history[Play.currentIndex].state, Users.self, opponent, {}, function() {
				Play.timeout(800, Play.startTriggering)
			})
		})
	}
}

Play.challengeById = function(opponent_id) {
	server.emit('games/challenge_id', { opponent:opponent_id })
	server.once('games/challenge_id/response', function(game_id) {
		if(game_id.err) return alert(game_id.err, 'error')
		Menu.displayGames()
		Games.getById(game_id, function(game) {
			Menu.loadGame(game)
		})
	})
}

Play.startTriggering = function() {
	if(Play.ready) {
		Play.ready = false
		Play.triggerHistory(function() {
			Play.ready = true
		})
	}
}

Play.triggerHistory = function(fn) {
	if(Play.currentIndex < Play.currentGame._.history.length-1) {
		var history_item = Play.currentGame._.history[++Play.currentIndex]
		History.addHistoryItem(Play.currentGame, history_item)
		Users.getById(Play.currentGame.opponent._id, function(opponent) {
			history_item.viewed = true
			Play.trigger(history_item.type, history_item.state, Users.self, opponent, history_item, function() {
				Play.triggerHistory(fn)
			})
		})
	} else fn() 
}

Play.on = function(eventType, eventHandler) {
	this.handlers[eventType] = this.handlers[eventType] || []
	this.handlers[eventType].push(eventHandler)
}

Play.trigger = function(eventType, game, player, opponent, event, done) {
	if(this.handlers[eventType]) {
		var remaining = 0
		this.handlers[eventType].forEach(function(handler) {
			remaining++
			handler(game, player, opponent, event, function() {
				remaining--
				if(!remaining) {
					Play.timeout(100, done)
				}
			})
		})
	} else {
		Play.timeout(100, done)
	}
}

Play.triggerPlayerEvent = function(eventType, user, event, done) {
	if(this.handlers[eventType]) {
		var remaining = 0
		this.handlers[eventType].forEach(function(handler) {
			remaining++
			handler(user, event, function() {
				remaining--
				if(!remaining) {
					Play.timeout(100, done)
				}
			})
		})
	} else {
		Play.timeout(0, done)
	}
}

Play.emit = function(eventType, eventData, game_id) {
	if(!server.connected) return server.reconnect()

	server.emit('games/event', game_id||Play.currentGame._id, eventType, eventData)
	server.once('games/event/response', function(game) {
		if(game.err) return alert(game.err)
	})
}

Play.timeout = function(ms, fn) {
	var game_id = Play.currentGame._id
	return setTimeout(function() {
		if(game_id == Play.currentGame._id) fn()
	}, ms)
}

Play.animationFrame = function(fn) {
	var game_id = Play.currentGame._id
	return requestAnimationFrame(function() {
		if(game_id == Play.currentGame._id) fn()
	})
}

Play.animate = function(elements, properties, duration, ease, callback, delay) {
	if(duration && typeof duration == 'object') {
		callback = duration.callback
		duration.callback = fn
	}
	require('../utils/animation')(elements, properties, duration, ease, fn, delay)
	function fn() { Play.animationFrame(callback) }
}

Play.countAnimation = function(element, target_amount, duration, fn) {
	var start_amount = parseInt(element.textContent)
	animateAmount(Date.now(), start_amount)
	function animateAmount(time_start, current_amount) {
		element.textContent = current_amount
		if(current_amount == target_amount 
			|| (start_amount > target_amount && current_amount < target_amount) 
			|| (start_amount < target_amount && current_amount > target_amount)) {
			element.textContent = target_amount 
			return fn && fn()
		}  
		Play.animationFrame(function() {
			var now = Date.now()
			  , new_amount = Math.floor(start_amount + (target_amount - start_amount)*(now-time_start)/duration)
			animateAmount(time_start, new_amount)
		})
	}
}

Play.calcSize = function() {
	console.log('oresize')
	var w = Play.container.clientWidth/16
	  , h = Play.container.clientHeight/16
	  , ratio = w/h
	  , classes = ratio > 1 ? 'landscape' : 'portrait'

	for(var i = 10; i <= 120; i+=10) {
		classes += ' w-' + (i < w ? 'gt' : 'lt') + '-' + i
		classes += ' h-' + (i < h ? 'gt' : 'lt') + '-' + i
	}

	Play.container.className = classes
	Play.trigger('resize')

}

function getHistoryIndex(game) {
	var last_index = game._.history.length-1
	  , index = last_index
	  , history_item = game._.history[index]

	while(index > 0 && !history_item.viewed) {
		if(history_item.state.turn != game._.history[index-1].state.turn && index != last_index) {
			break
		} else {
			history_item = game._.history[--index]
		}
	}
	return index > last_index ? last_index : index
}
},{"../models/games":10,"../models/users":11,"../socket.io":20,"../utils/animation":21,"./base":13,"./history":14,"./main":16,"./menu":17}],19:[function(require,module,exports){
var BasePresenter = require('./base')
  , Shop = module.exports = new BasePresenter('#shop')
  , Main = require('./main')
  , Login = require('./login')
  , server = require('../socket.io')
  , Users = require('../models/users')
  , Games = require('../models/games')
  , lang = require('../lang')

Shop._init = function(container) {
	Shop.render('shop', { items: Shop.items }, function(err, html) {
		Shop.container.innerHTML = html
		Shop.listen('button.close', 'click', Shop.hide)
		Shop.listen('TransitionEnd', displayNoneWhenDone)
		Shop.delegate('button.buy', 'click', buyItem)
	})
}

Shop.loadItems = function(items) {
	var items_array = []
	Object.keys(items).forEach(function(key) {
		items_array.push(items[key])
		items[key].key = key
	})
	Shop.items = items_array
}

Shop.show = function() {
	Shop.container.style.display = 'block'
	Shop.container.setAttribute('data-visible', true)
}

Shop.hide = function() {
	Shop.container.removeAttribute('data-visible')
}

function displayNoneWhenDone() {
	if(!parseInt(window.getComputedStyle(Shop.container).opacity)) {
		Shop.container.style.display = 'none'
	}
}

function buyItem() {
	server.emit('shop/buy', { key: this.attr('data-key') })
	server.once('shop/buy/response', function(err, res) {
		console.log(err)
		if(!err) {
			console.log(res)
		}
	})
}
},{"../lang":7,"../models/games":10,"../models/users":11,"../socket.io":20,"./base":13,"./login":15,"./main":16}],20:[function(require,module,exports){
var portDef = /port\=(\d+)/.exec(window.location.hash)
	, config = require('../../config.js')
  , socket = window.socket = module.exports = io.connect('http://'+config.hostname+':'+config.port)

socket.on('disconnect', function() {
	socket.connected = false
	socket.reconnect()
})

socket.on('connect', function() {
	socket.connected = true
})

socket.reconnect = function() {
	var reconnect = confirm('You have disconnected, do you want to try to reconnect?')
	if(reconnect) window.location.reload()
}

console.log('CONNECTED TO:', 'http://'+config.hostname+':'+config.port)

socket.request = function() {
	var lastIndex = arguments.length-1
	  , fn = arguments[lastIndex]
	  , route = arguments[0]
	  , args = Array.prototype.splice.call(arguments, 0, lastIndex)
	socket.emit.apply(socket, args)
	socket.once(route+'/response', fn)
}
},{"../../config.js":4}],21:[function(require,module,exports){
var prefix = '', eventPrefix, endEventName, endAnimationName,
    vendors = { Webkit: 'webkit', Moz: '', O: 'o', ms: 'MS' },
    document = window.document, testEl = document.createElement('div'),
    supportedTransforms = /^((translate|rotate|scale)(X|Y|Z|3d)?|matrix(3d)?|perspective|skew(X|Y)?)$/i,
    transform,
    transitionProperty, transitionDuration, transitionTiming, transitionDelay,
    animationName, animationDuration, animationTiming, animationDelay,
    cssReset = {}

function dasherize(str) { return str.replace(/([a-z])([A-Z])/, '$1-$2').toLowerCase() }
function normalizeEvent(name) { return eventPrefix ? eventPrefix + name : name.toLowerCase() }


Object.keys(vendors).forEach(function(vendorName){
	if(testEl.style[vendorName + 'TransitionProperty'] !== undefined) {
		prefix = '-' + vendorName.toLowerCase() + '-'
		eventPrefix = vendors[vendorName]
		return false
	}
})

transform = prefix + 'transform'
cssReset[transitionProperty = prefix + 'transition-property'] =
cssReset[transitionDuration = prefix + 'transition-duration'] =
cssReset[transitionDelay    = prefix + 'transition-delay'] =
cssReset[transitionTiming   = prefix + 'transition-timing-function'] =
cssReset[animationName      = prefix + 'animation-name'] =
cssReset[animationDuration  = prefix + 'animation-duration'] =
cssReset[animationDelay     = prefix + 'animation-delay'] =
cssReset[animationTiming    = prefix + 'animation-timing-function'] = ''

var fx = {
	  off: (eventPrefix === undefined && testEl.style.transitionProperty === undefined)
	, cssPrefix: prefix
	, transitionEnd: normalizeEvent('TransitionEnd')
	, animationEnd: normalizeEvent('AnimationEnd')
}

var animate = function(elements, properties, duration, ease, callback, delay) {
	if(!elements.length) {
		elements = [elements]
	}
	if(duration && typeof duration == 'object') {
		ease = duration.easing
		callback = duration.complete
		delay = duration.delay
		duration = duration.duration
	}
	if(duration && typeof duration == 'number') {
		duration = duration / 1000
	}
	if(delay) {
		delay = parseFloat(delay) / 1000
	}
	return anim(elements, properties, duration, ease, callback, delay)
}

var anim = function(elements, properties, duration, ease, callback, delay) {
	var key
	  , cssValues = {}
	  , cssProperties
	  , transforms = ''
	  , wrappedCallback
	  , endEvent = fx.transitionEnd
	  , remaining = 0

    if(typeof duration != 'number') duration = 0.4
    if(typeof delay != 'number') delay = 0
    if(fx.off) duration = 0

	if(typeof properties == 'string') {
		// keyframe animation
		cssValues[animationName] = properties
		cssValues[animationDuration] = duration + 's'
		cssValues[animationDelay] = delay + 's'
		cssValues[animationTiming] = (ease || 'linear')
		endEvent = fx.animationEnd
	} else {
		cssProperties = []
		// CSS transitions
		for(key in properties) {
			if (supportedTransforms.test(key)) transforms += key + '(' + properties[key] + ') '
			else cssValues[key] = properties[key], cssProperties.push(dasherize(key))
		}
		if(transforms) {
			cssValues[transform] = transforms, cssProperties.push(transform)
		}
		if(duration > 0 && typeof properties === 'object') {
			cssValues[transitionProperty] = cssProperties.join(', ')
			cssValues[transitionDuration] = duration + 's'
			cssValues[transitionDelay] = delay + 's'
			cssValues[transitionTiming] = (ease || 'linear')
		}
	}

	wrappedCallback = function(event){
		if(typeof event !== 'undefined') {
			// make sure the event didn't bubble from "below"
			if (event.target !== event.currentTarget) return
			$(event.target).unbind(endEvent, wrappedCallback)
		}
		setCss(elements, cssReset)
		callback && callback.call(this)
	}

	if(duration > 0) {
		Array.prototype.forEach.call(elements, function(element) {
			remaining++
			bindOnce(element, endEvent, function() {
				remaining--
				if(!remaining) {
					setCss(elements, cssReset)
					callback && callback()
				}
			})
		})
	} else {
		setTimeout(function() {
			callback && callback()
		}, 0)
	}

	// trigger page reflow so new elements can animate
	elements.length && elements[0].clientLeft

	setCss(elements, cssValues)
}

var setCss = function(elements, properties) {
	var css = ''
	for(key in properties) {
		if(!properties[key] && properties[key] !== 0) {
			Array.prototype.forEach.call(elements, function(element) {
				element.style.removeProperty(dasherize(key)) 
			})
		} else {
			css += dasherize(key) + ':' + properties[key] + ';'
		}
	}
	Array.prototype.forEach.call(elements, function(element) {
		element.style.cssText += ';' + css
	})
}

var bindOnce = function(element, event, callback) {
	element.addEventListener(event, function(e) {
		if (event.target !== event.currentTarget) return
		element.removeEventListener(event, callback, false)
		callback && callback.call(this, e)
	}, false)
}

//remove refence to test div element
testEl = null

module.exports = animate
},{}],22:[function(require,module,exports){
exports.htmlToNode = function(html) {
	var div = document.createElement('div')
	div.innerHTML = html
	return div.children[0]
}
},{}],23:[function(require,module,exports){
// taken from
// https://github.com/linkedin/dustjs-helpers/blob/master/lib/dust-helpers.js

dust.helpers.tap = function( input, chunk, context ){
	// return given input if there is no dust reference to resolve
	var output = input;
	// dust compiles a string/reference such as {foo} to function, 
	if( typeof input === "function"){
		// just a plain function (a.k.a anonymous functions) in the context, not a dust `body` function created by the dust compiler
		if( input.isFunction === true ){
			output = input();
		} else {
			output = '';
			chunk.tap(function(data){
				output += data;
				return '';
			}).render(input, context).untap();
			if( output === '' ){
				output = false;
			}
		}
	}
	return output;
}

dust.helpers.sep = function(chunk, context, bodies) {
	var body = bodies.block;
	if (context.stack.index === context.stack.of - 1) {
		return chunk;
	}
	if(body) {
		return bodies.block(chunk, context);
	}
	else {
		return chunk;
	}
}

dust.helpers.idx = function(chunk, context, bodies) {
	var body = bodies.block;
	if(body) {
		return bodies.block(chunk, context.push(context.stack.index));
	}
	else {
		return chunk;
	}
}

dust.helpers.eq = function(chunk, ctx, bodies, params) {
	var a = dust.helpers.tap(params.this, chunk, ctx)
	  , b = dust.helpers.tap(params.that, chunk, ctx)
    return chunk.map(function(chunk) {
		if(a == b) chunk.render(bodies.block, ctx)
		else if(bodies['else']) chunk.render(bodies['else'], ctx)
		chunk.end()
    })
}
},{}],24:[function(require,module,exports){
(function (global, factory) {
            typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
            typeof define === 'function' && define.amd ? define(factory) :
            global.ramjet = factory()
}(this, function () { 'use strict';

            // for the sake of Safari, may it burn in hell
            var BLACKLIST = ['length', 'parentRule'];

            var styleKeys = undefined;

            if (typeof CSS2Properties !== 'undefined') {
            	// why hello Firefox
            	styleKeys = Object.keys(CSS2Properties.prototype);
            } else {
            	styleKeys = Object.keys(document.createElement('div').style).filter(function (k) {
            		return ! ~BLACKLIST.indexOf(k);
            	});
            }

            var utils_styleKeys = styleKeys;

            var svgns = 'http://www.w3.org/2000/svg';
            var svg = undefined;
            try {
            	svg = document.createElementNS(svgns, 'svg');

            	svg.style.position = 'fixed';
            	svg.style.top = svg.style.left = '0';
            	svg.style.width = svg.style.height = '100%';
            	svg.style.overflow = 'visible';
            	svg.style.pointerEvents = 'none';
            	svg.setAttribute('class', 'mogrify-svg');
            } catch (e) {
            	console.log("The current browser doesn't support SVG");
            }

            var appendedSvg = false;

            function appendSvg() {
            	document.body.appendChild(svg);
            	appendedSvg = true;
            }

            function cloneNode(node, depth, overrideClone) {
            	var clone = overrideClone ? overrideClone(node, depth) : node.cloneNode();

            	if (typeof clone === "undefined") {
            		return;
            	}

            	var style = undefined;
            	var len = undefined;
            	var i = undefined;
            	var clonedNode = undefined;

            	if (node.nodeType === 1) {
            		style = window.getComputedStyle(node);

            		utils_styleKeys.forEach(function (prop) {
            			clone.style[prop] = style[prop];
            		});

            		len = node.childNodes.length;
            		for (i = 0; i < len; i += 1) {
            			clonedNode = cloneNode(node.childNodes[i], depth + 1, overrideClone);
            			if (clonedNode) {
            				clone.appendChild(clonedNode);
            			}
            		}
            	}

            	return clone;
            }

            function wrapNode(node, destinationIsFixed, overrideClone, appendToBody) {
            	var isSvg = node.namespaceURI === svgns;

            	var _node$getBoundingClientRect = node.getBoundingClientRect();

            	var left = _node$getBoundingClientRect.left;
            	var right = _node$getBoundingClientRect.right;
            	var top = _node$getBoundingClientRect.top;
            	var bottom = _node$getBoundingClientRect.bottom;

            	var style = window.getComputedStyle(node);
            	var clone = cloneNode(node, 0, overrideClone);

            	var wrapper = {
            		node: node, clone: clone, isSvg: isSvg,
            		cx: (left + right) / 2,
            		cy: (top + bottom) / 2,
            		width: right - left,
            		height: bottom - top,
            		transform: null,
            		borderRadius: null
            	};

            	if (isSvg) {
            		var ctm = node.getScreenCTM();
            		wrapper.transform = 'matrix(' + [ctm.a, ctm.b, ctm.c, ctm.d, ctm.e, ctm.f].join(',') + ')';
            		wrapper.borderRadius = [0, 0, 0, 0];

            		svg.appendChild(clone);
            	} else {

            		if (destinationIsFixed) {
            			// position relative to the viewport
            			clone.style.position = 'fixed';
            			clone.style.top = top - parseInt(style.marginTop, 10) + 'px';
            			clone.style.left = left - parseInt(style.marginLeft, 10) + 'px';
            		} else {
            			var offsetParent = node.offsetParent;

            			if (offsetParent === null || offsetParent === document.body || appendToBody) {
            				// parent is fixed, or I want to append the node to the body
            				// position relative to the document
            				var docElem = document.documentElement;
            				clone.style.position = 'absolute';
            				clone.style.top = top + window.pageYOffset - docElem.clientTop - parseInt(style.marginTop, 10) + 'px';
            				clone.style.left = left + window.pageXOffset - docElem.clientLeft - parseInt(style.marginLeft, 10) + 'px';
            			} else {
            				//position relative to the parent
            				var offsetParentStyle = window.getComputedStyle(offsetParent);
            				var offsetParentBcr = offsetParent.getBoundingClientRect();

            				clone.style.position = 'absolute';
            				clone.style.top = top - parseInt(style.marginTop, 10) - (offsetParentBcr.top - parseInt(offsetParentStyle.marginTop, 10)) + 'px';
            				clone.style.left = left - parseInt(style.marginLeft, 10) - (offsetParentBcr.left - parseInt(offsetParentStyle.marginLeft, 10)) + 'px';
            			}
            		}

            		wrapper.transform = ''; // TODO...?
            		wrapper.borderRadius = [parseFloat(style.borderTopLeftRadius), parseFloat(style.borderTopRightRadius), parseFloat(style.borderBottomRightRadius), parseFloat(style.borderBottomLeftRadius)];

            		if (appendToBody) {
            			document.body.appendChild(clone);
            		} else {
            			node.parentNode.appendChild(clone);
            		}
            	}

            	return wrapper;
            }

            function hideNode(node) {
            	node.__ramjetOriginalTransition__ = node.style.transition;
            	node.style.transition = '';

            	node.style.opacity = 0;
            }

            function showNode(node) {
            	node.style.transition = '';
            	node.style.opacity = 1;

            	if (node.__ramjetOriginalTransition__) {
            		setTimeout(function () {
            			node.style.transition = node.__ramjetOriginalTransition__;
            		});
            	}
            }

            function isNodeFixed(node) {
            	while (node !== null) {
            		if (window.getComputedStyle(node).position === "fixed") {
            			return true;
            		}
            		node = node.namespaceURI === svgns ? node.parentNode : node.offsetParent;
            	}
            	return false;
            }

            var utils_getTransform = getTransform;

            function getTransform(isSvg, cx, cy, dx, dy, dsx, dsy, t, t_scale) {
            	var transform = isSvg ? "translate(" + cx + " " + cy + ") scale(" + (1 + t_scale * dsx) + " " + (1 + t_scale * dsy) + ") translate(" + -cx + " " + -cy + ") translate(" + t * dx + " " + t * dy + ")" : "translate(" + t * dx + "px," + t * dy + "px) scale(" + (1 + t_scale * dsx) + "," + (1 + t_scale * dsy) + ")";

            	return transform;
            }

            var utils_getBorderRadius = getBorderRadius;

            function getBorderRadius(a, b, dsx, dsy, t) {
            	var sx = 1 + t * dsx;
            	var sy = 1 + t * dsy;

            	return a.map(function (from, i) {
            		var to = b[i];

            		var rx = (from + t * (to - from)) / sx;
            		var ry = (from + t * (to - from)) / sy;

            		return rx + "px " + ry + "px";
            	});
            }

            function linear(pos) {
            	return pos;
            }

            function easeIn(pos) {
            	return Math.pow(pos, 3);
            }

            function easeOut(pos) {
            	return Math.pow(pos - 1, 3) + 1;
            }

            function easeInOut(pos) {
            	if ((pos /= 0.5) < 1) {
            		return 0.5 * Math.pow(pos, 3);
            	}

            	return 0.5 * (Math.pow(pos - 2, 3) + 2);
            }

            var rAF = window.requestAnimationFrame || window.webkitRequestAnimationFrame || function (fn) {
                        return setTimeout(fn, 16);
            };

            var utils_rAF = rAF;

            function transformers_TimerTransformer___classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

            var transformers_TimerTransformer__TimerTransformer = function TimerTransformer(from, to, options) {
            	transformers_TimerTransformer___classCallCheck(this, transformers_TimerTransformer__TimerTransformer);

            	var dx = to.cx - from.cx;
            	var dy = to.cy - from.cy;

            	var dsxf = to.width / from.width - 1;
            	var dsyf = to.height / from.height - 1;

            	var dsxt = from.width / to.width - 1;
            	var dsyt = from.height / to.height - 1;

            	var startTime = Date.now();
            	var duration = options.duration || 400;
            	var easing = options.easing || linear;
            	var easingScale = options.easingScale || easing;

            	function tick() {
            		var timeNow = Date.now();
            		var elapsed = timeNow - startTime;

            		if (elapsed > duration) {
            			from.clone.parentNode.removeChild(from.clone);
            			to.clone.parentNode.removeChild(to.clone);

            			if (options.done) {
            				options.done();
            			}

            			return;
            		}

            		var t = easing(elapsed / duration);
            		var t_scale = easingScale(elapsed / duration);

            		// opacity
            		from.clone.style.opacity = 1 - t;
            		to.clone.style.opacity = t;

            		// border radius
            		var fromBorderRadius = utils_getBorderRadius(from.borderRadius, to.borderRadius, dsxf, dsyf, t);
            		var toBorderRadius = utils_getBorderRadius(to.borderRadius, from.borderRadius, dsxt, dsyt, 1 - t);

            		applyBorderRadius(from.clone, fromBorderRadius);
            		applyBorderRadius(to.clone, toBorderRadius);

            		var cx = from.cx + dx * t;
            		var cy = from.cy + dy * t;

            		var fromTransform = utils_getTransform(from.isSvg, cx, cy, dx, dy, dsxf, dsyf, t, t_scale) + ' ' + from.transform;
            		var toTransform = utils_getTransform(to.isSvg, cx, cy, -dx, -dy, dsxt, dsyt, 1 - t, 1 - t_scale) + ' ' + to.transform;

            		if (from.isSvg) {
            			from.clone.setAttribute('transform', fromTransform);
            		} else {
            			from.clone.style.transform = from.clone.style.webkitTransform = from.clone.style.msTransform = fromTransform;
            		}

            		if (to.isSvg) {
            			to.clone.setAttribute('transform', toTransform);
            		} else {
            			to.clone.style.transform = to.clone.style.webkitTransform = to.clone.style.msTransform = toTransform;
            		}

            		utils_rAF(tick);
            	}

            	tick();
            };

            var transformers_TimerTransformer = transformers_TimerTransformer__TimerTransformer;

            function applyBorderRadius(node, borderRadius) {
            	node.style.borderTopLeftRadius = borderRadius[0];
            	node.style.borderTopRightRadius = borderRadius[1];
            	node.style.borderBottomRightRadius = borderRadius[2];
            	node.style.borderBottomLeftRadius = borderRadius[3];
            }

            var div = document.createElement('div');

            var keyframesSupported = true;
            var TRANSFORM = undefined;
            var KEYFRAMES = undefined;
            var ANIMATION_DIRECTION = undefined;
            var ANIMATION_DURATION = undefined;
            var ANIMATION_ITERATION_COUNT = undefined;
            var ANIMATION_NAME = undefined;
            var ANIMATION_TIMING_FUNCTION = undefined;
            var ANIMATION_END = undefined;

            // We have to browser-sniff for IE11, because it was apparently written
            // by a barrel of stoned monkeys - http://jsfiddle.net/rich_harris/oquLu2qL/

            // http://stackoverflow.com/questions/17907445/how-to-detect-ie11
            var isIe11 = !window.ActiveXObject && 'ActiveXObject' in window;

            if (!isIe11 && ('transform' in div.style || 'webkitTransform' in div.style) && ('animation' in div.style || 'webkitAnimation' in div.style)) {
            	keyframesSupported = true;

            	TRANSFORM = 'transform' in div.style ? 'transform' : '-webkit-transform';

            	if ('animation' in div.style) {
            		KEYFRAMES = '@keyframes';

            		ANIMATION_DIRECTION = 'animationDirection';
            		ANIMATION_DURATION = 'animationDuration';
            		ANIMATION_ITERATION_COUNT = 'animationIterationCount';
            		ANIMATION_NAME = 'animationName';
            		ANIMATION_TIMING_FUNCTION = 'animationTimingFunction';

            		ANIMATION_END = 'animationend';
            	} else {
            		KEYFRAMES = '@-webkit-keyframes';

            		ANIMATION_DIRECTION = 'webkitAnimationDirection';
            		ANIMATION_DURATION = 'webkitAnimationDuration';
            		ANIMATION_ITERATION_COUNT = 'webkitAnimationIterationCount';
            		ANIMATION_NAME = 'webkitAnimationName';
            		ANIMATION_TIMING_FUNCTION = 'webkitAnimationTimingFunction';

            		ANIMATION_END = 'webkitAnimationEnd';
            	}
            } else {
            	keyframesSupported = false;
            }

            function transformers_KeyframeTransformer___classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

            var transformers_KeyframeTransformer__KeyframeTransformer = function KeyframeTransformer(from, to, options) {
            	transformers_KeyframeTransformer___classCallCheck(this, transformers_KeyframeTransformer__KeyframeTransformer);

            	var _getKeyframes = getKeyframes(from, to, options);

            	var fromKeyframes = _getKeyframes.fromKeyframes;
            	var toKeyframes = _getKeyframes.toKeyframes;

            	var fromId = '_' + ~ ~(Math.random() * 1000000);
            	var toId = '_' + ~ ~(Math.random() * 1000000);

            	var css = KEYFRAMES + ' ' + fromId + ' { ' + fromKeyframes + ' } ' + KEYFRAMES + ' ' + toId + ' { ' + toKeyframes + ' }';
            	var dispose = addCss(css);

            	from.clone.style[ANIMATION_DIRECTION] = 'alternate';
            	from.clone.style[ANIMATION_DURATION] = options.duration / 1000 + 's';
            	from.clone.style[ANIMATION_ITERATION_COUNT] = 1;
            	from.clone.style[ANIMATION_NAME] = fromId;
            	from.clone.style[ANIMATION_TIMING_FUNCTION] = 'linear';

            	to.clone.style[ANIMATION_DIRECTION] = 'alternate';
            	to.clone.style[ANIMATION_DURATION] = options.duration / 1000 + 's';
            	to.clone.style[ANIMATION_ITERATION_COUNT] = 1;
            	to.clone.style[ANIMATION_NAME] = toId;
            	to.clone.style[ANIMATION_TIMING_FUNCTION] = 'linear';

            	var fromDone = undefined;
            	var toDone = undefined;

            	function done() {
            		if (fromDone && toDone) {
            			from.clone.parentNode.removeChild(from.clone);
            			to.clone.parentNode.removeChild(to.clone);

            			if (options.done) options.done();

            			dispose();
            		}
            	}

            	from.clone.addEventListener(ANIMATION_END, function () {
            		fromDone = true;
            		done();
            	});

            	to.clone.addEventListener(ANIMATION_END, function () {
            		toDone = true;
            		done();
            	});
            };

            var transformers_KeyframeTransformer = transformers_KeyframeTransformer__KeyframeTransformer;

            function addCss(css) {
            	var styleElement = document.createElement('style');
            	styleElement.type = 'text/css';

            	var head = document.getElementsByTagName('head')[0];

            	// Internet Exploder won't let you use styleSheet.innerHTML - we have to
            	// use styleSheet.cssText instead
            	var styleSheet = styleElement.styleSheet;

            	if (styleSheet) {
            		styleSheet.cssText = css;
            	} else {
            		styleElement.innerHTML = css;
            	}

            	head.appendChild(styleElement);

            	return function () {
            		return head.removeChild(styleElement);
            	};
            }

            function getKeyframes(from, to, options) {
            	var dx = to.cx - from.cx;
            	var dy = to.cy - from.cy;

            	var dsxf = to.width / from.width - 1;
            	var dsyf = to.height / from.height - 1;

            	var dsxt = from.width / to.width - 1;
            	var dsyt = from.height / to.height - 1;

            	var easing = options.easing || linear;
            	var easingScale = options.easingScale || easing;

            	var numFrames = options.duration / 50; // one keyframe per 50ms is probably enough... this may prove not to be the case though

            	var fromKeyframes = [];
            	var toKeyframes = [];
            	var i;

            	function addKeyframes(pc, t, t_scale) {
            		var cx = from.cx + dx * t;
            		var cy = from.cy + dy * t;

            		var fromBorderRadius = utils_getBorderRadius(from.borderRadius, to.borderRadius, dsxf, dsyf, t);
            		var toBorderRadius = utils_getBorderRadius(to.borderRadius, from.borderRadius, dsxt, dsyt, 1 - t);

            		var fromTransform = utils_getTransform(false, cx, cy, dx, dy, dsxf, dsyf, t, t_scale) + ' ' + from.transform;
            		var toTransform = utils_getTransform(false, cx, cy, -dx, -dy, dsxt, dsyt, 1 - t, 1 - t_scale) + ' ' + to.transform;

            		fromKeyframes.push('\n\t\t\t' + pc + '% {\n\t\t\t\topacity: ' + (1 - t) + ';\n\t\t\t\tborder-top-left-radius: ' + fromBorderRadius[0] + ';\n\t\t\t\tborder-top-right-radius: ' + fromBorderRadius[1] + ';\n\t\t\t\tborder-bottom-right-radius: ' + fromBorderRadius[2] + ';\n\t\t\t\tborder-bottom-left-radius: ' + fromBorderRadius[3] + ';\n\t\t\t\t' + TRANSFORM + ': ' + fromTransform + ';\n\t\t\t}');

            		toKeyframes.push('\n\t\t\t' + pc + '% {\n\t\t\t\topacity: ' + t + ';\n\t\t\t\tborder-top-left-radius: ' + toBorderRadius[0] + ';\n\t\t\t\tborder-top-right-radius: ' + toBorderRadius[1] + ';\n\t\t\t\tborder-bottom-right-radius: ' + toBorderRadius[2] + ';\n\t\t\t\tborder-bottom-left-radius: ' + toBorderRadius[3] + ';\n\t\t\t\t' + TRANSFORM + ': ' + toTransform + ';\n\t\t\t}');
            	}

            	for (i = 0; i < numFrames; i += 1) {
            		var pc = 100 * (i / numFrames);
            		var t = easing(i / numFrames);
            		var t_scale = easingScale(i / numFrames);

            		addKeyframes(pc, t, t_scale);
            	}

            	addKeyframes(100, 1, 1);

            	fromKeyframes = fromKeyframes.join('\n');
            	toKeyframes = toKeyframes.join('\n');

            	return { fromKeyframes: fromKeyframes, toKeyframes: toKeyframes };
            }

            var ramjet = {
            	transform: function (fromNode, toNode) {
            		var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

            		if (typeof options === 'function') {
            			options = { done: options };
            		}

            		if (!('duration' in options)) {
            			options.duration = 400;
            		}

            		var appendToBody = !!options.appendToBody;
            		var destinationIsFixed = isNodeFixed(toNode);
            		var from = wrapNode(fromNode, destinationIsFixed, options.overrideClone, appendToBody);
            		var to = wrapNode(toNode, destinationIsFixed, options.overrideClone, appendToBody);

            		if (from.isSvg || to.isSvg && !appendedSvg) {
            			appendSvg();
            		}

            		if (!keyframesSupported || options.useTimer || from.isSvg || to.isSvg) {
            			return new transformers_TimerTransformer(from, to, options);
            		} else {
            			return new transformers_KeyframeTransformer(from, to, options);
            		}
            	},

            	hide: function () {
            		for (var _len = arguments.length, nodes = Array(_len), _key = 0; _key < _len; _key++) {
            			nodes[_key] = arguments[_key];
            		}

            		nodes.forEach(hideNode);
            	},

            	show: function () {
            		for (var _len2 = arguments.length, nodes = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            			nodes[_key2] = arguments[_key2];
            		}

            		nodes.forEach(showNode);
            	},

            	// expose some basic easing functions
            	linear: linear, easeIn: easeIn, easeOut: easeOut, easeInOut: easeInOut
            };

            return ramjet;

}));
},{}],25:[function(require,module,exports){
module.exports.monkey_chicken = {
	'GrCh': { 
		deck_type: 'monkey_chicken',
		id: 1,
		order: 10,
		name: 'Greedy Chicken',
		count: 4,
		player: { draw: 2 },
		img: 'monkey_chicken/greedy_chicken.png'
	}, 
	'PoBa': { 
		deck_type: 'monkey_chicken',
		id: 2,
		order: 20,
		name: 'Poisonous Baboon',
		count: 4,
		opponent: { poison: 3 },
		img: 'monkey_chicken/poisonous_baboon.png'
	},
	'HeHe': { 
		deck_type: 'monkey_chicken',
		id: 3,
		order: 30,
		name: 'Helpful Hen',
		count: 4,
		player: { health: 15 },
		img: 'monkey_chicken/helpful_hen.png'
	},
	'KaMo': { 
		deck_type: 'monkey_chicken',
		id: 4,
		order: 40,
		name: 'Kamikaze Monkey',
		count: 1,
		player: { health: -50 },
		opponent: { health: -50 },
		img: 'monkey_chicken/kamikaze_monkey.png'
	}, 
	'InCh': { 
		deck_type: 'monkey_chicken',
		id: 5,
		order: 50,
		name: 'Interrupting Chicken',
		count: 2,
		bonus: true,
		player: { 
			unpoison: true, 
			undamage: true 
		},
		img: 'monkey_chicken/interrupting_chicken.png'
	},
	'MC3': {
		deck_type: 'monkey_chicken',
		id: 6,
		order: 11,
		name: '3 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -3 },
		img: 'monkey_chicken/3.png'
	},
	'MC4': {
		deck_type: 'monkey_chicken',
		id: 7,
		order: 12,
		name: '4 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -4 },
		img: 'monkey_chicken/4.png'
	},
	'MC5': {
		deck_type: 'monkey_chicken',
		id: 8,
		order: 13,
		name: '5 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -5 },
		img: 'monkey_chicken/5.png'
	},
	'MC6': {
		deck_type: 'monkey_chicken',
		id: 9,
		order: 14,
		name: '6 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -6 },
		img: 'monkey_chicken/6.png'
	},
	'MC7': {
		deck_type: 'monkey_chicken',
		id: 10,
		order: 15,
		name: '7 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -7 },
		img: 'monkey_chicken/7.png'
	},
	'MC8': {
		deck_type: 'monkey_chicken',
		id: 11,
		order: 16,
		name: '8 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -8 },
		img: 'monkey_chicken/8.png'
	},
	'MC9': {
		deck_type: 'monkey_chicken',
		id: 12,
		order: 17,
		name: '9 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -9 },
		img: 'monkey_chicken/9.png'
	},
	'MC10': {
		deck_type: 'monkey_chicken',
		id: 13,
		order: 18,
		name: '10 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -10 },
		img: 'monkey_chicken/10.png'
	},
	'MoCh': {
		deck_type: 'monkey_chicken',
		id: 100,
		order: 100,
		name: 'The Legendary MonkeyChicken',
		count: 1,
		unrestrictable: true,
		opponent: { health: -40 },
		chance: 1000,
		img: 'monkey_chicken/10.png'
	},
	'Back': {
		deck_type: 'monkey_chicken',
		id: 41,
		count: 0,
		name: 'Card Back',
		img: 'monkey_chicken/back.png'
	}
}

module.exports.camel_toad = {
	'CaCr': { 
		deck_type: 'camel_toad',
		id: 14,
		order: 10,
		name: 'Camel Crook',
		count: 4,
		opponent: { steal_cards: 1 },
		img: 'camel_toad/camel_crook.png'
	},
	'TrTo': { 
		deck_type: 'camel_toad',
		id: 15,
		order: 20,
		name: 'Treacherous Toad',
		count: 4,
		stackable: true,
		player: { health: -7 },
		opponent: { health: -15 },
		img: 'camel_toad/treacherous_toad.png'
	},
	'FrFr': { 
		deck_type: 'camel_toad',
		id: 16,
		order: 30,
		name: 'Friendly Frog',
		count: 4,
		player: {
			health: 5,
			energize: 3
		},
		img: 'camel_toad/friendly_frog.png'
	}, 
	'FraFr': {
		deck_type: 'camel_toad',
		id: 17,
		order: 40,
		name: 'Fracking Frog',
		count: 1,
		player: {
			poison: 3
		},
		opponent: {
			poison: 5,
			health: -5
		},
		img: 'camel_toad/fracking_frog.png'
	},
	'CoCa': { 
		deck_type: 'camel_toad',
		id: 18,
		order: 50,
		name: 'Copy Camel',
		count: 0,
		opponent: { copy: { turns_ago: 1 } },
		img: 'camel_toad/copy_camel.png'
	},
	'CT3': {
		deck_type: 'camel_toad',
		id: 6,
		order: 11,
		name: '3 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -3 },
		img: 'camel_toad/3.png'
	},
	'CT4': {
		deck_type: 'camel_toad',
		id: 7,
		order: 12,
		name: '4 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -4 },
		img: 'camel_toad/4.png'
	},
	'CT5': {
		deck_type: 'camel_toad',
		id: 8,
		order: 13,
		name: '5 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -5 },
		img: 'camel_toad/5.png'
	},
	'CT6': {
		deck_type: 'camel_toad',
		id: 9,
		order: 14,
		name: '6 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -6 },
		img: 'camel_toad/6.png'
	},
	'CT7': {
		deck_type: 'camel_toad',
		id: 10,
		order: 15,
		name: '7 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -7 },
		img: 'camel_toad/7.png'
	},
	'CT8': {
		deck_type: 'camel_toad',
		id: 11,
		order: 16,
		name: '8 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -8 },
		img: 'camel_toad/8.png'
	},
	'CT9': {
		deck_type: 'camel_toad',
		id: 12,
		order: 17,
		name: '9 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -9 },
		img: 'camel_toad/9.png'
	},
	'CT10': {
		deck_type: 'camel_toad',
		id: 13,
		order: 18,
		name: '10 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -10 },
		img: 'camel_toad/10.png'
	},
	'CaTo': {
		deck_type: 'camel_toad',
		id: 100,
		order: 100,
		name: 'The Legendary CamelToad',
		count: 1,
		unrestrictable: true,
		opponent: { health: -40 },
		chance: 1000000,
		img: 'monkey_chicken/10.png'
	},
	'Back': {
		id: 42,
		count: 0,
		name: 'Card Back',
		img: 'camel_toad/back.png'
	}
}

module.exports.snail_whale = {
	'SneSn': {
		deck_type: 'snail_whale',
		id: 27,
		order: 10,
		name: 'Sneaky Snail',
		count: 4,
		player: { draw: 1 },
		opponent: { poison: 1 },
		img: 'snail_whale/sneaky_snail.png'
	}, 
	'WhoWo': { 
		deck_type: 'snail_whale',
		id: 28,
		order: 20,
		name: 'Whale of Wonder',
		count: 4,
		player: { 
			health: -10,
			energize: 3
		},
		opponent: { poison: 3 },
		img: 'snail_whale/whale_of_wonder.png'
	},
	'WaWh': { 
		deck_type: 'snail_whale',
		id: 29,
		order: 30,
		name: 'Wayward Whale',
		count: 4,
		player: { discard: 2 },
		opponent: { health: -25 },
		img: 'snail_whale/wayward_whale.png'
	},
	'SnSnr': {
		deck_type: 'snail_whale',
		id: 30,
		order: 40,
		name: 'Snail Snare',
		count: 1,
		opponent: { restrict: 2 },
		img: 'snail_whale/snail_snare.png'
	},
	'FlSn': { 
		deck_type: 'snail_whale',
		id: 31,
		order: 50,
		name: 'Flailing Snail',
		count: 2,
		bonus: true,
		opponent: { health: -10 },
		img: 'snail_whale/flailing_snail.png'
	},
	'SW3': {
		deck_type: 'snail_whale',
		id: 6,
		order: 11,
		name: '3 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -3 },
		img: 'snail_whale/3.png'
	},
	'SW4': {
		deck_type: 'snail_whale',
		id: 7,
		order: 12,
		name: '4 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -4 },
		img: 'snail_whale/4.png'
	},
	'SW5': {
		deck_type: 'snail_whale',
		id: 8,
		order: 13,
		name: '5 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -5 },
		img: 'snail_whale/5.png'
	},
	'SW6': {
		deck_type: 'snail_whale',
		id: 9,
		order: 14,
		name: '6 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -6 },
		img: 'snail_whale/6.png'
	},
	'SW7': {
		deck_type: 'snail_whale',
		id: 10,
		order: 15,
		name: '7 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -7 },
		img: 'snail_whale/7.png'
	},
	'SW8': {
		deck_type: 'snail_whale',
		id: 11,
		order: 16,
		name: '8 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -8 },
		img: 'snail_whale/8.png'
	},
	'SW9': {
		deck_type: 'snail_whale',
		id: 12,
		order: 17,
		name: '9 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -9 },
		img: 'snail_whale/9.png'
	},
	'SW10': {
		deck_type: 'snail_whale',
		id: 13,
		order: 18,
		name: '10 Damage',
		count: 4,
		stackable: true,
		unrestrictable: true,
		opponent: { health: -10 },
		img: 'snail_whale/10.png'
	},
	'CaTo': {
		deck_type: 'snail_whale',
		id: 300,
		order: 100,
		name: 'The Legendary SnailWhale',
		count: 1,
		unrestrictable: true,
		opponent: { health: -40 },
		chance: 1000000,
		img: 'monkey_chicken/10.png'
	},
	'Back': {
		deck_type: 'snail_whale',
		id: 42,
		count: 0,
		name: 'Card Back',
		img: 'snail_whale/back.png'
	}
}
},{}],26:[function(require,module,exports){
// module.exports = {
//     baby_chicken: {
//     	name: 'Baby Chicken'
//       , cost: 10
//     }
//   , baby_monkey: {
//         name: 'Baby Monkey'
//       , cost: 10
//     }
//   , greedier_chicken: {
//         name: 'Greedier Chicken'
//       , cost: 50
//     }
//   , camel_toad: {
//   		name: 'CamelToad'
//   	  , cost: 1000
//     }
//   , snail_whale: {
//   		name: 'SnailWhale'
//   	  , cost: 1000
//     }
// }
},{}],27:[function(require,module,exports){
var process=require("__browserify_process");if (!process.EventEmitter) process.EventEmitter = function () {};

var EventEmitter = exports.EventEmitter = process.EventEmitter;
var isArray = typeof Array.isArray === 'function'
    ? Array.isArray
    : function (xs) {
        return Object.prototype.toString.call(xs) === '[object Array]'
    }
;
function indexOf (xs, x) {
    if (xs.indexOf) return xs.indexOf(x);
    for (var i = 0; i < xs.length; i++) {
        if (x === xs[i]) return i;
    }
    return -1;
}

// By default EventEmitters will print a warning if more than
// 10 listeners are added to it. This is a useful default which
// helps finding memory leaks.
//
// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
var defaultMaxListeners = 10;
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!this._events) this._events = {};
  this._events.maxListeners = n;
};


EventEmitter.prototype.emit = function(type) {
  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events || !this._events.error ||
        (isArray(this._events.error) && !this._events.error.length))
    {
      if (arguments[1] instanceof Error) {
        throw arguments[1]; // Unhandled 'error' event
      } else {
        throw new Error("Uncaught, unspecified 'error' event.");
      }
      return false;
    }
  }

  if (!this._events) return false;
  var handler = this._events[type];
  if (!handler) return false;

  if (typeof handler == 'function') {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        var args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
    return true;

  } else if (isArray(handler)) {
    var args = Array.prototype.slice.call(arguments, 1);

    var listeners = handler.slice();
    for (var i = 0, l = listeners.length; i < l; i++) {
      listeners[i].apply(this, args);
    }
    return true;

  } else {
    return false;
  }
};

// EventEmitter is defined in src/node_events.cc
// EventEmitter.prototype.emit() is also defined there.
EventEmitter.prototype.addListener = function(type, listener) {
  if ('function' !== typeof listener) {
    throw new Error('addListener only takes instances of Function');
  }

  if (!this._events) this._events = {};

  // To avoid recursion in the case that type == "newListeners"! Before
  // adding it to the listeners, first emit "newListeners".
  this.emit('newListener', type, listener);

  if (!this._events[type]) {
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  } else if (isArray(this._events[type])) {

    // Check for listener leak
    if (!this._events[type].warned) {
      var m;
      if (this._events.maxListeners !== undefined) {
        m = this._events.maxListeners;
      } else {
        m = defaultMaxListeners;
      }

      if (m && m > 0 && this._events[type].length > m) {
        this._events[type].warned = true;
        console.error('(node) warning: possible EventEmitter memory ' +
                      'leak detected. %d listeners added. ' +
                      'Use emitter.setMaxListeners() to increase limit.',
                      this._events[type].length);
        console.trace();
      }
    }

    // If we've already got an array, just append.
    this._events[type].push(listener);
  } else {
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  var self = this;
  self.on(type, function g() {
    self.removeListener(type, g);
    listener.apply(this, arguments);
  });

  return this;
};

EventEmitter.prototype.removeListener = function(type, listener) {
  if ('function' !== typeof listener) {
    throw new Error('removeListener only takes instances of Function');
  }

  // does not use listeners(), so no side effect of creating _events[type]
  if (!this._events || !this._events[type]) return this;

  var list = this._events[type];

  if (isArray(list)) {
    var i = indexOf(list, listener);
    if (i < 0) return this;
    list.splice(i, 1);
    if (list.length == 0)
      delete this._events[type];
  } else if (this._events[type] === listener) {
    delete this._events[type];
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  if (arguments.length === 0) {
    this._events = {};
    return this;
  }

  // does not use listeners(), so no side effect of creating _events[type]
  if (type && this._events && this._events[type]) this._events[type] = null;
  return this;
};

EventEmitter.prototype.listeners = function(type) {
  if (!this._events) this._events = {};
  if (!this._events[type]) this._events[type] = [];
  if (!isArray(this._events[type])) {
    this._events[type] = [this._events[type]];
  }
  return this._events[type];
};

EventEmitter.listenerCount = function(emitter, type) {
  var ret;
  if (!emitter._events || !emitter._events[type])
    ret = 0;
  else if (typeof emitter._events[type] === 'function')
    ret = 1;
  else
    ret = emitter._events[type].length;
  return ret;
};

},{"__browserify_process":28}],28:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            var source = ev.source;
            if ((source === window || source === null) && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

process.binding = function (name) {
    throw new Error('process.binding is not supported');
}

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}]},{},[1])
//@ sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcY2xpZW50XFxpbmRleC5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcY2xpZW50XFx1dGlsc1xcbXVsdGlwbGUuZHVzdC5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxjbGllbnQuanMiLCJDOlxcVXNlcnNcXHJoYnJpXzAwMFxcRGVza3RvcFxccHJvamVjdHNcXG1vbmtleWNoaWNrZW5cXG5vZGVfbW9kdWxlc1xcb25lLW9uLW9uZVxcY29uZmlnLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxhcHAuanMiLCJDOlxcVXNlcnNcXHJoYnJpXzAwMFxcRGVza3RvcFxccHJvamVjdHNcXG1vbmtleWNoaWNrZW5cXG5vZGVfbW9kdWxlc1xcb25lLW9uLW9uZVxcZnJvbnRlbmRcXHNjcmlwdHNcXGxhbmdcXGVuZ2xpc2guanMiLCJDOlxcVXNlcnNcXHJoYnJpXzAwMFxcRGVza3RvcFxccHJvamVjdHNcXG1vbmtleWNoaWNrZW5cXG5vZGVfbW9kdWxlc1xcb25lLW9uLW9uZVxcZnJvbnRlbmRcXHNjcmlwdHNcXGxhbmdcXGluZGV4LmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxsYW5nXFx3ZWlyZC5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxmcm9udGVuZFxcc2NyaXB0c1xcbW9kZWxzXFxiYXNlLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxtb2RlbHNcXGdhbWVzLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxtb2RlbHNcXHVzZXJzLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxwcmVzZW50ZXJzXFxhY2NvdW50LmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxwcmVzZW50ZXJzXFxiYXNlLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxwcmVzZW50ZXJzXFxoaXN0b3J5LmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFxwcmVzZW50ZXJzXFxsb2dpbi5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxmcm9udGVuZFxcc2NyaXB0c1xccHJlc2VudGVyc1xcbWFpbi5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxmcm9udGVuZFxcc2NyaXB0c1xccHJlc2VudGVyc1xcbWVudS5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxmcm9udGVuZFxcc2NyaXB0c1xccHJlc2VudGVyc1xccGxheS5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxmcm9udGVuZFxcc2NyaXB0c1xccHJlc2VudGVyc1xcc2hvcC5qcyIsIkM6XFxVc2Vyc1xccmhicmlfMDAwXFxEZXNrdG9wXFxwcm9qZWN0c1xcbW9ua2V5Y2hpY2tlblxcbm9kZV9tb2R1bGVzXFxvbmUtb24tb25lXFxmcm9udGVuZFxcc2NyaXB0c1xcc29ja2V0LmlvLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFx1dGlsc1xcYW5pbWF0aW9uLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFx1dGlsc1xcZG9tLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXG9uZS1vbi1vbmVcXGZyb250ZW5kXFxzY3JpcHRzXFx1dGlsc1xcZHVzdC1oZWxwZXJzLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxub2RlX21vZHVsZXNcXHJhbWpldFxcZGlzdFxccmFtamV0LmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxtb25rZXljaGlja2VuXFxzZXJ2ZXJcXHN0YXRpY1xcZGVja3MuanMiLCJDOlxcVXNlcnNcXHJoYnJpXzAwMFxcRGVza3RvcFxccHJvamVjdHNcXG1vbmtleWNoaWNrZW5cXHNlcnZlclxcc3RhdGljXFxzaG9wLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxvbmUtb24tb25lXFxub2RlX21vZHVsZXNcXGJyb3dzZXJpZnlcXG5vZGVfbW9kdWxlc1xcYnJvd3Nlci1idWlsdGluc1xcYnVpbHRpblxcZXZlbnRzLmpzIiwiQzpcXFVzZXJzXFxyaGJyaV8wMDBcXERlc2t0b3BcXHByb2plY3RzXFxvbmUtb24tb25lXFxub2RlX21vZHVsZXNcXGJyb3dzZXJpZnlcXG5vZGVfbW9kdWxlc1xcaW5zZXJ0LW1vZHVsZS1nbG9iYWxzXFxub2RlX21vZHVsZXNcXHByb2Nlc3NcXGJyb3dzZXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbGFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdEtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcldBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbGhCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbGVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbE1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsidmFyIE9uZU9uT25lID0gcmVxdWlyZSgnb25lLW9uLW9uZS9jbGllbnQnKVxyXG4gICwgRGVja3MgPSByZXF1aXJlKCcuLi9zZXJ2ZXIvc3RhdGljL2RlY2tzLmpzJylcclxuICAsIFNob3AgPSByZXF1aXJlKCcuLi9zZXJ2ZXIvc3RhdGljL3Nob3AuanMnKVxyXG4gICwgUGxheSA9IHdpbmRvdy5QbGF5ID0gT25lT25PbmUuUGxheUhhbmRsZXJcclxuICAsIFVzZXIgPSBPbmVPbk9uZS5Vc2VyTW9kZWxcclxuICAsIHJhbWpldCA9IHJlcXVpcmUoJ3JhbWpldCcpXHJcblxyXG5yZXF1aXJlKCcuL3V0aWxzL211bHRpcGxlLmR1c3QuanMnKVxyXG5cclxuV2ViRm9udENvbmZpZyA9IHtcclxuXHRnb29nbGU6IHsgZmFtaWxpZXM6IFsgJ01jTGFyZW46OmxhdGluJyBdIH1cclxufVxyXG5cclxuOyhmdW5jdGlvbigpIHtcclxuXHR2YXIgd2YgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuXHR3Zi5zcmMgPSAoJ2h0dHBzOicgPT0gZG9jdW1lbnQubG9jYXRpb24ucHJvdG9jb2wgPyAnaHR0cHMnIDogJ2h0dHAnKSArXHJcblx0JzovL2FqYXguZ29vZ2xlYXBpcy5jb20vYWpheC9saWJzL3dlYmZvbnQvMS93ZWJmb250LmpzJztcclxuXHR3Zi50eXBlID0gJ3RleHQvamF2YXNjcmlwdCc7XHJcblx0d2YuYXN5bmMgPSAndHJ1ZSc7XHJcblx0dmFyIHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF07XHJcblx0cy5wYXJlbnROb2RlLmluc2VydEJlZm9yZSh3Ziwgcyk7XHJcbn0pKClcclxuXHJcbk9uZU9uT25lLlNldFVwU2hvcChTaG9wKVxyXG5PbmVPbk9uZS5BcHAoKVxyXG5cclxuUGxheS5vbignbG9hZCcsIHJlbmRlcilcclxuUGxheS5vbignc2VsZWN0LWRlY2snLCByZW5kZXIpXHJcblBsYXkub24oJ3BsYXktY2FyZCcsIGFuaW1hdGUpXHJcblBsYXkub24oJ3BvaXNvbicsIGFuaW1hdGUpXHJcblBsYXkub24oJ2NoYW5nZS10dXJuJywgYW5pbWF0ZSlcclxuUGxheS5vbigncGFzcycsIGFuaW1hdGUpXHJcblBsYXkub24oJ3Jlc2l6ZScsIHJlc2l6ZSlcclxuXHJcblBsYXkub24oJ3BsYXllcjp1cGRhdGUnLCB1cGRhdGVQbGF5ZXIpXHJcblBsYXkub24oJ29wcG9uZW50OnVwZGF0ZScsIHVwZGF0ZU9wcG9uZW50KVxyXG5cclxuZnVuY3Rpb24gcmVuZGVyKGdhbWUsIHBsYXllciwgb3Bwb25lbnQsIGV2ZW50LCBkb25lKSB7XHJcblx0aWYoIWdhbWUucGxheWVyLmRlY2tfdHlwZSkgUGxheS5lbWl0KCdzZWxlY3QtZGVjaycsIHsgZGVjazonbW9ua2V5X2NoaWNrZW4nIH0pXHJcblxyXG5cdHZhciBvcHBfc3VwZXJfaGVhbHRoID0gZ2FtZS5vcHBvbmVudC5oZWFsdGggLSAxMDBcclxuXHRpZihvcHBfc3VwZXJfaGVhbHRoIDw9IDApIG9wcF9zdXBlcl9oZWFsdGggPSAwXHJcblx0dmFyIHBsYXllcl9zdXBlcl9oZWFsdGggPSBnYW1lLnBsYXllci5oZWFsdGggLSAxMDBcclxuXHRpZihwbGF5ZXJfc3VwZXJfaGVhbHRoIDw9IDApIHBsYXllcl9zdXBlcl9oZWFsdGggPSAwXHJcblxyXG5cdFBsYXkucmVuZGVyKCdwbGF5JywgeyBnYW1lOmdhbWUsIHBsYXllcjpwbGF5ZXIsIG9wcG9uZW50Om9wcG9uZW50LCB3aW5uZXI6Z2FtZS5yZXN1bHQgPT0gcGxheWVyLl9pZCA/IHBsYXllciA6IG9wcG9uZW50LCB0dXJuOmdhbWUudHVybiAhPSBnYW1lLm9wcG9uZW50Ll9pZCwgb3BwX3N1cGVyX2hlYWx0aDpvcHBfc3VwZXJfaGVhbHRoLCBwbGF5ZXJfc3VwZXJfaGVhbHRoOnBsYXllcl9zdXBlcl9oZWFsdGggIH0sIGZ1bmN0aW9uKGVyciwgaHRtbCkge1xyXG5cdFx0UGxheS5jb250YWluZXIuaW5uZXJIVE1MID0gaHRtbFxyXG5cdFx0UGxheS5kZWxlZ2F0ZSgnLnBsYXllciAuaGFuZCcsICcuY2FyZCcsICdjbGljaycsIHBsYXlDYXJkKVxyXG5cdFx0UGxheS5kZWxlZ2F0ZSgnLnBsYXllciAuaGFuZCcsICcuY2FyZCcsICd0YXAnLCBwbGF5Q2FyZClcclxuXHRcdFBsYXkuZGVsZWdhdGUoJy5wbGF5ZXIgLmhhbmQnLCAnLmNhcmQnLCAnbW91c2VvdmVyJywgY2FyZEhvdmVyKVxyXG5cdFx0UGxheS5kZWxlZ2F0ZSgnLnBsYXllciAuaGFuZCcsICcuY2FyZCcsICdtb3VzZW91dCcsIGNhcmRTdG9wSG92ZXIpXHJcblx0XHRQbGF5Lmxpc3RlbignLnBhc3MnLCAnY2xpY2snLCBwYXNzKVxyXG5cclxuXHRcdHNldFRpbWVvdXQocmVzaXplLCAxMDApXHJcblxyXG5cdFx0ZG9uZSgpXHJcblx0fSlcclxufVxyXG5cclxuZnVuY3Rpb24gc2VsZWN0RGVjaygpIHtcclxuXHR2YXIgZGVja3MgPSBQbGF5LmZpbmRBbGwoJ1tuYW1lPVwic2VsZWN0X2RlY2tcIl0nKVxyXG5cdGZvcih2YXIgaT0wOyBpPGRlY2tzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRpZihkZWNrc1tpXS5jaGVja2VkKSB7XHJcblx0XHRcdFBsYXkuZW1pdCgnc2VsZWN0LWRlY2snLCB7IGRlY2s6ZGVja3NbaV0udmFsdWUgfSlcclxuXHRcdFx0YnJlYWs7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5mdW5jdGlvbiBwYXNzKCkge1xyXG5cdFBsYXkuZW1pdCgncGFzcycsIHt9KVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRDYXJkV2lkdGgoY2FyZHMpIHtcclxuXHRmb3IodmFyIGk9MTsgaTxjYXJkcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0aWYoY2FyZHNbaV0uY2xpZW50V2lkdGgpIHJldHVybiBjYXJkc1tpXS5jbGllbnRXaWR0aFxyXG5cdH1cclxuXHRyZXR1cm4gMFxyXG59XHJcblxyXG5mdW5jdGlvbiByZXNpemUoKSB7XHJcblx0dmFyIGhhbmQgPSBQbGF5LmZpbmQoJy5wbGF5ZXIgLmhhbmQnKVxyXG5cdGlmKGhhbmQgJiYgaGFuZC5jaGlsZE5vZGVzWzBdKSB7XHJcblx0XHR2YXIgY2FyZHMgPSBoYW5kLmNoaWxkTm9kZXNcclxuXHRcdCAgLCBzaXplID0gY2FyZHMubGVuZ3RoXHJcblx0XHQgICwgc3BhY2UgPSBQbGF5LmNvbnRhaW5lci5jbGllbnRXaWR0aCAtIGdldENhcmRXaWR0aChjYXJkcykqc2l6ZVxyXG5cdFx0ICAsIG1hcmdpbiA9IHNwYWNlLyhzaXplLTEpLTFcclxuXHJcblx0XHRpZihtYXJnaW4+MCkgbWFyZ2luID0gMFxyXG5cclxuXHRcdGZvcih2YXIgaT0xOyBpPHNpemU7IGkrKykge1xyXG5cdFx0XHRjYXJkc1tpXS5zdHlsZS5tYXJnaW5MZWZ0ID0gbWFyZ2luKydweCdcclxuXHRcdH1cclxuXHJcblx0XHRjYXJkc1swXS5zdHlsZS5tYXJnaW5MZWZ0ID0gMFxyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gcGxheUNhcmQoZSkge1xyXG5cdGUuc3RvcFByb3BhZ2F0aW9uKClcclxuXHRlLnByZXZlbnREZWZhdWx0KClcclxuXHRpZigoUGxheS5yZWFkeSAmJiBQbGF5LmN1cnJlbnRHYW1lLnR1cm4gIT0gUGxheS5jdXJyZW50R2FtZS5vcHBvbmVudC5faWQpKSB7XHJcblx0XHQvLyBpZihlLnRhcmdldC5jbGFzc05hbWUuaW5kZXhPZignc3RhY2thYmxlJykgIT0gLTEpIHtcclxuXHRcdC8vIFx0dmFyIGNhcmRfZWxlbWVudHMgPSBQbGF5LmZpbmRBbGwoJ1tkYXRhLWNhcmRrZXk9XCInK2UudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1jYXJka2V5JykrJ1wiXScpXHJcblx0XHQvLyBcdGZvcih2YXIgaT0wOyBpPGNhcmRfZWxlbWVudHMubGVuZ3RoOyBpKyspIGNhcmRfZWxlbWVudHNbaV0ucmVtb3ZlKClcclxuXHRcdC8vIH0gZWxzZSBlLnRhcmdldC5yZW1vdmUoKVxyXG5cdFx0UGxheS5lbWl0KCdwbGF5LWNhcmQnLCB7IGNhcmQ6IHsgZGVjazplLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtZGVjaycpLCBjYXJkX2tleTplLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY2FyZGtleScpIH0gfSlcdFxyXG5cdH1cclxuXHRyZXNpemUoKVxyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJkSG92ZXIoZSkge1xyXG5cdGlmKGUudGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnc3RhY2thYmxlJykpIHtcclxuXHRcdHZhciBjYXJkcyA9IFBsYXkuZmluZEFsbCgnLnBsYXllciAuaGFuZCAuY2FyZFtkYXRhLWNhcmRrZXk9XCInK2UudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1jYXJka2V5JykrJ1wiXScpXHJcblx0XHRmb3IodmFyIGk9MDsgaTxjYXJkcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRjYXJkc1tpXS5jbGFzc0xpc3QuYWRkKCdob3ZlcicpXHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdGUudGFyZ2V0LmNsYXNzTGlzdC5hZGQoJ2hvdmVyJylcclxuXHR9XHJcblx0ZS5zdG9wUHJvcGFnYXRpb24oKVxyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJkU3RvcEhvdmVyKGUpIHtcclxuXHRpZihlLnRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ3N0YWNrYWJsZScpKSB7XHJcblx0XHR2YXIgY2FyZHMgPSBQbGF5LmZpbmRBbGwoJy5wbGF5ZXIgLmhhbmQgLmNhcmRbZGF0YS1jYXJka2V5PVwiJytlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY2FyZGtleScpKydcIl0nKVxyXG5cdFx0Zm9yKHZhciBpPTA7IGk8Y2FyZHMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0Y2FyZHNbaV0uY2xhc3NMaXN0LnJlbW92ZSgnaG92ZXInKVxyXG5cdFx0fVxyXG5cdH0gZWxzZSB7XHJcblx0XHRlLnRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdob3ZlcicpXHJcblx0fVxyXG59XHJcblxyXG5mdW5jdGlvbiB1cGRhdGVQbGF5ZXIodXNlciwgZXZlbnQsIGRvbmUpIHtcclxuXHRcclxuXHRQbGF5LmZpbmQoJy5wbGF5ZXIgLmNvaW5zIC5hbW91bnQnKS5pbm5lckhUTUwgPSB1c2VyLmNvaW5zXHJcblx0UGxheS5maW5kKCcucGxheWVyIC53aW5zIC5hbW91bnQnKS5pbm5lckhUTUwgPSB1c2VyLndpbnNcclxuXHRcclxuXHRkb25lKClcclxufVxyXG5cclxuZnVuY3Rpb24gdXBkYXRlT3Bwb25lbnQodXNlciwgZXZlbnQsIGRvbmUpIHtcclxuXHJcblx0UGxheS5maW5kKCcub3Bwb25lbnQgLmNvaW5zIC5hbW91bnQnKS5pbm5lckhUTUwgPSB1c2VyLmNvaW5zXHJcblx0UGxheS5maW5kKCcub3Bwb25lbnQgLndpbnMgLmFtb3VudCcpLmlubmVySFRNTCA9IHVzZXIud2luc1xyXG5cdFxyXG5cdGRvbmUoKVxyXG59XHJcblxyXG5mdW5jdGlvbiBhbmltYXRlQ2FyZChnYW1lLCBwbGF5ZXIsIGV2ZW50LCBkb25lKSB7XHJcblx0aWYoZXZlbnQudHlwZSAhPSAncGxheS1jYXJkJykgcmV0dXJuIGRvbmUoKVxyXG5cclxuXHR2YXIgb2xkX3Jlc3VsdCA9IFBsYXkuZmluZCgnLmdhbWVfcmVzdWx0JylcclxuXHQgICwgbmV3X3Jlc3VsdCA9IG9sZF9yZXN1bHQuY2xvbmVOb2RlKHRydWUpXHJcblxyXG5cdG9sZF9yZXN1bHQuc3R5bGUub3BhY2l0eSA9IDBcclxuXHJcblx0bmV3X3Jlc3VsdC5pbm5lckhUTUwgPSAnJ1xyXG5cclxuXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0b2xkX3Jlc3VsdC5yZW1vdmUoKVxyXG5cdH0sIDEwMDApXHJcblx0XHJcblx0aWYoZ2FtZS5sYXN0X21vdmUpIHtcclxuXHRcdGlmKGdhbWUubGFzdF9tb3ZlLmJvbnVzKSB7XHJcblx0XHRcdGZvcih2YXIgaT0wOyBpPGdhbWUubGFzdF9tb3ZlLmJvbnVzLm51bTsgaSsrKSB7XHJcblx0XHRcdFx0UGxheS5yZW5kZXIoJ2NhcmQnLCB7IGltZzpnYW1lLmxhc3RfbW92ZS5ib251cy5pbWcsIGNhcmRfa2V5OmdhbWUubGFzdF9tb3ZlLmJvbnVzLmtleSwgZGVja190eXBlOidtb25rZXlfY2hpY2tlbicgfSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRcdFx0XHRuZXdfcmVzdWx0LmlubmVySFRNTCArPSBodG1sXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFxyXG5cdFx0aWYoZ2FtZS5sYXN0X21vdmUuY2FyZCkge1xyXG5cdFx0XHRmb3IodmFyIGk9MDsgaTxnYW1lLmxhc3RfbW92ZS5jYXJkLm51bTsgaSsrKSB7XHJcblx0XHRcdFx0UGxheS5yZW5kZXIoJ2NhcmQnLCB7IGltZzpnYW1lLmxhc3RfbW92ZS5jYXJkLmltZywgY2FyZF9rZXk6Z2FtZS5sYXN0X21vdmUuY2FyZC5rZXksIGRlY2tfdHlwZTonbW9ua2V5X2NoaWNrZW4nIH0sIGZ1bmN0aW9uKGVyciwgaHRtbCkge1xyXG5cdFx0XHRcdFx0bmV3X3Jlc3VsdC5pbm5lckhUTUwgKz0gaHRtbFxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdFxyXG5cclxuXHRpZihwbGF5ZXIuX2lkID09IGdhbWUubGFzdF9tb3ZlLmluaXRpYXRvcikge1xyXG5cdFx0b2xkX3Jlc3VsdC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShuZXdfcmVzdWx0LCBvbGRfcmVzdWx0KVxyXG5cclxuXHRcdHZhciBjYXJkID0gZ2FtZS5sYXN0X21vdmUuY2FyZCA/IGdhbWUubGFzdF9tb3ZlLmNhcmQgOiBnYW1lLmxhc3RfbW92ZS5ib251c1xyXG5cdFx0ICAsIGNhcmRzX2luX2hhbmQgPSBQbGF5LmZpbmRBbGwoJy5wbGF5ZXIgLmhhbmQgW2RhdGEtY2FyZGtleT1cIicrY2FyZC5rZXkrJ1wiXScpXHJcblx0XHQgICwgcmVzdWx0X2NhcmRzID0gbmV3X3Jlc3VsdC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1jYXJka2V5PVwiJytjYXJkLmtleSsnXCJdJylcclxuXHRcdCAgLCBjb21wbGV0ZWQgPSAwXHJcblx0XHRcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBjYXJkLm51bTsgaSsrKSAoZnVuY3Rpb24oaSkge1xyXG5cdFx0XHRyYW1qZXQudHJhbnNmb3JtKGNhcmRzX2luX2hhbmRbaV0sIHJlc3VsdF9jYXJkc1tpXSwgeyBkb25lOmZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHJlc3VsdF9jYXJkc1tpXS5zdHlsZS5kaXNwbGF5ID0gJydcclxuXHRcdFx0XHRpZigrK2NvbXBsZXRlZCA9PSBjYXJkLm51bSkgZG9uZSgpXHJcblx0XHRcdH0gfSlcclxuXHRcdFx0Y2FyZHNfaW5faGFuZFtpXS5yZW1vdmUoKVxyXG5cdFx0XHRyZXN1bHRfY2FyZHNbaV0uc3R5bGUuZGlzcGxheSA9ICdub25lJ1xyXG5cdFx0fSkoaSlcclxuXHJcblx0fSBlbHNlIHtcclxuXHRcdG5ld19yZXN1bHQuc3R5bGUudG9wID0gJy0xMGVtJ1xyXG5cdFx0b2xkX3Jlc3VsdC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShuZXdfcmVzdWx0LCBvbGRfcmVzdWx0KVxyXG5cdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuXHRcdFx0bmV3X3Jlc3VsdC5zdHlsZS50b3AgPSAnJ1xyXG5cdFx0XHRkb25lKClcclxuXHRcdH0sIDEwKVxyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gYW5pbWF0ZShnYW1lLCBwbGF5ZXIsIG9wcG9uZW50LCBldmVudCwgZG9uZSkge1xyXG5cdGFuaW1hdGVDYXJkKGdhbWUsIHBsYXllciwgZXZlbnQsIGZ1bmN0aW9uKCkge1xyXG5cdFx0YW5pbWF0ZUhlYWx0aChQbGF5LmZpbmQoJy5wbGF5ZXIgLmhlYWx0aCcpLCAncGxheWVyJywgZ2FtZS5wbGF5ZXIuaGVhbHRoLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0YW5pbWF0ZUhlYWx0aChQbGF5LmZpbmQoJy5vcHBvbmVudCAuaGVhbHRoJyksICdvcHBvbmVudCcsIGdhbWUub3Bwb25lbnQuaGVhbHRoLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRtYWtlTGlrZU9yZGVyZWRBcnJheShnYW1lLnBsYXllci5oYW5kLCBQbGF5LmZpbmRBbGwoJy5wbGF5ZXIgLmhhbmQgLmNhcmQnKSlcclxuXHRcdFx0XHRhbmltYXRlUG9pc29uKFBsYXkuZmluZCgnLnBsYXllciAuc3RhdHVzZXMgLnBvaXNvbicpLCBnYW1lLnBsYXllci5wb2lzb24sIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0YW5pbWF0ZVBvaXNvbihQbGF5LmZpbmQoJy5vcHBvbmVudCAuc3RhdHVzZXMgLnBvaXNvbicpLCBnYW1lLm9wcG9uZW50LnBvaXNvbiwgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdGlmKGdhbWUub3Bwb25lbnQuaGFuZC5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0XHRcdFx0YW5pbWF0ZU9wcG9uZW50SGFuZChQbGF5LmZpbmQoJy5vcHBvbmVudCAuaGFuZCAubnVtYmVyJyksIGdhbWUub3Bwb25lbnQuaGFuZC5sZW5ndGgsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0UGxheS50aW1lb3V0KDUwMCwgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHZhciBpbl9nYW1lID0gUGxheS5maW5kKCcuaW5fZ2FtZScpXHJcblx0XHRcdFx0XHRcdFx0XHRcdGlmKGdhbWUudHVybiA9PSBwbGF5ZXIuX2lkKSBpbl9nYW1lLnNldEF0dHJpYnV0ZSgnZGF0YS15b3VyX3R1cm4nLCB0cnVlKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRlbHNlIGluX2dhbWUucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXlvdXJfdHVybicpXHJcblx0XHRcdFx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRyZXNpemUoKVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRkb25lKClcclxuXHRcdFx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0fSBlbHNlIFBsYXkudGltZW91dCg1MDAsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRcdHZhciBpbl9nYW1lID0gUGxheS5maW5kKCcuaW5fZ2FtZScpXHJcblx0XHRcdFx0XHRcdFx0aWYoZ2FtZS50dXJuID09IHBsYXllci5faWQpIGluX2dhbWUuc2V0QXR0cmlidXRlKCdkYXRhLXlvdXJfdHVybicsIHRydWUpXHJcblx0XHRcdFx0XHRcdFx0ZWxzZSBpbl9nYW1lLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS15b3VyX3R1cm4nKVxyXG5cclxuXHRcdFx0XHRcdFx0XHRyZXNpemUoKVxyXG5cdFx0XHRcdFx0XHRcdGRvbmUoKVxyXG5cdFx0XHRcdFx0XHR9KVxyXG5cclxuXHRcdFx0XHRcdFx0aWYoZ2FtZS5yZXN1bHQpIHtcclxuXHRcdFx0XHRcdFx0XHRQbGF5LnJlbmRlcignZW5kX3NjcmVlbicsIHsgb3Bwb25lbnRfaWQ6IG9wcG9uZW50Ll9pZCwgb3Bwb25lbnRfbmFtZTogb3Bwb25lbnQudXNlcm5hbWUsIGlzX3dpbm5lcjpnYW1lLnJlc3VsdCA9PSBwbGF5ZXIuX2lkLCB3aG9fd29uOihnYW1lLnJlc3VsdCA9PSBwbGF5ZXIuX2lkID8gcGxheWVyIDogb3Bwb25lbnQpLnVzZXJuYW1lLCBjb2luczogZ2FtZS5wbGF5ZXIuY29pbnMgfSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRQbGF5LmRlbGVnYXRlKCcuZW5kX2dhbWVfc2NyZWVuJywgJy5wbGF5X2FnYWluJywgJ2NsaWNrJywgUGxheS5jaGFsbGVuZ2VCeUlkLmJpbmQodGhpcywgb3Bwb25lbnQuX2lkKSlcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRQbGF5LmZpbmQoJy5lbmRfZ2FtZV9zY3JlZW4nKS5zZXRBdHRyaWJ1dGUoJ2RhdGEtZ2FtZW92ZXInLCB0cnVlKVxyXG5cdFx0XHRcdFx0XHRcdFx0UGxheS5maW5kKCcuZW5kX2dhbWVfc2NyZWVuJykuaW5uZXJIVE1MID0gaHRtbFxyXG5cdFx0XHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0fSlcclxuXHRcdH0pXHJcblx0fSlcclxufVxyXG5cclxuZnVuY3Rpb24gYW5pbWF0ZU9wcG9uZW50SGFuZChhbW91bnQsIG51bV9jYXJkcywgZG9uZSkge1xyXG5cdHZhciBzdGFydF9hbW91bnQgPSBwYXJzZUludChhbW91bnQudGV4dENvbnRlbnQpXHJcblx0ICAsIHRvdGFsX2NoYW5nZSA9IE1hdGguYWJzKG51bV9jYXJkcy1zdGFydF9hbW91bnQpXHJcblxyXG5cdGlmKCF0b3RhbF9jaGFuZ2UpIHJldHVybiBkb25lKClcclxuXHJcblx0UGxheS5jb3VudEFuaW1hdGlvbihhbW91bnQsIG51bV9jYXJkcywgdG90YWxfY2hhbmdlKjMwMCwgZG9uZSlcclxufVxyXG5cclxuZnVuY3Rpb24gYW5pbWF0ZVBvaXNvbihlbGVtZW50LCBuZXdfcG9pc29uLCBkb25lKSB7XHJcblx0ZWxlbWVudC5pbm5lckhUTUwgPSAnJ1xyXG5cclxuXHRmb3IodmFyIGk9MDsgaTxuZXdfcG9pc29uOyBpKyspIHtcclxuXHRcdGVsZW1lbnQuaW5uZXJIVE1MICs9ICc8aW1nIHNyYz1cImltYWdlcy9pY29ucy9wb2lzb24ucG5nXCI+J1xyXG5cdH1cclxuXHJcblx0ZG9uZSgpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGFuaW1hdGVIZWFsdGgoZWxlbWVudCwgcGxheWVyLCBuZXdIZWFsdGgsIGRvbmUpIHtcclxuXHR2YXIgYW1vdW50ID0gZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuYW1vdW50JylcclxuXHQgICwgY2hhbmdlX2Ftb3VudCA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcignLmNoYW5nZV9hbW91bnQnKVxyXG5cdCAgLCBzdGFydF9hbW91bnQgPSBwYXJzZUludChhbW91bnQudGV4dENvbnRlbnQpXHJcblx0ICAsIHRvdGFsX2NoYW5nZSA9IE1hdGguYWJzKG5ld0hlYWx0aC1zdGFydF9hbW91bnQpXHJcblx0ICAsIHRvdGFsX2R1cmF0aW9uID0gNDAwICsgdG90YWxfY2hhbmdlKjE1XHJcblx0ICAsIG5lZ2F0aXZlX2JhciA9IGdldEJhckRhdGEoJy5uZWdhdGl2ZScsIG5ld0hlYWx0aCA8IDAgPyAxMDAgKyBuZXdIZWFsdGggOiAxMDApXHJcblx0ICAsIG5vcm1hbF9iYXIgPSBnZXRCYXJEYXRhKCcubm9ybWFsJywgbmV3SGVhbHRoID4gMTAwID8gMTAwIDogKG5ld0hlYWx0aCA8IDAgPyAwIDogbmV3SGVhbHRoKSlcclxuXHQgICwgc3VwZXJfYmFyID0gZ2V0QmFyRGF0YSgnLnN1cGVyJywgbmV3SGVhbHRoID4gMTAwID8gbmV3SGVhbHRoIC0gMTAwIDogMClcclxuXHRcclxuXHRpZighdG90YWxfY2hhbmdlKSByZXR1cm4gZG9uZSgpXHJcblxyXG5cdGZ1bmN0aW9uIGFuaW1hdGVDaGFuZ2UoKSB7XHJcblx0XHR2YXIgdGhpcmRfZHVyYXRpb24gPSBNYXRoLmZsb29yKHRvdGFsX2R1cmF0aW9uLzMpXHJcblx0XHQgICwgZ2FtZVdpbmRvdyA9IFBsYXkuY29udGFpbmVyXHJcblx0XHQgICwgd2luZG93U2l6ZSA9IE1hdGgubWluKGdhbWVXaW5kb3cub2Zmc2V0V2lkdGgsIGdhbWVXaW5kb3cub2Zmc2V0SGVpZ2h0KVxyXG5cdFx0ICAsIGVkZ2UgPSBwbGF5ZXIgPT0gJ3BsYXllcicgPyAnYm90dG9tJyA6ICd0b3AnXHJcblx0XHQgICwgYW5pbWF0aW9uID0ge31cclxuXHRcdCAgLCBzY2FsZSA9IE1hdGguZmxvb3IoTWF0aC5wb3cod2luZG93U2l6ZSp0b3RhbF9jaGFuZ2UsIDAuOCkvMjApXHJcblx0XHQgICwgZHVyYXRpb24gPSA1MDAgKyB0b3RhbF9kdXJhdGlvbi8yLjVcclxuXHJcblx0XHRjaGFuZ2VfYW1vdW50LnRleHRDb250ZW50ID0gKG5ld0hlYWx0aCA+IHN0YXJ0X2Ftb3VudCA/ICcrJyA6ICctJykgKyB0b3RhbF9jaGFuZ2VcclxuXHRcdGNoYW5nZV9hbW91bnQuc3R5bGUuY29sb3IgPSBuZXdIZWFsdGggPiBzdGFydF9hbW91bnQgPyAnIzA4YycgOiAnI2MwMCdcclxuXHRcdGNoYW5nZV9hbW91bnQuc3R5bGVbZWRnZV0gPSBNYXRoLmZsb29yKHNjYWxlLzIuNSotMSkrJ3B4J1xyXG5cdFx0Y2hhbmdlX2Ftb3VudC5zdHlsZS5mb250U2l6ZSA9ICcxcHgnXHJcblx0XHRjaGFuZ2VfYW1vdW50LnN0eWxlLm9wYWNpdHkgPSAwXHJcblx0XHRjaGFuZ2VfYW1vdW50LnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snXHJcblxyXG5cdFx0YW5pbWF0aW9uWydmb250LXNpemUnXSA9IHNjYWxlKydweCdcclxuXHRcdGFuaW1hdGlvbltlZGdlXSA9IE1hdGguZmxvb3Ioc2NhbGUvMi41KSsncHgnXHJcblx0XHRhbmltYXRpb24ub3BhY2l0eSA9IDFcclxuXHJcblx0XHRQbGF5LmFuaW1hdGUoY2hhbmdlX2Ftb3VudCwgYW5pbWF0aW9uLCBkdXJhdGlvbiwgJ2xpbmVhcicsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRhbmltYXRpb25bJ2ZvbnQtc2l6ZSddID0gTWF0aC5mbG9vcihzY2FsZSoxLjIpKydweCdcclxuXHRcdFx0YW5pbWF0aW9uW2VkZ2VdID0gTWF0aC5mbG9vcigoc2NhbGUvNi4yNSkrKHNjYWxlLzIuNSkpKydweCdcclxuXHRcdFx0YW5pbWF0aW9uLm9wYWNpdHkgPSAwXHJcblx0XHRcdFBsYXkuYW5pbWF0ZShjaGFuZ2VfYW1vdW50LCBhbmltYXRpb24sIE1hdGguZmxvb3IoZHVyYXRpb24vNSksICdsaW5lYXInLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRjaGFuZ2VfYW1vdW50LnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcclxuXHRcdFx0fSlcclxuXHRcdH0pXHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBnZXRCYXJEYXRhKGNscywgbmV3VmFsdWUpIHtcclxuXHRcdHZhciBlbCA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcihjbHMpXHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHQgIGVsZW1lbnQ6IGVsXHJcblx0XHRcdCwgd2lkdGg6IG5ld1ZhbHVlXHJcblx0XHRcdCwgZWFzZTogJ2xpbmVhcidcclxuXHRcdFx0LCBkdXJhdGlvbjogTWF0aC5mbG9vcihcclxuXHRcdFx0XHRNYXRoLmFicyhwYXJzZUludChlbC5zdHlsZS53aWR0aCktbmV3VmFsdWUpL3RvdGFsX2NoYW5nZSp0b3RhbF9kdXJhdGlvblxyXG5cdFx0XHQpXHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBhbmltYXRlQmFyKGJhciwgZm4pIHtcclxuXHRcdFBsYXkuYW5pbWF0ZShiYXIuZWxlbWVudCwgeyB3aWR0aDpiYXIud2lkdGgrJyUnIH0sIGJhci5kdXJhdGlvbiwgYmFyLmVhc2UsIGZuKVxyXG5cdH1cclxuXHJcblx0UGxheS5jb3VudEFuaW1hdGlvbihhbW91bnQsIG5ld0hlYWx0aCwgdG90YWxfZHVyYXRpb24pXHJcblxyXG5cdGlmKG5ld0hlYWx0aCA+IHN0YXJ0X2Ftb3VudCkge1xyXG5cdFx0YW5pbWF0ZUJhcihuZWdhdGl2ZV9iYXIsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRhbmltYXRlQmFyKG5vcm1hbF9iYXIsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGFuaW1hdGVCYXIoc3VwZXJfYmFyLCBkb25lKVxyXG5cdFx0XHR9KVxyXG5cdFx0fSlcclxuXHR9IGVsc2Uge1xyXG5cdFx0YW5pbWF0ZUJhcihzdXBlcl9iYXIsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRhbmltYXRlQmFyKG5vcm1hbF9iYXIsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGFuaW1hdGVCYXIobmVnYXRpdmVfYmFyLCBkb25lKVxyXG5cdFx0XHR9KVxyXG5cdFx0fSlcclxuXHR9XHJcblxyXG5cdGFuaW1hdGVDaGFuZ2UoKVxyXG59XHJcblxyXG5mdW5jdGlvbiBtYWtlTGlrZU9yZGVyZWRBcnJheShhcnJheTEsIGFycmF5Mikge1xyXG5cdHZhciBjbGllbnRDYXJkcyA9IGdldENsaWVudENhcmRBcnJheShhcnJheTIpXHJcblx0Zm9yKHZhciBpPTA7IGk8YXJyYXkxLmxlbmd0aCB8fCBpPGFycmF5Mi5sZW5ndGg7IGkrKykge1xyXG5cdFx0aWYoIWFycmF5MltpXSkge1xyXG5cdFx0XHRpbnNlcnRFbGVtZW50QXQoYXJyYXkxW2ldLCBhcnJheTIsIGkpXHJcblx0XHR9IGVsc2UgaWYoIWFycmF5MVtpXSkge1xyXG5cdFx0XHRyZW1vdmVFbGVtZW50QXQoYXJyYXkyLCBpLS0pXHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR2YXIgcmVzdWx0ID0gY29tcGFyZShhcnJheTFbaV0sIGFycmF5MltpXSlcclxuXHRcdFx0aWYocmVzdWx0IDwgMCkge1xyXG5cdFx0XHRcdGluc2VydEVsZW1lbnRBdChhcnJheTFbaV0sIGFycmF5MiwgaSlcclxuXHRcdFx0fSBlbHNlIGlmIChyZXN1bHQgPiAwKSB7XHJcblx0XHRcdFx0cmVtb3ZlRWxlbWVudEF0KGFycmF5MiwgaS0tKVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHR2YXIgYXJyYXkyID0gUGxheS5maW5kQWxsKCcucGxheWVyIC5oYW5kIC5jYXJkJylcclxuXHR9XHJcblx0aWYoYXJyYXkxLmxlbmd0aCAhPSBhcnJheTIubGVuZ3RoKSB7XHJcblx0XHRcclxuXHR9IGVsc2Uge1xyXG5cdFx0Zm9yKHZhciBpPTA7IGk8YXJyYXkxLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGlmKGNvbXBhcmUoYXJyYXkxW2ldLCBhcnJheTJbaV0pICE9IDApIHtcclxuXHRcdFx0XHRicmVha1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cdHJlc2l6ZSgpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldFNlcnZlckNhcmRBcnJheShhcnIpIHtcclxuXHR2YXIgYSA9IFtdXHJcblx0YXJyLmZvckVhY2goZnVuY3Rpb24oZSkge1xyXG5cdFx0YS5wdXNoKGUuY2FyZF9rZXkpXHJcblx0fSlcclxuXHRyZXR1cm4gYVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRDbGllbnRDYXJkQXJyYXkoYXJyKSB7XHJcblx0dmFyIGEgPSBbXVxyXG5cdDtbXS5mb3JFYWNoLmNhbGwoYXJyLCBmdW5jdGlvbihlKSB7XHJcblx0XHRhLnB1c2goZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtY2FyZGtleScpKVxyXG5cdH0pXHJcblx0cmV0dXJuIGFcclxufVxyXG5cclxuZnVuY3Rpb24gY29tcGFyZShhLGIpIHtcclxuXHRyZXR1cm4gYS5vcmRlci1wYXJzZUludChiLmRhdGFzZXQub3JkZXIpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGluc2VydEVsZW1lbnRBdChlbGVtZW50LCBhcnJheSwgaW5kZXgpIHtcclxuXHR2YXIgaGFuZCA9IFBsYXkuZmluZCgnLnBsYXllciAuaGFuZCcpXHJcblx0UGxheS5yZW5kZXJOb2RlKCdjYXJkJywgZWxlbWVudCwgZnVuY3Rpb24oZXJyLCBjYXJkKSB7XHJcblx0XHRoYW5kLmNsYXNzTGlzdC5yZW1vdmUoJ3NpemVfJythcnJheS5sZW5ndGgpXHJcblx0XHRoYW5kLmNsYXNzTGlzdC5hZGQoJ3NpemVfJysoYXJyYXkubGVuZ3RoKzEpKVxyXG5cdFx0Y2FyZC5jbGFzc0xpc3QuYWRkKCdkcmF3JylcclxuXHRcdGhhbmQuaW5zZXJ0QmVmb3JlKGNhcmQsIGFycmF5W2luZGV4XSlcclxuXHRcdHJlc2l6ZSgpXHJcblx0XHRQbGF5LnRpbWVvdXQoMTAwLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0Y2FyZC5jbGFzc0xpc3QucmVtb3ZlKCdkcmF3JylcclxuXHRcdH0pXHJcblx0fSlcclxufVxyXG5cclxuZnVuY3Rpb24gcmVtb3ZlRWxlbWVudEF0KGFycmF5LCBpbmRleCkge1xyXG5cdHZhciBoYW5kID0gUGxheS5maW5kKCcucGxheWVyIC5oYW5kJylcclxuXHRoYW5kLmNsYXNzTGlzdC5yZW1vdmUoJ3NpemVfJythcnJheS5sZW5ndGgpXHJcblx0aGFuZC5jbGFzc0xpc3QuYWRkKCdzaXplXycrKGFycmF5Lmxlbmd0aC0xKSlcclxuXHRhcnJheVtpbmRleF0ucmVtb3ZlKClcclxufSIsImR1c3QuaGVscGVycy5tdWx0aXBsZSA9IGZ1bmN0aW9uKGNodW5rLCBjdHgsIGJvZGllcywgcGFyYW1zKSB7XHJcblx0dmFyIG51bWJlciA9IGR1c3QuaGVscGVycy50YXAocGFyYW1zLm51bWJlciwgY2h1bmssIGN0eClcclxuXHRyZXR1cm4gY2h1bmsud3JpdGUoZ2V0TXVsdGlwbGUobnVtYmVyKSlcclxufVxyXG5cclxuZHVzdC5oZWxwZXJzLnRpbWVzID0gZnVuY3Rpb24oY2h1bmssIGN0eCwgYm9kaWVzLCBwYXJhbXMpIHtcclxuXHR2YXIgbnVtYmVyID0gcGFyc2VJbnQoZHVzdC5oZWxwZXJzLnRhcChwYXJhbXMubnVtYmVyLCBjaHVuaywgY3R4KSlcclxuICAgIHJldHVybiBjaHVuay5tYXAoZnVuY3Rpb24oY2h1bmspIHtcclxuXHRcdGlmKG51bWJlcikgZm9yKHZhciBpID0gMDsgaSA8IG51bWJlcjsgaSsrKSBjaHVuay5yZW5kZXIoYm9kaWVzLmJsb2NrLCBjdHgpXHJcblx0XHRlbHNlIGlmKGJvZGllc1snZWxzZSddKSBjaHVuay5yZW5kZXIoYm9kaWVzWydlbHNlJ10sIGN0eClcclxuXHRcdGNodW5rLmVuZCgpXHJcbiAgICB9KVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRNdWx0aXBsZShudW1iZXIpIHtcclxuXHRpZihudW1iZXIgPD0gMSkgcmV0dXJuICcnXHJcblx0aWYobnVtYmVyID09IDIpIHJldHVybiAnRG91YmxlJ1xyXG5cdGlmKG51bWJlciA9PSAzKSByZXR1cm4gJ1RyaXBsZSdcclxuXHRpZihudW1iZXIgPT0gNCkgcmV0dXJuICdRdWFkcnVwbGUnXHJcblx0aWYobnVtYmVyID09IDUpIHJldHVybiAnUXVpbnR1cGxlJ1xyXG5cdGlmKG51bWJlciA9PSA2KSByZXR1cm4gJ0hleHR1cGxlJ1xyXG5cdGlmKG51bWJlciA9PSA3KSByZXR1cm4gJ1NlcHR1cGxlJ1xyXG5cdGlmKG51bWJlciA9PSA4KSByZXR1cm4gJ09jdHVwbGUnXHJcbn0iLCJ2YXIgT25lT25PbmUgPSBtb2R1bGUuZXhwb3J0c1xyXG5cclxuT25lT25PbmUuQXBwID0gcmVxdWlyZSgnLi9mcm9udGVuZC9zY3JpcHRzL2FwcCcpXHJcbk9uZU9uT25lLlBsYXlIYW5kbGVyID0gcmVxdWlyZSgnLi9mcm9udGVuZC9zY3JpcHRzL3ByZXNlbnRlcnMvcGxheScpXHJcbk9uZU9uT25lLkxhbmd1YWdlcyA9IHJlcXVpcmUoJy4vZnJvbnRlbmQvc2NyaXB0cy9sYW5nJykubGFuZ3VhZ2VzXHJcbk9uZU9uT25lLlNldFVwU2hvcCA9IHJlcXVpcmUoJy4vZnJvbnRlbmQvc2NyaXB0cy9wcmVzZW50ZXJzL3Nob3AnKS5sb2FkSXRlbXNcclxuIiwidmFyIGVudiA9ICdwcm9kJyAvLyBkZXYsIHByb2RcclxuICAsIHNldHRpbmdzXHJcblxyXG5zZXR0aW5ncyA9IHtcclxuICAnZGV2Jzoge1xyXG4gICAgaG9zdG5hbWU6ICcxOTIuMTY4LjEuMTInLCAvLyAxOTIuMTY4LjEuMTJcclxuICAgIHBvcnQ6IDMwMDFcclxuICB9LFxyXG4gICdwcm9kJzoge1xyXG4gICAgaG9zdG5hbWU6ICd3d3cucGxheW1vbmtleWNoaWNrZW4uY29tJyxcclxuICAgIHBvcnQ6IDMwMDFcclxuICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG4gIGhvc3RuYW1lOiBzZXR0aW5nc1tlbnZdLmhvc3RuYW1lLFxyXG4gIHBvcnQ6IHNldHRpbmdzW2Vudl0ucG9ydFxyXG59Iiwid2luZG93LnVzZUZhY2Vib29rID0gZmFsc2VcclxuXHJcbnZhciBzZXJ2ZXIgPSByZXF1aXJlKCcuL3NvY2tldC5pbycpXHJcbiAgLCBNYWluID0gcmVxdWlyZSgnLi9wcmVzZW50ZXJzL21haW4nKVxyXG4gICwgTG9naW4gPSByZXF1aXJlKCcuL3ByZXNlbnRlcnMvbG9naW4nKVxyXG4gICwgZmFjZWJvb2tSZWFkeSA9IGZhbHNlXHJcbiAgLCBzdGFydGVkID0gZmFsc2VcclxuXHJcbnJlcXVpcmUoJy4vdXRpbHMvZHVzdC1oZWxwZXJzJylcclxuXHJcbmZ1bmN0aW9uIHN0YXJ0KCkge1xyXG5cdHdpbmRvdy5zdGFydENhbGxlZCA9IERhdGUubm93KClcclxuXHRpZih1c2VGYWNlYm9vaykge1xyXG5cdFx0RkIuaW5pdCh7XHJcblx0XHRcdCAgYXBwSWQ6ICcxNTAwNjIyNjUxMjYwMzMnXHJcblx0XHRcdCwgc3RhdHVzOiB0cnVlXHJcblx0XHRcdCwgY2hhbm5lbFVybDogd2luZG93LmxvY2F0aW9uLnByb3RvY29sICsgJy8vJyArIHdpbmRvdy5sb2NhdGlvbi5ob3N0ICsnL2NoYW5uZWwuaHRtbCdcclxuXHRcdH0pXHJcblx0XHRGQi5nZXRMb2dpblN0YXR1cyhmdW5jdGlvbihyZXNwb25zZSkge1xyXG5cdFx0XHR3aW5kb3cuZmFjZWJvb2tTdGF0dXNSZWNlaXZlZCA9IERhdGUubm93KClcclxuXHRcdFx0aWYocmVzcG9uc2Uuc3RhdHVzID09ICdjb25uZWN0ZWQnKSB7XHJcblx0XHRcdFx0c2VydmVyLmVtaXQoJ2F1dGgvZmFjZWJvb2snLCByZXNwb25zZS5hdXRoUmVzcG9uc2UuYWNjZXNzVG9rZW4pXHJcblx0XHRcdFx0c2VydmVyLm9uY2UoJ2F1dGgvZmFjZWJvb2svcmVzcG9uc2UnLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRcdFx0XHRpZighZGF0YS5lcnIpIHtcclxuXHRcdFx0XHRcdFx0TG9naW4uY29tcGxldGUoZGF0YSlcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGFsZXJ0KGRhdGEuZXJyKVxyXG5cdFx0XHRcdFx0XHRMb2dpbi5pbml0KClcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9IGVsc2Ugc3RhbmRhcmRMb2dpbigpXHJcblx0XHR9KVxyXG5cdH0gZWxzZSB7XHJcblx0XHRzdGFuZGFyZExvZ2luKClcclxuXHR9XHJcblxyXG5cdHNldFRpbWVvdXQoc2Nyb2xsVG8sMjAwMCwwLDEpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIHN0YW5kYXJkTG9naW4oKSB7XHJcblx0aWYod2luZG93LmxvY2FsU3RvcmFnZS50b2tlbikge1xyXG5cdFx0c2VydmVyLmVtaXQoJ2F1dGgvdG9rZW4nLCB3aW5kb3cubG9jYWxTdG9yYWdlLnRva2VuKVxyXG5cdFx0c2VydmVyLm9uY2UoJ2F1dGgvdG9rZW4vcmVzcG9uc2UnLCBmdW5jdGlvbihkYXRhLCB0b2tlbikge1xyXG5cdFx0XHRpZighZGF0YS5lcnIpIHtcclxuXHRcdFx0XHRMb2dpbi5jb21wbGV0ZShkYXRhLCB0b2tlbilcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRhbGVydChkYXRhLmVycilcclxuXHRcdFx0XHRMb2dpbi5pbml0KClcclxuXHRcdFx0fVxyXG5cdFx0fSlcclxuXHR9IGVsc2Uge1xyXG5cdFx0TG9naW4uaW5pdCgpXHJcblx0fVxyXG59XHJcblxyXG5pZih3aW5kb3cubmF2aWdhdG9yLnN0YW5kYWxvbmUpIHtcclxuXHRkb2N1bWVudC5ib2R5LmNsYXNzTmFtZSArPSAnIHN0YW5kYWxvbmUnXHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gc3RhcnQiLCJ2YXIgRW5nbGlzaCA9IG1vZHVsZS5leHBvcnRzID0ge1xyXG5cdC8vIExhbmd1YWdlIE5hbWVcclxuXHRsYW5ndWFnZV9uYW1lOiAnRW5nbGlzaCcgXHJcblxyXG5cdC8vIEF1dGhlbnRpY2F0aW9uXHJcblx0LCBsb2dpbl9idXR0b246ICdMb2dpbidcclxuXHQsIGxvZ2luX2xpbms6ICdBbHJlYWR5IGhhdmUgYW4gYWNjb3VudD8gTG9naW4hJ1xyXG5cdCwgcmVnaXN0ZXJfYnV0dG9uOiAnUmVnaXN0ZXInXHJcblx0LCByZWdpc3Rlcl9saW5rOiAnTmV3IGhlcmU/IFJlZ2lzdGVyIGFuIEFjY291bnQhJ1xyXG5cdCwgdXNlcm5hbWVfbGFiZWw6ICdVc2VybmFtZSdcclxuXHQsIHBhc3N3b3JkX2xhYmVsOiAnUGFzc3dvcmQnXHJcblx0LCBsb2dvdXRfYnV0dG9uOiAnTG9nb3V0J1xyXG5cclxuXHQvLyBHYW1lIExpc3RcclxuXHQsIGNoYWxsZW5nZV9idXR0b246ICdDaGFsbGVuZ2UnXHJcblx0LCBjaGFsbGVuZ2VfcmFuZG9tX2J1dHRvbjogJ1JhbmRvbSBDaGFsbGVuZ2UnXHJcblx0LCBub19nYW1lczogJ1lvdSBoYXZlIG5vIGdhbWVzLCB3aHkgbm90IHN0YXJ0IG9uZT8nXHJcblx0LCBuZXdfZ2FtZTogJ1N0YXJ0IGEgTmV3IEdhbWUnXHJcblx0LCBnYW1lX2xpc3Q6ICdCYWNrIHRvIEdhbWUgTGlzdCdcclxuXHQsIHlvdXJfdHVybjogJ1lvdXIgVHVybidcclxuXHQsIHRoZWlyX3R1cm46ICdUaGVpciBUdXJuJ1xyXG5cdCwgZ2FtZV9vdmVyOiAnR2FtZSBPdmVyJ1xyXG59XHJcbiIsInZhciBMYW5nID0gbW9kdWxlLmV4cG9ydHMgPSB7fVxyXG5cclxuTGFuZy5zZXQgPSBmdW5jdGlvbihuZXdMYW5nKSB7XHJcblx0TGFuZy5jdXJyZW50ID0gTGFuZy5sYW5ndWFnZXNbbmV3TGFuZ11cclxuXHR3aW5kb3cubG9jYWxTdG9yYWdlLmxhbmcgPSBuZXdMYW5nXHJcblx0TGFuZy5hbGwuZm9yRWFjaChmdW5jdGlvbihsYW5nKSB7IFxyXG5cdFx0bGFuZy5zZWxlY3RlZCA9IGxhbmcua2V5ID09IG5ld0xhbmcgXHJcblx0fSlcclxuXHJcbn1cclxuXHJcbkxhbmcubGFuZ3VhZ2VzID0ge1xyXG5cdCAgZW5nbGlzaDogcmVxdWlyZSgnLi9lbmdsaXNoJylcclxuXHQsIHdlaXJkOiByZXF1aXJlKCcuL3dlaXJkJylcclxufVxyXG5cclxuTGFuZy5hbGwgPSBbXVxyXG5PYmplY3Qua2V5cyhMYW5nLmxhbmd1YWdlcykuZm9yRWFjaChmdW5jdGlvbihsYW5nKSB7XHJcblx0TGFuZy5hbGwucHVzaCh7IGtleTpsYW5nLCBuYW1lOkxhbmcubGFuZ3VhZ2VzW2xhbmddLmxhbmd1YWdlX25hbWUsIHNlbGVjdGVkOndpbmRvdy5sb2NhbFN0b3JhZ2UubGFuZz09bGFuZyB9KVxyXG59KVxyXG5cclxuTGFuZy5jdXJyZW50ID0gTGFuZy5sYW5ndWFnZXNbd2luZG93LmxvY2FsU3RvcmFnZS5sYW5nXSB8fCBMYW5nLmxhbmd1YWdlcy5lbmdsaXNoXHJcbiIsInZhciBXZWlyZCA9IG1vZHVsZS5leHBvcnRzID0ge1xyXG5cdC8vIExhbmd1YWdlIE5hbWVcclxuXHRsYW5ndWFnZV9uYW1lOiAnRW5nbGlzaCAoV2VpcmQgPVApJyBcclxuXHJcblx0Ly8gQXV0aGVudGljYXRpb25cclxuXHQsIGxvZ2luX2J1dHRvbjogJ0xvZ2luIDopJ1xyXG5cdCwgbG9naW5fbGluazogJ1NvIHlvdSB0aGluayB5b3UgaGF2ZSBhbiBhY2NvdW50LCBlaD8nXHJcblx0LCByZWdpc3Rlcl9idXR0b246ICdSZWdpc3RlciA6KSdcclxuXHQsIHJlZ2lzdGVyX2xpbms6ICdVIG5ldz8gQ29tZSByZWdpc3RlciB3aXRoIG1lISdcclxuXHQsIHVzZXJuYW1lX2xhYmVsOiAnemUgVXNlcm5hbWUnXHJcblx0LCBwYXNzd29yZF9sYWJlbDogJ3plIFBhc3N3b3JkJ1xyXG5cdCwgbG9nb3V0X2J1dHRvbjogJ0xvZ291dCEhISEhISdcclxuXHJcblx0Ly8gR2FtZSBMaXN0XHJcblx0LCBjaGFsbGVuZ2VfYnV0dG9uOiAnVGhpcyBpcyBTcGFydGEhJ1xyXG5cdCwgbm9fZ2FtZXM6ICdXaHkgeW91IG5vIHBsYXkgZ2FtZXM/J1xyXG5cdCwgbmV3X2dhbWU6ICdTdGFydCB6ZSBOZXcgR2FtZSdcclxuXHQsIGdhbWVfbGlzdDogJ0JhY2sgdG8gemUgR2FtZSBMaXN0J1xyXG5cdFxyXG59XHJcbiIsInZhciBFdmVudCA9IHJlcXVpcmUoJ2V2ZW50cycpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG4gICwgQmFzZU1vZGVsXHJcblxyXG5CYXNlTW9kZWwgPSBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG5hbWUpIHtcclxuXHR0aGlzLl9sb2FkZWQgPSBmYWxzZVxyXG5cdHRoaXMuX2xvYWRpbmcgPSBmYWxzZVxyXG5cdHRoaXMuX25hbWUgPSBuYW1lXHJcbn1cclxuXHJcbkJhc2VNb2RlbC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEV2ZW50LkV2ZW50RW1pdHRlci5wcm90b3R5cGUpXHJcblxyXG5CYXNlTW9kZWwucHJvdG90eXBlLl9sb2FkID0gZnVuY3Rpb24oKSB7XHJcblx0d2luZG93W3RoaXMuX25hbWUrJ0xvYWRTdGFydCddID0gRGF0ZS5ub3coKVxyXG5cdHZhciBzZWxmID0gdGhpc1xyXG5cdHNlbGYuX2xvYWRpbmcgPSB0cnVlXHJcblx0c2VydmVyLmVtaXQoc2VsZi5fbmFtZSsnL2xpc3QnKVxyXG5cdHNlcnZlci5vbmNlKHNlbGYuX25hbWUrJy9saXN0L3Jlc3BvbnNlJywgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0aWYoZGF0YS5lcnIpIHJldHVybiBhbGVydChkYXRhLmVycilcclxuXHRcdHNlbGYuX2RhdGEgPSBzZWxmLl9kYXRhIHx8IHt9XHJcblx0XHRkYXRhLmZvckVhY2goZnVuY3Rpb24oaXRlbSkge1xyXG5cdFx0XHRzZWxmLl9kYXRhW2l0ZW0uX2lkXSA9IGl0ZW1cclxuXHRcdH0pXHJcblx0XHRzZWxmLl9sb2FkZWQgPSB0cnVlXHJcblx0XHRzZWxmLl9sb2FkaW5nID0gZmFsc2VcclxuXHRcdGlmKHNlbGYuX2luaXQpIHNlbGYuX2luaXQoKVxyXG5cdFx0d2luZG93W3NlbGYuX25hbWUrJ0xvYWRFbmQnXSA9IERhdGUubm93KClcclxuXHRcdHNlbGYuZW1pdCgnbG9hZGVkJywgc2VsZi5fZGF0YSlcclxuXHR9KVxyXG5cdHNlcnZlci5vbihzZWxmLl9uYW1lKycvbmV3JywgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0c2VsZi5fZGF0YVtkYXRhLl9pZF0gPSBkYXRhXHJcblx0XHRzZWxmLl9uZXcoZGF0YSlcclxuXHR9KVxyXG59XHJcblxyXG5CYXNlTW9kZWwucHJvdG90eXBlLl9uZXcgPSBmdW5jdGlvbihkYXRhKSB7XHJcblx0dGhpcy5lbWl0KCduZXcnLCBkYXRhKVxyXG59XHJcblxyXG5CYXNlTW9kZWwucHJvdG90eXBlLnJlbW92ZSA9IGZ1bmN0aW9uKF9pZCkge1xyXG5cdHZhciBkYXRhID0gdGhpcy5fZGF0YVtfaWRdXHJcblx0ZGVsZXRlIHRoaXMuX2RhdGFbX2lkXVxyXG5cdHRoaXMuZW1pdCgncmVtb3ZlJywgZGF0YSlcclxufVxyXG5cclxuQmFzZU1vZGVsLnByb3RvdHlwZS5jbGVhciA9IGZ1bmN0aW9uKCkge1xyXG5cdHRoaXMuX2xvYWRlZCA9IGZhbHNlXHJcblx0dGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoKVxyXG5cdGRlbGV0ZSB0aGlzLl9kYXRhXHJcbn1cclxuXHJcbkJhc2VNb2RlbC5wcm90b3R5cGUuYWxsID0gZnVuY3Rpb24oZm4pIHtcclxuXHRpZih0aGlzLl9sb2FkZWQpIHJldHVybiBmbih0aGlzLl9kYXRhKVxyXG5cdGlmKCF0aGlzLl9sb2FkaW5nKSB0aGlzLl9sb2FkKClcclxuXHR0aGlzLm9uY2UoJ2xvYWRlZCcsIGZuKVxyXG59XHJcblxyXG5CYXNlTW9kZWwucHJvdG90eXBlLmdldEJ5SWQgPSBmdW5jdGlvbihfaWQsIGZuKSB7XHJcblx0dmFyIHNlbGYgPSB0aGlzXHJcblx0dGhpcy5hbGwoZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0aWYoZGF0YVtfaWRdKSB7XHJcblx0XHRcdGZuKGRhdGFbX2lkXSlcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHNlbGYub25jZSgnbmV3JywgbmV3TGlzdGVuZXIpXHJcblx0XHR9XHJcblx0fSlcclxuXHJcblx0ZnVuY3Rpb24gbmV3TGlzdGVuZXIoZGF0YSkge1xyXG5cdFx0aWYoZGF0YS5faWQgPT0gX2lkKSB7XHJcblx0XHRcdGZuKGRhdGEpXHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRzZWxmLm9uY2UoJ25ldycsIG5ld0xpc3RlbmVyKVxyXG5cdFx0fVxyXG5cdH1cclxufSIsInZhciBCYXNlTW9kZWwgPSByZXF1aXJlKCcuL2Jhc2UnKVxyXG4gICwgR2FtZXMgPSB3aW5kb3cuR2FtZXMgPSBtb2R1bGUuZXhwb3J0cyA9IG5ldyBCYXNlTW9kZWwoJ2dhbWVzJylcclxuICAsIFVzZXJzID0gcmVxdWlyZSgnLi91c2VycycpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG5cclxuR2FtZXMuYnlMaXN0ID0gZnVuY3Rpb24oZm4pIHtcclxuXHRpZih0aGlzLl9sb2FkZWQpIHJldHVybiBmbih0aGlzLmxpc3RzKVxyXG5cdGlmKCF0aGlzLl9sb2FkaW5nKSB0aGlzLl9sb2FkKClcclxuXHR0aGlzLm9uY2UoJ2xpc3RlZCcsIGZuKVxyXG59XHJcblxyXG5HYW1lcy5nZXRCeU9wcG9uZW50SWQgPSBmdW5jdGlvbihvcHBvbmVudF9pZCwgZm4pIHtcclxuXHRpZih0aGlzLl9sb2FkZWQpIHJldHVybiBmbih0aGlzLm9wcG9uZW50c1tvcHBvbmVudF9pZF0pXHJcblx0aWYoIXRoaXMuX2xvYWRpbmcpIHRoaXMuX2xvYWQoKVxyXG5cdHRoaXMub25jZSgnb3Bwb25lbnRpemVkJywgZnVuY3Rpb24ob3Bwb25lbnRzKSB7XHJcblx0XHRmbihvcHBvbmVudHNbb3Bwb25lbnRfaWRdKVxyXG5cdH0pXHJcbn1cclxuXHJcbkdhbWVzLmdldEJ5SWQgPSBmdW5jdGlvbihnYW1lX2lkLCBmbikge1xyXG5cdHZhciBzZWxmID0gdGhpc1xyXG5cclxuXHRCYXNlTW9kZWwucHJvdG90eXBlLmdldEJ5SWQuY2FsbChHYW1lcywgZ2FtZV9pZCwgZnVuY3Rpb24oZ2FtZSkge1xyXG5cdFx0aWYoIWdhbWUuX2luY29tcGxldGUpIHJldHVybiBmbihnYW1lKVxyXG5cdFx0c2VsZi5vbmNlKCdjb21wbGV0ZWQnLCBjb21wbGV0ZWRMaXN0ZW5lcilcclxuXHR9KVxyXG5cclxuXHRmdW5jdGlvbiBjb21wbGV0ZWRMaXN0ZW5lcihnYW1lKSB7XHJcblx0XHRpZihnYW1lLl9pZCA9PSBnYW1lX2lkKSB7XHJcblx0XHRcdGZuKGdhbWUpXHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRzZWxmLm9uY2UoJ2NvbXBsZXRlZCcsIGNvbXBsZXRlZExpc3RlbmVyKVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuR2FtZXMuX2luaXQgPSBmdW5jdGlvbigpIHtcclxuXHR0aGlzLm9uY2UoJ2xvYWRlZCcsIGxpc3RHYW1lcylcclxuXHR0aGlzLm9uY2UoJ2xvYWRlZCcsIG9wcG9uZW50aXplKVxyXG5cdHRoaXMub25jZSgnbGlzdGVkJywgbG9hZEZ1bGxHYW1lcylcclxuXHRVc2Vycy5vbignYWN0aXZpdHknLCBmdW5jdGlvbih1c2VyX2lkKSB7XHJcblx0XHR2YXIgZ2FtZXMgPSBHYW1lcy5vcHBvbmVudHNbdXNlcl9pZF1cclxuXHRcdGlmKGdhbWVzKSB7XHJcblx0XHRcdGdhbWVzLmZvckVhY2goZnVuY3Rpb24oZ2FtZSkge1xyXG5cdFx0XHRcdHZhciBsaXN0ID0gZ2V0TGlzdChnYW1lKVxyXG5cdFx0XHRcdCAgLCBvbGRfaW5kZXggPSBHYW1lcy5saXN0c1tsaXN0XS5pbmRleE9mKGdhbWUpXHJcblx0XHRcdFx0ICAsIG5ld19pbmRleFxyXG5cclxuXHRcdFx0XHRHYW1lcy5saXN0c1tsaXN0XS5zcGxpY2Uob2xkX2luZGV4LCAxKVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdG5ld19pbmRleCA9IGdldEluZGV4KGdhbWUsIGxpc3QpXHJcblxyXG5cdFx0XHRcdEdhbWVzLmxpc3RzW2xpc3RdLnNwbGljZShuZXdfaW5kZXgsIDAsIGdhbWUpXHJcblxyXG5cdFx0XHRcdEdhbWVzLmVtaXQoJ29wcG9uZW50LWFjdGl2aXR5JywgZ2FtZSwgbGlzdCwgbmV3X2luZGV4KVxyXG5cdFx0XHR9KVxyXG5cdFx0fVxyXG5cdH0pXHJcblx0c2VydmVyLm9uKCdnYW1lcy9ldmVudCcsIGZ1bmN0aW9uKGdhbWVfaWQsIGV2ZW50KSB7XHJcblx0XHRpZihldmVudC50eXBlID09ICdyZXNpZ24nICYmIGV2ZW50LmluaXRpYXRvciA9PSBVc2Vycy5zZWxmLl9pZCkge1xyXG5cdFx0XHRHYW1lcy5yZW1vdmUoZ2FtZV9pZClcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHZhciBnYW1lID0gZXZlbnQuc3RhdGVcclxuXHRcdFx0ICAsIGxpc3QgPSBnZXRMaXN0KGdhbWUpXHJcblxyXG5cdFx0XHRHYW1lcy5nZXRCeUlkKGdhbWVfaWQsIGZ1bmN0aW9uKG9sZF9nYW1lKSB7XHJcblx0XHRcdFx0dmFyIG9sZF9saXN0ID0gZ2V0TGlzdChvbGRfZ2FtZSlcclxuXHRcdFx0XHQgICwgb2xkX2luZGV4ID0gR2FtZXMubGlzdHNbb2xkX2xpc3RdLmluZGV4T2Yob2xkX2dhbWUpXHJcblx0XHRcdFx0ICAsXHRuZXdfaW5kZXhcclxuXHJcblx0XHRcdFx0R2FtZXMubGlzdHNbb2xkX2xpc3RdLnNwbGljZShvbGRfaW5kZXgsIDEpXHJcblx0XHRcdFx0XHJcblx0XHRcdFx0Z2FtZS5faWQgPSBnYW1lX2lkXHJcblx0XHRcdFx0Z2FtZS5fID0geyBoaXN0b3J5OiBvbGRfZ2FtZS5fLmhpc3RvcnkgfVxyXG5cdFx0XHRcdGdhbWUuXy5oaXN0b3J5LnB1c2goZXZlbnQpXHJcblx0XHRcdFx0R2FtZXMuX2RhdGFbZ2FtZV9pZF0gPSBnYW1lXHJcblxyXG5cdFx0XHRcdG5ld19pbmRleCA9IGdldEluZGV4KGdhbWUsIGxpc3QpXHJcblx0XHRcdFx0R2FtZXMubGlzdHNbbGlzdF0uc3BsaWNlKG5ld19pbmRleCwgMCwgZ2FtZSlcclxuXHJcblx0XHRcdFx0R2FtZXMuZW1pdCgnZXZlbnQnLCBnYW1lLCBldmVudCwgbGlzdCwgb2xkX2xpc3QsIG5ld19pbmRleClcclxuXHRcdFx0XHRpZihnYW1lLnJlc3VsdCkgR2FtZXMuZW1pdCgnZW5kJywgZ2FtZSlcclxuXHRcdFx0fSlcclxuXHRcdH1cclxuXHR9KVxyXG59XHJcblxyXG5HYW1lcy5yZW1vdmUgPSBmdW5jdGlvbihfaWQpIHtcclxuXHR2YXIgZ2FtZSwgbGlzdCwgbGlzdEluZGV4XHJcblx0aWYoZ2FtZSA9IHRoaXMuX2RhdGFbX2lkXSkge1xyXG5cdFx0bGlzdCA9IGdldExpc3QoZ2FtZSlcclxuXHRcdGxpc3RJbmRleCA9IEdhbWVzLmxpc3RzW2xpc3RdLmluZGV4T2YoZ2FtZSlcclxuXHRcdGRlbGV0ZSB0aGlzLl9kYXRhW19pZF1cclxuXHRcdEdhbWVzLmxpc3RzW2xpc3RdLnNwbGljZShsaXN0SW5kZXgsIDEpXHJcblx0XHRHYW1lcy5saXN0cy5jb3VudC0tXHJcblx0XHR0aGlzLmVtaXQoJ3JlbW92ZScsIGdhbWUsIGxpc3QsIEdhbWVzLmxpc3RzLmNvdW50KVxyXG5cdFx0aWYoIWdhbWUucmVzdWx0KSBHYW1lcy5lbWl0KCdlbmQnLCBnYW1lKVxyXG5cdH1cclxufVxyXG5cclxuR2FtZXMuX25ldyA9IGZ1bmN0aW9uKGdhbWUpIHtcclxuXHR2YXIgbGlzdCA9IGdldExpc3QoZ2FtZSlcclxuXHR2YXIgaW5kZXggPSBnZXRJbmRleChnYW1lLCBsaXN0KVxyXG5cdEdhbWVzLmxpc3RzW2xpc3RdLnNwbGljZShpbmRleCwgMCwgZ2FtZSlcclxuXHRHYW1lcy5saXN0cy5jb3VudCsrXHJcblx0dGhpcy5lbWl0KCduZXcnLCBnYW1lLCBsaXN0LCBpbmRleClcclxufVxyXG5cclxuZnVuY3Rpb24gbG9hZEZ1bGxHYW1lcyhsaXN0cykge1xyXG5cdHZhciBzZWxmID0gdGhpc1xyXG5cdCAgLCBnYW1lcyA9IHRoaXMuX2RhdGFcclxuXHQ7Wyd5b3VyX3R1cm4nLCAndGhlaXJfdHVybicsICdnYW1lX292ZXInXS5mb3JFYWNoKGZ1bmN0aW9uKGxpc3RfbmFtZSkge1xyXG5cdFx0bGlzdHNbbGlzdF9uYW1lXS5mb3JFYWNoKGZ1bmN0aW9uKGdhbWUpIHtcclxuXHRcdFx0Z2FtZS5faW5jb21wbGV0ZSA9IHRydWVcclxuXHRcdFx0c2VydmVyLmVtaXQoJ2dhbWVzL2J5X2lkJywgZ2FtZS5faWQpXHJcblx0XHR9KVxyXG5cdH0pXHJcblx0c2VydmVyLm9uKCdnYW1lcy9ieV9pZC9yZXNwb25zZScsIGZ1bmN0aW9uKGdhbWUpIHtcclxuXHRcdGdhbWVzW2dhbWUuX2lkXSA9IGV4dGVuZChnYW1lc1tnYW1lLl9pZF0sIGdhbWUpXHJcblx0XHRkZWxldGUgZ2FtZXNbZ2FtZS5faWRdLl9pbmNvbXBsZXRlXHJcblx0XHRzZWxmLmVtaXQoJ2NvbXBsZXRlZCcsIGdhbWVzW2dhbWUuX2lkXSlcclxuXHR9KVxyXG59XHJcblxyXG5mdW5jdGlvbiBvcHBvbmVudGl6ZShnYW1lcykge1xyXG5cdHZhciBnYW1lX2lkcyA9IE9iamVjdC5rZXlzKGdhbWVzKVxyXG5cdEdhbWVzLm9wcG9uZW50cyA9IHt9XHJcblx0XHJcblx0Z2FtZV9pZHMuZm9yRWFjaChmdW5jdGlvbihfaWQpIHtcclxuXHRcdHZhciBnYW1lID0gZ2FtZXNbX2lkXVxyXG5cdFx0R2FtZXMub3Bwb25lbnRzW2dhbWUub3Bwb25lbnQuX2lkXSA9IEdhbWVzLm9wcG9uZW50c1tnYW1lLm9wcG9uZW50Ll9pZF0gfHwgW11cclxuXHRcdEdhbWVzLm9wcG9uZW50c1tnYW1lLm9wcG9uZW50Ll9pZF0ucHVzaChnYW1lKVxyXG5cdH0pXHJcblx0R2FtZXMuZW1pdCgnb3Bwb25lbnRpemVkJywgR2FtZXMub3Bwb25lbnRzKVxyXG59XHJcblxyXG5mdW5jdGlvbiBsaXN0R2FtZXMoZ2FtZXMpIHtcclxuXHR2YXIgR2FtZXMgPSB0aGlzXHJcblx0ICAsIGdhbWVfaWRzID0gT2JqZWN0LmtleXMoZ2FtZXMpXHJcblx0R2FtZXMubGlzdHMgPSB7IHlvdXJfdHVybjpbXSwgdGhlaXJfdHVybjpbXSwgZ2FtZV9vdmVyOltdLCBjb3VudDpnYW1lX2lkcy5sZW5ndGggfVxyXG5cdFxyXG5cdGdhbWVfaWRzLmZvckVhY2goZnVuY3Rpb24oX2lkKSB7XHJcblx0XHR2YXIgZ2FtZSA9IGdhbWVzW19pZF1cclxuXHRcdCAgLCBsaXN0ID0gZ2V0TGlzdChnYW1lKVxyXG5cclxuXHRcdEdhbWVzLmxpc3RzW2xpc3RdLnB1c2goZ2FtZSlcclxuXHR9KVxyXG5cdFVzZXJzLmFsbChmdW5jdGlvbigpIHtcclxuXHRcdEdhbWVzLmxpc3RzLnlvdXJfdHVybi5zb3J0KGNvbXBhcmUpXHJcblx0XHRHYW1lcy5saXN0cy50aGVpcl90dXJuLnNvcnQoY29tcGFyZSlcclxuXHRcdEdhbWVzLmxpc3RzLmdhbWVfb3Zlci5zb3J0KGNvbXBhcmUpXHJcblx0XHRHYW1lcy5lbWl0KCdsaXN0ZWQnLCBHYW1lcy5saXN0cylcclxuXHR9KVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRJbmRleChnYW1lLCBsaXN0KSB7XHJcblx0dmFyIGl0ZW1zID0gR2FtZXMubGlzdHNbbGlzdF1cclxuXHJcblx0aWYoaXRlbXMubGVuZ3RoID4gMCkge1xyXG5cdFx0dmFyIHN0YXJ0X2luZGV4ID0gMFxyXG5cdFx0ICAsIHN0b3BfaW5kZXggPSBpdGVtcy5sZW5ndGggLSAxXHJcblx0XHQgICwgbWlkZGxlID0gTWF0aC5mbG9vcigoc3RvcF9pbmRleCArIHN0YXJ0X2luZGV4KS8yKVxyXG5cdFx0ICAsIGluc2VydGVkID0gZmFsc2VcclxuXHRcdCAgLCBpbmRleCA9IDBcclxuXHRcdHdoaWxlKCFpbnNlcnRlZCkge1xyXG5cdFx0XHR2YXIgaXRlbSA9IGl0ZW1zW21pZGRsZV1cclxuXHRcdFx0ICAsIGNvbXBhcmlzb24gPSBjb21wYXJlKGdhbWUsIGl0ZW0pXHJcblxyXG5cdFx0XHRpZihjb21wYXJpc29uIDwgMCkge1xyXG5cdFx0XHRcdHN0b3BfaW5kZXggPSBtaWRkbGUgLSAxXHJcblx0XHRcdFx0aWYobWlkZGxlID09IHN0YXJ0X2luZGV4KSB7XHJcblx0XHRcdFx0XHRpbnNlcnRlZCA9IHRydWVcclxuXHRcdFx0XHRcdGluZGV4ID0gbWlkZGxlXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2UgaWYoY29tcGFyaXNvbiA+IDApIHtcclxuXHRcdFx0XHRzdGFydF9pbmRleCA9IG1pZGRsZSArIDFcclxuXHRcdFx0XHRpZihtaWRkbGUgPT0gc3RvcF9pbmRleCkge1xyXG5cdFx0XHRcdFx0aW5zZXJ0ZWQgPSB0cnVlXHJcblx0XHRcdFx0XHRpbmRleCA9IG1pZGRsZSArIDFcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aW5zZXJ0ZWQgPSB0cnVlXHJcblx0XHRcdFx0aW5kZXggPSBtaWRkbGUgKyAxXHJcblx0XHRcdH1cclxuXHRcdFx0bWlkZGxlID0gTWF0aC5mbG9vcigoc3RvcF9pbmRleCArIHN0YXJ0X2luZGV4KS8yKVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIGluZGV4XHJcblx0fSBlbHNlIHtcclxuXHRcdHJldHVybiAwXHJcblx0fVxyXG59XHJcblxyXG5mdW5jdGlvbiBjb21wYXJlKGEsIGIpIHtcclxuXHR2YXIgcmVzdWx0ID0gMFxyXG5cdHJldHVybiByZXN1bHRcclxuXHRVc2Vycy5hbGwoZnVuY3Rpb24odXNlcnMpIHtcclxuXHRcdGNvbnNvbGUubG9nKHVzZXJzKVxyXG5cdFx0Y29uc29sZS5sb2coYi5vcHBvbmVudC5faWQpXHJcblx0XHRjb25zb2xlLmxvZyhhLm9wcG9uZW50KVxyXG5cdFx0aWYodXNlcnNbYS5vcHBvbmVudC5faWRdLm9ubGluZSAhPSB1c2Vyc1tiLm9wcG9uZW50Ll9pZF0ub25saW5lKSByZXN1bHQgPSB1c2Vyc1thLm9wcG9uZW50Ll9pZF0ub25saW5lID8gLTEgOiAxXHJcblx0XHRlbHNlIGlmKHVzZXJzW2Eub3Bwb25lbnQuX2lkXS5hY3RpdmUgIT0gdXNlcnNbYi5vcHBvbmVudC5faWRdLmFjdGl2ZSkgcmVzdWx0ID0gdXNlcnNbYS5vcHBvbmVudC5faWRdLmFjdGl2ZSA/IC0xIDogMVxyXG5cdH0pXHJcblxyXG5cdGlmKCFyZXN1bHQpIEdhbWVzLmFsbChmdW5jdGlvbihnYW1lcykge1xyXG5cdFx0dmFyIGFfaGlzdG9yeSA9IGdhbWVzW2EuX2lkXS5sYXN0X21vdmVcclxuXHRcdHZhciBiX2hpc3RvcnkgPSBnYW1lc1tiLl9pZF0ubGFzdF9tb3ZlXHJcblx0XHRpZihhX2hpc3RvcnkgJiYgYl9oaXN0b3J5KSBcclxuXHRcdFx0cmVzdWx0ID0gbmV3IERhdGUoYl9oaXN0b3J5LnRpbWVzdGFtcCkgLSBuZXcgRGF0ZShhX2hpc3RvcnkudGltZXN0YW1wKVxyXG5cdH0pXHJcblxyXG5cdHJldHVybiByZXN1bHRcclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0TGlzdChnYW1lKSB7XHJcblx0cmV0dXJuIGdhbWUucmVzdWx0ID8gJ2dhbWVfb3ZlcicgOiBnYW1lLnR1cm4gPT0gZ2FtZS5vcHBvbmVudC5faWQgPyAndGhlaXJfdHVybicgOiAneW91cl90dXJuJ1xyXG59XHJcblxyXG5mdW5jdGlvbiBleHRlbmQoZXh0ZW5kaW5nLCBkYXRhKSB7XHJcblx0T2JqZWN0LmtleXMoZGF0YSkuZm9yRWFjaChmdW5jdGlvbihrZXkpIHtcclxuXHRcdGV4dGVuZGluZ1trZXldID0gZGF0YVtrZXldXHJcblx0fSlcclxuXHRyZXR1cm4gZXh0ZW5kaW5nXHJcbn0iLCJ2YXIgQmFzZU1vZGVsID0gcmVxdWlyZSgnLi9iYXNlJylcclxuICAsIFVzZXJzID0gd2luZG93LlVzZXJzID0gbW9kdWxlLmV4cG9ydHMgPSBuZXcgQmFzZU1vZGVsKCd1c2VycycpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG5cclxuVXNlcnMuX2luaXQgPSBmdW5jdGlvbigpIHtcclxuXHRzZXJ2ZXIub24oJ3VzZXJzL2FjdGl2aXR5JywgZnVuY3Rpb24odXNlcl9pZCwgb25saW5lLCBhY3RpdmUpIHtcclxuXHRcdFVzZXJzLmdldEJ5SWQodXNlcl9pZCwgZnVuY3Rpb24odXNlcikge1xyXG5cdFx0XHR1c2VyLm9ubGluZSA9IG9ubGluZVxyXG5cdFx0XHR1c2VyLmFjdGl2ZSA9IGFjdGl2ZVxyXG5cdFx0XHRVc2Vycy5lbWl0KCdhY3Rpdml0eScsIHVzZXJfaWQsIG9ubGluZSwgYWN0aXZlKVxyXG5cdFx0fSlcclxuXHR9KVxyXG5cdHNlcnZlci5vbigndXNlcnMvZXZlbnQnLCBmdW5jdGlvbih1c2VyX2lkLCB1c2VyLCBldmVudCkge1xyXG5cdFx0T2JqZWN0LmtleXModXNlcikuZm9yRWFjaChmdW5jdGlvbihrZXkpIHtcclxuXHRcdFx0VXNlcnMuX2RhdGFbdXNlcl9pZF1ba2V5XSA9IHVzZXJba2V5XVxyXG5cdFx0fSlcclxuXHRcdFVzZXJzLmVtaXQoJ3VwZGF0ZScsIFVzZXJzLl9kYXRhW3VzZXJfaWRdLCBldmVudClcclxuXHR9KVxyXG59XHJcblxyXG5Vc2Vycy5zZXRTZWxmID0gZnVuY3Rpb24odXNlcikge1xyXG5cdHRoaXMuX2RhdGEgPSB0aGlzLl9kYXRhIHx8IHt9XHJcblx0dGhpcy5fZGF0YVt1c2VyLl9pZF0gPSB1c2VyXHJcblx0dGhpcy5zZWxmID0gdXNlclxyXG59XHJcbiIsInZhciBCYXNlUHJlc2VudGVyID0gcmVxdWlyZSgnLi9iYXNlJylcclxuICAsIEFjY291bnQgPSBtb2R1bGUuZXhwb3J0cyA9IG5ldyBCYXNlUHJlc2VudGVyKCcjYWNjb3VudCcpXHJcbiAgLCBNYWluID0gcmVxdWlyZSgnLi9tYWluJylcclxuICAsIExvZ2luID0gcmVxdWlyZSgnLi9sb2dpbicpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG4gICwgVXNlcnMgPSByZXF1aXJlKCcuLi9tb2RlbHMvdXNlcnMnKVxyXG4gICwgR2FtZXMgPSByZXF1aXJlKCcuLi9tb2RlbHMvZ2FtZXMnKVxyXG4gICwgbGFuZyA9IHJlcXVpcmUoJy4uL2xhbmcnKVxyXG5cclxuQWNjb3VudC5faW5pdCA9IGZ1bmN0aW9uKGNvbnRhaW5lcikge1xyXG5cdEFjY291bnQucmVuZGVyKCdhY2NvdW50JywgeyBsYW5ndWFnZXM6bGFuZy5hbGwgfSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRBY2NvdW50LmNvbnRhaW5lci5pbm5lckhUTUwgPSBodG1sXHJcblx0XHRBY2NvdW50Lmxpc3RlbignYnV0dG9uLmNsb3NlJywgJ2NsaWNrJywgQWNjb3VudC5oaWRlKVxyXG5cdFx0QWNjb3VudC5saXN0ZW4oJy5hY2NvdW50X2luZm8gc2VsZWN0Lmxhbmd1YWdlcycsICdjaGFuZ2UnLCBjaGFuZ2VMYW5ndWFnZSlcclxuXHRcdEFjY291bnQubGlzdGVuKCdmb3JtLmFjY291bnRfaW5mbycsICdzdWJtaXQnLCBzYXZlQWNjb3VudClcclxuXHRcdEFjY291bnQubGlzdGVuKCdidXR0b24ubG9nb3V0JywgJ2NsaWNrJywgbG9nb3V0KVxyXG5cdFx0QWNjb3VudC5saXN0ZW4oJ1RyYW5zaXRpb25FbmQnLCBkaXNwbGF5Tm9uZVdoZW5Eb25lKVxyXG5cdH0pXHJcbn1cclxuXHJcbkFjY291bnQuc2hvdyA9IGZ1bmN0aW9uKCkge1xyXG5cdEFjY291bnQuY29udGFpbmVyLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snXHJcblx0QWNjb3VudC5jb250YWluZXIuc2V0QXR0cmlidXRlKCdkYXRhLXZpc2libGUnLCB0cnVlKVxyXG59XHJcblxyXG5BY2NvdW50LmhpZGUgPSBmdW5jdGlvbigpIHtcclxuXHRBY2NvdW50LmNvbnRhaW5lci5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtdmlzaWJsZScpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGRpc3BsYXlOb25lV2hlbkRvbmUoKSB7XHJcblx0aWYoIXBhcnNlSW50KHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKEFjY291bnQuY29udGFpbmVyKS5vcGFjaXR5KSkge1xyXG5cdFx0QWNjb3VudC5jb250YWluZXIuc3R5bGUuZGlzcGxheSA9ICdub25lJ1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gc2F2ZUFjY291bnQoZSkge1xyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG5cdGRhdGEgPSB7fVxyXG5cdGRhdGEub2xkX3Bhc3N3b3JkID0gQWNjb3VudC5maW5kKCdbbmFtZT1cIm9sZF9wYXNzd29yZFwiXScpLnZhbHVlXHJcblx0ZGF0YS5wYXNzd29yZCA9IEFjY291bnQuZmluZCgnW25hbWU9XCJwYXNzd29yZFwiXScpLnZhbHVlXHJcblx0c2VydmVyLmVtaXQoJ2FjY291bnQvc2F2ZVBhc3N3b3JkJywgeyBkYXRhOmRhdGEgfSlcclxuXHRzZXJ2ZXIub25jZSgnYWNjb3VudC9zYXZlUGFzc3dvcmQvcmVzcG9uc2UnLCBmdW5jdGlvbihlcnIsIHRva2VuKSB7XHJcblx0XHRpZighZXJyKSB7XHJcblx0XHRcdHdpbmRvdy5sb2NhbFN0b3JhZ2UudG9rZW4gPSB0b2tlblxyXG5cdFx0XHRVc2Vycy5zZXRTZWxmKHVzZXIpXHJcblx0XHR9XHJcblx0fSlcclxufVxyXG5cclxuZnVuY3Rpb24gY2hhbmdlTGFuZ3VhZ2UoZSkge1xyXG5cdGxhbmcuc2V0KGUudGFyZ2V0LnZhbHVlKVxyXG5cdE1haW4uaW5pdCh0cnVlKVxyXG5cdEFjY291bnQuc2hvdygpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGxvZ291dCgpIHtcclxuXHRzZXJ2ZXIuZW1pdCgnYXV0aC9sb2dvdXQnLCBsb2NhbFN0b3JhZ2UudG9rZW4pXHJcblx0c2VydmVyLnJlbW92ZUFsbExpc3RlbmVycygpXHJcblx0R2FtZXMuY2xlYXIoKVxyXG5cdFVzZXJzLmNsZWFyKClcclxuXHR3aW5kb3cubG9jYWxTdG9yYWdlLnRva2VuID0gJydcclxuXHRMb2dpbi5pbml0KClcclxufSIsInZhciBCYXNlUHJlc2VudGVyXHJcbiAgLCBFdmVudCA9IHJlcXVpcmUoJ2V2ZW50cycpXHJcbiAgLCBsYW5nID0gcmVxdWlyZSgnLi4vbGFuZycpXHJcblxyXG5CYXNlUHJlc2VudGVyID0gbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihzZWxlY3Rvcikge1xyXG5cdHRoaXMuY29udGFpbmVyU2VsZWN0b3IgPSBzZWxlY3RvciB8fCAnI2FwcCdcclxufVxyXG5cclxuQmFzZVByZXNlbnRlci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEV2ZW50LkV2ZW50RW1pdHRlci5wcm90b3R5cGUpXHJcblxyXG5CYXNlUHJlc2VudGVyLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24oKSB7XHJcblx0dGhpcy5jb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRoaXMuY29udGFpbmVyU2VsZWN0b3IpXHJcblx0cmV0dXJuIHRoaXMuX2luaXQuYXBwbHkodGhpcywgYXJndW1lbnRzKVxyXG59XHJcblxyXG5CYXNlUHJlc2VudGVyLnByb3RvdHlwZS5maW5kID0gZnVuY3Rpb24oc2VsZWN0b3IpIHtcclxuXHRyZXR1cm4gdGhpcy5jb250YWluZXIucXVlcnlTZWxlY3RvcihzZWxlY3RvcilcclxufVxyXG5cclxuQmFzZVByZXNlbnRlci5wcm90b3R5cGUuZmluZEFsbCA9IGZ1bmN0aW9uKHNlbGVjdG9yKSB7XHJcblx0cmV0dXJuIHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpIFxyXG59XHJcblxyXG5CYXNlUHJlc2VudGVyLnByb3RvdHlwZS5saXN0ZW4gPSBmdW5jdGlvbihzZWxlY3RvciwgZXZlbnQsIGZuKSB7XHJcblx0dmFyIGVsZW1lbnRcclxuXHRpZihhcmd1bWVudHMubGVuZ3RoID09IDIpIHtcclxuXHRcdGVsZW1lbnQgPSB0aGlzLmNvbnRhaW5lclxyXG5cdFx0Zm4gPSBldmVudFxyXG5cdFx0ZXZlbnQgPSBzZWxlY3RvclxyXG5cdH0gZWxzZSB7XHJcblx0XHRlbGVtZW50ID0gdHlwZW9mIHNlbGVjdG9yICE9ICdzdHJpbmcnID8gc2VsZWN0b3IgOiB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKVxyXG5cdH1cclxuXHRhZGRFdmVudExpc3RlbmVyKGVsZW1lbnQsIGV2ZW50LCBmbiwgdHJ1ZSlcclxufVxyXG5cclxuQmFzZVByZXNlbnRlci5wcm90b3R5cGUubGlzdGVuQWxsID0gZnVuY3Rpb24oc2VsZWN0b3IsIGV2ZW50LCBmbikge1xyXG5cdHZhciB2aWV3ID0gdGhpc1xyXG5cdEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwodmlldy5jb250YWluZXIucXVlcnlTZWxlY3RvckFsbChzZWxlY3RvciksIGZ1bmN0aW9uKGVsZW1lbnQpIHtcclxuXHRcdGFkZEV2ZW50TGlzdGVuZXIoZWxlbWVudCwgZXZlbnQsIGZuLCB0cnVlKVxyXG5cdH0pXHJcbn1cclxuXHJcbkJhc2VQcmVzZW50ZXIucHJvdG90eXBlLmRlbGVnYXRlID0gZnVuY3Rpb24ocGFyZW50U2VsZWN0b3IsIGNoaWxkU2VsZWN0b3IsIGV2ZW50LCBmbikge1xyXG5cdHZhciBlbGVtZW50LCBkZWxlZ2F0aW9ucyA9IHRoaXMuZGVsZWdhdGlvbnMgPSB0aGlzLmRlbGVnYXRpb25zIHx8IHt9XHJcblxyXG5cdGlmKGFyZ3VtZW50cy5sZW5ndGggPT0gMykge1xyXG5cdFx0ZWxlbWVudCA9IHRoaXMuY29udGFpbmVyXHJcblx0XHRmbiA9IGV2ZW50XHJcblx0XHRldmVudCA9IGNoaWxkU2VsZWN0b3JcclxuXHRcdGNoaWxkU2VsZWN0b3IgPSBwYXJlbnRTZWxlY3RvclxyXG5cdH0gZWxzZSB7XHJcblx0XHRlbGVtZW50ID0gdHlwZW9mIHBhcmVudFNlbGVjdG9yICE9ICdzdHJpbmcnID8gcGFyZW50U2VsZWN0b3IgOiB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKHBhcmVudFNlbGVjdG9yKVxyXG5cdH1cclxuXHJcblx0aWYoIWVsZW1lbnQuaGFzQXR0cmlidXRlKCdkYXRhLWRlbGVnYXRlZC0nK2V2ZW50KSkge1xyXG5cdFx0dmFyIGlkID0gZXZlbnQgKyAnLScgKyBNYXRoLnJhbmRvbSgpXHJcblx0XHRlbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1kZWxlZ2F0ZWQtJytldmVudCwgaWQpXHJcblx0XHRhZGRFdmVudExpc3RlbmVyKGVsZW1lbnQsIGV2ZW50LCBmdW5jdGlvbiBldmVudEhhbmRsZXIoZSwgcHJveGllZCkge1xyXG5cdFx0XHR2YXIgcGFyZW50ID0gZS50YXJnZXQucGFyZW50Tm9kZVxyXG5cdFx0XHRcclxuXHRcdFx0aWYoIXByb3hpZWQpIGUgPSBldmVudFByb3h5KGUpXHJcblxyXG5cdFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZGVsZWdhdGlvbnNbaWRdLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0aWYoZS50YXJnZXQubWF0Y2hlcyhkZWxlZ2F0aW9uc1tpZF1baV0uc2VsZWN0b3IpKSB7XHJcblx0XHRcdFx0XHRkZWxlZ2F0aW9uc1tpZF1baV0uaGFuZGxlcihlKVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYoIWUuY2FuY2VsQnViYmxlICYmICFlLnByb3BTdG9wcGVkICYmIGUudGFyZ2V0ICE9IGVsZW1lbnQgJiYgcGFyZW50KSB7XHJcblx0XHRcdFx0ZS50YXJnZXQgPSBwYXJlbnRcclxuXHRcdFx0XHRldmVudEhhbmRsZXIoZSwgdHJ1ZSlcclxuXHRcdFx0fVxyXG5cdFx0fSlcclxuXHRcdHRoaXMuZGVsZWdhdGlvbnNbZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtZGVsZWdhdGVkLScrZXZlbnQpXSA9IFtdXHJcblx0XHRcclxuXHR9XHJcblx0dGhpcy5kZWxlZ2F0aW9uc1tlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1kZWxlZ2F0ZWQtJytldmVudCldLnB1c2goe1xyXG5cdFx0ICBzZWxlY3RvcjogY2hpbGRTZWxlY3RvclxyXG5cdFx0LCBoYW5kbGVyOiBmblxyXG5cdH0pXHJcbn1cclxuXHJcbkJhc2VQcmVzZW50ZXIucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uKHRlbXBsYXRlLCBkYXRhLCBmbikge1xyXG5cdGRhdGEubGFuZyA9IGxhbmcuY3VycmVudFxyXG5cdGR1c3QucmVuZGVyKHRlbXBsYXRlLCBkYXRhLCBmbilcclxufVxyXG5cclxuQmFzZVByZXNlbnRlci5wcm90b3R5cGUucmVuZGVyTm9kZSA9IGZ1bmN0aW9uKHRlbXBsYXRlLCBkYXRhLCBmbikge1xyXG5cdHRoaXMucmVuZGVyTm9kZXModGVtcGxhdGUsIGRhdGEsIGZ1bmN0aW9uKGVyciwgbm9kZXMpIHtcclxuXHRcdGlmKGVycikgcmV0dXJuIGZuKGVycilcclxuXHRcdGZuKGVyciwgbm9kZXNbMF0pXHJcblx0fSlcclxufVxyXG5cclxuQmFzZVByZXNlbnRlci5wcm90b3R5cGUucmVuZGVyTm9kZXMgPSBmdW5jdGlvbih0ZW1wbGF0ZSwgZGF0YSwgZm4pIHtcclxuXHR0aGlzLnJlbmRlcih0ZW1wbGF0ZSwgZGF0YSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRpZihlcnIpIHJldHVybiBmbihlcnIpXHJcblx0XHR2YXIgd3JhcHBlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXHJcblx0XHR3cmFwcGVyLmlubmVySFRNTCA9IGh0bWxcclxuXHRcdGZuKGVyciwgd3JhcHBlci5jaGlsZHJlbilcclxuXHR9KVxyXG59XHJcblxyXG5mdW5jdGlvbiBldmVudFByb3h5KGV2ZW50KSB7XHJcblx0dmFyIGtleSwgcHJveHkgPSB7IG9yaWdpbmFsRXZlbnQ6IGV2ZW50IH1cclxuXHRmb3Ioa2V5IGluIGV2ZW50KVxyXG4gICAgXHRpZighL14oW0EtWl18bGF5ZXJbWFldJCkvLnRlc3Qoa2V5KSAmJiBldmVudFtrZXldICE9PSB1bmRlZmluZWQpIFxyXG4gICAgXHRcdHByb3h5W2tleV0gPSBldmVudFtrZXldXHJcblx0cHJveHkuc3RvcFByb3BhZ2F0aW9uID0gZnVuY3Rpb24oKSB7XHJcblx0XHRwcm94eS5wcm9wU3RvcHBlZCA9IHRydWVcclxuXHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbi5jYWxsKGV2ZW50LCBhcmd1bWVudHMpXHJcblx0fVxyXG5cdHByb3h5LnByZXZlbnREZWZhdWx0ID0gZnVuY3Rpb24oKSB7XHJcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdC5jYWxsKGV2ZW50LCBhcmd1bWVudHMpXHJcblx0fVxyXG5cdHJldHVybiBwcm94eVxyXG59XHJcblxyXG5mdW5jdGlvbiBhZGRFdmVudExpc3RlbmVyKGVsZW1lbnQsIGV2ZW50LCBsaXN0ZW5lciwgbm90RGVsZWdhdGluZykge1xyXG5cdHZhciB4LCB5LCBwcmVmaXhlcyA9IFsnV2Via2l0JywgJ01veicsICdNUyddXHJcblx0aWYobm90RGVsZWdhdGluZyAmJiAoZXZlbnQgPT0gJ2NsaWNrJyB8fCBldmVudCA9PSAndGFwJykgJiYgJ29udG91Y2hzdGFydCcgaW4gd2luZG93KSB7XHJcblx0XHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLCBmdW5jdGlvbihlKSB7XHJcblx0XHRcdHggPSBlLnRvdWNoZXNbMF0uY2xpZW50WFxyXG5cdFx0XHR5ID0gZS50b3VjaGVzWzBdLmNsaWVudFlcclxuXHRcdFx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2htb3ZlJywgcmVzZXQsIGZhbHNlKVxyXG5cdFx0XHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoZW5kJywgZG9uZSwgZmFsc2UpXHJcblx0XHR9LCBmYWxzZSlcclxuXHJcblx0XHRmdW5jdGlvbiBkb25lKGUpIHtcclxuXHRcdFx0bGlzdGVuZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKVxyXG5cdFx0XHRwcmV2ZW50R2hvc3RDbGlja3MoeCwgeSlcclxuXHRcdH1cclxuXHRcdGZ1bmN0aW9uIHJlc2V0KCkge1xyXG5cdFx0XHRkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCByZXNldCwgZmFsc2UpXHJcblx0XHRcdGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBkb25lLCBmYWxzZSlcclxuXHRcdH1cclxuXHR9XHJcblx0aWYoISgnb24nK2V2ZW50LnRvTG93ZXJDYXNlKCkgaW4gd2luZG93KSkge1xyXG5cdFx0cHJlZml4ZXMuc29tZShmdW5jdGlvbihwcmVmaXgpIHtcclxuXHRcdFx0aWYoKCdvbicrcHJlZml4K2V2ZW50KS50b0xvd2VyQ2FzZSgpIGluIHdpbmRvdykge1xyXG5cdFx0XHRcdHJldHVybiBldmVudCA9IHByZWZpeCArIGV2ZW50XHJcblx0XHRcdH1cclxuXHRcdH0pXHJcblx0fSBlbHNlIHtcclxuXHRcdGV2ZW50ID0gZXZlbnQudG9Mb3dlckNhc2UoKVxyXG5cdH1cclxuXHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyLCBmYWxzZSlcclxufVxyXG5cclxudmFyIHRhcHMgPSBbXVxyXG5mdW5jdGlvbiBwcmV2ZW50R2hvc3RDbGlja3MoeCwgeSkge1xyXG5cdHRhcHMucHVzaCh4LCB5KVxyXG5cdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcblx0XHR0YXBzLnNwbGljZSgwLCAyKVxyXG5cdH0sIDI1MDApXHJcbn1cclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xyXG5cdGZvciAodmFyIGkgPSAwOyBpIDwgdGFwcy5sZW5ndGg7IGkgKz0gMikge1xyXG5cdFx0dmFyIHggPSB0YXBzW2ldXHJcblx0XHR2YXIgeSA9IHRhcHNbaSArIDFdXHJcblx0XHRpZiAoTWF0aC5hYnMoZS5jbGllbnRYIC0geCkgPCAyNSAmJiBNYXRoLmFicyhlLmNsaWVudFkgLSB5KSA8IDI1KSB7XHJcblx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKClcclxuXHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcblx0XHR9XHJcblx0fVxyXG59LCB0cnVlKSIsInZhciBCYXNlUHJlc2VudGVyID0gcmVxdWlyZSgnLi9iYXNlJylcclxuICAsIEhpc3RvcnkgPSBtb2R1bGUuZXhwb3J0cyA9IG5ldyBCYXNlUHJlc2VudGVyKCcjaGlzdG9yeScpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG4gICwgUGxheSA9IHJlcXVpcmUoJy4vcGxheScpXHJcbiAgLCBVc2VycyA9IHJlcXVpcmUoJy4uL21vZGVscy91c2VycycpXHJcbiAgLCBHYW1lcyA9IHJlcXVpcmUoJy4uL21vZGVscy9nYW1lcycpXHJcblxyXG5IaXN0b3J5Ll9pbml0ID0gZnVuY3Rpb24oY29udGFpbmVyLCB0b2dnbGUsIHJlaW5pdCkge1xyXG5cclxufVxyXG5cclxuSGlzdG9yeS5sb2FkR2FtZSA9IGZ1bmN0aW9uKGdhbWUsIGhpc3RvcnlfaW5kZXgpIHtcclxuXHRIaXN0b3J5LmN1cnJlbnRHYW1lSWQgPSBnYW1lLl9pZFxyXG5cdEhpc3RvcnkucmVuZGVyKCdoaXN0b3J5L21haW4nLCB7IGdhbWU6Z2FtZSB9LCBmdW5jdGlvbihlcnIsIGh0bWwpIHtcclxuXHRcdEhpc3RvcnkuY29udGFpbmVyLmlubmVySFRNTCA9IGh0bWxcclxuXHRcdEhpc3RvcnkubGlzdGVuKCcudG9nZ2xlJywgJ2NsaWNrJywgb3Blbkhpc3RvcnkpXHJcblx0XHRIaXN0b3J5Lmxpc3RlbignYnV0dG9uLmNsb3NlJywgJ2NsaWNrJywgY2xvc2VIaXN0b3J5KVxyXG5cdFx0SGlzdG9yeS5saXN0ZW4oJ2Zvcm0nLCAnc3VibWl0JywgSGlzdG9yeS5zZW5kTWVzc2FnZSlcclxuXHRcdEhpc3Rvcnkuc2Nyb2xsZXIgPSBuZXcgSVNjcm9sbChIaXN0b3J5LmZpbmQoJy5saXN0JyksIHsgXHJcblx0XHRcdCAgc2Nyb2xsYmFyczogJ2N1c3RvbSdcclxuXHRcdFx0LCBtb3VzZVdoZWVsOiB0cnVlXHJcblx0XHRcdCwgaW50ZXJhY3RpdmVTY3JvbGxiYXJzOiB0cnVlXHJcblx0XHR9KVxyXG5cdFx0SGlzdG9yeS5zY3JvbGxlci5vbignc2Nyb2xsU3RhcnQnLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0SGlzdG9yeS5jb250YWluZXIuc2V0QXR0cmlidXRlKCdkYXRhLXNjcm9sbGluZycsIHRydWUpXHJcblx0XHR9KVxyXG5cdFx0SGlzdG9yeS5zY3JvbGxlci5vbignc2Nyb2xsRW5kJywgZnVuY3Rpb24oKSB7XHJcblx0XHRcdEhpc3RvcnkuY29udGFpbmVyLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1zY3JvbGxpbmcnKVxyXG5cdFx0fSlcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPD0gaGlzdG9yeV9pbmRleDsgaSsrKSB7XHJcblx0XHRcdEhpc3RvcnkuYWRkSGlzdG9yeUl0ZW0oZ2FtZSwgZ2FtZS5fLmhpc3RvcnlbaV0pXHJcblx0XHR9XHJcblx0XHRIaXN0b3J5LnNjcm9sbFRvRW5kT2ZDaGF0KClcclxuXHR9KVxyXG59XHJcblxyXG5IaXN0b3J5LmFkZEhpc3RvcnlJdGVtID0gZnVuY3Rpb24oZ2FtZSwgZXZlbnQpIHtcclxuXHR2YXIgd2FzX3Njcm9sbGVkX3RvX2JvdHRvbSA9IEhpc3Rvcnkuc2Nyb2xsSXNBdEVuZE9mQ2hhdCgpXHJcblxyXG5cdHZhciBoaXN0b3J5X2xpc3QgPSBIaXN0b3J5LmZpbmQoJy5saXN0ID4gZGl2JylcclxuXHQgICwgbGFzdF9ldmVudCA9IGhpc3RvcnlfbGlzdC5sYXN0Q2hpbGRcclxuXHQgICwgdGltZSA9IGxhc3RfZXZlbnQgJiYgbGFzdF9ldmVudC5xdWVyeVNlbGVjdG9yKCd0aW1lJylcclxuXHQgICwgY2hhdF9ieV9zYW1lX3NlbmRlciA9IGV2ZW50LnR5cGUgPT0gJ2NoYXQnICYmIGxhc3RfZXZlbnQgJiYgbGFzdF9ldmVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY2hhdC1zZW5kZXInKSA9PSBldmVudC5kYXRhLnNlbmRlclxyXG5cdCAgLCB3YXNfcmVjZW50ID0gdGltZSAmJiAobmV3IERhdGUoZXZlbnQudGltZXN0YW1wKSAtIG5ldyBEYXRlKHRpbWUuZ2V0QXR0cmlidXRlKCdkYXRldGltZScpKSA8IDYwKjEwMDApXHJcblx0aWYoY2hhdF9ieV9zYW1lX3NlbmRlciAmJiB3YXNfcmVjZW50KSB7XHJcblx0XHR2YXIgdGV4dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXHJcblx0XHR0ZXh0LmNsYXNzTmFtZSA9ICd0ZXh0J1xyXG5cdFx0dGV4dC50ZXh0Q29udGVudCA9IGV2ZW50LmRhdGEubWVzc2FnZVxyXG5cdFx0dGltZS5zZXRBdHRyaWJ1dGUoJ2RhdGV0aW1lJywgZXZlbnQudGltZXN0YW1wKVxyXG5cdFx0dGltZS5wYXJlbnROb2RlLmluc2VydEJlZm9yZSh0ZXh0LCB0aW1lKVxyXG5cdFx0SGlzdG9yeS5zY3JvbGxlci5yZWZyZXNoKClcclxuXHJcblx0XHRIaXN0b3J5LnNjcm9sbFRvRW5kT2ZDaGF0KClcclxuXHR9IGVsc2UgeyBcclxuXHRcdFVzZXJzLmFsbChmdW5jdGlvbih1c2Vycykge1xyXG5cdFx0XHRIaXN0b3J5LnJlbmRlcignaGlzdG9yeS8nK2V2ZW50LnR5cGUsIHsgZ2FtZTpnYW1lLCBldmVudDpldmVudCwgdXNlcnM6dXNlcnMgfSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRcdFx0aWYoZXJyKSByZXR1cm5cclxuXHRcdFx0XHRoaXN0b3J5X2xpc3QuaW5uZXJIVE1MICs9IGh0bWxcclxuXHRcdFx0XHRoaXN0b3J5X2xpc3Quc2Nyb2xsVG9wID0gaGlzdG9yeV9saXN0LnNjcm9sbEhlaWdodFxyXG5cdFx0XHRcdEhpc3Rvcnkuc2Nyb2xsZXIucmVmcmVzaCgpXHJcblxyXG5cdFx0XHRcdGlmKHdhc19zY3JvbGxlZF90b19ib3R0b20pXHJcblx0XHRcdFx0XHRIaXN0b3J5LnNjcm9sbFRvRW5kT2ZDaGF0KClcclxuXHRcdFx0fSlcclxuXHRcdH0pXHJcblx0fVxyXG59XHJcblxyXG5IaXN0b3J5LnNjcm9sbFRvRW5kT2ZDaGF0ID0gZnVuY3Rpb24oKSB7XHJcblx0dmFyIGxhc3RfY2hhdCA9IEhpc3RvcnkuZmluZCgnLmxpc3QgPiBkaXY6Zmlyc3QtY2hpbGQgPiBkaXY6bGFzdC1jaGlsZCcpXHJcblx0aWYobGFzdF9jaGF0KVxyXG5cdFx0SGlzdG9yeS5zY3JvbGxlci5zY3JvbGxUb0VsZW1lbnQobGFzdF9jaGF0LCAwLCAwLCBsYXN0X2NoYXQub2Zmc2V0SGVpZ2h0KVxyXG59XHJcblxyXG5IaXN0b3J5LnNjcm9sbElzQXRFbmRPZkNoYXQgPSBmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gTWF0aC5hYnMoSGlzdG9yeS5zY3JvbGxlci5tYXhTY3JvbGxZKSAtIE1hdGguYWJzKEhpc3Rvcnkuc2Nyb2xsZXIueSkgPCAxMFxyXG59XHJcblxyXG5IaXN0b3J5LnNlbmRNZXNzYWdlID0gZnVuY3Rpb24oZSkge1xyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG5cdHZhciBtZXNzYWdlID0gSGlzdG9yeS5maW5kKCdbbmFtZT1cIm1lc3NhZ2VcIl0nKS52YWx1ZVxyXG5cdGlmKG1lc3NhZ2UubGVuZ3RoKSB7XHJcblx0XHRzZXJ2ZXIuZW1pdCgnZ2FtZXMvbWVzc2FnZScsIHsgZ2FtZV9pZDogSGlzdG9yeS5jdXJyZW50R2FtZUlkLCBtZXNzYWdlOm1lc3NhZ2UgfSlcclxuXHRcdHNlcnZlci5vbmNlKCdnYW1lcy9tZXNzYWdlL3Jlc3BvbnNlJywgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHRpZihkYXRhLmVycikgY29uc29sZS5sb2coZGF0YS5lcnIpXHJcblx0XHR9KVxyXG5cdFx0SGlzdG9yeS5maW5kKCdbbmFtZT1cIm1lc3NhZ2VcIl0nKS52YWx1ZSA9ICcnXHJcblx0fVxyXG59XHJcblxyXG5IaXN0b3J5LnVwZGF0ZVVucmVhZE1lc3NhZ2VzID0gZnVuY3Rpb24odW5yZWFkQ291bnQpIHtcclxuXHRIaXN0b3J5LmZpbmQoJy51bnJlYWRNZXNzYWdlcycpLmlubmVySFRNTCA9IHVucmVhZENvdW50XHJcbn1cclxuXHJcbkhpc3RvcnkuZm9jdXNDaGF0ID0gZnVuY3Rpb24odGV4dCkge1xyXG5cdHZhciBpbnB1dCA9IEhpc3RvcnkuZmluZCgnaW5wdXRbbmFtZT1tZXNzYWdlXScpXHJcblx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0aWYoaW5wdXQpe1xyXG5cdFx0XHRpbnB1dC5mb2N1cygpXHJcblx0XHRcdGlmKHRleHQpIHtcclxuXHRcdFx0XHRpZihIaXN0b3J5LmNvbnRhaW5lci5oYXNBdHRyaWJ1dGUoJ2RhdGEtb3BlbicpKXtcclxuXHRcdFx0XHRcdGlucHV0LnZhbHVlICs9IHRleHRcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0aW5wdXQudmFsdWUgPSB0ZXh0XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdG9wZW5IaXN0b3J5KClcclxuXHRcdH0gXHJcblx0fSwgMClcclxufVxyXG5cclxuSGlzdG9yeS5mb2N1c0NoYXRJbnB1dCA9IGZ1bmN0aW9uKHRleHQpIHtcclxuXHR2YXIgaW5wdXQgPSBIaXN0b3J5LmZpbmQoJ2lucHV0W25hbWU9bWVzc2FnZV0nKVxyXG5cdGlucHV0LmZvY3VzKClcclxuXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0SGlzdG9yeS5zY3JvbGxUb0VuZE9mQ2hhdCgpXHJcblx0fSwgNzAwKVxyXG59XHJcblxyXG5IaXN0b3J5LnVuZm9jdXNDaGF0SW5wdXQgPSBmdW5jdGlvbih0ZXh0KSB7XHJcblx0dmFyIGlucHV0ID0gSGlzdG9yeS5maW5kKCdpbnB1dFtuYW1lPW1lc3NhZ2VdJylcclxuXHRpbnB1dC5ibHVyKClcclxufVxyXG5cclxuZnVuY3Rpb24gb3Blbkhpc3RvcnkoKSB7XHJcblxyXG5cdHNlcnZlci5lbWl0KCdnYW1lcy9yZXNldFVucmVhZE1lc3NhZ2VzJywgeyBnYW1lX2lkOiBIaXN0b3J5LmN1cnJlbnRHYW1lSWQgfSlcclxuXHRzZXJ2ZXIub25jZSgnZ2FtZXMvcmVzZXRVbnJlYWRNZXNzYWdlcy9yZXNwb25zZScsIGZ1bmN0aW9uKGRhdGEpIHtcclxuXHRcdGlmKGRhdGEuZXJyKSBjb25zb2xlLmxvZyhkYXRhLmVycilcclxuXHR9KVxyXG5cdEhpc3RvcnkudXBkYXRlVW5yZWFkTWVzc2FnZXMoJycpXHJcblxyXG5cdEhpc3RvcnkuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS1vcGVuJywgdHJ1ZSlcclxuXHRIaXN0b3J5LnNjcm9sbFRvRW5kT2ZDaGF0KClcclxuXHRIaXN0b3J5LmVtaXQoJ29wZW4tc3RhcnQnKVxyXG5cdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7IEhpc3RvcnkuZW1pdCgnb3Blbi1lbmQnKSB9LCAzMDApXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNsb3NlSGlzdG9yeShlKSB7XHJcblx0ZS5zdG9wUHJvcGFnYXRpb24oKVxyXG5cclxuXHRzZXJ2ZXIuZW1pdCgnZ2FtZXMvcmVzZXRVbnJlYWRNZXNzYWdlcycsIHsgZ2FtZV9pZDogSGlzdG9yeS5jdXJyZW50R2FtZUlkIH0pXHJcblx0c2VydmVyLm9uY2UoJ2dhbWVzL3Jlc2V0VW5yZWFkTWVzc2FnZXMvcmVzcG9uc2UnLCBmdW5jdGlvbihkYXRhKSB7XHJcblx0XHRpZihkYXRhLmVycikgY29uc29sZS5sb2coZGF0YS5lcnIpXHJcblx0fSlcclxuXHRIaXN0b3J5LnVwZGF0ZVVucmVhZE1lc3NhZ2VzKCcnKVxyXG5cclxuXHRIaXN0b3J5LmNvbnRhaW5lci5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtb3BlbicpXHJcblx0SGlzdG9yeS5lbWl0KCdjbG9zZS1zdGFydCcpXHJcblx0c2V0VGltZW91dChmdW5jdGlvbigpIHsgSGlzdG9yeS5lbWl0KCdjbG9zZS1lbmQnKSB9LCAzMDApXHJcbn0iLCJ2YXIgQmFzZVByZXNlbnRlciA9IHJlcXVpcmUoJy4vYmFzZScpXHJcbiAgLCBMb2dpbiA9IG1vZHVsZS5leHBvcnRzID0gbmV3IEJhc2VQcmVzZW50ZXIoKVxyXG4gICwgc2VydmVyID0gcmVxdWlyZSgnLi4vc29ja2V0LmlvJylcclxuICAsIGxhbmcgPSByZXF1aXJlKCcuLi9sYW5nJylcclxuICAsIE1haW4gPSByZXF1aXJlKCcuL21haW4nKVxyXG4gICwgVXNlcnMgPSByZXF1aXJlKCcuLi9tb2RlbHMvdXNlcnMnKVxyXG5cclxuTG9naW4uX2luaXQgPSBmdW5jdGlvbihyZWluaXQpIHtcclxuXHR3aW5kb3cubG9naW5Jbml0U3RhcnQgPSBEYXRlLm5vdygpXHJcblx0TG9naW4ucmVuZGVyKCdsb2dpbicsIHsgbGFuZ3VhZ2VzOmxhbmcuYWxsIH0sIGZ1bmN0aW9uKGVyciwgaHRtbCkge1xyXG5cdFx0TG9naW4uY29udGFpbmVyLmlubmVySFRNTCA9IGh0bWxcclxuXHRcdExvZ2luLmxpc3RlbignYnV0dG9uLmZhY2Vib29rJywgJ2NsaWNrJywgbG9naW5XaXRoRmFjZWJvb2spXHJcblx0XHRMb2dpbi5saXN0ZW4oJy5zZWxlY3QgYnV0dG9uLmxvZ2luJywgJ2NsaWNrJywgc2hvd0xvZ2luRm9ybSlcclxuXHRcdExvZ2luLmxpc3RlbignLnNlbGVjdCBidXR0b24ucmVnaXN0ZXInLCAnY2xpY2snLCBzaG93UmVnaXN0ZXJGb3JtKVxyXG5cdFx0TG9naW4ubGlzdGVuKCcjYXV0aCBmb3JtIGJ1dHRvbi5jYW5jZWwnLCAnY2xpY2snLCBjYW5jZWxGb3JtKVxyXG5cdFx0TG9naW4ubGlzdGVuKCdmb3JtLmxhbmd1YWdlcyBzZWxlY3QnLCAnY2hhbmdlJywgY2hhbmdlTGFuZ3VhZ2UpXHJcblx0XHRMb2dpbi5saXN0ZW4oJyNhdXRoIGZvcm0nLCAnc3VibWl0Jywgc3VibWl0Rm9ybSlcclxuXHRcdGlmKCF1c2VGYWNlYm9vaykge1xyXG5cdFx0XHRMb2dpbi5maW5kKCcuZmFjZWJvb2snKS5yZW1vdmUoKVxyXG5cdFx0XHRMb2dpbi5maW5kKCcub3InKS5yZW1vdmUoKVxyXG5cdFx0fVxyXG5cdFx0d2luZG93LmxvZ2luSW5pdEVuZCA9IERhdGUubm93KClcclxuXHR9KVxyXG59XHJcblxyXG5mdW5jdGlvbiBjaGFuZ2VMYW5ndWFnZShlKSB7XHJcblx0bGFuZy5zZXQoZS50YXJnZXQudmFsdWUpXHJcblx0TG9naW4uaW5pdCh0cnVlKVxyXG59XHJcblxyXG5mdW5jdGlvbiBjYW5jZWxGb3JtKGUpIHtcclxuXHRMb2dpbi5maW5kKCcuc2VsZWN0JykuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcclxuXHRMb2dpbi5maW5kKCcjYXV0aCBmb3JtJykucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXR5cGUnKVxyXG5cdExvZ2luLmZpbmQoJyNhdXRoIGZvcm0gLmVycm9yJykuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcclxuXHRlLnByZXZlbnREZWZhdWx0KClcclxufVxyXG5cclxuZnVuY3Rpb24gc2hvd0Vycm9yKGVycikge1xyXG5cdHZhciBlcnJvciA9IExvZ2luLmZpbmQoJyNhdXRoIGZvcm0gLmVycm9yJylcclxuXHRlcnJvci5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKVxyXG5cdGVycm9yLnRleHRDb250ZW50ID0gZXJyXHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNob3dSZWdpc3RlckZvcm0oKSB7XHJcblx0TG9naW4uZmluZCgnLnNlbGVjdCcpLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpXHJcblx0TG9naW4uZmluZCgnI2F1dGggZm9ybScpLnNldEF0dHJpYnV0ZSgnZGF0YS10eXBlJywgJ3JlZ2lzdGVyJylcclxuXHRMb2dpbi5maW5kKCcjYXV0aCBmb3JtIC5sb2dpbicpLnNldEF0dHJpYnV0ZSgndHlwZScsICdidXR0b24nKVxyXG5cdExvZ2luLmZpbmQoJyNhdXRoIGZvcm0gLnJlZ2lzdGVyJykuc2V0QXR0cmlidXRlKCd0eXBlJywgJ3N1Ym1pdCcpXHJcblx0TG9naW4uZmluZCgnW25hbWU9XCJ1c2VybmFtZVwiXScpLmZvY3VzKClcclxufVxyXG5cclxuZnVuY3Rpb24gc2hvd0xvZ2luRm9ybSgpIHtcclxuXHRMb2dpbi5maW5kKCcuc2VsZWN0JykuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcclxuXHRMb2dpbi5maW5kKCcjYXV0aCBmb3JtJykuc2V0QXR0cmlidXRlKCdkYXRhLXR5cGUnLCAnbG9naW4nKVxyXG5cdExvZ2luLmZpbmQoJyNhdXRoIGZvcm0gLnJlZ2lzdGVyJykuc2V0QXR0cmlidXRlKCd0eXBlJywgJ2J1dHRvbicpXHJcblx0TG9naW4uZmluZCgnI2F1dGggZm9ybSAubG9naW4nKS5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCAnc3VibWl0JylcclxuXHRMb2dpbi5maW5kKCdbbmFtZT1cInVzZXJuYW1lXCJdJykuZm9jdXMoKVxyXG59XHJcblxyXG5mdW5jdGlvbiBzdWJtaXRGb3JtKGUpIHtcclxuXHR2YXIgbWV0aG9kID0gTG9naW4uZmluZCgnI2F1dGggZm9ybScpLmdldEF0dHJpYnV0ZSgnZGF0YS10eXBlJylcclxuXHRzZXJ2ZXIuZW1pdCgnYXV0aC8nK21ldGhvZCwgeyBcclxuXHRcdCAgdXNlcm5hbWU6IExvZ2luLmZpbmQoJyNhdXRoIGZvcm0gW25hbWU9XCJ1c2VybmFtZVwiXScpLnZhbHVlXHJcblx0XHQsIHBhc3N3b3JkOiBMb2dpbi5maW5kKCcjYXV0aCBmb3JtIFtuYW1lPVwicGFzc3dvcmRcIl0nKS52YWx1ZVxyXG5cdFx0LCBsYW5ndWFnZTogTG9naW4uZmluZCgnLmxhbmd1YWdlcyBbbmFtZT1cImxhbmdcIl0nKS52YWx1ZVxyXG5cdH0pXHJcblx0c2VydmVyLm9uY2UoJ2F1dGgvJyttZXRob2QrJy9yZXNwb25zZScsIGZ1bmN0aW9uKHVzZXIsIHRva2VuKSB7XHJcblx0XHRpZih1c2VyLmVycikgcmV0dXJuIHNob3dFcnJvcih1c2VyLmVycilcclxuXHRcdExvZ2luLmNvbXBsZXRlKHVzZXIsIHRva2VuKVxyXG5cdH0pXHJcblx0ZS5wcmV2ZW50RGVmYXVsdCgpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGxvZ2luV2l0aEZhY2Vib29rKCkge1xyXG5cdEZCLmxvZ2luKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XHJcblx0XHRpZihyZXNwb25zZS5zdGF0dXMgPT0gJ2Nvbm5lY3RlZCcpIHtcclxuXHRcdFx0c2VydmVyLmVtaXQoJ2F1dGgvZmFjZWJvb2snLCByZXNwb25zZS5hdXRoUmVzcG9uc2UuYWNjZXNzVG9rZW4pXHJcblx0XHRcdHNlcnZlci5vbmNlKCdhdXRoL2ZhY2Vib29rL3Jlc3BvbnNlJywgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0XHRcdGlmKGRhdGEuZXJyKSByZXR1cm4gc2hvd0Vycm9yKGRhdGEuZXJyKVxyXG5cdFx0XHRcdExvZ2luLmNvbXBsZXRlKGRhdGEpXHJcblx0XHRcdH0pXHJcblx0XHR9XHJcblx0fSlcclxufVxyXG5cclxuTG9naW4uY29tcGxldGUgPSBmdW5jdGlvbih1c2VyLCB0b2tlbikge1xyXG5cdGlmKHRva2VuKSB3aW5kb3cubG9jYWxTdG9yYWdlLnRva2VuID0gdG9rZW5cclxuXHRVc2Vycy5zZXRTZWxmKHVzZXIpXHJcblx0TWFpbi5pbml0KClcclxufSIsInZhciBCYXNlUHJlc2VudGVyID0gcmVxdWlyZSgnLi9iYXNlJylcclxuICAsIE1haW4gPSBtb2R1bGUuZXhwb3J0cyA9IG5ldyBCYXNlUHJlc2VudGVyKClcclxuICAsIHNlcnZlciA9IHJlcXVpcmUoJy4uL3NvY2tldC5pbycpXHJcbiAgLCBQbGF5ID0gcmVxdWlyZSgnLi9wbGF5JylcclxuICAsIEhpc3RvcnkgPSByZXF1aXJlKCcuL2hpc3RvcnknKVxyXG4gICwgTG9naW4gPSByZXF1aXJlKCcuL2xvZ2luJylcclxuICAsIEFjY291bnQgPSByZXF1aXJlKCcuL2FjY291bnQnKVxyXG4gICwgU2hvcCA9IHJlcXVpcmUoJy4vc2hvcCcpXHJcbiAgLCBNZW51ID0gcmVxdWlyZSgnLi9tZW51JylcclxuICAsIFVzZXJzID0gcmVxdWlyZSgnLi4vbW9kZWxzL3VzZXJzJylcclxuXHJcbk1haW4uX2luaXQgPSBmdW5jdGlvbihyZWluaXQpIHtcclxuXHR3aW5kb3cubWFpbkluaXRTdGFydCA9IERhdGUubm93KClcclxuXHRNYWluLmFjdGl2ZSA9IHRydWVcclxuXHRNYWluLmxhc3RBY3Rpdml0eSA9IERhdGUubm93KClcclxuXHRNYWluLmFjdGl2aXR5VGltZW91dCA9IDUwMDBcclxuXHRNYWluLnJlbmRlcignbWFpbicsIHsgdXNlcjogVXNlcnMuc2VsZiB9LCBmdW5jdGlvbihlcnIsIGh0bWwpIHtcclxuXHRcdE1haW4uY29udGFpbmVyLmlubmVySFRNTCA9IGh0bWxcclxuXHJcblx0XHRNZW51LmluaXQoTWFpbi5maW5kKCcjbWVudScpLCBNYWluLmZpbmQoJyNtZW51X3RvZ2dsZScpLCByZWluaXQpXHJcblx0XHRQbGF5LmluaXQoTWFpbi5maW5kKCcjZ2FtZScpLCByZWluaXQpXHJcblx0XHRIaXN0b3J5LmluaXQoTWFpbi5maW5kKCcjaGlzdG9yeScpLCBNYWluLmZpbmQoJyNoaXN0b3J5X3RvZ2dsZScpLCByZWluaXQpXHJcblx0XHRBY2NvdW50LmluaXQoTWFpbi5maW5kKCcjYWNjb3VudCcpLCByZWluaXQpXHJcblx0XHQvLyBTaG9wLmluaXQoTWFpbi5maW5kKCcjc2hvcCcpLCByZWluaXQpXHJcblxyXG5cdFx0TWFpbi5saXN0ZW4oJ2JvZHkgOm5vdChpbnB1dCknLCAnY2xpY2snLCByZWZvY3VzKVxyXG5cdFx0TWFpbi5saXN0ZW4oZG9jdW1lbnQuYm9keSwgJ21vdXNlZG93bicsIG1ha2VBY3RpdmUpXHJcblx0XHRNYWluLmxpc3Rlbihkb2N1bWVudC5ib2R5LCAnbW91c2Vtb3ZlJywgbWFrZUFjdGl2ZSlcclxuXHRcdE1haW4ubGlzdGVuKGRvY3VtZW50LmJvZHksICd0b3VjaHN0YXJ0JywgbWFrZUFjdGl2ZSlcclxuXHRcdE1haW4ubGlzdGVuKGRvY3VtZW50LmJvZHksICdrZXlwcmVzcycsIG1ha2VBY3RpdmUpXHJcblx0XHRNYWluLmxpc3Rlbihkb2N1bWVudC5ib2R5LCAnc2Nyb2xsJywgbWFrZUFjdGl2ZSlcclxuXHJcblx0XHRNYWluLmxpc3Rlbihkb2N1bWVudC5ib2R5LCAna2V5cHJlc3MnLCBjaGF0Rm9jdXMpXHJcblx0XHR3aW5kb3cubWFpbkluaXRFbmQgPSBEYXRlLm5vdygpXHJcblx0fSlcclxuXHRzZXRUaW1lb3V0KGNoZWNrQWN0aXZlLCBNYWluLmFjdGl2aXR5VGltZW91dClcclxuXHRpZighcmVpbml0KSB7XHJcblx0XHRNZW51Lm9uKCdvcGVuLWVuZCcsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRjb25zb2xlLmxvZygndGhlIGdhbWUtbGlzdCBvcGVuZWQnKVxyXG5cdFx0fSlcclxuXHR9XHJcbn1cclxuXHJcbk1haW4uc2V0QWN0aXZlID0gZnVuY3Rpb24oYWN0aXZlKSB7XHJcblx0TWFpbi5jb250YWluZXIuc2V0QXR0cmlidXRlKCdkYXRhLWFjdGl2ZScsIGFjdGl2ZSlcclxuXHRNYWluLmNvbnRhaW5lci5jbGFzc0xpc3QuYWRkKCdmb3JjZV9yZXBhaW50JylcclxuXHRNYWluLmNvbnRhaW5lci5jbGFzc0xpc3QucmVtb3ZlKCdmb3JjZV9yZXBhaW50JylcclxufVxyXG5cclxuTWFpbi5nb0JhY2sgPSBmdW5jdGlvbigpIHtcclxuXHR2YXIgYWN0aXZlID0gTWFpbi5jb250YWluZXIuZ2V0QXR0cmlidXRlKCdkYXRhLWFjdGl2ZScpXHJcblx0aWYoYWN0aXZlID09ICdjaGF0JykgTWFpbi5zZXRBY3RpdmUoJ3BsYXknKVxyXG5cdGVsc2UgTWFpbi5zZXRBY3RpdmUoJ2dhbWVzJylcclxufVxyXG5cclxuZnVuY3Rpb24gY2hhdEZvY3VzKGUpIHtcclxuXHR2YXIgY2hhckNvZGUgPSBlLmNoYXJDb2RlIHx8IGUud2hpY2hcclxuXHRpZihjaGFyQ29kZSAhPT0gMCAmJiBkb2N1bWVudC5hY3RpdmVFbGVtZW50LnRhZ05hbWUudG9VcHBlckNhc2UoKSAhPSAnSU5QVVQnKSB7XHJcblx0XHRIaXN0b3J5LmZvY3VzQ2hhdChTdHJpbmcuZnJvbUNoYXJDb2RlKGNoYXJDb2RlKSlcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJlZm9jdXMoZSkge1xyXG5cdGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQuYmx1cigpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIG1ha2VBY3RpdmUoKSB7XHJcblx0aWYoIU1haW4uYWN0aXZlKSB7XHJcblx0XHRNYWluLmFjdGl2ZSA9IHRydWVcclxuXHRcdHNlcnZlci5lbWl0KCdhdXRoL2FjdGl2aXR5JywgdHJ1ZSlcclxuXHRcdHNldFRpbWVvdXQoY2hlY2tBY3RpdmUsIE1haW4uYWN0aXZpdHlUaW1lb3V0KVxyXG5cdH1cclxuXHRNYWluLmxhc3RBY3Rpdml0eSA9IERhdGUubm93KClcclxufVxyXG5cclxuZnVuY3Rpb24gY2hlY2tBY3RpdmUoKSB7XHJcblx0dmFyIHRpbWVTaW5jZUxhc3RBY3Rpdml0eSA9IERhdGUubm93KCkgLSBNYWluLmxhc3RBY3Rpdml0eVxyXG5cdGlmKHRpbWVTaW5jZUxhc3RBY3Rpdml0eSA+PSBNYWluLmFjdGl2aXR5VGltZW91dCkge1xyXG5cdFx0TWFpbi5hY3RpdmUgPSBmYWxzZVxyXG5cdFx0c2VydmVyLmVtaXQoJ2F1dGgvYWN0aXZpdHknLCBmYWxzZSlcclxuXHR9IGVsc2Uge1xyXG5cdFx0c2V0VGltZW91dChjaGVja0FjdGl2ZSwgTWFpbi5hY3Rpdml0eVRpbWVvdXQtdGltZVNpbmNlTGFzdEFjdGl2aXR5KVxyXG5cdH1cclxufSIsInZhciBCYXNlUHJlc2VudGVyID0gcmVxdWlyZSgnLi9iYXNlJylcclxuICAsIE1lbnUgPSBtb2R1bGUuZXhwb3J0cyA9IG5ldyBCYXNlUHJlc2VudGVyKCcjbWVudScpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG4gICwgTWFpbiA9IHJlcXVpcmUoJy4vbWFpbicpXHJcbiAgLCBQbGF5ID0gcmVxdWlyZSgnLi9wbGF5JylcclxuICAsIEFjY291bnQgPSByZXF1aXJlKCcuL2FjY291bnQnKVxyXG4gICwgU2hvcCA9IHJlcXVpcmUoJy4vc2hvcCcpXHJcbiAgLCBVc2VycyA9IHJlcXVpcmUoJy4uL21vZGVscy91c2VycycpXHJcbiAgLCBHYW1lcyA9IHJlcXVpcmUoJy4uL21vZGVscy9nYW1lcycpXHJcbiAgLCBET00gPSByZXF1aXJlKCcuLi91dGlscy9kb20nKVxyXG4gICwgdW5mcmllbmRpbmcgPSBmYWxzZVxyXG5cclxuTWVudS5faW5pdCA9IGZ1bmN0aW9uKGNvbnRhaW5lciwgdG9nZ2xlLCByZWluaXQpIHtcclxuXHR3aW5kb3cubWVudUluaXRTdGFydCA9IERhdGUubm93KClcclxuXHRHYW1lcy5ieUxpc3QoZnVuY3Rpb24oZ2FtZXMpIHtcclxuXHRcdHdpbmRvdy5nYW1lc1JlYWR5ID0gRGF0ZS5ub3coKVxyXG5cdFx0dmFyIGFjdGl2ZSA9IGdhbWVzLnlvdXJfdHVyblswXXx8Z2FtZXMudGhlaXJfdHVyblswXXx8Z2FtZXMuZ2FtZV9vdmVyWzBdXHJcblx0XHRVc2Vycy5hbGwoZnVuY3Rpb24odXNlcnMpIHtcclxuXHRcdFx0d2luZG93LnVzZXJzUmVhZHkgPSBEYXRlLm5vdygpXHJcblx0XHRcdHZhciBjaGFsbGVuZ2VfYXJyYXkgPSBbXVxyXG5cdFx0XHR2YXIgaW5fZ2FtZSA9IGdhbWVzLnRoZWlyX3R1cm4uY29uY2F0KGdhbWVzLnlvdXJfdHVybilcclxuXHRcdFx0Zm9yKHZhciB1c2VyIGluIHVzZXJzKSB7XHJcblx0XHRcdFx0dmFyIG1hdGNoID0gZmFsc2VcclxuXHRcdFx0XHRmb3IodmFyIGk9MDsgaTxpbl9nYW1lLmxlbmd0aCAmJiAhbWF0Y2g7IGkrKykge1xyXG5cdFx0XHRcdFx0aWYoaW5fZ2FtZVtpXS5vcHBvbmVudC5faWQgPT0gdXNlcikge1xyXG5cdFx0XHRcdFx0XHRtYXRjaCA9IHRydWVcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYoIW1hdGNoICYmIHVzZXIgIT0gVXNlcnMuc2VsZi5faWQpIGNoYWxsZW5nZV9hcnJheS5wdXNoKHVzZXJzW3VzZXJdKVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRzZXJ2ZXIuZW1pdCgnZ2FtZXMvY2hlY2tJblF1ZXVlJywge30pXHJcblx0XHRcdHNlcnZlci5vbmNlKCdnYW1lcy9jaGVja0luUXVldWUvcmVzcG9uc2UnLCBmdW5jdGlvbihyZXMpIHtcclxuXHRcdFx0XHRNZW51LnJlbmRlcignbWVudS9tYWluJywgeyBnYW1lczpnYW1lcywgdXNlcnM6dXNlcnMsIHVzZXJzQXJyYXk6Y2hhbGxlbmdlX2FycmF5LCBpc0luUXVldWU6IHJlcy5pc0luUXVldWUgfSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRcdFx0XHRNZW51LnRvZ2dsZSA9IHRvZ2dsZVxyXG5cdFx0XHRcdFx0TWVudS5jb250YWluZXIuaW5uZXJIVE1MID0gaHRtbFxyXG5cclxuXHRcdFx0XHRcdE1lbnUubGlzdGVuKCd0b3VjaG1vdmUnLCBmdW5jdGlvbihlKXsgZS5wcmV2ZW50RGVmYXVsdCgpIH0pXHJcblxyXG5cdFx0XHRcdFx0TWVudS5saXN0ZW4oJy50b2dnbGUnLCAnY2xpY2snLCBvcGVuTWVudSlcclxuXHJcblx0XHRcdFx0XHRNZW51Lmxpc3RlbignYnV0dG9uLmNsb3NlJywgJ2NsaWNrJywgY2xvc2VNZW51KVxyXG5cclxuXHRcdFx0XHRcdE1lbnUubGlzdGVuKCdmb3JtLnBsYXlfcmFuZG9tX29wcG9uZW50IGJ1dHRvbicsICdjbGljaycsIHJhbmRvbV9jaGFsbGVuZ2UpXHJcblx0XHRcdFx0XHRNZW51Lmxpc3RlbignZm9ybS5wbGF5X29wcG9uZW50IGJ1dHRvbicsICdjbGljaycsIGNoYWxsZW5nZSlcclxuXHRcdFx0XHRcdE1lbnUubGlzdGVuKCdmb3JtLnBsYXlfb3Bwb25lbnQgaW5wdXQnLCAna2V5cHJlc3MnLCBjaGFsbGVuZ2VFbnRlcilcclxuXHJcblx0XHRcdFx0XHRNZW51Lmxpc3RlbignLmdhbWVzIC50b19mcmllbmRzJywgJ2NsaWNrJywgTWVudS5kaXNwbGF5RnJpZW5kcylcclxuXHRcdFx0XHRcdE1lbnUubGlzdGVuKCcuZnJpZW5kcyAudG9fZ2FtZXMnLCAnY2xpY2snLCBNZW51LmRpc3BsYXlHYW1lcylcclxuXHJcblx0XHRcdFx0XHRNZW51Lmxpc3RlbignLmFjY291bnQnLCAnY2xpY2snLCBBY2NvdW50LnNob3cpXHJcblx0XHRcdFx0XHQvLyBNZW51Lmxpc3RlbignLnNob3AnLCAnY2xpY2snLCBTaG9wLnNob3cpXHJcblxyXG5cdFx0XHRcdFx0TWVudS5kZWxlZ2F0ZSgnLnBsYXlfZnJpZW5kJywgJy5mcmllbmRfaXRlbScsICdjbGljaycsIGZyaWVuZF9jaGFsbGVuZ2UpXHJcblx0XHRcdFx0XHRNZW51LmRlbGVnYXRlKCcucGxheV9mcmllbmQnLCAnLnVuZnJpZW5kJywgJ2NsaWNrJywgdW5mcmllbmQpXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdE1lbnUuZGVsZWdhdGUoJy5nYW1lcycsICcuZ2FtZV9pdGVtJywgJ2NsaWNrJywgc3RhcnRHYW1lKVxyXG5cdFx0XHRcdFx0TWVudS5kZWxlZ2F0ZSgnLmdhbWVzJywgJy5yZXNpZ24nLCAnY2xpY2snLCByZXNpZ25HYW1lKVxyXG5cdFx0XHRcclxuXHRcdFx0XHRcdGlmKGFjdGl2ZSkgR2FtZXMuZ2V0QnlJZChhY3RpdmUuX2lkLCBmdW5jdGlvbihhY3RpdmUpIHtcclxuXHRcdFx0XHRcdFx0TWVudS5sb2FkR2FtZShhY3RpdmUsIHRydWUpXHJcblx0XHRcdFx0XHR9KVxyXG5cclxuXHRcdFx0XHRcdE1lbnUuc2Nyb2xsZXIgPSBuZXcgSVNjcm9sbChNZW51LmZpbmQoJy5zY3JvbGwnKSwgeyBcclxuXHRcdFx0XHRcdFx0ICBzY3JvbGxiYXJzOiAnY3VzdG9tJ1xyXG5cdFx0XHRcdFx0XHQsIG1vdXNlV2hlZWw6IHRydWVcclxuXHRcdFx0XHRcdFx0LCBpbnRlcmFjdGl2ZVNjcm9sbGJhcnM6IHRydWVcclxuXHRcdFx0XHRcdFx0LCBjbGljazogdHJ1ZVxyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdE1lbnUuc2Nyb2xsZXIub24oJ3Njcm9sbFN0YXJ0JywgZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0XHRcdE1lbnUuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS1zY3JvbGxpbmcnLCB0cnVlKVxyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdE1lbnUuc2Nyb2xsZXIub24oJ3Njcm9sbEVuZCcsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRNZW51LmNvbnRhaW5lci5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtc2Nyb2xsaW5nJylcclxuXHRcdFx0XHRcdH0pXHJcblxyXG5cdFx0XHRcdFx0aWYoIWdhbWVzLmNvdW50KSB7XHJcblx0XHRcdFx0XHRcdE1lbnUuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS1uby1nYW1lcycsIHRydWUpXHJcblx0XHRcdFx0XHRcdE1lbnUuZGlzcGxheUZyaWVuZHMoKVxyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0TWVudS5kaXNwbGF5R2FtZXMoKVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0d2luZG93Lm1lbnVJbml0RW5kID0gRGF0ZS5ub3coKVxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdH0pXHJcblx0XHR9KVxyXG5cdH0pXHJcblx0aWYoIXJlaW5pdCl7XHJcblx0XHRHYW1lcy5vbignbmV3JywgaW5zZXJ0R2FtZUl0ZW0pXHJcblx0XHRHYW1lcy5vbignZXZlbnQnLCB1cGRhdGVHYW1lSXRlbSlcclxuXHRcdEdhbWVzLm9uKCdvcHBvbmVudC1hY3Rpdml0eScsIG1vdmVHYW1lSXRlbSlcclxuXHRcdEdhbWVzLm9uKCdlbmQnLCBlbmRHYW1lSXRlbSlcclxuXHRcdEdhbWVzLm9uKCdyZW1vdmUnLCByZW1vdmVHYW1lSXRlbSlcclxuXHRcdFVzZXJzLm9uKCdhY3Rpdml0eScsIHVwZGF0ZUFjdGl2aXR5U3RhdHVzKVxyXG5cdFx0c2VydmVyLm9uKCdnYW1lcy9uZXcnLCBuZXdSYW5kb21HYW1lKVxyXG5cdFx0VXNlcnMub24oJ3VwZGF0ZScsIHVwZGF0ZVVzZXJHYW1lSXRlbXMpXHJcblx0fVxyXG59XHJcblxyXG5NZW51LnJlZnJlc2hTY3JvbGxlciA9IGZ1bmN0aW9uKCkge1xyXG5cdE1lbnUuc2Nyb2xsZXIuZGlzYWJsZSgpXHJcblx0TWVudS5zY3JvbGxlci5yZWZyZXNoKClcclxuXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0TWVudS5zY3JvbGxlci5lbmFibGUoKVxyXG5cdH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIG9wZW5NZW51KCkge1xyXG5cdE1lbnUuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS1vcGVuJywgdHJ1ZSlcclxuXHRNZW51LmNvbnRhaW5lci5zZXRBdHRyaWJ1dGUoJ2RhdGEtZm9jdXNlZCcsIHRydWUpXHJcblx0TWVudS5lbWl0KCdvcGVuLXN0YXJ0JylcclxuXHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkgeyBNZW51LmVtaXQoJ29wZW4tZW5kJykgfSwgMzAwKVxyXG59XHJcblxyXG5mdW5jdGlvbiBjbG9zZU1lbnUoZSkge1xyXG5cdGUuc3RvcFByb3BhZ2F0aW9uKClcclxuXHRNZW51LmNvbnRhaW5lci5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtb3BlbicpXHJcblx0TWVudS5jb250YWluZXIucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWZvY3VzZWQnKVxyXG5cdE1lbnUuZW1pdCgnY2xvc2Utc3RhcnQnKVxyXG5cdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7IE1lbnUuZW1pdCgnY2xvc2UtZW5kJykgfSwgMzAwKVxyXG59XHJcblxyXG5mdW5jdGlvbiBtb3ZlR2FtZUl0ZW0oZ2FtZSwgbGlzdCwgaW5kZXgpIHtcclxuXHR2YXIgaXRlbV9jb250YWluZXIgPSBNZW51LmZpbmQoJy5nYW1lcyAudHVybl9saXN0LicrbGlzdCsnIHVsLmNvbnRlbnQnKVxyXG5cdHZhciBnYW1lX2l0ZW0gPSBpdGVtX2NvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCcuZ2FtZV9pdGVtW2RhdGEtaWQ9XCInK2dhbWUuX2lkKydcIl0nKVxyXG5cdGlmKGdhbWVfaXRlbSkge1xyXG5cdFx0aWYoaW5kZXggPj0gaXRlbV9jb250YWluZXIuY2hpbGRyZW4ubGVuZ3RoKSB7XHJcblx0XHRcdGl0ZW1fY29udGFpbmVyLmFwcGVuZENoaWxkKGdhbWVfaXRlbSlcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGl0ZW1fY29udGFpbmVyLmluc2VydEJlZm9yZShnYW1lX2l0ZW0sIGl0ZW1fY29udGFpbmVyLmNoaWxkcmVuW2luZGV4XSlcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGluc2VydEdhbWVJdGVtKGdhbWUsIGxpc3QsIGluZGV4LCBzZWxlY3RlZCkge1xyXG5cdFVzZXJzLmFsbChmdW5jdGlvbih1c2Vycykge1xyXG5cdFx0TWVudS5yZW5kZXJOb2RlKCdtZW51L2dhbWUtaXRlbScsIHsgZ2FtZTpnYW1lLCB1c2Vyczp1c2Vycywgb3Bwb25lbnRfdXNlcjp1c2Vyc1tnYW1lLm9wcG9uZW50Ll9pZF0gfSwgZnVuY3Rpb24oZXJyLCBnYW1lX2l0ZW0pIHtcclxuXHRcdFx0dmFyIHR1cm5fbGlzdCA9IE1lbnUuZmluZCgnLmdhbWVzIC50dXJuX2xpc3QuJytsaXN0KVxyXG5cdFx0XHR2YXIgaXRlbV9jb250YWluZXIgPSB0dXJuX2xpc3QucXVlcnlTZWxlY3RvcigndWwuY29udGVudCcpXHJcblx0XHRcdE1lbnUuY29udGFpbmVyLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1uby1nYW1lcycpXHJcblx0XHRcdHR1cm5fbGlzdC5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtZW1wdHknKVxyXG5cdFx0XHRpZihpbmRleCA+PSBpdGVtX2NvbnRhaW5lci5jaGlsZHJlbi5sZW5ndGgpIHtcclxuXHRcdFx0XHRpdGVtX2NvbnRhaW5lci5hcHBlbmRDaGlsZChnYW1lX2l0ZW0pXHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aXRlbV9jb250YWluZXIuaW5zZXJ0QmVmb3JlKGdhbWVfaXRlbSwgaXRlbV9jb250YWluZXIuY2hpbGRyZW5baW5kZXhdKVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmKHNlbGVjdGVkKSBnYW1lX2l0ZW0uY2xhc3NMaXN0LmFkZCgnc2VsZWN0ZWQnKVxyXG5cdFx0XHR2YXIgZnJpZW5kcyA9IE1lbnUuZmluZCgnLnBsYXlfZnJpZW5kIC5jb250ZW50IFtkYXRhLWlkPVwiJytnYW1lLm9wcG9uZW50Ll9pZCsnXCJdJylcclxuXHRcdFx0aWYoZnJpZW5kcykge1xyXG5cdFx0XHRcdGZyaWVuZHMucmVtb3ZlKClcclxuXHRcdFx0XHR1cGRhdGVGcmllbmRDb3VudCgtMSlcclxuXHRcdFx0fVxyXG5cdFx0XHRNZW51LnJlZnJlc2hTY3JvbGxlcigpXHJcblx0XHR9KVxyXG5cdH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIHVwZGF0ZUZyaWVuZENvdW50KGRpZmYpIHtcclxuXHR2YXIgcGxheV9mcmllbmRfY29udGFpbmVyID0gTWVudS5maW5kKCcucGxheV9mcmllbmQnKVxyXG5cdHZhciBmcmllbmRfY291bnQgPSBwYXJzZUludChwbGF5X2ZyaWVuZF9jb250YWluZXIuZ2V0QXR0cmlidXRlKCdkYXRhLWZyaWVuZGNvdW50JykpXHJcblx0ZnJpZW5kX2NvdW50ID0gZnJpZW5kX2NvdW50ID8gZnJpZW5kX2NvdW50IDogMFxyXG5cdGZyaWVuZF9jb3VudCA9IGZyaWVuZF9jb3VudCtkaWZmXHJcblx0aWYoZnJpZW5kX2NvdW50IDwgMClcclxuXHRcdGZyaWVuZF9jb3VudCA9IDBcclxuXHJcblx0cGxheV9mcmllbmRfY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS1mcmllbmRjb3VudCcsIGZyaWVuZF9jb3VudClcdFxyXG59XHJcblxyXG5mdW5jdGlvbiBuZXdSYW5kb21HYW1lKGdhbWUsIGlzX3JhbmRvbV9nYW1lKSB7XHJcblx0aWYoaXNfcmFuZG9tX2dhbWUpIHtcclxuXHRcdE1lbnUuZmluZCgnLnBsYXlfcmFuZG9tX29wcG9uZW50IGJ1dHRvbicpLnNldEF0dHJpYnV0ZSgnZGF0YS1xdWV1ZWQnLCBmYWxzZSlcclxuXHRcdE1lbnUuZmluZCgnLndhaXRpbmdRdWV1ZScpLnNldEF0dHJpYnV0ZSgnZGF0YS1xdWV1ZWQnLCBmYWxzZSlcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHVwZGF0ZUdhbWVJdGVtKGdhbWUsIGV2ZW50LCBsaXN0LCBvbGRfbGlzdCwgaW5kZXgpIHtcclxuXHR2YXIgb2xkX2xpc3QgPSBNZW51LmZpbmQoJy50dXJuX2xpc3QuJytvbGRfbGlzdClcclxuXHQgICwgbmV3X2xpc3QgPSBNZW51LmZpbmQoJy50dXJuX2xpc3QuJytsaXN0KVxyXG5cdCAgLCBvbGRfZ2FtZV9pdGVtID0gb2xkX2xpc3QucXVlcnlTZWxlY3RvcignLmdhbWVfaXRlbVtkYXRhLWlkPVwiJytnYW1lLl9pZCsnXCJdJylcclxuXHQgICwgc2VsZWN0ZWQgPSBvbGRfZ2FtZV9pdGVtLmNsYXNzTGlzdC5jb250YWlucygnc2VsZWN0ZWQnKVxyXG5cdG9sZF9nYW1lX2l0ZW0ucmVtb3ZlKClcclxuXHRpZighb2xkX2xpc3QucXVlcnlTZWxlY3RvcigndWwuY29udGVudCcpLmNoaWxkTm9kZXMubGVuZ3RoKSBvbGRfbGlzdC5zZXRBdHRyaWJ1dGUoJ2RhdGEtZW1wdHknLCB0cnVlKVxyXG5cdGluc2VydEdhbWVJdGVtKGdhbWUsIGxpc3QsIGluZGV4LCBzZWxlY3RlZClcclxufVxyXG5cclxuZnVuY3Rpb24gZW5kR2FtZUl0ZW0oZ2FtZSkge1xyXG5cdFVzZXJzLmFsbChmdW5jdGlvbih1c2Vycykge1xyXG5cdFx0TWVudS5yZW5kZXIoJ21lbnUvZnJpZW5kLWl0ZW0nLCB7IF9pZDogZ2FtZS5vcHBvbmVudC5faWQsIHVzZXJuYW1lOiB1c2Vyc1tnYW1lLm9wcG9uZW50Ll9pZF0udXNlcm5hbWUgfSwgZnVuY3Rpb24oZXJyLCBodG1sKSB7XHJcblx0XHRcdE1lbnUuZmluZCgnLnBsYXlfZnJpZW5kIC5jb250ZW50JykuYXBwZW5kQ2hpbGQoRE9NLmh0bWxUb05vZGUoaHRtbCkpXHJcblx0XHRcdHVwZGF0ZUZyaWVuZENvdW50KDEpXHJcblx0XHR9KVxyXG5cdH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJlbW92ZUdhbWVJdGVtKGdhbWUsIGxpc3QsIHJlbWFpbmluZykge1xyXG5cdHZhciBsaXN0ID0gTWVudS5maW5kKCcudHVybl9saXN0LicrbGlzdClcclxuXHQgICwgZ2FtZV9pdGVtID0gbGlzdC5xdWVyeVNlbGVjdG9yKCcuZ2FtZV9pdGVtW2RhdGEtaWQ9XCInK2dhbWUuX2lkKydcIl0nKVxyXG5cclxuXHRnYW1lX2l0ZW0ucmVtb3ZlKClcclxuXHRpZighbGlzdC5xdWVyeVNlbGVjdG9yKCd1bC5jb250ZW50JykuY2hpbGROb2Rlcy5sZW5ndGgpIHtcclxuXHRcdGxpc3Quc2V0QXR0cmlidXRlKCdkYXRhLWVtcHR5JywgdHJ1ZSlcclxuXHR9XHJcblx0aWYoIXJlbWFpbmluZykge1xyXG5cdFx0TWVudS5jb250YWluZXIuc2V0QXR0cmlidXRlKCdkYXRhLW5vLWdhbWVzJywgdHJ1ZSlcclxuXHRcdE1lbnUuZGlzcGxheUZyaWVuZHMoKVxyXG5cdH1cclxuXHRNZW51LnJlZnJlc2hTY3JvbGxlcigpXHJcbn1cclxuXHJcbk1lbnUuZGlzcGxheUZyaWVuZHMgPSBmdW5jdGlvbihlKSB7XHJcblx0TWVudS5jb250YWluZXIuc2V0QXR0cmlidXRlKCdkYXRhLWFjdGl2ZScsJ2ZyaWVuZHMnKVxyXG5cdE1lbnUucmVmcmVzaFNjcm9sbGVyKClcclxufVxyXG5cclxuTWVudS5kaXNwbGF5R2FtZXMgPSBmdW5jdGlvbihlKSB7XHJcblx0TWVudS5jb250YWluZXIuc2V0QXR0cmlidXRlKCdkYXRhLWFjdGl2ZScsJ2dhbWVzJylcclxuXHRNZW51LnJlZnJlc2hTY3JvbGxlcigpXHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJlc2lnbkdhbWUoZSkge1xyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG5cdHZhciBfaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKVxyXG5cdEdhbWVzLnJlbW92ZShfaWQpXHJcblx0c2VydmVyLmVtaXQoJ2dhbWVzL3Jlc2lnbicsIF9pZClcclxuXHRzZXJ2ZXIub25jZSgnZ2FtZXMvcmVzaWduL3Jlc3BvbnNlJywgZnVuY3Rpb24oZGF0YSkge1xyXG5cdFx0aWYoZGF0YS5lcnIpIHJldHVybiBhbGVydChkYXRhLmVyciwgJ2Vycm9yJylcclxuXHRcdEdhbWVzLmJ5TGlzdChmdW5jdGlvbihnYW1lcykge1xyXG5cdFx0XHRpZihnYW1lcy55b3VyX3R1cm4ubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdE1lbnUubG9hZEdhbWUoZ2FtZXMueW91cl90dXJuWzBdKVxyXG5cdFx0XHR9IGVsc2UgaWYoZ2FtZXMudGhlaXJfdHVybi5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0TWVudS5sb2FkR2FtZShnYW1lcy50aGVpcl90dXJuWzBdKVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFBsYXkuZmluZCgnLmluX2dhbWUnKS5pbm5lckhUTUwgPSAnJ1xyXG5cdFx0XHR9XHJcblx0XHR9KVxyXG5cdH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNoYWxsZW5nZShlKSB7XHJcblx0ZS5zdG9wUHJvcGFnYXRpb24oKVxyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG5cdHZhciBzZWFyY2hGaWVsZCA9IE1lbnUuZmluZCgnLnBsYXlfb3Bwb25lbnQgW25hbWU9XCJzZWFyY2hfdGV4dFwiXScpXHJcblx0c2VydmVyLmVtaXQoJ2dhbWVzL2NoYWxsZW5nZScsIHsgb3Bwb25lbnQ6c2VhcmNoRmllbGQudmFsdWUgfSlcclxuXHRzZXJ2ZXIub25jZSgnZ2FtZXMvY2hhbGxlbmdlL3Jlc3BvbnNlJywgZnVuY3Rpb24oZ2FtZV9pZCkge1xyXG5cdFx0aWYoZ2FtZV9pZC5lcnIpIHJldHVybiBhbGVydChnYW1lX2lkLmVyciwgJ2Vycm9yJylcclxuXHRcdHNlYXJjaEZpZWxkLnZhbHVlID0gJydcclxuXHRcdE1lbnUuZGlzcGxheUdhbWVzKClcclxuXHRcdEdhbWVzLmdldEJ5SWQoZ2FtZV9pZCwgZnVuY3Rpb24oZ2FtZSkge1xyXG5cdFx0XHRNZW51LmxvYWRHYW1lKGdhbWUpXHJcblx0XHR9KVxyXG5cdH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNoYWxsZW5nZUVudGVyKGUpIHtcclxuXHRpZihlLmtleUNvZGUgPT0gMTMpXHJcblx0XHRjaGFsbGVuZ2UoZSlcclxufVxyXG5cclxuZnVuY3Rpb24gcmFuZG9tX2NoYWxsZW5nZShlKSB7XHJcblx0ZS5zdG9wUHJvcGFnYXRpb24oKVxyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG5cdE1lbnUuZmluZCgnLnJhbmRvbV9jaGFsbGVuZ2UgLndhaXRpbmdRdWV1ZScpLnNldEF0dHJpYnV0ZSgnZGF0YS1xdWV1ZWQnLCB0cnVlKVxyXG5cdE1lbnUuZmluZCgnLnJhbmRvbV9jaGFsbGVuZ2UgLnBsYXlfcmFuZG9tX29wcG9uZW50IGJ1dHRvbicpLnNldEF0dHJpYnV0ZSgnZGF0YS1xdWV1ZWQnLCB0cnVlKVxyXG5cdHNlcnZlci5lbWl0KCdnYW1lcy9yYW5kb21fY2hhbGxlbmdlJylcclxuXHRzZXJ2ZXIub25jZSgnZ2FtZXMvcmFuZG9tX2NoYWxsZW5nZS9yZXNwb25zZScsIGZ1bmN0aW9uKGdhbWVfaWQpIHtcclxuXHRcdGlmKGdhbWVfaWQuZXJyKSByZXR1cm4gYWxlcnQoZ2FtZV9pZC5lcnIsICdlcnJvcicpXHJcblx0XHRcclxuXHRcdGlmKGdhbWVfaWQpIHtcclxuXHRcdFx0TWVudS5kaXNwbGF5R2FtZXMoKVxyXG5cdFx0XHRHYW1lcy5nZXRCeUlkKGdhbWVfaWQsIGZ1bmN0aW9uKGdhbWUpIHtcclxuXHRcdFx0XHRNZW51LmxvYWRHYW1lKGdhbWUpXHJcblx0XHRcdH0pXHJcblx0XHR9XHJcblx0fSlcclxufVxyXG5cclxuZnVuY3Rpb24gZnJpZW5kX2NoYWxsZW5nZShlKSB7XHJcblx0aWYoIXVuZnJpZW5kaW5nKSB7XHJcblx0XHRlLnByZXZlbnREZWZhdWx0KClcclxuXHRcdHZhciBvcHBfaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKVxyXG5cdFx0UGxheS5jaGFsbGVuZ2VCeUlkKG9wcF9pZClcclxuXHR9IGVsc2Uge1xyXG5cdFx0dW5mcmllbmRpbmcgPSBmYWxzZVxyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gdW5mcmllbmQoZSkge1xyXG5cdGUucHJldmVudERlZmF1bHQoKVxyXG5cdGUuc3RvcFByb3BhZ2F0aW9uKClcclxuXHR2YXIgdW5mcmllbmRfY29uZmlybSA9IGNvbmZpcm0oJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byB1bmZyaWVuZCAnICsgZS50YXJnZXQucGFyZW50Tm9kZS5maXJzdENoaWxkLnRleHRDb250ZW50KVxyXG5cdFxyXG5cdGlmKCF1bmZyaWVuZF9jb25maXJtKSByZXR1cm5cclxuXHJcblx0dW5mcmllbmRpbmcgPSB0cnVlXHJcblx0dmFyIGZyaWVuZF9pZCA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpXHJcblx0c2VydmVyLmVtaXQoJ3VzZXJzL3VuZnJpZW5kJywgeyBmcmllbmRfaWQ6ZnJpZW5kX2lkIH0pXHJcblx0c2VydmVyLm9uY2UoJ3VzZXJzL3VuZnJpZW5kL3Jlc3BvbnNlJywgZnVuY3Rpb24oKSB7XHJcblx0XHRHYW1lcy5ieUxpc3QoZnVuY3Rpb24oZ2FtZXMpIHtcclxuXHRcdFx0Zm9yKHZhciBpPTA7IGk8Z2FtZXMuZ2FtZV9vdmVyLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0aWYoZ2FtZXMuZ2FtZV9vdmVyW2ldLm9wcG9uZW50Ll9pZCA9PSBmcmllbmRfaWQpIHtcclxuXHRcdFx0XHRcdHZhciBnYW1lID0gTWVudS5maW5kKCcuZ2FtZV9pdGVtW2RhdGEtaWQ9XCInK2dhbWVzLmdhbWVfb3ZlcltpXS5faWQrJ1wiXScpXHJcblx0XHRcdFx0XHRpZihnYW1lKSBnYW1lLnJlbW92ZSgpXHJcblx0XHRcdFx0XHRnYW1lcy5nYW1lX292ZXIuc3BsaWNlKGksIDEpXHJcblx0XHRcdFx0XHRpLS1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0aWYoZ2FtZXMuZ2FtZV9vdmVyLmxlbmd0aCA9PSAwKSBNZW51LmZpbmQoJy50dXJuX2xpc3QuZ2FtZV9vdmVyJykuc2V0QXR0cmlidXRlKCdkYXRhLWVtcHR5JywgdHJ1ZSlcclxuXHJcblx0XHRcdE1lbnUuZmluZCgnLmZyaWVuZF9pdGVtW2RhdGEtaWQ9XCInK2ZyaWVuZF9pZCsnXCJdJykucmVtb3ZlKClcclxuXHRcdH0pXHJcblx0fSlcclxufVxyXG5cclxuZnVuY3Rpb24gc3RhcnRHYW1lKGUpIHtcclxuXHRlLnByZXZlbnREZWZhdWx0KClcclxuXHRlLnN0b3BQcm9wYWdhdGlvbigpXHJcblx0R2FtZXMuZ2V0QnlJZChlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKSwgZnVuY3Rpb24oZ2FtZSkge1xyXG5cdFx0TWVudS5sb2FkR2FtZShnYW1lKVxyXG5cdH0pXHJcbn1cclxuXHJcbk1lbnUubG9hZEdhbWUgPSBmdW5jdGlvbihnYW1lLCBpbml0KSB7XHJcblx0dmFyIHNlbGVjdGVkID0gTWVudS5maW5kKCcuc2VsZWN0ZWQnKVxyXG5cdGlmKHNlbGVjdGVkKSBzZWxlY3RlZC5jbGFzc0xpc3QucmVtb3ZlKCdzZWxlY3RlZCcpXHJcblx0TWVudS5maW5kKCdbZGF0YS1pZD1cIicrZ2FtZS5faWQrJ1wiXScpLmNsYXNzTGlzdC5hZGQoXCJzZWxlY3RlZFwiKVxyXG5cdGlmKCFpbml0KSBNZW51LmNvbnRhaW5lci5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtZm9jdXNlZCcpXHJcblx0UGxheS5sb2FkR2FtZShnYW1lLCBpbml0KVxyXG59XHJcblxyXG5mdW5jdGlvbiB1cGRhdGVBY3Rpdml0eVN0YXR1cyh1c2VyX2lkLCBvbmxpbmUsIGFjdGl2ZSkge1xyXG5cdHZhciB1c2VycyA9IE1lbnUuZmluZEFsbCgnLmdhbWVfaXRlbVtkYXRhLW9wcG9uZW50LWlkPVwiJyt1c2VyX2lkKydcIl0nKVxyXG5cdGlmKG9ubGluZSAmJiBhY3RpdmUpIHtcclxuXHRcdHZhciBhY3Rpdml0eSA9ICdvbmxpbmUnXHJcblx0fSBlbHNlIGlmKG9ubGluZSAmJiAhYWN0aXZlKSB7XHJcblx0XHR2YXIgYWN0aXZpdHkgPSAnYXdheSdcclxuXHR9IGVsc2Uge1xyXG5cdFx0dmFyIGFjdGl2aXR5ID0gJ29mZmxpbmUnXHJcblx0fVxyXG5cclxuXHRmb3IodmFyIGk9MDsgaTx1c2Vycy5sZW5ndGg7IGkrKykge1xyXG5cdFx0dXNlcnNbaV0uc2V0QXR0cmlidXRlKCdkYXRhLXVzZXItc3RhdHVzJywgYWN0aXZpdHkpXHJcblx0fVxyXG59XHJcblxyXG5mdW5jdGlvbiB1cGRhdGVVc2VyR2FtZUl0ZW1zKHVzZXIpIHtcclxuXHR2YXIgZ2FtZV9pdGVtcyA9IE1lbnUuZmluZEFsbCgnLmdhbWVfaXRlbVtkYXRhLW9wcG9uZW50LWlkPVwiJyt1c2VyLl9pZCsnXCJdJylcclxuXHJcblx0VXNlcnMuYWxsKGZ1bmN0aW9uKHVzZXJzKSB7XHJcblx0XHQ7W10uZm9yRWFjaC5jYWxsKGdhbWVfaXRlbXMsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuXHRcdFx0R2FtZXMuZ2V0QnlJZChpdGVtLmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpLCBmdW5jdGlvbihnYW1lKSB7XHJcblx0XHRcdFx0TWVudS5yZW5kZXJOb2RlKCdtZW51L2dhbWUtaXRlbScsIHsgZ2FtZTpnYW1lLCB1c2Vyczp1c2Vycywgb3Bwb25lbnRfdXNlcjp1c2VyIH0sIGZ1bmN0aW9uKGVyciwgZ2FtZV9pdGVtKSB7XHJcblx0XHRcdFx0XHRpdGVtLnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKGdhbWVfaXRlbSwgaXRlbSlcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9KVxyXG5cdFx0fSlcclxuXHR9KVxyXG5cclxufSIsInZhciBCYXNlUHJlc2VudGVyID0gcmVxdWlyZSgnLi9iYXNlJylcclxuICAsIFBsYXkgPSBtb2R1bGUuZXhwb3J0cyA9IG5ldyBCYXNlUHJlc2VudGVyKCcjZ2FtZScpXHJcbiAgLCBzZXJ2ZXIgPSByZXF1aXJlKCcuLi9zb2NrZXQuaW8nKVxyXG4gICwgTWFpbiA9IHJlcXVpcmUoJy4vbWFpbicpXHJcbiAgLCBNZW51ID0gcmVxdWlyZSgnLi9tZW51JylcclxuICAsIEhpc3RvcnkgPSByZXF1aXJlKCcuL2hpc3RvcnknKVxyXG4gICwgR2FtZXMgPSByZXF1aXJlKCcuLi9tb2RlbHMvZ2FtZXMnKVxyXG4gICwgVXNlcnMgPSByZXF1aXJlKCcuLi9tb2RlbHMvdXNlcnMnKVxyXG4gICwgcG9wID0gbmV3IEF1ZGlvKCcvc291bmRzL3BvcC5tcDMnKVxyXG5cclxuUGxheS5oYW5kbGVycyA9IHt9XHJcblxyXG5QbGF5Ll9pbml0ID0gZnVuY3Rpb24oY29udGFpbmVyLCByZWluaXQpIHtcclxuXHRQbGF5LmNhbGNTaXplKClcclxuXHRpZighcmVpbml0KSB7XHJcblx0XHRHYW1lcy5vbignZXZlbnQnLCBmdW5jdGlvbihnYW1lLCBldmVudCkge1xyXG5cdFx0XHRpZihnYW1lLl9pZCA9PSBQbGF5LmN1cnJlbnRHYW1lLl9pZCkge1xyXG5cdFx0XHRcdFBsYXkuY3VycmVudEdhbWUgPSBnYW1lXHJcblx0XHRcdFx0UGxheS5zdGFydFRyaWdnZXJpbmcoKVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZihldmVudC5pbml0aWF0b3IgIT0gVXNlcnMuc2VsZi5faWQgJiYgZXZlbnQudHlwZSA9PSAnY2hhdCcpXHJcblx0XHRcdHtcclxuXHRcdFx0XHRIaXN0b3J5LnVwZGF0ZVVucmVhZE1lc3NhZ2VzKGV2ZW50LnN0YXRlLnBsYXllci51bnJlYWRNZXNzYWdlcylcclxuXHRcdFx0XHRwb3AucGxheSgpXHJcblx0XHRcdH1cclxuXHRcdH0pXHJcblx0XHRVc2Vycy5vbigndXBkYXRlJywgZnVuY3Rpb24odXNlciwgZXZlbnQpIHtcclxuXHRcdFx0aWYodXNlciA9PSBVc2Vycy5zZWxmKSB7XHJcblx0XHRcdFx0UGxheS50cmlnZ2VyUGxheWVyRXZlbnQoJ3BsYXllcjonK2V2ZW50LnR5cGUsIHVzZXIsIGV2ZW50LCBmdW5jdGlvbigpIHt9KVxyXG5cdFx0XHRcdFBsYXkudHJpZ2dlclBsYXllckV2ZW50KCdwbGF5ZXJzOicrZXZlbnQudHlwZSwgdXNlciwgZXZlbnQsIGZ1bmN0aW9uKCkge30pXHJcblx0XHRcdFx0UGxheS50cmlnZ2VyUGxheWVyRXZlbnQoJ3BsYXllcjp1cGRhdGUnLCB1c2VyLCBldmVudCwgZnVuY3Rpb24oKSB7fSlcclxuXHRcdFx0XHRQbGF5LnRyaWdnZXJQbGF5ZXJFdmVudCgncGxheWVyczp1cGRhdGUnLCB1c2VyLCBldmVudCwgZnVuY3Rpb24oKSB7fSlcclxuXHRcdFx0fSBlbHNlIGlmKHVzZXIuX2lkID09IFBsYXkuY3VycmVudEdhbWUub3Bwb25lbnQuX2lkKSB7XHJcblx0XHRcdFx0UGxheS50cmlnZ2VyUGxheWVyRXZlbnQoJ29wcG9uZW50OicrZXZlbnQudHlwZSwgdXNlciwgZXZlbnQsIGZ1bmN0aW9uKCkge30pXHJcblx0XHRcdFx0UGxheS50cmlnZ2VyUGxheWVyRXZlbnQoJ3BsYXllcnM6JytldmVudC50eXBlLCB1c2VyLCBldmVudCwgZnVuY3Rpb24oKSB7fSlcclxuXHRcdFx0XHRQbGF5LnRyaWdnZXJQbGF5ZXJFdmVudCgnb3Bwb25lbnQ6dXBkYXRlJywgdXNlciwgZXZlbnQsIGZ1bmN0aW9uKCkge30pXHJcblx0XHRcdFx0UGxheS50cmlnZ2VyUGxheWVyRXZlbnQoJ3BsYXllcnM6dXBkYXRlJywgdXNlciwgZXZlbnQsIGZ1bmN0aW9uKCkge30pXHJcblx0XHRcdH1cclxuXHRcdH0pXHJcblx0fVxyXG59XHJcblxyXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgUGxheS5jYWxjU2l6ZSwgZmFsc2UpXHJcblxyXG5NZW51Lm9uKCdvcGVuLWVuZCcsIGZ1bmN0aW9uKCkge1xyXG5cdFBsYXkuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS1tZW51JywgdHJ1ZSlcclxuXHRQbGF5LmNhbGNTaXplKClcclxufSlcclxuXHJcbk1lbnUub24oJ2Nsb3NlLXN0YXJ0JywgZnVuY3Rpb24oKSB7XHJcblx0UGxheS5jb250YWluZXIucmVtb3ZlQXR0cmlidXRlKCdkYXRhLW1lbnUnKVxyXG5cdFBsYXkuY2FsY1NpemUoKVxyXG59KVxyXG5cclxuSGlzdG9yeS5vbignb3Blbi1lbmQnLCBmdW5jdGlvbigpIHtcclxuXHRQbGF5LmNvbnRhaW5lci5zZXRBdHRyaWJ1dGUoJ2RhdGEtaGlzdG9yeScsIHRydWUpXHJcblx0SGlzdG9yeS5mb2N1c0NoYXRJbnB1dCgpXHJcblx0UGxheS5jYWxjU2l6ZSgpXHJcbn0pXHJcblxyXG5IaXN0b3J5Lm9uKCdjbG9zZS1zdGFydCcsIGZ1bmN0aW9uKCkge1xyXG5cdFBsYXkuY29udGFpbmVyLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1oaXN0b3J5JylcclxuXHRIaXN0b3J5LnVuZm9jdXNDaGF0SW5wdXQoKVxyXG5cdFBsYXkuY2FsY1NpemUoKVxyXG59KVxyXG5cclxuUGxheS5yZWFkeSA9IHRydWVcclxuXHJcblBsYXkubG9hZEdhbWUgPSBmdW5jdGlvbihnYW1lLCBpbml0KSB7XHJcblx0aWYoIWluaXQpIE1haW4uc2V0QWN0aXZlKCdwbGF5JylcclxuXHRpZighUGxheS5jdXJyZW50R2FtZSB8fCBQbGF5LmN1cnJlbnRHYW1lLl9pZCAhPSBnYW1lLl9pZCkge1xyXG5cdFx0UGxheS5jdXJyZW50R2FtZSA9IGdhbWVcclxuXHRcdFBsYXkuY3VycmVudEluZGV4ID0gZ2V0SGlzdG9yeUluZGV4KGdhbWUpXHJcblx0XHRQbGF5LnJlYWR5ID0gdHJ1ZVxyXG5cdFx0SGlzdG9yeS5sb2FkR2FtZShQbGF5LmN1cnJlbnRHYW1lLCBQbGF5LmN1cnJlbnRJbmRleClcclxuXHRcdFVzZXJzLmdldEJ5SWQoZ2FtZS5vcHBvbmVudC5faWQsIGZ1bmN0aW9uKG9wcG9uZW50KSB7XHJcblx0XHRcdFBsYXkudHJpZ2dlcignbG9hZCcsIGdhbWUuXy5oaXN0b3J5W1BsYXkuY3VycmVudEluZGV4XS5zdGF0ZSwgVXNlcnMuc2VsZiwgb3Bwb25lbnQsIHt9LCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRQbGF5LnRpbWVvdXQoODAwLCBQbGF5LnN0YXJ0VHJpZ2dlcmluZylcclxuXHRcdFx0fSlcclxuXHRcdH0pXHJcblx0fVxyXG59XHJcblxyXG5QbGF5LmNoYWxsZW5nZUJ5SWQgPSBmdW5jdGlvbihvcHBvbmVudF9pZCkge1xyXG5cdHNlcnZlci5lbWl0KCdnYW1lcy9jaGFsbGVuZ2VfaWQnLCB7IG9wcG9uZW50Om9wcG9uZW50X2lkIH0pXHJcblx0c2VydmVyLm9uY2UoJ2dhbWVzL2NoYWxsZW5nZV9pZC9yZXNwb25zZScsIGZ1bmN0aW9uKGdhbWVfaWQpIHtcclxuXHRcdGlmKGdhbWVfaWQuZXJyKSByZXR1cm4gYWxlcnQoZ2FtZV9pZC5lcnIsICdlcnJvcicpXHJcblx0XHRNZW51LmRpc3BsYXlHYW1lcygpXHJcblx0XHRHYW1lcy5nZXRCeUlkKGdhbWVfaWQsIGZ1bmN0aW9uKGdhbWUpIHtcclxuXHRcdFx0TWVudS5sb2FkR2FtZShnYW1lKVxyXG5cdFx0fSlcclxuXHR9KVxyXG59XHJcblxyXG5QbGF5LnN0YXJ0VHJpZ2dlcmluZyA9IGZ1bmN0aW9uKCkge1xyXG5cdGlmKFBsYXkucmVhZHkpIHtcclxuXHRcdFBsYXkucmVhZHkgPSBmYWxzZVxyXG5cdFx0UGxheS50cmlnZ2VySGlzdG9yeShmdW5jdGlvbigpIHtcclxuXHRcdFx0UGxheS5yZWFkeSA9IHRydWVcclxuXHRcdH0pXHJcblx0fVxyXG59XHJcblxyXG5QbGF5LnRyaWdnZXJIaXN0b3J5ID0gZnVuY3Rpb24oZm4pIHtcclxuXHRpZihQbGF5LmN1cnJlbnRJbmRleCA8IFBsYXkuY3VycmVudEdhbWUuXy5oaXN0b3J5Lmxlbmd0aC0xKSB7XHJcblx0XHR2YXIgaGlzdG9yeV9pdGVtID0gUGxheS5jdXJyZW50R2FtZS5fLmhpc3RvcnlbKytQbGF5LmN1cnJlbnRJbmRleF1cclxuXHRcdEhpc3RvcnkuYWRkSGlzdG9yeUl0ZW0oUGxheS5jdXJyZW50R2FtZSwgaGlzdG9yeV9pdGVtKVxyXG5cdFx0VXNlcnMuZ2V0QnlJZChQbGF5LmN1cnJlbnRHYW1lLm9wcG9uZW50Ll9pZCwgZnVuY3Rpb24ob3Bwb25lbnQpIHtcclxuXHRcdFx0aGlzdG9yeV9pdGVtLnZpZXdlZCA9IHRydWVcclxuXHRcdFx0UGxheS50cmlnZ2VyKGhpc3RvcnlfaXRlbS50eXBlLCBoaXN0b3J5X2l0ZW0uc3RhdGUsIFVzZXJzLnNlbGYsIG9wcG9uZW50LCBoaXN0b3J5X2l0ZW0sIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFBsYXkudHJpZ2dlckhpc3RvcnkoZm4pXHJcblx0XHRcdH0pXHJcblx0XHR9KVxyXG5cdH0gZWxzZSBmbigpIFxyXG59XHJcblxyXG5QbGF5Lm9uID0gZnVuY3Rpb24oZXZlbnRUeXBlLCBldmVudEhhbmRsZXIpIHtcclxuXHR0aGlzLmhhbmRsZXJzW2V2ZW50VHlwZV0gPSB0aGlzLmhhbmRsZXJzW2V2ZW50VHlwZV0gfHwgW11cclxuXHR0aGlzLmhhbmRsZXJzW2V2ZW50VHlwZV0ucHVzaChldmVudEhhbmRsZXIpXHJcbn1cclxuXHJcblBsYXkudHJpZ2dlciA9IGZ1bmN0aW9uKGV2ZW50VHlwZSwgZ2FtZSwgcGxheWVyLCBvcHBvbmVudCwgZXZlbnQsIGRvbmUpIHtcclxuXHRpZih0aGlzLmhhbmRsZXJzW2V2ZW50VHlwZV0pIHtcclxuXHRcdHZhciByZW1haW5pbmcgPSAwXHJcblx0XHR0aGlzLmhhbmRsZXJzW2V2ZW50VHlwZV0uZm9yRWFjaChmdW5jdGlvbihoYW5kbGVyKSB7XHJcblx0XHRcdHJlbWFpbmluZysrXHJcblx0XHRcdGhhbmRsZXIoZ2FtZSwgcGxheWVyLCBvcHBvbmVudCwgZXZlbnQsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHJlbWFpbmluZy0tXHJcblx0XHRcdFx0aWYoIXJlbWFpbmluZykge1xyXG5cdFx0XHRcdFx0UGxheS50aW1lb3V0KDEwMCwgZG9uZSlcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHR9KVxyXG5cdH0gZWxzZSB7XHJcblx0XHRQbGF5LnRpbWVvdXQoMTAwLCBkb25lKVxyXG5cdH1cclxufVxyXG5cclxuUGxheS50cmlnZ2VyUGxheWVyRXZlbnQgPSBmdW5jdGlvbihldmVudFR5cGUsIHVzZXIsIGV2ZW50LCBkb25lKSB7XHJcblx0aWYodGhpcy5oYW5kbGVyc1tldmVudFR5cGVdKSB7XHJcblx0XHR2YXIgcmVtYWluaW5nID0gMFxyXG5cdFx0dGhpcy5oYW5kbGVyc1tldmVudFR5cGVdLmZvckVhY2goZnVuY3Rpb24oaGFuZGxlcikge1xyXG5cdFx0XHRyZW1haW5pbmcrK1xyXG5cdFx0XHRoYW5kbGVyKHVzZXIsIGV2ZW50LCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRyZW1haW5pbmctLVxyXG5cdFx0XHRcdGlmKCFyZW1haW5pbmcpIHtcclxuXHRcdFx0XHRcdFBsYXkudGltZW91dCgxMDAsIGRvbmUpXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0fSlcclxuXHR9IGVsc2Uge1xyXG5cdFx0UGxheS50aW1lb3V0KDAsIGRvbmUpXHJcblx0fVxyXG59XHJcblxyXG5QbGF5LmVtaXQgPSBmdW5jdGlvbihldmVudFR5cGUsIGV2ZW50RGF0YSwgZ2FtZV9pZCkge1xyXG5cdGlmKCFzZXJ2ZXIuY29ubmVjdGVkKSByZXR1cm4gc2VydmVyLnJlY29ubmVjdCgpXHJcblxyXG5cdHNlcnZlci5lbWl0KCdnYW1lcy9ldmVudCcsIGdhbWVfaWR8fFBsYXkuY3VycmVudEdhbWUuX2lkLCBldmVudFR5cGUsIGV2ZW50RGF0YSlcclxuXHRzZXJ2ZXIub25jZSgnZ2FtZXMvZXZlbnQvcmVzcG9uc2UnLCBmdW5jdGlvbihnYW1lKSB7XHJcblx0XHRpZihnYW1lLmVycikgcmV0dXJuIGFsZXJ0KGdhbWUuZXJyKVxyXG5cdH0pXHJcbn1cclxuXHJcblBsYXkudGltZW91dCA9IGZ1bmN0aW9uKG1zLCBmbikge1xyXG5cdHZhciBnYW1lX2lkID0gUGxheS5jdXJyZW50R2FtZS5faWRcclxuXHRyZXR1cm4gc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuXHRcdGlmKGdhbWVfaWQgPT0gUGxheS5jdXJyZW50R2FtZS5faWQpIGZuKClcclxuXHR9LCBtcylcclxufVxyXG5cclxuUGxheS5hbmltYXRpb25GcmFtZSA9IGZ1bmN0aW9uKGZuKSB7XHJcblx0dmFyIGdhbWVfaWQgPSBQbGF5LmN1cnJlbnRHYW1lLl9pZFxyXG5cdHJldHVybiByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZnVuY3Rpb24oKSB7XHJcblx0XHRpZihnYW1lX2lkID09IFBsYXkuY3VycmVudEdhbWUuX2lkKSBmbigpXHJcblx0fSlcclxufVxyXG5cclxuUGxheS5hbmltYXRlID0gZnVuY3Rpb24oZWxlbWVudHMsIHByb3BlcnRpZXMsIGR1cmF0aW9uLCBlYXNlLCBjYWxsYmFjaywgZGVsYXkpIHtcclxuXHRpZihkdXJhdGlvbiAmJiB0eXBlb2YgZHVyYXRpb24gPT0gJ29iamVjdCcpIHtcclxuXHRcdGNhbGxiYWNrID0gZHVyYXRpb24uY2FsbGJhY2tcclxuXHRcdGR1cmF0aW9uLmNhbGxiYWNrID0gZm5cclxuXHR9XHJcblx0cmVxdWlyZSgnLi4vdXRpbHMvYW5pbWF0aW9uJykoZWxlbWVudHMsIHByb3BlcnRpZXMsIGR1cmF0aW9uLCBlYXNlLCBmbiwgZGVsYXkpXHJcblx0ZnVuY3Rpb24gZm4oKSB7IFBsYXkuYW5pbWF0aW9uRnJhbWUoY2FsbGJhY2spIH1cclxufVxyXG5cclxuUGxheS5jb3VudEFuaW1hdGlvbiA9IGZ1bmN0aW9uKGVsZW1lbnQsIHRhcmdldF9hbW91bnQsIGR1cmF0aW9uLCBmbikge1xyXG5cdHZhciBzdGFydF9hbW91bnQgPSBwYXJzZUludChlbGVtZW50LnRleHRDb250ZW50KVxyXG5cdGFuaW1hdGVBbW91bnQoRGF0ZS5ub3coKSwgc3RhcnRfYW1vdW50KVxyXG5cdGZ1bmN0aW9uIGFuaW1hdGVBbW91bnQodGltZV9zdGFydCwgY3VycmVudF9hbW91bnQpIHtcclxuXHRcdGVsZW1lbnQudGV4dENvbnRlbnQgPSBjdXJyZW50X2Ftb3VudFxyXG5cdFx0aWYoY3VycmVudF9hbW91bnQgPT0gdGFyZ2V0X2Ftb3VudCBcclxuXHRcdFx0fHwgKHN0YXJ0X2Ftb3VudCA+IHRhcmdldF9hbW91bnQgJiYgY3VycmVudF9hbW91bnQgPCB0YXJnZXRfYW1vdW50KSBcclxuXHRcdFx0fHwgKHN0YXJ0X2Ftb3VudCA8IHRhcmdldF9hbW91bnQgJiYgY3VycmVudF9hbW91bnQgPiB0YXJnZXRfYW1vdW50KSkge1xyXG5cdFx0XHRlbGVtZW50LnRleHRDb250ZW50ID0gdGFyZ2V0X2Ftb3VudCBcclxuXHRcdFx0cmV0dXJuIGZuICYmIGZuKClcclxuXHRcdH0gIFxyXG5cdFx0UGxheS5hbmltYXRpb25GcmFtZShmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIG5vdyA9IERhdGUubm93KClcclxuXHRcdFx0ICAsIG5ld19hbW91bnQgPSBNYXRoLmZsb29yKHN0YXJ0X2Ftb3VudCArICh0YXJnZXRfYW1vdW50IC0gc3RhcnRfYW1vdW50KSoobm93LXRpbWVfc3RhcnQpL2R1cmF0aW9uKVxyXG5cdFx0XHRhbmltYXRlQW1vdW50KHRpbWVfc3RhcnQsIG5ld19hbW91bnQpXHJcblx0XHR9KVxyXG5cdH1cclxufVxyXG5cclxuUGxheS5jYWxjU2l6ZSA9IGZ1bmN0aW9uKCkge1xyXG5cdGNvbnNvbGUubG9nKCdvcmVzaXplJylcclxuXHR2YXIgdyA9IFBsYXkuY29udGFpbmVyLmNsaWVudFdpZHRoLzE2XHJcblx0ICAsIGggPSBQbGF5LmNvbnRhaW5lci5jbGllbnRIZWlnaHQvMTZcclxuXHQgICwgcmF0aW8gPSB3L2hcclxuXHQgICwgY2xhc3NlcyA9IHJhdGlvID4gMSA/ICdsYW5kc2NhcGUnIDogJ3BvcnRyYWl0J1xyXG5cclxuXHRmb3IodmFyIGkgPSAxMDsgaSA8PSAxMjA7IGkrPTEwKSB7XHJcblx0XHRjbGFzc2VzICs9ICcgdy0nICsgKGkgPCB3ID8gJ2d0JyA6ICdsdCcpICsgJy0nICsgaVxyXG5cdFx0Y2xhc3NlcyArPSAnIGgtJyArIChpIDwgaCA/ICdndCcgOiAnbHQnKSArICctJyArIGlcclxuXHR9XHJcblxyXG5cdFBsYXkuY29udGFpbmVyLmNsYXNzTmFtZSA9IGNsYXNzZXNcclxuXHRQbGF5LnRyaWdnZXIoJ3Jlc2l6ZScpXHJcblxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRIaXN0b3J5SW5kZXgoZ2FtZSkge1xyXG5cdHZhciBsYXN0X2luZGV4ID0gZ2FtZS5fLmhpc3RvcnkubGVuZ3RoLTFcclxuXHQgICwgaW5kZXggPSBsYXN0X2luZGV4XHJcblx0ICAsIGhpc3RvcnlfaXRlbSA9IGdhbWUuXy5oaXN0b3J5W2luZGV4XVxyXG5cclxuXHR3aGlsZShpbmRleCA+IDAgJiYgIWhpc3RvcnlfaXRlbS52aWV3ZWQpIHtcclxuXHRcdGlmKGhpc3RvcnlfaXRlbS5zdGF0ZS50dXJuICE9IGdhbWUuXy5oaXN0b3J5W2luZGV4LTFdLnN0YXRlLnR1cm4gJiYgaW5kZXggIT0gbGFzdF9pbmRleCkge1xyXG5cdFx0XHRicmVha1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0aGlzdG9yeV9pdGVtID0gZ2FtZS5fLmhpc3RvcnlbLS1pbmRleF1cclxuXHRcdH1cclxuXHR9XHJcblx0cmV0dXJuIGluZGV4ID4gbGFzdF9pbmRleCA/IGxhc3RfaW5kZXggOiBpbmRleFxyXG59IiwidmFyIEJhc2VQcmVzZW50ZXIgPSByZXF1aXJlKCcuL2Jhc2UnKVxyXG4gICwgU2hvcCA9IG1vZHVsZS5leHBvcnRzID0gbmV3IEJhc2VQcmVzZW50ZXIoJyNzaG9wJylcclxuICAsIE1haW4gPSByZXF1aXJlKCcuL21haW4nKVxyXG4gICwgTG9naW4gPSByZXF1aXJlKCcuL2xvZ2luJylcclxuICAsIHNlcnZlciA9IHJlcXVpcmUoJy4uL3NvY2tldC5pbycpXHJcbiAgLCBVc2VycyA9IHJlcXVpcmUoJy4uL21vZGVscy91c2VycycpXHJcbiAgLCBHYW1lcyA9IHJlcXVpcmUoJy4uL21vZGVscy9nYW1lcycpXHJcbiAgLCBsYW5nID0gcmVxdWlyZSgnLi4vbGFuZycpXHJcblxyXG5TaG9wLl9pbml0ID0gZnVuY3Rpb24oY29udGFpbmVyKSB7XHJcblx0U2hvcC5yZW5kZXIoJ3Nob3AnLCB7IGl0ZW1zOiBTaG9wLml0ZW1zIH0sIGZ1bmN0aW9uKGVyciwgaHRtbCkge1xyXG5cdFx0U2hvcC5jb250YWluZXIuaW5uZXJIVE1MID0gaHRtbFxyXG5cdFx0U2hvcC5saXN0ZW4oJ2J1dHRvbi5jbG9zZScsICdjbGljaycsIFNob3AuaGlkZSlcclxuXHRcdFNob3AubGlzdGVuKCdUcmFuc2l0aW9uRW5kJywgZGlzcGxheU5vbmVXaGVuRG9uZSlcclxuXHRcdFNob3AuZGVsZWdhdGUoJ2J1dHRvbi5idXknLCAnY2xpY2snLCBidXlJdGVtKVxyXG5cdH0pXHJcbn1cclxuXHJcblNob3AubG9hZEl0ZW1zID0gZnVuY3Rpb24oaXRlbXMpIHtcclxuXHR2YXIgaXRlbXNfYXJyYXkgPSBbXVxyXG5cdE9iamVjdC5rZXlzKGl0ZW1zKS5mb3JFYWNoKGZ1bmN0aW9uKGtleSkge1xyXG5cdFx0aXRlbXNfYXJyYXkucHVzaChpdGVtc1trZXldKVxyXG5cdFx0aXRlbXNba2V5XS5rZXkgPSBrZXlcclxuXHR9KVxyXG5cdFNob3AuaXRlbXMgPSBpdGVtc19hcnJheVxyXG59XHJcblxyXG5TaG9wLnNob3cgPSBmdW5jdGlvbigpIHtcclxuXHRTaG9wLmNvbnRhaW5lci5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJ1xyXG5cdFNob3AuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnZGF0YS12aXNpYmxlJywgdHJ1ZSlcclxufVxyXG5cclxuU2hvcC5oaWRlID0gZnVuY3Rpb24oKSB7XHJcblx0U2hvcC5jb250YWluZXIucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXZpc2libGUnKVxyXG59XHJcblxyXG5mdW5jdGlvbiBkaXNwbGF5Tm9uZVdoZW5Eb25lKCkge1xyXG5cdGlmKCFwYXJzZUludCh3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShTaG9wLmNvbnRhaW5lcikub3BhY2l0eSkpIHtcclxuXHRcdFNob3AuY29udGFpbmVyLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGJ1eUl0ZW0oKSB7XHJcblx0c2VydmVyLmVtaXQoJ3Nob3AvYnV5JywgeyBrZXk6IHRoaXMuYXR0cignZGF0YS1rZXknKSB9KVxyXG5cdHNlcnZlci5vbmNlKCdzaG9wL2J1eS9yZXNwb25zZScsIGZ1bmN0aW9uKGVyciwgcmVzKSB7XHJcblx0XHRjb25zb2xlLmxvZyhlcnIpXHJcblx0XHRpZighZXJyKSB7XHJcblx0XHRcdGNvbnNvbGUubG9nKHJlcylcclxuXHRcdH1cclxuXHR9KVxyXG59IiwidmFyIHBvcnREZWYgPSAvcG9ydFxcPShcXGQrKS8uZXhlYyh3aW5kb3cubG9jYXRpb24uaGFzaClcclxuXHQsIGNvbmZpZyA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy5qcycpXHJcbiAgLCBzb2NrZXQgPSB3aW5kb3cuc29ja2V0ID0gbW9kdWxlLmV4cG9ydHMgPSBpby5jb25uZWN0KCdodHRwOi8vJytjb25maWcuaG9zdG5hbWUrJzonK2NvbmZpZy5wb3J0KVxyXG5cclxuc29ja2V0Lm9uKCdkaXNjb25uZWN0JywgZnVuY3Rpb24oKSB7XHJcblx0c29ja2V0LmNvbm5lY3RlZCA9IGZhbHNlXHJcblx0c29ja2V0LnJlY29ubmVjdCgpXHJcbn0pXHJcblxyXG5zb2NrZXQub24oJ2Nvbm5lY3QnLCBmdW5jdGlvbigpIHtcclxuXHRzb2NrZXQuY29ubmVjdGVkID0gdHJ1ZVxyXG59KVxyXG5cclxuc29ja2V0LnJlY29ubmVjdCA9IGZ1bmN0aW9uKCkge1xyXG5cdHZhciByZWNvbm5lY3QgPSBjb25maXJtKCdZb3UgaGF2ZSBkaXNjb25uZWN0ZWQsIGRvIHlvdSB3YW50IHRvIHRyeSB0byByZWNvbm5lY3Q/JylcclxuXHRpZihyZWNvbm5lY3QpIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKVxyXG59XHJcblxyXG5jb25zb2xlLmxvZygnQ09OTkVDVEVEIFRPOicsICdodHRwOi8vJytjb25maWcuaG9zdG5hbWUrJzonK2NvbmZpZy5wb3J0KVxyXG5cclxuc29ja2V0LnJlcXVlc3QgPSBmdW5jdGlvbigpIHtcclxuXHR2YXIgbGFzdEluZGV4ID0gYXJndW1lbnRzLmxlbmd0aC0xXHJcblx0ICAsIGZuID0gYXJndW1lbnRzW2xhc3RJbmRleF1cclxuXHQgICwgcm91dGUgPSBhcmd1bWVudHNbMF1cclxuXHQgICwgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zcGxpY2UuY2FsbChhcmd1bWVudHMsIDAsIGxhc3RJbmRleClcclxuXHRzb2NrZXQuZW1pdC5hcHBseShzb2NrZXQsIGFyZ3MpXHJcblx0c29ja2V0Lm9uY2Uocm91dGUrJy9yZXNwb25zZScsIGZuKVxyXG59IiwidmFyIHByZWZpeCA9ICcnLCBldmVudFByZWZpeCwgZW5kRXZlbnROYW1lLCBlbmRBbmltYXRpb25OYW1lLFxyXG4gICAgdmVuZG9ycyA9IHsgV2Via2l0OiAnd2Via2l0JywgTW96OiAnJywgTzogJ28nLCBtczogJ01TJyB9LFxyXG4gICAgZG9jdW1lbnQgPSB3aW5kb3cuZG9jdW1lbnQsIHRlc3RFbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpLFxyXG4gICAgc3VwcG9ydGVkVHJhbnNmb3JtcyA9IC9eKCh0cmFuc2xhdGV8cm90YXRlfHNjYWxlKShYfFl8WnwzZCk/fG1hdHJpeCgzZCk/fHBlcnNwZWN0aXZlfHNrZXcoWHxZKT8pJC9pLFxyXG4gICAgdHJhbnNmb3JtLFxyXG4gICAgdHJhbnNpdGlvblByb3BlcnR5LCB0cmFuc2l0aW9uRHVyYXRpb24sIHRyYW5zaXRpb25UaW1pbmcsIHRyYW5zaXRpb25EZWxheSxcclxuICAgIGFuaW1hdGlvbk5hbWUsIGFuaW1hdGlvbkR1cmF0aW9uLCBhbmltYXRpb25UaW1pbmcsIGFuaW1hdGlvbkRlbGF5LFxyXG4gICAgY3NzUmVzZXQgPSB7fVxyXG5cclxuZnVuY3Rpb24gZGFzaGVyaXplKHN0cikgeyByZXR1cm4gc3RyLnJlcGxhY2UoLyhbYS16XSkoW0EtWl0pLywgJyQxLSQyJykudG9Mb3dlckNhc2UoKSB9XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZUV2ZW50KG5hbWUpIHsgcmV0dXJuIGV2ZW50UHJlZml4ID8gZXZlbnRQcmVmaXggKyBuYW1lIDogbmFtZS50b0xvd2VyQ2FzZSgpIH1cclxuXHJcblxyXG5PYmplY3Qua2V5cyh2ZW5kb3JzKS5mb3JFYWNoKGZ1bmN0aW9uKHZlbmRvck5hbWUpe1xyXG5cdGlmKHRlc3RFbC5zdHlsZVt2ZW5kb3JOYW1lICsgJ1RyYW5zaXRpb25Qcm9wZXJ0eSddICE9PSB1bmRlZmluZWQpIHtcclxuXHRcdHByZWZpeCA9ICctJyArIHZlbmRvck5hbWUudG9Mb3dlckNhc2UoKSArICctJ1xyXG5cdFx0ZXZlbnRQcmVmaXggPSB2ZW5kb3JzW3ZlbmRvck5hbWVdXHJcblx0XHRyZXR1cm4gZmFsc2VcclxuXHR9XHJcbn0pXHJcblxyXG50cmFuc2Zvcm0gPSBwcmVmaXggKyAndHJhbnNmb3JtJ1xyXG5jc3NSZXNldFt0cmFuc2l0aW9uUHJvcGVydHkgPSBwcmVmaXggKyAndHJhbnNpdGlvbi1wcm9wZXJ0eSddID1cclxuY3NzUmVzZXRbdHJhbnNpdGlvbkR1cmF0aW9uID0gcHJlZml4ICsgJ3RyYW5zaXRpb24tZHVyYXRpb24nXSA9XHJcbmNzc1Jlc2V0W3RyYW5zaXRpb25EZWxheSAgICA9IHByZWZpeCArICd0cmFuc2l0aW9uLWRlbGF5J10gPVxyXG5jc3NSZXNldFt0cmFuc2l0aW9uVGltaW5nICAgPSBwcmVmaXggKyAndHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb24nXSA9XHJcbmNzc1Jlc2V0W2FuaW1hdGlvbk5hbWUgICAgICA9IHByZWZpeCArICdhbmltYXRpb24tbmFtZSddID1cclxuY3NzUmVzZXRbYW5pbWF0aW9uRHVyYXRpb24gID0gcHJlZml4ICsgJ2FuaW1hdGlvbi1kdXJhdGlvbiddID1cclxuY3NzUmVzZXRbYW5pbWF0aW9uRGVsYXkgICAgID0gcHJlZml4ICsgJ2FuaW1hdGlvbi1kZWxheSddID1cclxuY3NzUmVzZXRbYW5pbWF0aW9uVGltaW5nICAgID0gcHJlZml4ICsgJ2FuaW1hdGlvbi10aW1pbmctZnVuY3Rpb24nXSA9ICcnXHJcblxyXG52YXIgZnggPSB7XHJcblx0ICBvZmY6IChldmVudFByZWZpeCA9PT0gdW5kZWZpbmVkICYmIHRlc3RFbC5zdHlsZS50cmFuc2l0aW9uUHJvcGVydHkgPT09IHVuZGVmaW5lZClcclxuXHQsIGNzc1ByZWZpeDogcHJlZml4XHJcblx0LCB0cmFuc2l0aW9uRW5kOiBub3JtYWxpemVFdmVudCgnVHJhbnNpdGlvbkVuZCcpXHJcblx0LCBhbmltYXRpb25FbmQ6IG5vcm1hbGl6ZUV2ZW50KCdBbmltYXRpb25FbmQnKVxyXG59XHJcblxyXG52YXIgYW5pbWF0ZSA9IGZ1bmN0aW9uKGVsZW1lbnRzLCBwcm9wZXJ0aWVzLCBkdXJhdGlvbiwgZWFzZSwgY2FsbGJhY2ssIGRlbGF5KSB7XHJcblx0aWYoIWVsZW1lbnRzLmxlbmd0aCkge1xyXG5cdFx0ZWxlbWVudHMgPSBbZWxlbWVudHNdXHJcblx0fVxyXG5cdGlmKGR1cmF0aW9uICYmIHR5cGVvZiBkdXJhdGlvbiA9PSAnb2JqZWN0Jykge1xyXG5cdFx0ZWFzZSA9IGR1cmF0aW9uLmVhc2luZ1xyXG5cdFx0Y2FsbGJhY2sgPSBkdXJhdGlvbi5jb21wbGV0ZVxyXG5cdFx0ZGVsYXkgPSBkdXJhdGlvbi5kZWxheVxyXG5cdFx0ZHVyYXRpb24gPSBkdXJhdGlvbi5kdXJhdGlvblxyXG5cdH1cclxuXHRpZihkdXJhdGlvbiAmJiB0eXBlb2YgZHVyYXRpb24gPT0gJ251bWJlcicpIHtcclxuXHRcdGR1cmF0aW9uID0gZHVyYXRpb24gLyAxMDAwXHJcblx0fVxyXG5cdGlmKGRlbGF5KSB7XHJcblx0XHRkZWxheSA9IHBhcnNlRmxvYXQoZGVsYXkpIC8gMTAwMFxyXG5cdH1cclxuXHRyZXR1cm4gYW5pbShlbGVtZW50cywgcHJvcGVydGllcywgZHVyYXRpb24sIGVhc2UsIGNhbGxiYWNrLCBkZWxheSlcclxufVxyXG5cclxudmFyIGFuaW0gPSBmdW5jdGlvbihlbGVtZW50cywgcHJvcGVydGllcywgZHVyYXRpb24sIGVhc2UsIGNhbGxiYWNrLCBkZWxheSkge1xyXG5cdHZhciBrZXlcclxuXHQgICwgY3NzVmFsdWVzID0ge31cclxuXHQgICwgY3NzUHJvcGVydGllc1xyXG5cdCAgLCB0cmFuc2Zvcm1zID0gJydcclxuXHQgICwgd3JhcHBlZENhbGxiYWNrXHJcblx0ICAsIGVuZEV2ZW50ID0gZngudHJhbnNpdGlvbkVuZFxyXG5cdCAgLCByZW1haW5pbmcgPSAwXHJcblxyXG4gICAgaWYodHlwZW9mIGR1cmF0aW9uICE9ICdudW1iZXInKSBkdXJhdGlvbiA9IDAuNFxyXG4gICAgaWYodHlwZW9mIGRlbGF5ICE9ICdudW1iZXInKSBkZWxheSA9IDBcclxuICAgIGlmKGZ4Lm9mZikgZHVyYXRpb24gPSAwXHJcblxyXG5cdGlmKHR5cGVvZiBwcm9wZXJ0aWVzID09ICdzdHJpbmcnKSB7XHJcblx0XHQvLyBrZXlmcmFtZSBhbmltYXRpb25cclxuXHRcdGNzc1ZhbHVlc1thbmltYXRpb25OYW1lXSA9IHByb3BlcnRpZXNcclxuXHRcdGNzc1ZhbHVlc1thbmltYXRpb25EdXJhdGlvbl0gPSBkdXJhdGlvbiArICdzJ1xyXG5cdFx0Y3NzVmFsdWVzW2FuaW1hdGlvbkRlbGF5XSA9IGRlbGF5ICsgJ3MnXHJcblx0XHRjc3NWYWx1ZXNbYW5pbWF0aW9uVGltaW5nXSA9IChlYXNlIHx8ICdsaW5lYXInKVxyXG5cdFx0ZW5kRXZlbnQgPSBmeC5hbmltYXRpb25FbmRcclxuXHR9IGVsc2Uge1xyXG5cdFx0Y3NzUHJvcGVydGllcyA9IFtdXHJcblx0XHQvLyBDU1MgdHJhbnNpdGlvbnNcclxuXHRcdGZvcihrZXkgaW4gcHJvcGVydGllcykge1xyXG5cdFx0XHRpZiAoc3VwcG9ydGVkVHJhbnNmb3Jtcy50ZXN0KGtleSkpIHRyYW5zZm9ybXMgKz0ga2V5ICsgJygnICsgcHJvcGVydGllc1trZXldICsgJykgJ1xyXG5cdFx0XHRlbHNlIGNzc1ZhbHVlc1trZXldID0gcHJvcGVydGllc1trZXldLCBjc3NQcm9wZXJ0aWVzLnB1c2goZGFzaGVyaXplKGtleSkpXHJcblx0XHR9XHJcblx0XHRpZih0cmFuc2Zvcm1zKSB7XHJcblx0XHRcdGNzc1ZhbHVlc1t0cmFuc2Zvcm1dID0gdHJhbnNmb3JtcywgY3NzUHJvcGVydGllcy5wdXNoKHRyYW5zZm9ybSlcclxuXHRcdH1cclxuXHRcdGlmKGR1cmF0aW9uID4gMCAmJiB0eXBlb2YgcHJvcGVydGllcyA9PT0gJ29iamVjdCcpIHtcclxuXHRcdFx0Y3NzVmFsdWVzW3RyYW5zaXRpb25Qcm9wZXJ0eV0gPSBjc3NQcm9wZXJ0aWVzLmpvaW4oJywgJylcclxuXHRcdFx0Y3NzVmFsdWVzW3RyYW5zaXRpb25EdXJhdGlvbl0gPSBkdXJhdGlvbiArICdzJ1xyXG5cdFx0XHRjc3NWYWx1ZXNbdHJhbnNpdGlvbkRlbGF5XSA9IGRlbGF5ICsgJ3MnXHJcblx0XHRcdGNzc1ZhbHVlc1t0cmFuc2l0aW9uVGltaW5nXSA9IChlYXNlIHx8ICdsaW5lYXInKVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0d3JhcHBlZENhbGxiYWNrID0gZnVuY3Rpb24oZXZlbnQpe1xyXG5cdFx0aWYodHlwZW9mIGV2ZW50ICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHQvLyBtYWtlIHN1cmUgdGhlIGV2ZW50IGRpZG4ndCBidWJibGUgZnJvbSBcImJlbG93XCJcclxuXHRcdFx0aWYgKGV2ZW50LnRhcmdldCAhPT0gZXZlbnQuY3VycmVudFRhcmdldCkgcmV0dXJuXHJcblx0XHRcdCQoZXZlbnQudGFyZ2V0KS51bmJpbmQoZW5kRXZlbnQsIHdyYXBwZWRDYWxsYmFjaylcclxuXHRcdH1cclxuXHRcdHNldENzcyhlbGVtZW50cywgY3NzUmVzZXQpXHJcblx0XHRjYWxsYmFjayAmJiBjYWxsYmFjay5jYWxsKHRoaXMpXHJcblx0fVxyXG5cclxuXHRpZihkdXJhdGlvbiA+IDApIHtcclxuXHRcdEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoZWxlbWVudHMsIGZ1bmN0aW9uKGVsZW1lbnQpIHtcclxuXHRcdFx0cmVtYWluaW5nKytcclxuXHRcdFx0YmluZE9uY2UoZWxlbWVudCwgZW5kRXZlbnQsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdHJlbWFpbmluZy0tXHJcblx0XHRcdFx0aWYoIXJlbWFpbmluZykge1xyXG5cdFx0XHRcdFx0c2V0Q3NzKGVsZW1lbnRzLCBjc3NSZXNldClcclxuXHRcdFx0XHRcdGNhbGxiYWNrICYmIGNhbGxiYWNrKClcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHR9KVxyXG5cdH0gZWxzZSB7XHJcblx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRjYWxsYmFjayAmJiBjYWxsYmFjaygpXHJcblx0XHR9LCAwKVxyXG5cdH1cclxuXHJcblx0Ly8gdHJpZ2dlciBwYWdlIHJlZmxvdyBzbyBuZXcgZWxlbWVudHMgY2FuIGFuaW1hdGVcclxuXHRlbGVtZW50cy5sZW5ndGggJiYgZWxlbWVudHNbMF0uY2xpZW50TGVmdFxyXG5cclxuXHRzZXRDc3MoZWxlbWVudHMsIGNzc1ZhbHVlcylcclxufVxyXG5cclxudmFyIHNldENzcyA9IGZ1bmN0aW9uKGVsZW1lbnRzLCBwcm9wZXJ0aWVzKSB7XHJcblx0dmFyIGNzcyA9ICcnXHJcblx0Zm9yKGtleSBpbiBwcm9wZXJ0aWVzKSB7XHJcblx0XHRpZighcHJvcGVydGllc1trZXldICYmIHByb3BlcnRpZXNba2V5XSAhPT0gMCkge1xyXG5cdFx0XHRBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKGVsZW1lbnRzLCBmdW5jdGlvbihlbGVtZW50KSB7XHJcblx0XHRcdFx0ZWxlbWVudC5zdHlsZS5yZW1vdmVQcm9wZXJ0eShkYXNoZXJpemUoa2V5KSkgXHJcblx0XHRcdH0pXHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRjc3MgKz0gZGFzaGVyaXplKGtleSkgKyAnOicgKyBwcm9wZXJ0aWVzW2tleV0gKyAnOydcclxuXHRcdH1cclxuXHR9XHJcblx0QXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChlbGVtZW50cywgZnVuY3Rpb24oZWxlbWVudCkge1xyXG5cdFx0ZWxlbWVudC5zdHlsZS5jc3NUZXh0ICs9ICc7JyArIGNzc1xyXG5cdH0pXHJcbn1cclxuXHJcbnZhciBiaW5kT25jZSA9IGZ1bmN0aW9uKGVsZW1lbnQsIGV2ZW50LCBjYWxsYmFjaykge1xyXG5cdGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgZnVuY3Rpb24oZSkge1xyXG5cdFx0aWYgKGV2ZW50LnRhcmdldCAhPT0gZXZlbnQuY3VycmVudFRhcmdldCkgcmV0dXJuXHJcblx0XHRlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoZXZlbnQsIGNhbGxiYWNrLCBmYWxzZSlcclxuXHRcdGNhbGxiYWNrICYmIGNhbGxiYWNrLmNhbGwodGhpcywgZSlcclxuXHR9LCBmYWxzZSlcclxufVxyXG5cclxuLy9yZW1vdmUgcmVmZW5jZSB0byB0ZXN0IGRpdiBlbGVtZW50XHJcbnRlc3RFbCA9IG51bGxcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gYW5pbWF0ZSIsImV4cG9ydHMuaHRtbFRvTm9kZSA9IGZ1bmN0aW9uKGh0bWwpIHtcclxuXHR2YXIgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcclxuXHRkaXYuaW5uZXJIVE1MID0gaHRtbFxyXG5cdHJldHVybiBkaXYuY2hpbGRyZW5bMF1cclxufSIsIi8vIHRha2VuIGZyb21cclxuLy8gaHR0cHM6Ly9naXRodWIuY29tL2xpbmtlZGluL2R1c3Rqcy1oZWxwZXJzL2Jsb2IvbWFzdGVyL2xpYi9kdXN0LWhlbHBlcnMuanNcclxuXHJcbmR1c3QuaGVscGVycy50YXAgPSBmdW5jdGlvbiggaW5wdXQsIGNodW5rLCBjb250ZXh0ICl7XHJcblx0Ly8gcmV0dXJuIGdpdmVuIGlucHV0IGlmIHRoZXJlIGlzIG5vIGR1c3QgcmVmZXJlbmNlIHRvIHJlc29sdmVcclxuXHR2YXIgb3V0cHV0ID0gaW5wdXQ7XHJcblx0Ly8gZHVzdCBjb21waWxlcyBhIHN0cmluZy9yZWZlcmVuY2Ugc3VjaCBhcyB7Zm9vfSB0byBmdW5jdGlvbiwgXHJcblx0aWYoIHR5cGVvZiBpbnB1dCA9PT0gXCJmdW5jdGlvblwiKXtcclxuXHRcdC8vIGp1c3QgYSBwbGFpbiBmdW5jdGlvbiAoYS5rLmEgYW5vbnltb3VzIGZ1bmN0aW9ucykgaW4gdGhlIGNvbnRleHQsIG5vdCBhIGR1c3QgYGJvZHlgIGZ1bmN0aW9uIGNyZWF0ZWQgYnkgdGhlIGR1c3QgY29tcGlsZXJcclxuXHRcdGlmKCBpbnB1dC5pc0Z1bmN0aW9uID09PSB0cnVlICl7XHJcblx0XHRcdG91dHB1dCA9IGlucHV0KCk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRvdXRwdXQgPSAnJztcclxuXHRcdFx0Y2h1bmsudGFwKGZ1bmN0aW9uKGRhdGEpe1xyXG5cdFx0XHRcdG91dHB1dCArPSBkYXRhO1xyXG5cdFx0XHRcdHJldHVybiAnJztcclxuXHRcdFx0fSkucmVuZGVyKGlucHV0LCBjb250ZXh0KS51bnRhcCgpO1xyXG5cdFx0XHRpZiggb3V0cHV0ID09PSAnJyApe1xyXG5cdFx0XHRcdG91dHB1dCA9IGZhbHNlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cdHJldHVybiBvdXRwdXQ7XHJcbn1cclxuXHJcbmR1c3QuaGVscGVycy5zZXAgPSBmdW5jdGlvbihjaHVuaywgY29udGV4dCwgYm9kaWVzKSB7XHJcblx0dmFyIGJvZHkgPSBib2RpZXMuYmxvY2s7XHJcblx0aWYgKGNvbnRleHQuc3RhY2suaW5kZXggPT09IGNvbnRleHQuc3RhY2sub2YgLSAxKSB7XHJcblx0XHRyZXR1cm4gY2h1bms7XHJcblx0fVxyXG5cdGlmKGJvZHkpIHtcclxuXHRcdHJldHVybiBib2RpZXMuYmxvY2soY2h1bmssIGNvbnRleHQpO1xyXG5cdH1cclxuXHRlbHNlIHtcclxuXHRcdHJldHVybiBjaHVuaztcclxuXHR9XHJcbn1cclxuXHJcbmR1c3QuaGVscGVycy5pZHggPSBmdW5jdGlvbihjaHVuaywgY29udGV4dCwgYm9kaWVzKSB7XHJcblx0dmFyIGJvZHkgPSBib2RpZXMuYmxvY2s7XHJcblx0aWYoYm9keSkge1xyXG5cdFx0cmV0dXJuIGJvZGllcy5ibG9jayhjaHVuaywgY29udGV4dC5wdXNoKGNvbnRleHQuc3RhY2suaW5kZXgpKTtcclxuXHR9XHJcblx0ZWxzZSB7XHJcblx0XHRyZXR1cm4gY2h1bms7XHJcblx0fVxyXG59XHJcblxyXG5kdXN0LmhlbHBlcnMuZXEgPSBmdW5jdGlvbihjaHVuaywgY3R4LCBib2RpZXMsIHBhcmFtcykge1xyXG5cdHZhciBhID0gZHVzdC5oZWxwZXJzLnRhcChwYXJhbXMudGhpcywgY2h1bmssIGN0eClcclxuXHQgICwgYiA9IGR1c3QuaGVscGVycy50YXAocGFyYW1zLnRoYXQsIGNodW5rLCBjdHgpXHJcbiAgICByZXR1cm4gY2h1bmsubWFwKGZ1bmN0aW9uKGNodW5rKSB7XHJcblx0XHRpZihhID09IGIpIGNodW5rLnJlbmRlcihib2RpZXMuYmxvY2ssIGN0eClcclxuXHRcdGVsc2UgaWYoYm9kaWVzWydlbHNlJ10pIGNodW5rLnJlbmRlcihib2RpZXNbJ2Vsc2UnXSwgY3R4KVxyXG5cdFx0Y2h1bmsuZW5kKClcclxuICAgIH0pXHJcbn0iLCIoZnVuY3Rpb24gKGdsb2JhbCwgZmFjdG9yeSkge1xuICAgICAgICAgICAgdHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gbW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCkgOlxuICAgICAgICAgICAgdHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kID8gZGVmaW5lKGZhY3RvcnkpIDpcbiAgICAgICAgICAgIGdsb2JhbC5yYW1qZXQgPSBmYWN0b3J5KClcbn0odGhpcywgZnVuY3Rpb24gKCkgeyAndXNlIHN0cmljdCc7XG5cbiAgICAgICAgICAgIC8vIGZvciB0aGUgc2FrZSBvZiBTYWZhcmksIG1heSBpdCBidXJuIGluIGhlbGxcbiAgICAgICAgICAgIHZhciBCTEFDS0xJU1QgPSBbJ2xlbmd0aCcsICdwYXJlbnRSdWxlJ107XG5cbiAgICAgICAgICAgIHZhciBzdHlsZUtleXMgPSB1bmRlZmluZWQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgQ1NTMlByb3BlcnRpZXMgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICBcdC8vIHdoeSBoZWxsbyBGaXJlZm94XG4gICAgICAgICAgICBcdHN0eWxlS2V5cyA9IE9iamVjdC5rZXlzKENTUzJQcm9wZXJ0aWVzLnByb3RvdHlwZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXHRzdHlsZUtleXMgPSBPYmplY3Qua2V5cyhkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKS5zdHlsZSkuZmlsdGVyKGZ1bmN0aW9uIChrKSB7XG4gICAgICAgICAgICBcdFx0cmV0dXJuICEgfkJMQUNLTElTVC5pbmRleE9mKGspO1xuICAgICAgICAgICAgXHR9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHV0aWxzX3N0eWxlS2V5cyA9IHN0eWxlS2V5cztcblxuICAgICAgICAgICAgdmFyIHN2Z25zID0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJztcbiAgICAgICAgICAgIHZhciBzdmcgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgXHRzdmcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMoc3ZnbnMsICdzdmcnKTtcblxuICAgICAgICAgICAgXHRzdmcuc3R5bGUucG9zaXRpb24gPSAnZml4ZWQnO1xuICAgICAgICAgICAgXHRzdmcuc3R5bGUudG9wID0gc3ZnLnN0eWxlLmxlZnQgPSAnMCc7XG4gICAgICAgICAgICBcdHN2Zy5zdHlsZS53aWR0aCA9IHN2Zy5zdHlsZS5oZWlnaHQgPSAnMTAwJSc7XG4gICAgICAgICAgICBcdHN2Zy5zdHlsZS5vdmVyZmxvdyA9ICd2aXNpYmxlJztcbiAgICAgICAgICAgIFx0c3ZnLnN0eWxlLnBvaW50ZXJFdmVudHMgPSAnbm9uZSc7XG4gICAgICAgICAgICBcdHN2Zy5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywgJ21vZ3JpZnktc3ZnJyk7XG4gICAgICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICBcdGNvbnNvbGUubG9nKFwiVGhlIGN1cnJlbnQgYnJvd3NlciBkb2Vzbid0IHN1cHBvcnQgU1ZHXCIpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgYXBwZW5kZWRTdmcgPSBmYWxzZTtcblxuICAgICAgICAgICAgZnVuY3Rpb24gYXBwZW5kU3ZnKCkge1xuICAgICAgICAgICAgXHRkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHN2Zyk7XG4gICAgICAgICAgICBcdGFwcGVuZGVkU3ZnID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gY2xvbmVOb2RlKG5vZGUsIGRlcHRoLCBvdmVycmlkZUNsb25lKSB7XG4gICAgICAgICAgICBcdHZhciBjbG9uZSA9IG92ZXJyaWRlQ2xvbmUgPyBvdmVycmlkZUNsb25lKG5vZGUsIGRlcHRoKSA6IG5vZGUuY2xvbmVOb2RlKCk7XG5cbiAgICAgICAgICAgIFx0aWYgKHR5cGVvZiBjbG9uZSA9PT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgXHRcdHJldHVybjtcbiAgICAgICAgICAgIFx0fVxuXG4gICAgICAgICAgICBcdHZhciBzdHlsZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIFx0dmFyIGxlbiA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIFx0dmFyIGkgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBcdHZhciBjbG9uZWROb2RlID0gdW5kZWZpbmVkO1xuXG4gICAgICAgICAgICBcdGlmIChub2RlLm5vZGVUeXBlID09PSAxKSB7XG4gICAgICAgICAgICBcdFx0c3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShub2RlKTtcblxuICAgICAgICAgICAgXHRcdHV0aWxzX3N0eWxlS2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChwcm9wKSB7XG4gICAgICAgICAgICBcdFx0XHRjbG9uZS5zdHlsZVtwcm9wXSA9IHN0eWxlW3Byb3BdO1xuICAgICAgICAgICAgXHRcdH0pO1xuXG4gICAgICAgICAgICBcdFx0bGVuID0gbm9kZS5jaGlsZE5vZGVzLmxlbmd0aDtcbiAgICAgICAgICAgIFx0XHRmb3IgKGkgPSAwOyBpIDwgbGVuOyBpICs9IDEpIHtcbiAgICAgICAgICAgIFx0XHRcdGNsb25lZE5vZGUgPSBjbG9uZU5vZGUobm9kZS5jaGlsZE5vZGVzW2ldLCBkZXB0aCArIDEsIG92ZXJyaWRlQ2xvbmUpO1xuICAgICAgICAgICAgXHRcdFx0aWYgKGNsb25lZE5vZGUpIHtcbiAgICAgICAgICAgIFx0XHRcdFx0Y2xvbmUuYXBwZW5kQ2hpbGQoY2xvbmVkTm9kZSk7XG4gICAgICAgICAgICBcdFx0XHR9XG4gICAgICAgICAgICBcdFx0fVxuICAgICAgICAgICAgXHR9XG5cbiAgICAgICAgICAgIFx0cmV0dXJuIGNsb25lO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiB3cmFwTm9kZShub2RlLCBkZXN0aW5hdGlvbklzRml4ZWQsIG92ZXJyaWRlQ2xvbmUsIGFwcGVuZFRvQm9keSkge1xuICAgICAgICAgICAgXHR2YXIgaXNTdmcgPSBub2RlLm5hbWVzcGFjZVVSSSA9PT0gc3ZnbnM7XG5cbiAgICAgICAgICAgIFx0dmFyIF9ub2RlJGdldEJvdW5kaW5nQ2xpZW50UmVjdCA9IG5vZGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICAgICAgICAgIFx0dmFyIGxlZnQgPSBfbm9kZSRnZXRCb3VuZGluZ0NsaWVudFJlY3QubGVmdDtcbiAgICAgICAgICAgIFx0dmFyIHJpZ2h0ID0gX25vZGUkZ2V0Qm91bmRpbmdDbGllbnRSZWN0LnJpZ2h0O1xuICAgICAgICAgICAgXHR2YXIgdG9wID0gX25vZGUkZ2V0Qm91bmRpbmdDbGllbnRSZWN0LnRvcDtcbiAgICAgICAgICAgIFx0dmFyIGJvdHRvbSA9IF9ub2RlJGdldEJvdW5kaW5nQ2xpZW50UmVjdC5ib3R0b207XG5cbiAgICAgICAgICAgIFx0dmFyIHN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUobm9kZSk7XG4gICAgICAgICAgICBcdHZhciBjbG9uZSA9IGNsb25lTm9kZShub2RlLCAwLCBvdmVycmlkZUNsb25lKTtcblxuICAgICAgICAgICAgXHR2YXIgd3JhcHBlciA9IHtcbiAgICAgICAgICAgIFx0XHRub2RlOiBub2RlLCBjbG9uZTogY2xvbmUsIGlzU3ZnOiBpc1N2ZyxcbiAgICAgICAgICAgIFx0XHRjeDogKGxlZnQgKyByaWdodCkgLyAyLFxuICAgICAgICAgICAgXHRcdGN5OiAodG9wICsgYm90dG9tKSAvIDIsXG4gICAgICAgICAgICBcdFx0d2lkdGg6IHJpZ2h0IC0gbGVmdCxcbiAgICAgICAgICAgIFx0XHRoZWlnaHQ6IGJvdHRvbSAtIHRvcCxcbiAgICAgICAgICAgIFx0XHR0cmFuc2Zvcm06IG51bGwsXG4gICAgICAgICAgICBcdFx0Ym9yZGVyUmFkaXVzOiBudWxsXG4gICAgICAgICAgICBcdH07XG5cbiAgICAgICAgICAgIFx0aWYgKGlzU3ZnKSB7XG4gICAgICAgICAgICBcdFx0dmFyIGN0bSA9IG5vZGUuZ2V0U2NyZWVuQ1RNKCk7XG4gICAgICAgICAgICBcdFx0d3JhcHBlci50cmFuc2Zvcm0gPSAnbWF0cml4KCcgKyBbY3RtLmEsIGN0bS5iLCBjdG0uYywgY3RtLmQsIGN0bS5lLCBjdG0uZl0uam9pbignLCcpICsgJyknO1xuICAgICAgICAgICAgXHRcdHdyYXBwZXIuYm9yZGVyUmFkaXVzID0gWzAsIDAsIDAsIDBdO1xuXG4gICAgICAgICAgICBcdFx0c3ZnLmFwcGVuZENoaWxkKGNsb25lKTtcbiAgICAgICAgICAgIFx0fSBlbHNlIHtcblxuICAgICAgICAgICAgXHRcdGlmIChkZXN0aW5hdGlvbklzRml4ZWQpIHtcbiAgICAgICAgICAgIFx0XHRcdC8vIHBvc2l0aW9uIHJlbGF0aXZlIHRvIHRoZSB2aWV3cG9ydFxuICAgICAgICAgICAgXHRcdFx0Y2xvbmUuc3R5bGUucG9zaXRpb24gPSAnZml4ZWQnO1xuICAgICAgICAgICAgXHRcdFx0Y2xvbmUuc3R5bGUudG9wID0gdG9wIC0gcGFyc2VJbnQoc3R5bGUubWFyZ2luVG9wLCAxMCkgKyAncHgnO1xuICAgICAgICAgICAgXHRcdFx0Y2xvbmUuc3R5bGUubGVmdCA9IGxlZnQgLSBwYXJzZUludChzdHlsZS5tYXJnaW5MZWZ0LCAxMCkgKyAncHgnO1xuICAgICAgICAgICAgXHRcdH0gZWxzZSB7XG4gICAgICAgICAgICBcdFx0XHR2YXIgb2Zmc2V0UGFyZW50ID0gbm9kZS5vZmZzZXRQYXJlbnQ7XG5cbiAgICAgICAgICAgIFx0XHRcdGlmIChvZmZzZXRQYXJlbnQgPT09IG51bGwgfHwgb2Zmc2V0UGFyZW50ID09PSBkb2N1bWVudC5ib2R5IHx8IGFwcGVuZFRvQm9keSkge1xuICAgICAgICAgICAgXHRcdFx0XHQvLyBwYXJlbnQgaXMgZml4ZWQsIG9yIEkgd2FudCB0byBhcHBlbmQgdGhlIG5vZGUgdG8gdGhlIGJvZHlcbiAgICAgICAgICAgIFx0XHRcdFx0Ly8gcG9zaXRpb24gcmVsYXRpdmUgdG8gdGhlIGRvY3VtZW50XG4gICAgICAgICAgICBcdFx0XHRcdHZhciBkb2NFbGVtID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICAgICAgXHRcdFx0XHRjbG9uZS5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XG4gICAgICAgICAgICBcdFx0XHRcdGNsb25lLnN0eWxlLnRvcCA9IHRvcCArIHdpbmRvdy5wYWdlWU9mZnNldCAtIGRvY0VsZW0uY2xpZW50VG9wIC0gcGFyc2VJbnQoc3R5bGUubWFyZ2luVG9wLCAxMCkgKyAncHgnO1xuICAgICAgICAgICAgXHRcdFx0XHRjbG9uZS5zdHlsZS5sZWZ0ID0gbGVmdCArIHdpbmRvdy5wYWdlWE9mZnNldCAtIGRvY0VsZW0uY2xpZW50TGVmdCAtIHBhcnNlSW50KHN0eWxlLm1hcmdpbkxlZnQsIDEwKSArICdweCc7XG4gICAgICAgICAgICBcdFx0XHR9IGVsc2Uge1xuICAgICAgICAgICAgXHRcdFx0XHQvL3Bvc2l0aW9uIHJlbGF0aXZlIHRvIHRoZSBwYXJlbnRcbiAgICAgICAgICAgIFx0XHRcdFx0dmFyIG9mZnNldFBhcmVudFN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUob2Zmc2V0UGFyZW50KTtcbiAgICAgICAgICAgIFx0XHRcdFx0dmFyIG9mZnNldFBhcmVudEJjciA9IG9mZnNldFBhcmVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuICAgICAgICAgICAgXHRcdFx0XHRjbG9uZS5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XG4gICAgICAgICAgICBcdFx0XHRcdGNsb25lLnN0eWxlLnRvcCA9IHRvcCAtIHBhcnNlSW50KHN0eWxlLm1hcmdpblRvcCwgMTApIC0gKG9mZnNldFBhcmVudEJjci50b3AgLSBwYXJzZUludChvZmZzZXRQYXJlbnRTdHlsZS5tYXJnaW5Ub3AsIDEwKSkgKyAncHgnO1xuICAgICAgICAgICAgXHRcdFx0XHRjbG9uZS5zdHlsZS5sZWZ0ID0gbGVmdCAtIHBhcnNlSW50KHN0eWxlLm1hcmdpbkxlZnQsIDEwKSAtIChvZmZzZXRQYXJlbnRCY3IubGVmdCAtIHBhcnNlSW50KG9mZnNldFBhcmVudFN0eWxlLm1hcmdpbkxlZnQsIDEwKSkgKyAncHgnO1xuICAgICAgICAgICAgXHRcdFx0fVxuICAgICAgICAgICAgXHRcdH1cblxuICAgICAgICAgICAgXHRcdHdyYXBwZXIudHJhbnNmb3JtID0gJyc7IC8vIFRPRE8uLi4/XG4gICAgICAgICAgICBcdFx0d3JhcHBlci5ib3JkZXJSYWRpdXMgPSBbcGFyc2VGbG9hdChzdHlsZS5ib3JkZXJUb3BMZWZ0UmFkaXVzKSwgcGFyc2VGbG9hdChzdHlsZS5ib3JkZXJUb3BSaWdodFJhZGl1cyksIHBhcnNlRmxvYXQoc3R5bGUuYm9yZGVyQm90dG9tUmlnaHRSYWRpdXMpLCBwYXJzZUZsb2F0KHN0eWxlLmJvcmRlckJvdHRvbUxlZnRSYWRpdXMpXTtcblxuICAgICAgICAgICAgXHRcdGlmIChhcHBlbmRUb0JvZHkpIHtcbiAgICAgICAgICAgIFx0XHRcdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoY2xvbmUpO1xuICAgICAgICAgICAgXHRcdH0gZWxzZSB7XG4gICAgICAgICAgICBcdFx0XHRub2RlLnBhcmVudE5vZGUuYXBwZW5kQ2hpbGQoY2xvbmUpO1xuICAgICAgICAgICAgXHRcdH1cbiAgICAgICAgICAgIFx0fVxuXG4gICAgICAgICAgICBcdHJldHVybiB3cmFwcGVyO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBoaWRlTm9kZShub2RlKSB7XG4gICAgICAgICAgICBcdG5vZGUuX19yYW1qZXRPcmlnaW5hbFRyYW5zaXRpb25fXyA9IG5vZGUuc3R5bGUudHJhbnNpdGlvbjtcbiAgICAgICAgICAgIFx0bm9kZS5zdHlsZS50cmFuc2l0aW9uID0gJyc7XG5cbiAgICAgICAgICAgIFx0bm9kZS5zdHlsZS5vcGFjaXR5ID0gMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gc2hvd05vZGUobm9kZSkge1xuICAgICAgICAgICAgXHRub2RlLnN0eWxlLnRyYW5zaXRpb24gPSAnJztcbiAgICAgICAgICAgIFx0bm9kZS5zdHlsZS5vcGFjaXR5ID0gMTtcblxuICAgICAgICAgICAgXHRpZiAobm9kZS5fX3JhbWpldE9yaWdpbmFsVHJhbnNpdGlvbl9fKSB7XG4gICAgICAgICAgICBcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcdFx0XHRub2RlLnN0eWxlLnRyYW5zaXRpb24gPSBub2RlLl9fcmFtamV0T3JpZ2luYWxUcmFuc2l0aW9uX187XG4gICAgICAgICAgICBcdFx0fSk7XG4gICAgICAgICAgICBcdH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gaXNOb2RlRml4ZWQobm9kZSkge1xuICAgICAgICAgICAgXHR3aGlsZSAobm9kZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgXHRcdGlmICh3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShub2RlKS5wb3NpdGlvbiA9PT0gXCJmaXhlZFwiKSB7XG4gICAgICAgICAgICBcdFx0XHRyZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIFx0XHR9XG4gICAgICAgICAgICBcdFx0bm9kZSA9IG5vZGUubmFtZXNwYWNlVVJJID09PSBzdmducyA/IG5vZGUucGFyZW50Tm9kZSA6IG5vZGUub2Zmc2V0UGFyZW50O1xuICAgICAgICAgICAgXHR9XG4gICAgICAgICAgICBcdHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHV0aWxzX2dldFRyYW5zZm9ybSA9IGdldFRyYW5zZm9ybTtcblxuICAgICAgICAgICAgZnVuY3Rpb24gZ2V0VHJhbnNmb3JtKGlzU3ZnLCBjeCwgY3ksIGR4LCBkeSwgZHN4LCBkc3ksIHQsIHRfc2NhbGUpIHtcbiAgICAgICAgICAgIFx0dmFyIHRyYW5zZm9ybSA9IGlzU3ZnID8gXCJ0cmFuc2xhdGUoXCIgKyBjeCArIFwiIFwiICsgY3kgKyBcIikgc2NhbGUoXCIgKyAoMSArIHRfc2NhbGUgKiBkc3gpICsgXCIgXCIgKyAoMSArIHRfc2NhbGUgKiBkc3kpICsgXCIpIHRyYW5zbGF0ZShcIiArIC1jeCArIFwiIFwiICsgLWN5ICsgXCIpIHRyYW5zbGF0ZShcIiArIHQgKiBkeCArIFwiIFwiICsgdCAqIGR5ICsgXCIpXCIgOiBcInRyYW5zbGF0ZShcIiArIHQgKiBkeCArIFwicHgsXCIgKyB0ICogZHkgKyBcInB4KSBzY2FsZShcIiArICgxICsgdF9zY2FsZSAqIGRzeCkgKyBcIixcIiArICgxICsgdF9zY2FsZSAqIGRzeSkgKyBcIilcIjtcblxuICAgICAgICAgICAgXHRyZXR1cm4gdHJhbnNmb3JtO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgdXRpbHNfZ2V0Qm9yZGVyUmFkaXVzID0gZ2V0Qm9yZGVyUmFkaXVzO1xuXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRCb3JkZXJSYWRpdXMoYSwgYiwgZHN4LCBkc3ksIHQpIHtcbiAgICAgICAgICAgIFx0dmFyIHN4ID0gMSArIHQgKiBkc3g7XG4gICAgICAgICAgICBcdHZhciBzeSA9IDEgKyB0ICogZHN5O1xuXG4gICAgICAgICAgICBcdHJldHVybiBhLm1hcChmdW5jdGlvbiAoZnJvbSwgaSkge1xuICAgICAgICAgICAgXHRcdHZhciB0byA9IGJbaV07XG5cbiAgICAgICAgICAgIFx0XHR2YXIgcnggPSAoZnJvbSArIHQgKiAodG8gLSBmcm9tKSkgLyBzeDtcbiAgICAgICAgICAgIFx0XHR2YXIgcnkgPSAoZnJvbSArIHQgKiAodG8gLSBmcm9tKSkgLyBzeTtcblxuICAgICAgICAgICAgXHRcdHJldHVybiByeCArIFwicHggXCIgKyByeSArIFwicHhcIjtcbiAgICAgICAgICAgIFx0fSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGxpbmVhcihwb3MpIHtcbiAgICAgICAgICAgIFx0cmV0dXJuIHBvcztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZWFzZUluKHBvcykge1xuICAgICAgICAgICAgXHRyZXR1cm4gTWF0aC5wb3cocG9zLCAzKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZWFzZU91dChwb3MpIHtcbiAgICAgICAgICAgIFx0cmV0dXJuIE1hdGgucG93KHBvcyAtIDEsIDMpICsgMTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZWFzZUluT3V0KHBvcykge1xuICAgICAgICAgICAgXHRpZiAoKHBvcyAvPSAwLjUpIDwgMSkge1xuICAgICAgICAgICAgXHRcdHJldHVybiAwLjUgKiBNYXRoLnBvdyhwb3MsIDMpO1xuICAgICAgICAgICAgXHR9XG5cbiAgICAgICAgICAgIFx0cmV0dXJuIDAuNSAqIChNYXRoLnBvdyhwb3MgLSAyLCAzKSArIDIpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgckFGID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fCB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IGZ1bmN0aW9uIChmbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZm4sIDE2KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHZhciB1dGlsc19yQUYgPSByQUY7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHRyYW5zZm9ybWVyc19UaW1lclRyYW5zZm9ybWVyX19fY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoJ0Nhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvbicpOyB9IH1cblxuICAgICAgICAgICAgdmFyIHRyYW5zZm9ybWVyc19UaW1lclRyYW5zZm9ybWVyX19UaW1lclRyYW5zZm9ybWVyID0gZnVuY3Rpb24gVGltZXJUcmFuc2Zvcm1lcihmcm9tLCB0bywgb3B0aW9ucykge1xuICAgICAgICAgICAgXHR0cmFuc2Zvcm1lcnNfVGltZXJUcmFuc2Zvcm1lcl9fX2NsYXNzQ2FsbENoZWNrKHRoaXMsIHRyYW5zZm9ybWVyc19UaW1lclRyYW5zZm9ybWVyX19UaW1lclRyYW5zZm9ybWVyKTtcblxuICAgICAgICAgICAgXHR2YXIgZHggPSB0by5jeCAtIGZyb20uY3g7XG4gICAgICAgICAgICBcdHZhciBkeSA9IHRvLmN5IC0gZnJvbS5jeTtcblxuICAgICAgICAgICAgXHR2YXIgZHN4ZiA9IHRvLndpZHRoIC8gZnJvbS53aWR0aCAtIDE7XG4gICAgICAgICAgICBcdHZhciBkc3lmID0gdG8uaGVpZ2h0IC8gZnJvbS5oZWlnaHQgLSAxO1xuXG4gICAgICAgICAgICBcdHZhciBkc3h0ID0gZnJvbS53aWR0aCAvIHRvLndpZHRoIC0gMTtcbiAgICAgICAgICAgIFx0dmFyIGRzeXQgPSBmcm9tLmhlaWdodCAvIHRvLmhlaWdodCAtIDE7XG5cbiAgICAgICAgICAgIFx0dmFyIHN0YXJ0VGltZSA9IERhdGUubm93KCk7XG4gICAgICAgICAgICBcdHZhciBkdXJhdGlvbiA9IG9wdGlvbnMuZHVyYXRpb24gfHwgNDAwO1xuICAgICAgICAgICAgXHR2YXIgZWFzaW5nID0gb3B0aW9ucy5lYXNpbmcgfHwgbGluZWFyO1xuICAgICAgICAgICAgXHR2YXIgZWFzaW5nU2NhbGUgPSBvcHRpb25zLmVhc2luZ1NjYWxlIHx8IGVhc2luZztcblxuICAgICAgICAgICAgXHRmdW5jdGlvbiB0aWNrKCkge1xuICAgICAgICAgICAgXHRcdHZhciB0aW1lTm93ID0gRGF0ZS5ub3coKTtcbiAgICAgICAgICAgIFx0XHR2YXIgZWxhcHNlZCA9IHRpbWVOb3cgLSBzdGFydFRpbWU7XG5cbiAgICAgICAgICAgIFx0XHRpZiAoZWxhcHNlZCA+IGR1cmF0aW9uKSB7XG4gICAgICAgICAgICBcdFx0XHRmcm9tLmNsb25lLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZnJvbS5jbG9uZSk7XG4gICAgICAgICAgICBcdFx0XHR0by5jbG9uZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHRvLmNsb25lKTtcblxuICAgICAgICAgICAgXHRcdFx0aWYgKG9wdGlvbnMuZG9uZSkge1xuICAgICAgICAgICAgXHRcdFx0XHRvcHRpb25zLmRvbmUoKTtcbiAgICAgICAgICAgIFx0XHRcdH1cblxuICAgICAgICAgICAgXHRcdFx0cmV0dXJuO1xuICAgICAgICAgICAgXHRcdH1cblxuICAgICAgICAgICAgXHRcdHZhciB0ID0gZWFzaW5nKGVsYXBzZWQgLyBkdXJhdGlvbik7XG4gICAgICAgICAgICBcdFx0dmFyIHRfc2NhbGUgPSBlYXNpbmdTY2FsZShlbGFwc2VkIC8gZHVyYXRpb24pO1xuXG4gICAgICAgICAgICBcdFx0Ly8gb3BhY2l0eVxuICAgICAgICAgICAgXHRcdGZyb20uY2xvbmUuc3R5bGUub3BhY2l0eSA9IDEgLSB0O1xuICAgICAgICAgICAgXHRcdHRvLmNsb25lLnN0eWxlLm9wYWNpdHkgPSB0O1xuXG4gICAgICAgICAgICBcdFx0Ly8gYm9yZGVyIHJhZGl1c1xuICAgICAgICAgICAgXHRcdHZhciBmcm9tQm9yZGVyUmFkaXVzID0gdXRpbHNfZ2V0Qm9yZGVyUmFkaXVzKGZyb20uYm9yZGVyUmFkaXVzLCB0by5ib3JkZXJSYWRpdXMsIGRzeGYsIGRzeWYsIHQpO1xuICAgICAgICAgICAgXHRcdHZhciB0b0JvcmRlclJhZGl1cyA9IHV0aWxzX2dldEJvcmRlclJhZGl1cyh0by5ib3JkZXJSYWRpdXMsIGZyb20uYm9yZGVyUmFkaXVzLCBkc3h0LCBkc3l0LCAxIC0gdCk7XG5cbiAgICAgICAgICAgIFx0XHRhcHBseUJvcmRlclJhZGl1cyhmcm9tLmNsb25lLCBmcm9tQm9yZGVyUmFkaXVzKTtcbiAgICAgICAgICAgIFx0XHRhcHBseUJvcmRlclJhZGl1cyh0by5jbG9uZSwgdG9Cb3JkZXJSYWRpdXMpO1xuXG4gICAgICAgICAgICBcdFx0dmFyIGN4ID0gZnJvbS5jeCArIGR4ICogdDtcbiAgICAgICAgICAgIFx0XHR2YXIgY3kgPSBmcm9tLmN5ICsgZHkgKiB0O1xuXG4gICAgICAgICAgICBcdFx0dmFyIGZyb21UcmFuc2Zvcm0gPSB1dGlsc19nZXRUcmFuc2Zvcm0oZnJvbS5pc1N2ZywgY3gsIGN5LCBkeCwgZHksIGRzeGYsIGRzeWYsIHQsIHRfc2NhbGUpICsgJyAnICsgZnJvbS50cmFuc2Zvcm07XG4gICAgICAgICAgICBcdFx0dmFyIHRvVHJhbnNmb3JtID0gdXRpbHNfZ2V0VHJhbnNmb3JtKHRvLmlzU3ZnLCBjeCwgY3ksIC1keCwgLWR5LCBkc3h0LCBkc3l0LCAxIC0gdCwgMSAtIHRfc2NhbGUpICsgJyAnICsgdG8udHJhbnNmb3JtO1xuXG4gICAgICAgICAgICBcdFx0aWYgKGZyb20uaXNTdmcpIHtcbiAgICAgICAgICAgIFx0XHRcdGZyb20uY2xvbmUuc2V0QXR0cmlidXRlKCd0cmFuc2Zvcm0nLCBmcm9tVHJhbnNmb3JtKTtcbiAgICAgICAgICAgIFx0XHR9IGVsc2Uge1xuICAgICAgICAgICAgXHRcdFx0ZnJvbS5jbG9uZS5zdHlsZS50cmFuc2Zvcm0gPSBmcm9tLmNsb25lLnN0eWxlLndlYmtpdFRyYW5zZm9ybSA9IGZyb20uY2xvbmUuc3R5bGUubXNUcmFuc2Zvcm0gPSBmcm9tVHJhbnNmb3JtO1xuICAgICAgICAgICAgXHRcdH1cblxuICAgICAgICAgICAgXHRcdGlmICh0by5pc1N2Zykge1xuICAgICAgICAgICAgXHRcdFx0dG8uY2xvbmUuc2V0QXR0cmlidXRlKCd0cmFuc2Zvcm0nLCB0b1RyYW5zZm9ybSk7XG4gICAgICAgICAgICBcdFx0fSBlbHNlIHtcbiAgICAgICAgICAgIFx0XHRcdHRvLmNsb25lLnN0eWxlLnRyYW5zZm9ybSA9IHRvLmNsb25lLnN0eWxlLndlYmtpdFRyYW5zZm9ybSA9IHRvLmNsb25lLnN0eWxlLm1zVHJhbnNmb3JtID0gdG9UcmFuc2Zvcm07XG4gICAgICAgICAgICBcdFx0fVxuXG4gICAgICAgICAgICBcdFx0dXRpbHNfckFGKHRpY2spO1xuICAgICAgICAgICAgXHR9XG5cbiAgICAgICAgICAgIFx0dGljaygpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgdmFyIHRyYW5zZm9ybWVyc19UaW1lclRyYW5zZm9ybWVyID0gdHJhbnNmb3JtZXJzX1RpbWVyVHJhbnNmb3JtZXJfX1RpbWVyVHJhbnNmb3JtZXI7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGFwcGx5Qm9yZGVyUmFkaXVzKG5vZGUsIGJvcmRlclJhZGl1cykge1xuICAgICAgICAgICAgXHRub2RlLnN0eWxlLmJvcmRlclRvcExlZnRSYWRpdXMgPSBib3JkZXJSYWRpdXNbMF07XG4gICAgICAgICAgICBcdG5vZGUuc3R5bGUuYm9yZGVyVG9wUmlnaHRSYWRpdXMgPSBib3JkZXJSYWRpdXNbMV07XG4gICAgICAgICAgICBcdG5vZGUuc3R5bGUuYm9yZGVyQm90dG9tUmlnaHRSYWRpdXMgPSBib3JkZXJSYWRpdXNbMl07XG4gICAgICAgICAgICBcdG5vZGUuc3R5bGUuYm9yZGVyQm90dG9tTGVmdFJhZGl1cyA9IGJvcmRlclJhZGl1c1szXTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXG4gICAgICAgICAgICB2YXIga2V5ZnJhbWVzU3VwcG9ydGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIHZhciBUUkFOU0ZPUk0gPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB2YXIgS0VZRlJBTUVTID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgdmFyIEFOSU1BVElPTl9ESVJFQ1RJT04gPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB2YXIgQU5JTUFUSU9OX0RVUkFUSU9OID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgdmFyIEFOSU1BVElPTl9JVEVSQVRJT05fQ09VTlQgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB2YXIgQU5JTUFUSU9OX05BTUUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB2YXIgQU5JTUFUSU9OX1RJTUlOR19GVU5DVElPTiA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIHZhciBBTklNQVRJT05fRU5EID0gdW5kZWZpbmVkO1xuXG4gICAgICAgICAgICAvLyBXZSBoYXZlIHRvIGJyb3dzZXItc25pZmYgZm9yIElFMTEsIGJlY2F1c2UgaXQgd2FzIGFwcGFyZW50bHkgd3JpdHRlblxuICAgICAgICAgICAgLy8gYnkgYSBiYXJyZWwgb2Ygc3RvbmVkIG1vbmtleXMgLSBodHRwOi8vanNmaWRkbGUubmV0L3JpY2hfaGFycmlzL29xdUx1MnFML1xuXG4gICAgICAgICAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzE3OTA3NDQ1L2hvdy10by1kZXRlY3QtaWUxMVxuICAgICAgICAgICAgdmFyIGlzSWUxMSA9ICF3aW5kb3cuQWN0aXZlWE9iamVjdCAmJiAnQWN0aXZlWE9iamVjdCcgaW4gd2luZG93O1xuXG4gICAgICAgICAgICBpZiAoIWlzSWUxMSAmJiAoJ3RyYW5zZm9ybScgaW4gZGl2LnN0eWxlIHx8ICd3ZWJraXRUcmFuc2Zvcm0nIGluIGRpdi5zdHlsZSkgJiYgKCdhbmltYXRpb24nIGluIGRpdi5zdHlsZSB8fCAnd2Via2l0QW5pbWF0aW9uJyBpbiBkaXYuc3R5bGUpKSB7XG4gICAgICAgICAgICBcdGtleWZyYW1lc1N1cHBvcnRlZCA9IHRydWU7XG5cbiAgICAgICAgICAgIFx0VFJBTlNGT1JNID0gJ3RyYW5zZm9ybScgaW4gZGl2LnN0eWxlID8gJ3RyYW5zZm9ybScgOiAnLXdlYmtpdC10cmFuc2Zvcm0nO1xuXG4gICAgICAgICAgICBcdGlmICgnYW5pbWF0aW9uJyBpbiBkaXYuc3R5bGUpIHtcbiAgICAgICAgICAgIFx0XHRLRVlGUkFNRVMgPSAnQGtleWZyYW1lcyc7XG5cbiAgICAgICAgICAgIFx0XHRBTklNQVRJT05fRElSRUNUSU9OID0gJ2FuaW1hdGlvbkRpcmVjdGlvbic7XG4gICAgICAgICAgICBcdFx0QU5JTUFUSU9OX0RVUkFUSU9OID0gJ2FuaW1hdGlvbkR1cmF0aW9uJztcbiAgICAgICAgICAgIFx0XHRBTklNQVRJT05fSVRFUkFUSU9OX0NPVU5UID0gJ2FuaW1hdGlvbkl0ZXJhdGlvbkNvdW50JztcbiAgICAgICAgICAgIFx0XHRBTklNQVRJT05fTkFNRSA9ICdhbmltYXRpb25OYW1lJztcbiAgICAgICAgICAgIFx0XHRBTklNQVRJT05fVElNSU5HX0ZVTkNUSU9OID0gJ2FuaW1hdGlvblRpbWluZ0Z1bmN0aW9uJztcblxuICAgICAgICAgICAgXHRcdEFOSU1BVElPTl9FTkQgPSAnYW5pbWF0aW9uZW5kJztcbiAgICAgICAgICAgIFx0fSBlbHNlIHtcbiAgICAgICAgICAgIFx0XHRLRVlGUkFNRVMgPSAnQC13ZWJraXQta2V5ZnJhbWVzJztcblxuICAgICAgICAgICAgXHRcdEFOSU1BVElPTl9ESVJFQ1RJT04gPSAnd2Via2l0QW5pbWF0aW9uRGlyZWN0aW9uJztcbiAgICAgICAgICAgIFx0XHRBTklNQVRJT05fRFVSQVRJT04gPSAnd2Via2l0QW5pbWF0aW9uRHVyYXRpb24nO1xuICAgICAgICAgICAgXHRcdEFOSU1BVElPTl9JVEVSQVRJT05fQ09VTlQgPSAnd2Via2l0QW5pbWF0aW9uSXRlcmF0aW9uQ291bnQnO1xuICAgICAgICAgICAgXHRcdEFOSU1BVElPTl9OQU1FID0gJ3dlYmtpdEFuaW1hdGlvbk5hbWUnO1xuICAgICAgICAgICAgXHRcdEFOSU1BVElPTl9USU1JTkdfRlVOQ1RJT04gPSAnd2Via2l0QW5pbWF0aW9uVGltaW5nRnVuY3Rpb24nO1xuXG4gICAgICAgICAgICBcdFx0QU5JTUFUSU9OX0VORCA9ICd3ZWJraXRBbmltYXRpb25FbmQnO1xuICAgICAgICAgICAgXHR9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgXHRrZXlmcmFtZXNTdXBwb3J0ZWQgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gdHJhbnNmb3JtZXJzX0tleWZyYW1lVHJhbnNmb3JtZXJfX19jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcignQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uJyk7IH0gfVxuXG4gICAgICAgICAgICB2YXIgdHJhbnNmb3JtZXJzX0tleWZyYW1lVHJhbnNmb3JtZXJfX0tleWZyYW1lVHJhbnNmb3JtZXIgPSBmdW5jdGlvbiBLZXlmcmFtZVRyYW5zZm9ybWVyKGZyb20sIHRvLCBvcHRpb25zKSB7XG4gICAgICAgICAgICBcdHRyYW5zZm9ybWVyc19LZXlmcmFtZVRyYW5zZm9ybWVyX19fY2xhc3NDYWxsQ2hlY2sodGhpcywgdHJhbnNmb3JtZXJzX0tleWZyYW1lVHJhbnNmb3JtZXJfX0tleWZyYW1lVHJhbnNmb3JtZXIpO1xuXG4gICAgICAgICAgICBcdHZhciBfZ2V0S2V5ZnJhbWVzID0gZ2V0S2V5ZnJhbWVzKGZyb20sIHRvLCBvcHRpb25zKTtcblxuICAgICAgICAgICAgXHR2YXIgZnJvbUtleWZyYW1lcyA9IF9nZXRLZXlmcmFtZXMuZnJvbUtleWZyYW1lcztcbiAgICAgICAgICAgIFx0dmFyIHRvS2V5ZnJhbWVzID0gX2dldEtleWZyYW1lcy50b0tleWZyYW1lcztcblxuICAgICAgICAgICAgXHR2YXIgZnJvbUlkID0gJ18nICsgfiB+KE1hdGgucmFuZG9tKCkgKiAxMDAwMDAwKTtcbiAgICAgICAgICAgIFx0dmFyIHRvSWQgPSAnXycgKyB+IH4oTWF0aC5yYW5kb20oKSAqIDEwMDAwMDApO1xuXG4gICAgICAgICAgICBcdHZhciBjc3MgPSBLRVlGUkFNRVMgKyAnICcgKyBmcm9tSWQgKyAnIHsgJyArIGZyb21LZXlmcmFtZXMgKyAnIH0gJyArIEtFWUZSQU1FUyArICcgJyArIHRvSWQgKyAnIHsgJyArIHRvS2V5ZnJhbWVzICsgJyB9JztcbiAgICAgICAgICAgIFx0dmFyIGRpc3Bvc2UgPSBhZGRDc3MoY3NzKTtcblxuICAgICAgICAgICAgXHRmcm9tLmNsb25lLnN0eWxlW0FOSU1BVElPTl9ESVJFQ1RJT05dID0gJ2FsdGVybmF0ZSc7XG4gICAgICAgICAgICBcdGZyb20uY2xvbmUuc3R5bGVbQU5JTUFUSU9OX0RVUkFUSU9OXSA9IG9wdGlvbnMuZHVyYXRpb24gLyAxMDAwICsgJ3MnO1xuICAgICAgICAgICAgXHRmcm9tLmNsb25lLnN0eWxlW0FOSU1BVElPTl9JVEVSQVRJT05fQ09VTlRdID0gMTtcbiAgICAgICAgICAgIFx0ZnJvbS5jbG9uZS5zdHlsZVtBTklNQVRJT05fTkFNRV0gPSBmcm9tSWQ7XG4gICAgICAgICAgICBcdGZyb20uY2xvbmUuc3R5bGVbQU5JTUFUSU9OX1RJTUlOR19GVU5DVElPTl0gPSAnbGluZWFyJztcblxuICAgICAgICAgICAgXHR0by5jbG9uZS5zdHlsZVtBTklNQVRJT05fRElSRUNUSU9OXSA9ICdhbHRlcm5hdGUnO1xuICAgICAgICAgICAgXHR0by5jbG9uZS5zdHlsZVtBTklNQVRJT05fRFVSQVRJT05dID0gb3B0aW9ucy5kdXJhdGlvbiAvIDEwMDAgKyAncyc7XG4gICAgICAgICAgICBcdHRvLmNsb25lLnN0eWxlW0FOSU1BVElPTl9JVEVSQVRJT05fQ09VTlRdID0gMTtcbiAgICAgICAgICAgIFx0dG8uY2xvbmUuc3R5bGVbQU5JTUFUSU9OX05BTUVdID0gdG9JZDtcbiAgICAgICAgICAgIFx0dG8uY2xvbmUuc3R5bGVbQU5JTUFUSU9OX1RJTUlOR19GVU5DVElPTl0gPSAnbGluZWFyJztcblxuICAgICAgICAgICAgXHR2YXIgZnJvbURvbmUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBcdHZhciB0b0RvbmUgPSB1bmRlZmluZWQ7XG5cbiAgICAgICAgICAgIFx0ZnVuY3Rpb24gZG9uZSgpIHtcbiAgICAgICAgICAgIFx0XHRpZiAoZnJvbURvbmUgJiYgdG9Eb25lKSB7XG4gICAgICAgICAgICBcdFx0XHRmcm9tLmNsb25lLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZnJvbS5jbG9uZSk7XG4gICAgICAgICAgICBcdFx0XHR0by5jbG9uZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHRvLmNsb25lKTtcblxuICAgICAgICAgICAgXHRcdFx0aWYgKG9wdGlvbnMuZG9uZSkgb3B0aW9ucy5kb25lKCk7XG5cbiAgICAgICAgICAgIFx0XHRcdGRpc3Bvc2UoKTtcbiAgICAgICAgICAgIFx0XHR9XG4gICAgICAgICAgICBcdH1cblxuICAgICAgICAgICAgXHRmcm9tLmNsb25lLmFkZEV2ZW50TGlzdGVuZXIoQU5JTUFUSU9OX0VORCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgXHRcdGZyb21Eb25lID0gdHJ1ZTtcbiAgICAgICAgICAgIFx0XHRkb25lKCk7XG4gICAgICAgICAgICBcdH0pO1xuXG4gICAgICAgICAgICBcdHRvLmNsb25lLmFkZEV2ZW50TGlzdGVuZXIoQU5JTUFUSU9OX0VORCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgXHRcdHRvRG9uZSA9IHRydWU7XG4gICAgICAgICAgICBcdFx0ZG9uZSgpO1xuICAgICAgICAgICAgXHR9KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHZhciB0cmFuc2Zvcm1lcnNfS2V5ZnJhbWVUcmFuc2Zvcm1lciA9IHRyYW5zZm9ybWVyc19LZXlmcmFtZVRyYW5zZm9ybWVyX19LZXlmcmFtZVRyYW5zZm9ybWVyO1xuXG4gICAgICAgICAgICBmdW5jdGlvbiBhZGRDc3MoY3NzKSB7XG4gICAgICAgICAgICBcdHZhciBzdHlsZUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpO1xuICAgICAgICAgICAgXHRzdHlsZUVsZW1lbnQudHlwZSA9ICd0ZXh0L2Nzcyc7XG5cbiAgICAgICAgICAgIFx0dmFyIGhlYWQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdO1xuXG4gICAgICAgICAgICBcdC8vIEludGVybmV0IEV4cGxvZGVyIHdvbid0IGxldCB5b3UgdXNlIHN0eWxlU2hlZXQuaW5uZXJIVE1MIC0gd2UgaGF2ZSB0b1xuICAgICAgICAgICAgXHQvLyB1c2Ugc3R5bGVTaGVldC5jc3NUZXh0IGluc3RlYWRcbiAgICAgICAgICAgIFx0dmFyIHN0eWxlU2hlZXQgPSBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldDtcblxuICAgICAgICAgICAgXHRpZiAoc3R5bGVTaGVldCkge1xuICAgICAgICAgICAgXHRcdHN0eWxlU2hlZXQuY3NzVGV4dCA9IGNzcztcbiAgICAgICAgICAgIFx0fSBlbHNlIHtcbiAgICAgICAgICAgIFx0XHRzdHlsZUVsZW1lbnQuaW5uZXJIVE1MID0gY3NzO1xuICAgICAgICAgICAgXHR9XG5cbiAgICAgICAgICAgIFx0aGVhZC5hcHBlbmRDaGlsZChzdHlsZUVsZW1lbnQpO1xuXG4gICAgICAgICAgICBcdHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcdFx0cmV0dXJuIGhlYWQucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KTtcbiAgICAgICAgICAgIFx0fTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZ2V0S2V5ZnJhbWVzKGZyb20sIHRvLCBvcHRpb25zKSB7XG4gICAgICAgICAgICBcdHZhciBkeCA9IHRvLmN4IC0gZnJvbS5jeDtcbiAgICAgICAgICAgIFx0dmFyIGR5ID0gdG8uY3kgLSBmcm9tLmN5O1xuXG4gICAgICAgICAgICBcdHZhciBkc3hmID0gdG8ud2lkdGggLyBmcm9tLndpZHRoIC0gMTtcbiAgICAgICAgICAgIFx0dmFyIGRzeWYgPSB0by5oZWlnaHQgLyBmcm9tLmhlaWdodCAtIDE7XG5cbiAgICAgICAgICAgIFx0dmFyIGRzeHQgPSBmcm9tLndpZHRoIC8gdG8ud2lkdGggLSAxO1xuICAgICAgICAgICAgXHR2YXIgZHN5dCA9IGZyb20uaGVpZ2h0IC8gdG8uaGVpZ2h0IC0gMTtcblxuICAgICAgICAgICAgXHR2YXIgZWFzaW5nID0gb3B0aW9ucy5lYXNpbmcgfHwgbGluZWFyO1xuICAgICAgICAgICAgXHR2YXIgZWFzaW5nU2NhbGUgPSBvcHRpb25zLmVhc2luZ1NjYWxlIHx8IGVhc2luZztcblxuICAgICAgICAgICAgXHR2YXIgbnVtRnJhbWVzID0gb3B0aW9ucy5kdXJhdGlvbiAvIDUwOyAvLyBvbmUga2V5ZnJhbWUgcGVyIDUwbXMgaXMgcHJvYmFibHkgZW5vdWdoLi4uIHRoaXMgbWF5IHByb3ZlIG5vdCB0byBiZSB0aGUgY2FzZSB0aG91Z2hcblxuICAgICAgICAgICAgXHR2YXIgZnJvbUtleWZyYW1lcyA9IFtdO1xuICAgICAgICAgICAgXHR2YXIgdG9LZXlmcmFtZXMgPSBbXTtcbiAgICAgICAgICAgIFx0dmFyIGk7XG5cbiAgICAgICAgICAgIFx0ZnVuY3Rpb24gYWRkS2V5ZnJhbWVzKHBjLCB0LCB0X3NjYWxlKSB7XG4gICAgICAgICAgICBcdFx0dmFyIGN4ID0gZnJvbS5jeCArIGR4ICogdDtcbiAgICAgICAgICAgIFx0XHR2YXIgY3kgPSBmcm9tLmN5ICsgZHkgKiB0O1xuXG4gICAgICAgICAgICBcdFx0dmFyIGZyb21Cb3JkZXJSYWRpdXMgPSB1dGlsc19nZXRCb3JkZXJSYWRpdXMoZnJvbS5ib3JkZXJSYWRpdXMsIHRvLmJvcmRlclJhZGl1cywgZHN4ZiwgZHN5ZiwgdCk7XG4gICAgICAgICAgICBcdFx0dmFyIHRvQm9yZGVyUmFkaXVzID0gdXRpbHNfZ2V0Qm9yZGVyUmFkaXVzKHRvLmJvcmRlclJhZGl1cywgZnJvbS5ib3JkZXJSYWRpdXMsIGRzeHQsIGRzeXQsIDEgLSB0KTtcblxuICAgICAgICAgICAgXHRcdHZhciBmcm9tVHJhbnNmb3JtID0gdXRpbHNfZ2V0VHJhbnNmb3JtKGZhbHNlLCBjeCwgY3ksIGR4LCBkeSwgZHN4ZiwgZHN5ZiwgdCwgdF9zY2FsZSkgKyAnICcgKyBmcm9tLnRyYW5zZm9ybTtcbiAgICAgICAgICAgIFx0XHR2YXIgdG9UcmFuc2Zvcm0gPSB1dGlsc19nZXRUcmFuc2Zvcm0oZmFsc2UsIGN4LCBjeSwgLWR4LCAtZHksIGRzeHQsIGRzeXQsIDEgLSB0LCAxIC0gdF9zY2FsZSkgKyAnICcgKyB0by50cmFuc2Zvcm07XG5cbiAgICAgICAgICAgIFx0XHRmcm9tS2V5ZnJhbWVzLnB1c2goJ1xcblxcdFxcdFxcdCcgKyBwYyArICclIHtcXG5cXHRcXHRcXHRcXHRvcGFjaXR5OiAnICsgKDEgLSB0KSArICc7XFxuXFx0XFx0XFx0XFx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogJyArIGZyb21Cb3JkZXJSYWRpdXNbMF0gKyAnO1xcblxcdFxcdFxcdFxcdGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAnICsgZnJvbUJvcmRlclJhZGl1c1sxXSArICc7XFxuXFx0XFx0XFx0XFx0Ym9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6ICcgKyBmcm9tQm9yZGVyUmFkaXVzWzJdICsgJztcXG5cXHRcXHRcXHRcXHRib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAnICsgZnJvbUJvcmRlclJhZGl1c1szXSArICc7XFxuXFx0XFx0XFx0XFx0JyArIFRSQU5TRk9STSArICc6ICcgKyBmcm9tVHJhbnNmb3JtICsgJztcXG5cXHRcXHRcXHR9Jyk7XG5cbiAgICAgICAgICAgIFx0XHR0b0tleWZyYW1lcy5wdXNoKCdcXG5cXHRcXHRcXHQnICsgcGMgKyAnJSB7XFxuXFx0XFx0XFx0XFx0b3BhY2l0eTogJyArIHQgKyAnO1xcblxcdFxcdFxcdFxcdGJvcmRlci10b3AtbGVmdC1yYWRpdXM6ICcgKyB0b0JvcmRlclJhZGl1c1swXSArICc7XFxuXFx0XFx0XFx0XFx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6ICcgKyB0b0JvcmRlclJhZGl1c1sxXSArICc7XFxuXFx0XFx0XFx0XFx0Ym9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6ICcgKyB0b0JvcmRlclJhZGl1c1syXSArICc7XFxuXFx0XFx0XFx0XFx0Ym9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogJyArIHRvQm9yZGVyUmFkaXVzWzNdICsgJztcXG5cXHRcXHRcXHRcXHQnICsgVFJBTlNGT1JNICsgJzogJyArIHRvVHJhbnNmb3JtICsgJztcXG5cXHRcXHRcXHR9Jyk7XG4gICAgICAgICAgICBcdH1cblxuICAgICAgICAgICAgXHRmb3IgKGkgPSAwOyBpIDwgbnVtRnJhbWVzOyBpICs9IDEpIHtcbiAgICAgICAgICAgIFx0XHR2YXIgcGMgPSAxMDAgKiAoaSAvIG51bUZyYW1lcyk7XG4gICAgICAgICAgICBcdFx0dmFyIHQgPSBlYXNpbmcoaSAvIG51bUZyYW1lcyk7XG4gICAgICAgICAgICBcdFx0dmFyIHRfc2NhbGUgPSBlYXNpbmdTY2FsZShpIC8gbnVtRnJhbWVzKTtcblxuICAgICAgICAgICAgXHRcdGFkZEtleWZyYW1lcyhwYywgdCwgdF9zY2FsZSk7XG4gICAgICAgICAgICBcdH1cblxuICAgICAgICAgICAgXHRhZGRLZXlmcmFtZXMoMTAwLCAxLCAxKTtcblxuICAgICAgICAgICAgXHRmcm9tS2V5ZnJhbWVzID0gZnJvbUtleWZyYW1lcy5qb2luKCdcXG4nKTtcbiAgICAgICAgICAgIFx0dG9LZXlmcmFtZXMgPSB0b0tleWZyYW1lcy5qb2luKCdcXG4nKTtcblxuICAgICAgICAgICAgXHRyZXR1cm4geyBmcm9tS2V5ZnJhbWVzOiBmcm9tS2V5ZnJhbWVzLCB0b0tleWZyYW1lczogdG9LZXlmcmFtZXMgfTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHJhbWpldCA9IHtcbiAgICAgICAgICAgIFx0dHJhbnNmb3JtOiBmdW5jdGlvbiAoZnJvbU5vZGUsIHRvTm9kZSkge1xuICAgICAgICAgICAgXHRcdHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA8PSAyIHx8IGFyZ3VtZW50c1syXSA9PT0gdW5kZWZpbmVkID8ge30gOiBhcmd1bWVudHNbMl07XG5cbiAgICAgICAgICAgIFx0XHRpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIFx0XHRcdG9wdGlvbnMgPSB7IGRvbmU6IG9wdGlvbnMgfTtcbiAgICAgICAgICAgIFx0XHR9XG5cbiAgICAgICAgICAgIFx0XHRpZiAoISgnZHVyYXRpb24nIGluIG9wdGlvbnMpKSB7XG4gICAgICAgICAgICBcdFx0XHRvcHRpb25zLmR1cmF0aW9uID0gNDAwO1xuICAgICAgICAgICAgXHRcdH1cblxuICAgICAgICAgICAgXHRcdHZhciBhcHBlbmRUb0JvZHkgPSAhIW9wdGlvbnMuYXBwZW5kVG9Cb2R5O1xuICAgICAgICAgICAgXHRcdHZhciBkZXN0aW5hdGlvbklzRml4ZWQgPSBpc05vZGVGaXhlZCh0b05vZGUpO1xuICAgICAgICAgICAgXHRcdHZhciBmcm9tID0gd3JhcE5vZGUoZnJvbU5vZGUsIGRlc3RpbmF0aW9uSXNGaXhlZCwgb3B0aW9ucy5vdmVycmlkZUNsb25lLCBhcHBlbmRUb0JvZHkpO1xuICAgICAgICAgICAgXHRcdHZhciB0byA9IHdyYXBOb2RlKHRvTm9kZSwgZGVzdGluYXRpb25Jc0ZpeGVkLCBvcHRpb25zLm92ZXJyaWRlQ2xvbmUsIGFwcGVuZFRvQm9keSk7XG5cbiAgICAgICAgICAgIFx0XHRpZiAoZnJvbS5pc1N2ZyB8fCB0by5pc1N2ZyAmJiAhYXBwZW5kZWRTdmcpIHtcbiAgICAgICAgICAgIFx0XHRcdGFwcGVuZFN2ZygpO1xuICAgICAgICAgICAgXHRcdH1cblxuICAgICAgICAgICAgXHRcdGlmICgha2V5ZnJhbWVzU3VwcG9ydGVkIHx8IG9wdGlvbnMudXNlVGltZXIgfHwgZnJvbS5pc1N2ZyB8fCB0by5pc1N2Zykge1xuICAgICAgICAgICAgXHRcdFx0cmV0dXJuIG5ldyB0cmFuc2Zvcm1lcnNfVGltZXJUcmFuc2Zvcm1lcihmcm9tLCB0bywgb3B0aW9ucyk7XG4gICAgICAgICAgICBcdFx0fSBlbHNlIHtcbiAgICAgICAgICAgIFx0XHRcdHJldHVybiBuZXcgdHJhbnNmb3JtZXJzX0tleWZyYW1lVHJhbnNmb3JtZXIoZnJvbSwgdG8sIG9wdGlvbnMpO1xuICAgICAgICAgICAgXHRcdH1cbiAgICAgICAgICAgIFx0fSxcblxuICAgICAgICAgICAgXHRoaWRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcdFx0Zm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIG5vZGVzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICAgICAgICBcdFx0XHRub2Rlc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgICAgICAgIFx0XHR9XG5cbiAgICAgICAgICAgIFx0XHRub2Rlcy5mb3JFYWNoKGhpZGVOb2RlKTtcbiAgICAgICAgICAgIFx0fSxcblxuICAgICAgICAgICAgXHRzaG93OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcdFx0Zm9yICh2YXIgX2xlbjIgPSBhcmd1bWVudHMubGVuZ3RoLCBub2RlcyA9IEFycmF5KF9sZW4yKSwgX2tleTIgPSAwOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICAgICAgICBcdFx0XHRub2Rlc1tfa2V5Ml0gPSBhcmd1bWVudHNbX2tleTJdO1xuICAgICAgICAgICAgXHRcdH1cblxuICAgICAgICAgICAgXHRcdG5vZGVzLmZvckVhY2goc2hvd05vZGUpO1xuICAgICAgICAgICAgXHR9LFxuXG4gICAgICAgICAgICBcdC8vIGV4cG9zZSBzb21lIGJhc2ljIGVhc2luZyBmdW5jdGlvbnNcbiAgICAgICAgICAgIFx0bGluZWFyOiBsaW5lYXIsIGVhc2VJbjogZWFzZUluLCBlYXNlT3V0OiBlYXNlT3V0LCBlYXNlSW5PdXQ6IGVhc2VJbk91dFxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgcmV0dXJuIHJhbWpldDtcblxufSkpOyIsIm1vZHVsZS5leHBvcnRzLm1vbmtleV9jaGlja2VuID0ge1xyXG5cdCdHckNoJzogeyBcclxuXHRcdGRlY2tfdHlwZTogJ21vbmtleV9jaGlja2VuJyxcclxuXHRcdGlkOiAxLFxyXG5cdFx0b3JkZXI6IDEwLFxyXG5cdFx0bmFtZTogJ0dyZWVkeSBDaGlja2VuJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0cGxheWVyOiB7IGRyYXc6IDIgfSxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuL2dyZWVkeV9jaGlja2VuLnBuZydcclxuXHR9LCBcclxuXHQnUG9CYSc6IHsgXHJcblx0XHRkZWNrX3R5cGU6ICdtb25rZXlfY2hpY2tlbicsXHJcblx0XHRpZDogMixcclxuXHRcdG9yZGVyOiAyMCxcclxuXHRcdG5hbWU6ICdQb2lzb25vdXMgQmFib29uJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0b3Bwb25lbnQ6IHsgcG9pc29uOiAzIH0sXHJcblx0XHRpbWc6ICdtb25rZXlfY2hpY2tlbi9wb2lzb25vdXNfYmFib29uLnBuZydcclxuXHR9LFxyXG5cdCdIZUhlJzogeyBcclxuXHRcdGRlY2tfdHlwZTogJ21vbmtleV9jaGlja2VuJyxcclxuXHRcdGlkOiAzLFxyXG5cdFx0b3JkZXI6IDMwLFxyXG5cdFx0bmFtZTogJ0hlbHBmdWwgSGVuJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0cGxheWVyOiB7IGhlYWx0aDogMTUgfSxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuL2hlbHBmdWxfaGVuLnBuZydcclxuXHR9LFxyXG5cdCdLYU1vJzogeyBcclxuXHRcdGRlY2tfdHlwZTogJ21vbmtleV9jaGlja2VuJyxcclxuXHRcdGlkOiA0LFxyXG5cdFx0b3JkZXI6IDQwLFxyXG5cdFx0bmFtZTogJ0thbWlrYXplIE1vbmtleScsXHJcblx0XHRjb3VudDogMSxcclxuXHRcdHBsYXllcjogeyBoZWFsdGg6IC01MCB9LFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNTAgfSxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuL2thbWlrYXplX21vbmtleS5wbmcnXHJcblx0fSwgXHJcblx0J0luQ2gnOiB7IFxyXG5cdFx0ZGVja190eXBlOiAnbW9ua2V5X2NoaWNrZW4nLFxyXG5cdFx0aWQ6IDUsXHJcblx0XHRvcmRlcjogNTAsXHJcblx0XHRuYW1lOiAnSW50ZXJydXB0aW5nIENoaWNrZW4nLFxyXG5cdFx0Y291bnQ6IDIsXHJcblx0XHRib251czogdHJ1ZSxcclxuXHRcdHBsYXllcjogeyBcclxuXHRcdFx0dW5wb2lzb246IHRydWUsIFxyXG5cdFx0XHR1bmRhbWFnZTogdHJ1ZSBcclxuXHRcdH0sXHJcblx0XHRpbWc6ICdtb25rZXlfY2hpY2tlbi9pbnRlcnJ1cHRpbmdfY2hpY2tlbi5wbmcnXHJcblx0fSxcclxuXHQnTUMzJzoge1xyXG5cdFx0ZGVja190eXBlOiAnbW9ua2V5X2NoaWNrZW4nLFxyXG5cdFx0aWQ6IDYsXHJcblx0XHRvcmRlcjogMTEsXHJcblx0XHRuYW1lOiAnMyBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTMgfSxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuLzMucG5nJ1xyXG5cdH0sXHJcblx0J01DNCc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ21vbmtleV9jaGlja2VuJyxcclxuXHRcdGlkOiA3LFxyXG5cdFx0b3JkZXI6IDEyLFxyXG5cdFx0bmFtZTogJzQgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC00IH0sXHJcblx0XHRpbWc6ICdtb25rZXlfY2hpY2tlbi80LnBuZydcclxuXHR9LFxyXG5cdCdNQzUnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdtb25rZXlfY2hpY2tlbicsXHJcblx0XHRpZDogOCxcclxuXHRcdG9yZGVyOiAxMyxcclxuXHRcdG5hbWU6ICc1IERhbWFnZScsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHN0YWNrYWJsZTogdHJ1ZSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNSB9LFxyXG5cdFx0aW1nOiAnbW9ua2V5X2NoaWNrZW4vNS5wbmcnXHJcblx0fSxcclxuXHQnTUM2Jzoge1xyXG5cdFx0ZGVja190eXBlOiAnbW9ua2V5X2NoaWNrZW4nLFxyXG5cdFx0aWQ6IDksXHJcblx0XHRvcmRlcjogMTQsXHJcblx0XHRuYW1lOiAnNiBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTYgfSxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuLzYucG5nJ1xyXG5cdH0sXHJcblx0J01DNyc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ21vbmtleV9jaGlja2VuJyxcclxuXHRcdGlkOiAxMCxcclxuXHRcdG9yZGVyOiAxNSxcclxuXHRcdG5hbWU6ICc3IERhbWFnZScsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHN0YWNrYWJsZTogdHJ1ZSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNyB9LFxyXG5cdFx0aW1nOiAnbW9ua2V5X2NoaWNrZW4vNy5wbmcnXHJcblx0fSxcclxuXHQnTUM4Jzoge1xyXG5cdFx0ZGVja190eXBlOiAnbW9ua2V5X2NoaWNrZW4nLFxyXG5cdFx0aWQ6IDExLFxyXG5cdFx0b3JkZXI6IDE2LFxyXG5cdFx0bmFtZTogJzggRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC04IH0sXHJcblx0XHRpbWc6ICdtb25rZXlfY2hpY2tlbi84LnBuZydcclxuXHR9LFxyXG5cdCdNQzknOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdtb25rZXlfY2hpY2tlbicsXHJcblx0XHRpZDogMTIsXHJcblx0XHRvcmRlcjogMTcsXHJcblx0XHRuYW1lOiAnOSBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTkgfSxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuLzkucG5nJ1xyXG5cdH0sXHJcblx0J01DMTAnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdtb25rZXlfY2hpY2tlbicsXHJcblx0XHRpZDogMTMsXHJcblx0XHRvcmRlcjogMTgsXHJcblx0XHRuYW1lOiAnMTAgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC0xMCB9LFxyXG5cdFx0aW1nOiAnbW9ua2V5X2NoaWNrZW4vMTAucG5nJ1xyXG5cdH0sXHJcblx0J01vQ2gnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdtb25rZXlfY2hpY2tlbicsXHJcblx0XHRpZDogMTAwLFxyXG5cdFx0b3JkZXI6IDEwMCxcclxuXHRcdG5hbWU6ICdUaGUgTGVnZW5kYXJ5IE1vbmtleUNoaWNrZW4nLFxyXG5cdFx0Y291bnQ6IDEsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTQwIH0sXHJcblx0XHRjaGFuY2U6IDEwMDAsXHJcblx0XHRpbWc6ICdtb25rZXlfY2hpY2tlbi8xMC5wbmcnXHJcblx0fSxcclxuXHQnQmFjayc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ21vbmtleV9jaGlja2VuJyxcclxuXHRcdGlkOiA0MSxcclxuXHRcdGNvdW50OiAwLFxyXG5cdFx0bmFtZTogJ0NhcmQgQmFjaycsXHJcblx0XHRpbWc6ICdtb25rZXlfY2hpY2tlbi9iYWNrLnBuZydcclxuXHR9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzLmNhbWVsX3RvYWQgPSB7XHJcblx0J0NhQ3InOiB7IFxyXG5cdFx0ZGVja190eXBlOiAnY2FtZWxfdG9hZCcsXHJcblx0XHRpZDogMTQsXHJcblx0XHRvcmRlcjogMTAsXHJcblx0XHRuYW1lOiAnQ2FtZWwgQ3Jvb2snLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRvcHBvbmVudDogeyBzdGVhbF9jYXJkczogMSB9LFxyXG5cdFx0aW1nOiAnY2FtZWxfdG9hZC9jYW1lbF9jcm9vay5wbmcnXHJcblx0fSxcclxuXHQnVHJUbyc6IHsgXHJcblx0XHRkZWNrX3R5cGU6ICdjYW1lbF90b2FkJyxcclxuXHRcdGlkOiAxNSxcclxuXHRcdG9yZGVyOiAyMCxcclxuXHRcdG5hbWU6ICdUcmVhY2hlcm91cyBUb2FkJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0cGxheWVyOiB7IGhlYWx0aDogLTcgfSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTE1IH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkL3RyZWFjaGVyb3VzX3RvYWQucG5nJ1xyXG5cdH0sXHJcblx0J0ZyRnInOiB7IFxyXG5cdFx0ZGVja190eXBlOiAnY2FtZWxfdG9hZCcsXHJcblx0XHRpZDogMTYsXHJcblx0XHRvcmRlcjogMzAsXHJcblx0XHRuYW1lOiAnRnJpZW5kbHkgRnJvZycsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHBsYXllcjoge1xyXG5cdFx0XHRoZWFsdGg6IDUsXHJcblx0XHRcdGVuZXJnaXplOiAzXHJcblx0XHR9LFxyXG5cdFx0aW1nOiAnY2FtZWxfdG9hZC9mcmllbmRseV9mcm9nLnBuZydcclxuXHR9LCBcclxuXHQnRnJhRnInOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdjYW1lbF90b2FkJyxcclxuXHRcdGlkOiAxNyxcclxuXHRcdG9yZGVyOiA0MCxcclxuXHRcdG5hbWU6ICdGcmFja2luZyBGcm9nJyxcclxuXHRcdGNvdW50OiAxLFxyXG5cdFx0cGxheWVyOiB7XHJcblx0XHRcdHBvaXNvbjogM1xyXG5cdFx0fSxcclxuXHRcdG9wcG9uZW50OiB7XHJcblx0XHRcdHBvaXNvbjogNSxcclxuXHRcdFx0aGVhbHRoOiAtNVxyXG5cdFx0fSxcclxuXHRcdGltZzogJ2NhbWVsX3RvYWQvZnJhY2tpbmdfZnJvZy5wbmcnXHJcblx0fSxcclxuXHQnQ29DYSc6IHsgXHJcblx0XHRkZWNrX3R5cGU6ICdjYW1lbF90b2FkJyxcclxuXHRcdGlkOiAxOCxcclxuXHRcdG9yZGVyOiA1MCxcclxuXHRcdG5hbWU6ICdDb3B5IENhbWVsJyxcclxuXHRcdGNvdW50OiAwLFxyXG5cdFx0b3Bwb25lbnQ6IHsgY29weTogeyB0dXJuc19hZ286IDEgfSB9LFxyXG5cdFx0aW1nOiAnY2FtZWxfdG9hZC9jb3B5X2NhbWVsLnBuZydcclxuXHR9LFxyXG5cdCdDVDMnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdjYW1lbF90b2FkJyxcclxuXHRcdGlkOiA2LFxyXG5cdFx0b3JkZXI6IDExLFxyXG5cdFx0bmFtZTogJzMgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC0zIH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkLzMucG5nJ1xyXG5cdH0sXHJcblx0J0NUNCc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ2NhbWVsX3RvYWQnLFxyXG5cdFx0aWQ6IDcsXHJcblx0XHRvcmRlcjogMTIsXHJcblx0XHRuYW1lOiAnNCBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTQgfSxcclxuXHRcdGltZzogJ2NhbWVsX3RvYWQvNC5wbmcnXHJcblx0fSxcclxuXHQnQ1Q1Jzoge1xyXG5cdFx0ZGVja190eXBlOiAnY2FtZWxfdG9hZCcsXHJcblx0XHRpZDogOCxcclxuXHRcdG9yZGVyOiAxMyxcclxuXHRcdG5hbWU6ICc1IERhbWFnZScsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHN0YWNrYWJsZTogdHJ1ZSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNSB9LFxyXG5cdFx0aW1nOiAnY2FtZWxfdG9hZC81LnBuZydcclxuXHR9LFxyXG5cdCdDVDYnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdjYW1lbF90b2FkJyxcclxuXHRcdGlkOiA5LFxyXG5cdFx0b3JkZXI6IDE0LFxyXG5cdFx0bmFtZTogJzYgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC02IH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkLzYucG5nJ1xyXG5cdH0sXHJcblx0J0NUNyc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ2NhbWVsX3RvYWQnLFxyXG5cdFx0aWQ6IDEwLFxyXG5cdFx0b3JkZXI6IDE1LFxyXG5cdFx0bmFtZTogJzcgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC03IH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkLzcucG5nJ1xyXG5cdH0sXHJcblx0J0NUOCc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ2NhbWVsX3RvYWQnLFxyXG5cdFx0aWQ6IDExLFxyXG5cdFx0b3JkZXI6IDE2LFxyXG5cdFx0bmFtZTogJzggRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC04IH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkLzgucG5nJ1xyXG5cdH0sXHJcblx0J0NUOSc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ2NhbWVsX3RvYWQnLFxyXG5cdFx0aWQ6IDEyLFxyXG5cdFx0b3JkZXI6IDE3LFxyXG5cdFx0bmFtZTogJzkgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC05IH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkLzkucG5nJ1xyXG5cdH0sXHJcblx0J0NUMTAnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdjYW1lbF90b2FkJyxcclxuXHRcdGlkOiAxMyxcclxuXHRcdG9yZGVyOiAxOCxcclxuXHRcdG5hbWU6ICcxMCBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTEwIH0sXHJcblx0XHRpbWc6ICdjYW1lbF90b2FkLzEwLnBuZydcclxuXHR9LFxyXG5cdCdDYVRvJzoge1xyXG5cdFx0ZGVja190eXBlOiAnY2FtZWxfdG9hZCcsXHJcblx0XHRpZDogMTAwLFxyXG5cdFx0b3JkZXI6IDEwMCxcclxuXHRcdG5hbWU6ICdUaGUgTGVnZW5kYXJ5IENhbWVsVG9hZCcsXHJcblx0XHRjb3VudDogMSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNDAgfSxcclxuXHRcdGNoYW5jZTogMTAwMDAwMCxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuLzEwLnBuZydcclxuXHR9LFxyXG5cdCdCYWNrJzoge1xyXG5cdFx0aWQ6IDQyLFxyXG5cdFx0Y291bnQ6IDAsXHJcblx0XHRuYW1lOiAnQ2FyZCBCYWNrJyxcclxuXHRcdGltZzogJ2NhbWVsX3RvYWQvYmFjay5wbmcnXHJcblx0fVxyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cy5zbmFpbF93aGFsZSA9IHtcclxuXHQnU25lU24nOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdzbmFpbF93aGFsZScsXHJcblx0XHRpZDogMjcsXHJcblx0XHRvcmRlcjogMTAsXHJcblx0XHRuYW1lOiAnU25lYWt5IFNuYWlsJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0cGxheWVyOiB7IGRyYXc6IDEgfSxcclxuXHRcdG9wcG9uZW50OiB7IHBvaXNvbjogMSB9LFxyXG5cdFx0aW1nOiAnc25haWxfd2hhbGUvc25lYWt5X3NuYWlsLnBuZydcclxuXHR9LCBcclxuXHQnV2hvV28nOiB7IFxyXG5cdFx0ZGVja190eXBlOiAnc25haWxfd2hhbGUnLFxyXG5cdFx0aWQ6IDI4LFxyXG5cdFx0b3JkZXI6IDIwLFxyXG5cdFx0bmFtZTogJ1doYWxlIG9mIFdvbmRlcicsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHBsYXllcjogeyBcclxuXHRcdFx0aGVhbHRoOiAtMTAsXHJcblx0XHRcdGVuZXJnaXplOiAzXHJcblx0XHR9LFxyXG5cdFx0b3Bwb25lbnQ6IHsgcG9pc29uOiAzIH0sXHJcblx0XHRpbWc6ICdzbmFpbF93aGFsZS93aGFsZV9vZl93b25kZXIucG5nJ1xyXG5cdH0sXHJcblx0J1dhV2gnOiB7IFxyXG5cdFx0ZGVja190eXBlOiAnc25haWxfd2hhbGUnLFxyXG5cdFx0aWQ6IDI5LFxyXG5cdFx0b3JkZXI6IDMwLFxyXG5cdFx0bmFtZTogJ1dheXdhcmQgV2hhbGUnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRwbGF5ZXI6IHsgZGlzY2FyZDogMiB9LFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtMjUgfSxcclxuXHRcdGltZzogJ3NuYWlsX3doYWxlL3dheXdhcmRfd2hhbGUucG5nJ1xyXG5cdH0sXHJcblx0J1NuU25yJzoge1xyXG5cdFx0ZGVja190eXBlOiAnc25haWxfd2hhbGUnLFxyXG5cdFx0aWQ6IDMwLFxyXG5cdFx0b3JkZXI6IDQwLFxyXG5cdFx0bmFtZTogJ1NuYWlsIFNuYXJlJyxcclxuXHRcdGNvdW50OiAxLFxyXG5cdFx0b3Bwb25lbnQ6IHsgcmVzdHJpY3Q6IDIgfSxcclxuXHRcdGltZzogJ3NuYWlsX3doYWxlL3NuYWlsX3NuYXJlLnBuZydcclxuXHR9LFxyXG5cdCdGbFNuJzogeyBcclxuXHRcdGRlY2tfdHlwZTogJ3NuYWlsX3doYWxlJyxcclxuXHRcdGlkOiAzMSxcclxuXHRcdG9yZGVyOiA1MCxcclxuXHRcdG5hbWU6ICdGbGFpbGluZyBTbmFpbCcsXHJcblx0XHRjb3VudDogMixcclxuXHRcdGJvbnVzOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtMTAgfSxcclxuXHRcdGltZzogJ3NuYWlsX3doYWxlL2ZsYWlsaW5nX3NuYWlsLnBuZydcclxuXHR9LFxyXG5cdCdTVzMnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdzbmFpbF93aGFsZScsXHJcblx0XHRpZDogNixcclxuXHRcdG9yZGVyOiAxMSxcclxuXHRcdG5hbWU6ICczIERhbWFnZScsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHN0YWNrYWJsZTogdHJ1ZSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtMyB9LFxyXG5cdFx0aW1nOiAnc25haWxfd2hhbGUvMy5wbmcnXHJcblx0fSxcclxuXHQnU1c0Jzoge1xyXG5cdFx0ZGVja190eXBlOiAnc25haWxfd2hhbGUnLFxyXG5cdFx0aWQ6IDcsXHJcblx0XHRvcmRlcjogMTIsXHJcblx0XHRuYW1lOiAnNCBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTQgfSxcclxuXHRcdGltZzogJ3NuYWlsX3doYWxlLzQucG5nJ1xyXG5cdH0sXHJcblx0J1NXNSc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ3NuYWlsX3doYWxlJyxcclxuXHRcdGlkOiA4LFxyXG5cdFx0b3JkZXI6IDEzLFxyXG5cdFx0bmFtZTogJzUgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC01IH0sXHJcblx0XHRpbWc6ICdzbmFpbF93aGFsZS81LnBuZydcclxuXHR9LFxyXG5cdCdTVzYnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdzbmFpbF93aGFsZScsXHJcblx0XHRpZDogOSxcclxuXHRcdG9yZGVyOiAxNCxcclxuXHRcdG5hbWU6ICc2IERhbWFnZScsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHN0YWNrYWJsZTogdHJ1ZSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNiB9LFxyXG5cdFx0aW1nOiAnc25haWxfd2hhbGUvNi5wbmcnXHJcblx0fSxcclxuXHQnU1c3Jzoge1xyXG5cdFx0ZGVja190eXBlOiAnc25haWxfd2hhbGUnLFxyXG5cdFx0aWQ6IDEwLFxyXG5cdFx0b3JkZXI6IDE1LFxyXG5cdFx0bmFtZTogJzcgRGFtYWdlJyxcclxuXHRcdGNvdW50OiA0LFxyXG5cdFx0c3RhY2thYmxlOiB0cnVlLFxyXG5cdFx0dW5yZXN0cmljdGFibGU6IHRydWUsXHJcblx0XHRvcHBvbmVudDogeyBoZWFsdGg6IC03IH0sXHJcblx0XHRpbWc6ICdzbmFpbF93aGFsZS83LnBuZydcclxuXHR9LFxyXG5cdCdTVzgnOiB7XHJcblx0XHRkZWNrX3R5cGU6ICdzbmFpbF93aGFsZScsXHJcblx0XHRpZDogMTEsXHJcblx0XHRvcmRlcjogMTYsXHJcblx0XHRuYW1lOiAnOCBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTggfSxcclxuXHRcdGltZzogJ3NuYWlsX3doYWxlLzgucG5nJ1xyXG5cdH0sXHJcblx0J1NXOSc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ3NuYWlsX3doYWxlJyxcclxuXHRcdGlkOiAxMixcclxuXHRcdG9yZGVyOiAxNyxcclxuXHRcdG5hbWU6ICc5IERhbWFnZScsXHJcblx0XHRjb3VudDogNCxcclxuXHRcdHN0YWNrYWJsZTogdHJ1ZSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtOSB9LFxyXG5cdFx0aW1nOiAnc25haWxfd2hhbGUvOS5wbmcnXHJcblx0fSxcclxuXHQnU1cxMCc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ3NuYWlsX3doYWxlJyxcclxuXHRcdGlkOiAxMyxcclxuXHRcdG9yZGVyOiAxOCxcclxuXHRcdG5hbWU6ICcxMCBEYW1hZ2UnLFxyXG5cdFx0Y291bnQ6IDQsXHJcblx0XHRzdGFja2FibGU6IHRydWUsXHJcblx0XHR1bnJlc3RyaWN0YWJsZTogdHJ1ZSxcclxuXHRcdG9wcG9uZW50OiB7IGhlYWx0aDogLTEwIH0sXHJcblx0XHRpbWc6ICdzbmFpbF93aGFsZS8xMC5wbmcnXHJcblx0fSxcclxuXHQnQ2FUbyc6IHtcclxuXHRcdGRlY2tfdHlwZTogJ3NuYWlsX3doYWxlJyxcclxuXHRcdGlkOiAzMDAsXHJcblx0XHRvcmRlcjogMTAwLFxyXG5cdFx0bmFtZTogJ1RoZSBMZWdlbmRhcnkgU25haWxXaGFsZScsXHJcblx0XHRjb3VudDogMSxcclxuXHRcdHVucmVzdHJpY3RhYmxlOiB0cnVlLFxyXG5cdFx0b3Bwb25lbnQ6IHsgaGVhbHRoOiAtNDAgfSxcclxuXHRcdGNoYW5jZTogMTAwMDAwMCxcclxuXHRcdGltZzogJ21vbmtleV9jaGlja2VuLzEwLnBuZydcclxuXHR9LFxyXG5cdCdCYWNrJzoge1xyXG5cdFx0ZGVja190eXBlOiAnc25haWxfd2hhbGUnLFxyXG5cdFx0aWQ6IDQyLFxyXG5cdFx0Y291bnQ6IDAsXHJcblx0XHRuYW1lOiAnQ2FyZCBCYWNrJyxcclxuXHRcdGltZzogJ3NuYWlsX3doYWxlL2JhY2sucG5nJ1xyXG5cdH1cclxufSIsIi8vIG1vZHVsZS5leHBvcnRzID0ge1xyXG4vLyAgICAgYmFieV9jaGlja2VuOiB7XHJcbi8vICAgICBcdG5hbWU6ICdCYWJ5IENoaWNrZW4nXHJcbi8vICAgICAgICwgY29zdDogMTBcclxuLy8gICAgIH1cclxuLy8gICAsIGJhYnlfbW9ua2V5OiB7XHJcbi8vICAgICAgICAgbmFtZTogJ0JhYnkgTW9ua2V5J1xyXG4vLyAgICAgICAsIGNvc3Q6IDEwXHJcbi8vICAgICB9XHJcbi8vICAgLCBncmVlZGllcl9jaGlja2VuOiB7XHJcbi8vICAgICAgICAgbmFtZTogJ0dyZWVkaWVyIENoaWNrZW4nXHJcbi8vICAgICAgICwgY29zdDogNTBcclxuLy8gICAgIH1cclxuLy8gICAsIGNhbWVsX3RvYWQ6IHtcclxuLy8gICBcdFx0bmFtZTogJ0NhbWVsVG9hZCdcclxuLy8gICBcdCAgLCBjb3N0OiAxMDAwXHJcbi8vICAgICB9XHJcbi8vICAgLCBzbmFpbF93aGFsZToge1xyXG4vLyAgIFx0XHRuYW1lOiAnU25haWxXaGFsZSdcclxuLy8gICBcdCAgLCBjb3N0OiAxMDAwXHJcbi8vICAgICB9XHJcbi8vIH0iLCJ2YXIgcHJvY2Vzcz1yZXF1aXJlKFwiX19icm93c2VyaWZ5X3Byb2Nlc3NcIik7aWYgKCFwcm9jZXNzLkV2ZW50RW1pdHRlcikgcHJvY2Vzcy5FdmVudEVtaXR0ZXIgPSBmdW5jdGlvbiAoKSB7fTtcblxudmFyIEV2ZW50RW1pdHRlciA9IGV4cG9ydHMuRXZlbnRFbWl0dGVyID0gcHJvY2Vzcy5FdmVudEVtaXR0ZXI7XG52YXIgaXNBcnJheSA9IHR5cGVvZiBBcnJheS5pc0FycmF5ID09PSAnZnVuY3Rpb24nXG4gICAgPyBBcnJheS5pc0FycmF5XG4gICAgOiBmdW5jdGlvbiAoeHMpIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh4cykgPT09ICdbb2JqZWN0IEFycmF5XSdcbiAgICB9XG47XG5mdW5jdGlvbiBpbmRleE9mICh4cywgeCkge1xuICAgIGlmICh4cy5pbmRleE9mKSByZXR1cm4geHMuaW5kZXhPZih4KTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHhzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGlmICh4ID09PSB4c1tpXSkgcmV0dXJuIGk7XG4gICAgfVxuICAgIHJldHVybiAtMTtcbn1cblxuLy8gQnkgZGVmYXVsdCBFdmVudEVtaXR0ZXJzIHdpbGwgcHJpbnQgYSB3YXJuaW5nIGlmIG1vcmUgdGhhblxuLy8gMTAgbGlzdGVuZXJzIGFyZSBhZGRlZCB0byBpdC4gVGhpcyBpcyBhIHVzZWZ1bCBkZWZhdWx0IHdoaWNoXG4vLyBoZWxwcyBmaW5kaW5nIG1lbW9yeSBsZWFrcy5cbi8vXG4vLyBPYnZpb3VzbHkgbm90IGFsbCBFbWl0dGVycyBzaG91bGQgYmUgbGltaXRlZCB0byAxMC4gVGhpcyBmdW5jdGlvbiBhbGxvd3Ncbi8vIHRoYXQgdG8gYmUgaW5jcmVhc2VkLiBTZXQgdG8gemVybyBmb3IgdW5saW1pdGVkLlxudmFyIGRlZmF1bHRNYXhMaXN0ZW5lcnMgPSAxMDtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuc2V0TWF4TGlzdGVuZXJzID0gZnVuY3Rpb24obikge1xuICBpZiAoIXRoaXMuX2V2ZW50cykgdGhpcy5fZXZlbnRzID0ge307XG4gIHRoaXMuX2V2ZW50cy5tYXhMaXN0ZW5lcnMgPSBuO1xufTtcblxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbih0eXBlKSB7XG4gIC8vIElmIHRoZXJlIGlzIG5vICdlcnJvcicgZXZlbnQgbGlzdGVuZXIgdGhlbiB0aHJvdy5cbiAgaWYgKHR5cGUgPT09ICdlcnJvcicpIHtcbiAgICBpZiAoIXRoaXMuX2V2ZW50cyB8fCAhdGhpcy5fZXZlbnRzLmVycm9yIHx8XG4gICAgICAgIChpc0FycmF5KHRoaXMuX2V2ZW50cy5lcnJvcikgJiYgIXRoaXMuX2V2ZW50cy5lcnJvci5sZW5ndGgpKVxuICAgIHtcbiAgICAgIGlmIChhcmd1bWVudHNbMV0gaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgICB0aHJvdyBhcmd1bWVudHNbMV07IC8vIFVuaGFuZGxlZCAnZXJyb3InIGV2ZW50XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmNhdWdodCwgdW5zcGVjaWZpZWQgJ2Vycm9yJyBldmVudC5cIik7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMpIHJldHVybiBmYWxzZTtcbiAgdmFyIGhhbmRsZXIgPSB0aGlzLl9ldmVudHNbdHlwZV07XG4gIGlmICghaGFuZGxlcikgcmV0dXJuIGZhbHNlO1xuXG4gIGlmICh0eXBlb2YgaGFuZGxlciA9PSAnZnVuY3Rpb24nKSB7XG4gICAgc3dpdGNoIChhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAvLyBmYXN0IGNhc2VzXG4gICAgICBjYXNlIDE6XG4gICAgICAgIGhhbmRsZXIuY2FsbCh0aGlzKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIDI6XG4gICAgICAgIGhhbmRsZXIuY2FsbCh0aGlzLCBhcmd1bWVudHNbMV0pO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgMzpcbiAgICAgICAgaGFuZGxlci5jYWxsKHRoaXMsIGFyZ3VtZW50c1sxXSwgYXJndW1lbnRzWzJdKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICAvLyBzbG93ZXJcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHZhciBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcbiAgICAgICAgaGFuZGxlci5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG5cbiAgfSBlbHNlIGlmIChpc0FycmF5KGhhbmRsZXIpKSB7XG4gICAgdmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuXG4gICAgdmFyIGxpc3RlbmVycyA9IGhhbmRsZXIuc2xpY2UoKTtcbiAgICBmb3IgKHZhciBpID0gMCwgbCA9IGxpc3RlbmVycy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGxpc3RlbmVyc1tpXS5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG5cbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn07XG5cbi8vIEV2ZW50RW1pdHRlciBpcyBkZWZpbmVkIGluIHNyYy9ub2RlX2V2ZW50cy5jY1xuLy8gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5lbWl0KCkgaXMgYWxzbyBkZWZpbmVkIHRoZXJlLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lciA9IGZ1bmN0aW9uKHR5cGUsIGxpc3RlbmVyKSB7XG4gIGlmICgnZnVuY3Rpb24nICE9PSB0eXBlb2YgbGlzdGVuZXIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ2FkZExpc3RlbmVyIG9ubHkgdGFrZXMgaW5zdGFuY2VzIG9mIEZ1bmN0aW9uJyk7XG4gIH1cblxuICBpZiAoIXRoaXMuX2V2ZW50cykgdGhpcy5fZXZlbnRzID0ge307XG5cbiAgLy8gVG8gYXZvaWQgcmVjdXJzaW9uIGluIHRoZSBjYXNlIHRoYXQgdHlwZSA9PSBcIm5ld0xpc3RlbmVyc1wiISBCZWZvcmVcbiAgLy8gYWRkaW5nIGl0IHRvIHRoZSBsaXN0ZW5lcnMsIGZpcnN0IGVtaXQgXCJuZXdMaXN0ZW5lcnNcIi5cbiAgdGhpcy5lbWl0KCduZXdMaXN0ZW5lcicsIHR5cGUsIGxpc3RlbmVyKTtcblxuICBpZiAoIXRoaXMuX2V2ZW50c1t0eXBlXSkge1xuICAgIC8vIE9wdGltaXplIHRoZSBjYXNlIG9mIG9uZSBsaXN0ZW5lci4gRG9uJ3QgbmVlZCB0aGUgZXh0cmEgYXJyYXkgb2JqZWN0LlxuICAgIHRoaXMuX2V2ZW50c1t0eXBlXSA9IGxpc3RlbmVyO1xuICB9IGVsc2UgaWYgKGlzQXJyYXkodGhpcy5fZXZlbnRzW3R5cGVdKSkge1xuXG4gICAgLy8gQ2hlY2sgZm9yIGxpc3RlbmVyIGxlYWtcbiAgICBpZiAoIXRoaXMuX2V2ZW50c1t0eXBlXS53YXJuZWQpIHtcbiAgICAgIHZhciBtO1xuICAgICAgaWYgKHRoaXMuX2V2ZW50cy5tYXhMaXN0ZW5lcnMgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBtID0gdGhpcy5fZXZlbnRzLm1heExpc3RlbmVycztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG0gPSBkZWZhdWx0TWF4TGlzdGVuZXJzO1xuICAgICAgfVxuXG4gICAgICBpZiAobSAmJiBtID4gMCAmJiB0aGlzLl9ldmVudHNbdHlwZV0ubGVuZ3RoID4gbSkge1xuICAgICAgICB0aGlzLl9ldmVudHNbdHlwZV0ud2FybmVkID0gdHJ1ZTtcbiAgICAgICAgY29uc29sZS5lcnJvcignKG5vZGUpIHdhcm5pbmc6IHBvc3NpYmxlIEV2ZW50RW1pdHRlciBtZW1vcnkgJyArXG4gICAgICAgICAgICAgICAgICAgICAgJ2xlYWsgZGV0ZWN0ZWQuICVkIGxpc3RlbmVycyBhZGRlZC4gJyArXG4gICAgICAgICAgICAgICAgICAgICAgJ1VzZSBlbWl0dGVyLnNldE1heExpc3RlbmVycygpIHRvIGluY3JlYXNlIGxpbWl0LicsXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5fZXZlbnRzW3R5cGVdLmxlbmd0aCk7XG4gICAgICAgIGNvbnNvbGUudHJhY2UoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBJZiB3ZSd2ZSBhbHJlYWR5IGdvdCBhbiBhcnJheSwganVzdCBhcHBlbmQuXG4gICAgdGhpcy5fZXZlbnRzW3R5cGVdLnB1c2gobGlzdGVuZXIpO1xuICB9IGVsc2Uge1xuICAgIC8vIEFkZGluZyB0aGUgc2Vjb25kIGVsZW1lbnQsIG5lZWQgdG8gY2hhbmdlIHRvIGFycmF5LlxuICAgIHRoaXMuX2V2ZW50c1t0eXBlXSA9IFt0aGlzLl9ldmVudHNbdHlwZV0sIGxpc3RlbmVyXTtcbiAgfVxuXG4gIHJldHVybiB0aGlzO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbiA9IEV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub25jZSA9IGZ1bmN0aW9uKHR5cGUsIGxpc3RlbmVyKSB7XG4gIHZhciBzZWxmID0gdGhpcztcbiAgc2VsZi5vbih0eXBlLCBmdW5jdGlvbiBnKCkge1xuICAgIHNlbGYucmVtb3ZlTGlzdGVuZXIodHlwZSwgZyk7XG4gICAgbGlzdGVuZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgfSk7XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyID0gZnVuY3Rpb24odHlwZSwgbGlzdGVuZXIpIHtcbiAgaWYgKCdmdW5jdGlvbicgIT09IHR5cGVvZiBsaXN0ZW5lcikge1xuICAgIHRocm93IG5ldyBFcnJvcigncmVtb3ZlTGlzdGVuZXIgb25seSB0YWtlcyBpbnN0YW5jZXMgb2YgRnVuY3Rpb24nKTtcbiAgfVxuXG4gIC8vIGRvZXMgbm90IHVzZSBsaXN0ZW5lcnMoKSwgc28gbm8gc2lkZSBlZmZlY3Qgb2YgY3JlYXRpbmcgX2V2ZW50c1t0eXBlXVxuICBpZiAoIXRoaXMuX2V2ZW50cyB8fCAhdGhpcy5fZXZlbnRzW3R5cGVdKSByZXR1cm4gdGhpcztcblxuICB2YXIgbGlzdCA9IHRoaXMuX2V2ZW50c1t0eXBlXTtcblxuICBpZiAoaXNBcnJheShsaXN0KSkge1xuICAgIHZhciBpID0gaW5kZXhPZihsaXN0LCBsaXN0ZW5lcik7XG4gICAgaWYgKGkgPCAwKSByZXR1cm4gdGhpcztcbiAgICBsaXN0LnNwbGljZShpLCAxKTtcbiAgICBpZiAobGlzdC5sZW5ndGggPT0gMClcbiAgICAgIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG4gIH0gZWxzZSBpZiAodGhpcy5fZXZlbnRzW3R5cGVdID09PSBsaXN0ZW5lcikge1xuICAgIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID0gZnVuY3Rpb24odHlwZSkge1xuICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgIHRoaXMuX2V2ZW50cyA9IHt9O1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgLy8gZG9lcyBub3QgdXNlIGxpc3RlbmVycygpLCBzbyBubyBzaWRlIGVmZmVjdCBvZiBjcmVhdGluZyBfZXZlbnRzW3R5cGVdXG4gIGlmICh0eXBlICYmIHRoaXMuX2V2ZW50cyAmJiB0aGlzLl9ldmVudHNbdHlwZV0pIHRoaXMuX2V2ZW50c1t0eXBlXSA9IG51bGw7XG4gIHJldHVybiB0aGlzO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5saXN0ZW5lcnMgPSBmdW5jdGlvbih0eXBlKSB7XG4gIGlmICghdGhpcy5fZXZlbnRzKSB0aGlzLl9ldmVudHMgPSB7fTtcbiAgaWYgKCF0aGlzLl9ldmVudHNbdHlwZV0pIHRoaXMuX2V2ZW50c1t0eXBlXSA9IFtdO1xuICBpZiAoIWlzQXJyYXkodGhpcy5fZXZlbnRzW3R5cGVdKSkge1xuICAgIHRoaXMuX2V2ZW50c1t0eXBlXSA9IFt0aGlzLl9ldmVudHNbdHlwZV1dO1xuICB9XG4gIHJldHVybiB0aGlzLl9ldmVudHNbdHlwZV07XG59O1xuXG5FdmVudEVtaXR0ZXIubGlzdGVuZXJDb3VudCA9IGZ1bmN0aW9uKGVtaXR0ZXIsIHR5cGUpIHtcbiAgdmFyIHJldDtcbiAgaWYgKCFlbWl0dGVyLl9ldmVudHMgfHwgIWVtaXR0ZXIuX2V2ZW50c1t0eXBlXSlcbiAgICByZXQgPSAwO1xuICBlbHNlIGlmICh0eXBlb2YgZW1pdHRlci5fZXZlbnRzW3R5cGVdID09PSAnZnVuY3Rpb24nKVxuICAgIHJldCA9IDE7XG4gIGVsc2VcbiAgICByZXQgPSBlbWl0dGVyLl9ldmVudHNbdHlwZV0ubGVuZ3RoO1xuICByZXR1cm4gcmV0O1xufTtcbiIsIi8vIHNoaW0gZm9yIHVzaW5nIHByb2Nlc3MgaW4gYnJvd3NlclxuXG52YXIgcHJvY2VzcyA9IG1vZHVsZS5leHBvcnRzID0ge307XG5cbnByb2Nlc3MubmV4dFRpY2sgPSAoZnVuY3Rpb24gKCkge1xuICAgIHZhciBjYW5TZXRJbW1lZGlhdGUgPSB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJ1xuICAgICYmIHdpbmRvdy5zZXRJbW1lZGlhdGU7XG4gICAgdmFyIGNhblBvc3QgPSB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJ1xuICAgICYmIHdpbmRvdy5wb3N0TWVzc2FnZSAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lclxuICAgIDtcblxuICAgIGlmIChjYW5TZXRJbW1lZGlhdGUpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChmKSB7IHJldHVybiB3aW5kb3cuc2V0SW1tZWRpYXRlKGYpIH07XG4gICAgfVxuXG4gICAgaWYgKGNhblBvc3QpIHtcbiAgICAgICAgdmFyIHF1ZXVlID0gW107XG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtZXNzYWdlJywgZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgICAgICB2YXIgc291cmNlID0gZXYuc291cmNlO1xuICAgICAgICAgICAgaWYgKChzb3VyY2UgPT09IHdpbmRvdyB8fCBzb3VyY2UgPT09IG51bGwpICYmIGV2LmRhdGEgPT09ICdwcm9jZXNzLXRpY2snKSB7XG4gICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgaWYgKHF1ZXVlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGZuID0gcXVldWUuc2hpZnQoKTtcbiAgICAgICAgICAgICAgICAgICAgZm4oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHRydWUpO1xuXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiBuZXh0VGljayhmbikge1xuICAgICAgICAgICAgcXVldWUucHVzaChmbik7XG4gICAgICAgICAgICB3aW5kb3cucG9zdE1lc3NhZ2UoJ3Byb2Nlc3MtdGljaycsICcqJyk7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIG5leHRUaWNrKGZuKSB7XG4gICAgICAgIHNldFRpbWVvdXQoZm4sIDApO1xuICAgIH07XG59KSgpO1xuXG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcblxucHJvY2Vzcy5iaW5kaW5nID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuYmluZGluZyBpcyBub3Qgc3VwcG9ydGVkJyk7XG59XG5cbi8vIFRPRE8oc2h0eWxtYW4pXG5wcm9jZXNzLmN3ZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICcvJyB9O1xucHJvY2Vzcy5jaGRpciA9IGZ1bmN0aW9uIChkaXIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuY2hkaXIgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcbiJdfQ==
;