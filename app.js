var OneOnOne = require('one-on-one')
  , connect = require('connect')
  , port = process.env.NODE_ENV == 'production' ? 80 : 2456
  , built

require('./server/play')

OneOnOne.startBackend({
	  mongo_url: 'mongodb://localhost/mc'
	, port: process.argv[2] || 3001
	, store: new OneOnOne.MemoryStore()
	, log_level: 1
})

if(!process.argv[2]) {

	OneOnOne.buildFrontEnd({
		  game_title: 'MonkeyChicken'
		, entry_script: __dirname + '/client/index.js'
		, assets_directory: __dirname + '/client/assets'
		, templates_directory: __dirname + '/client/templates'
		, polyfills_directory: __dirname + '/client/polyfills'
		, styles_directory: __dirname + '/client/styles'
		, output_directory: __dirname + '/generated'
	}, function() { built = true })

	connect()
		.use(function(req, res, next) {
			(function waitForBuild() {
				if(built) return next()
				setTimeout(waitForBuild, 10)
			})()
		})
		.use(connect.static(__dirname + '/generated', {maxAge: 86400000}))
		.listen(port)
	
}
